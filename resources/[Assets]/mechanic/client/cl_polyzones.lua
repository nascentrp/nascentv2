Tuner, auto_exotics, paleto_mech, auto_bodies = false, false, false, false

Citizen.CreateThread(function()
	exports["ethical-polyzone"]:AddBoxZone("tuner", vector3(929.1856, -970.6857, 39.4998), 35.0, 30.0, {
        name="tuner",
        heading=275,
        debugPoly=false,
        minZ=38.00,
        maxZ=40.39
    })

    exports["ethical-polyzone"]:AddBoxZone("auto_exotics", vector3(-210.718, -1326.39, 30.571), 34, 25, {
        name="auto_exotics",
        heading=89,
        debugPoly=false,
        minZ=29.29,
        maxZ=35.29
    })

    exports["ethical-polyzone"]:AddBoxZone("paleto_mech", vector3(109.18, 6625.12, 31.79), 16.2, 10, {
        name="paleto_mech",
        heading=314,
        --debugPoly=true,
        minZ=29.39,
        maxZ=33.39
    })

    exports["ethical-polyzone"]:AddBoxZone("auto_bodies", vector3(-1417.0, -446.71, 35.91), 20.0, 10, {
        name="auto_bodies",
        heading=302,
        debugPoly=false,
        minZ=34.91,
        maxZ=38.11
    })
end)


RegisterNetEvent('ethical-polyzone:enter')
AddEventHandler('ethical-polyzone:enter', function(name)
    local job = exports["isPed"]:isPed("myjob")
    if name == "tuner" then
        if job == "tuner_shop" then
            TriggerEvent("bennys:control:enteredTunerShop")
            Tuner = true
        end
    elseif name == "auto_exotics" then
        if job == "auto_exotics" then
            TriggerEvent("bennys:control:enteredAutoExotics")
            auto_exotics = true
        end
    elseif name == "paleto_mech" then
        if job == "paleto_mech" then
            paleto_mech = true
        end
    elseif name == "auto_bodies" then
        if job == "auto_bodies" then
            TriggerEvent("bennys:control:enteredHayes")
            auto_bodies = true
        end
    end
end)

RegisterNetEvent('ethical-polyzone:exit')
AddEventHandler('ethical-polyzone:exit', function(name)
    local job = exports["isPed"]:isPed("myjob")
    if name == "tuner" then
        if job == "tuner_shop" then
            TriggerEvent("bennys:control:exitTunerShop")
            Tuner = false
        end
    elseif name == "auto_exotics" then
        if job == "auto_exotics" then
            TriggerEvent("bennys:control:exitAutoExotics")
            auto_exotics = false
        end
    elseif name == "paleto_mech" then
        if job == "paleto_mech" then
            TriggerEvent("bennys:control:exitPaleto")
            paleto_mech = false
        end
    elseif name == "auto_bodies" then
        if job == "auto_bodies" then
            print("exit auto bodies")
            TriggerEvent("bennys:control:exitHayes")
            auto_bodies = false
        end
    end
end)