RegisterServerEvent('ethical:idcard')
AddEventHandler('ethical:idcard', function()
    local src = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local price = 100
    if (tonumber(user:getCash()) >= price) then
        user:removeMoney(price)
        TriggerClientEvent('courthouse:idbuy', src)
    else
        TriggerClientEvent("DoLongHudText", src, "You need R" .. price, 2)
    end
end)

RegisterServerEvent('cash:remove')
AddEventHandler('cash:remove', function(pSrc, amount)
    local user = exports["ethical-base"]:getModule("Player"):GetUser(tonumber(pSrc))
	if (tonumber(user:getCash()) >= amount) then
		user:removeMoney(amount)
	end
end)