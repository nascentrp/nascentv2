resource_manifest_version "44febabe-d386-4d18-afbe-5e627f4af937"

Discription "Tablet"

Version "0.2"

ui_page "nui/ui.html"

files {
    "nui/ui.html",
    "nui/material-icons.ttf",
    "nui/material-icons.css",
    "nui/loadscreen.jpg",
    "nui/fancy-crap.css",
    "nui/fancy-crap.js",
    "nui/jquery.min/js",
    "nui/html/spotify.html",
    "nui/bootstrap.min.css",
    "nui/html/the-law.html",
    "nui/html/the-law_files/colorschememapping.xml",
    "nui/html/the-law_files/filelist.xml",
    "nui/html/the-law_files/header.html",
    "nui/html/the-law_files/image001.png",
    "nui/html/the-law_files/image002.png",
    "nui/html/the-law_files/image003.png",
    "nui/html/the-law_files/item0010.xml",
    "nui/html/the-law_files/themedata.thmx",
    "nui/html/embedded-style.css",
    "nui/html/darkweb_files/colorschememapping.xml",
    "nui/html/darkweb_files/filelist.xml",
    "nui/html/darkweb_files/image001.png",
    "nui/html/darkweb_files/item0001.xml",
    "nui/html/darkweb_files/props002.xml",
    "nui/html/darkweb_files/themedata.thmx",
    "nui/html/darkweb.html"
}

client_script "client.lua"
