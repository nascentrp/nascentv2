--[[Register]]--

RegisterNetEvent("garages:getVehicles")
RegisterNetEvent('garages:SpawnVehicle')
RegisterNetEvent('garages:StoreVehicle')
RegisterNetEvent('garages:SelVehicle')
--

local markerColor = 
{
	["Red"] = 222,
	["Green"] = 50,
	["Blue"] = 50
}
local garagesRunning = false

local garages = {}

Citizen.CreateThread(function()
	DefaultGarages(function(res)
		garages = res
		TriggerEvent("Garages:ShowGarageBlip")
	end)
end)

function DefaultGarages(cb)
	exports["nsrp_util"]:getGarageLocations(function(rec)
		local resp = parseNSRPGaragesIntoGarages(rec)
		cb(resp)
	end)
end

function parseNSRPGaragesIntoGarages(rec)
	local resp = {}
	for i = 1, #rec do
		local item = rec[i]
		resp[i] = {
			loc = {
				item["X"],
				item["Y"],
				item["Z"]
			},
			spawn = {
				item["spawnX"],
				item["spawnY"],
				item["spawnZ"]
			},
			garage = item["info"]
		}
	end
	return resp
end

RegisterNetEvent("checkifbike")
AddEventHandler("checkifbike", function(vehicle, veh_id)
	local bicycle = IsThisModelABicycle( GetHashKey(vehicle) )
	if bicycle then
		TriggerServerEvent( "garages:CheckForSpawnVeh2", veh_id)
	else
		TriggerEvent( "DoLongHudText", "No License.",2 )
	end
end)

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

--[[Local/Global]]--

--["garage"..table_id] = { ["x"] = 0.0, ["y"] = 0.0, ["z"] = 0.0, ["table_id"] = table_id },
local mygarages = {}
local scannedPOIs = {}


function DeleteSpawnedHouse()

	TriggerEvent("inhouse",false)
    local playerped = PlayerPedId()
    local plycoords = GetEntityCoords(playerped)
    local handle, ObjectFound = FindFirstObject()
    local success
    repeat
        local pos = GetEntityCoords(ObjectFound)
        local distance = #(vector3(plycoords["x"], plycoords["y"], (plycoords["z"])) - pos)
        if distance < 35.0 and ObjectFound ~= playerped then
        	if not IsEntityAPed(ObjectFound) then
        		DeleteObject(ObjectFound)
        	end            
        end
        success, ObjectFound = FindNextObject(handle)
    until not success
    EndFindObject(handle)
end


function hasKey(id) 
	local myhousekeys = exports["isPed"]:isPed("myhousekeys")
	for i = 1, #myhousekeys do
		if tonumber(myhousekeys[i]) == id then
			return true
		end
	end
	return false
end


-- this is called if we get a new key
RegisterNetEvent("RequestKeyUpdate")
AddEventHandler("RequestKeyUpdate", function()
	TriggerServerEvent("ReturnHouseKeys")
end)

-- this is called if a key is altered, we request new ones if one of ours is.
RegisterNetEvent("CheckForKeyUpdate")
AddEventHandler("CheckForKeyUpdate", function(id)
	if hasKey(id) then
		TriggerServerEvent("ReturnHouseKeys")
	end
end)


RegisterNetEvent("UpdateCurrentHouseSpawns")
AddEventHandler("UpdateCurrentHouseSpawns", function(id,house_poi)

	local id = tonumber(id)
	if hasKey(id) then
		TriggerServerEvent("GarageData")
	end

end)

RegisterNetEvent("house:garagelocations")
AddEventHandler("house:garagelocations", function(sentmygarages)
	mygarages = {}
	scannedPOIs = {}
	mygarages = sentmygarages
	garagesRunning = false
	TriggerEvent("RecreateGarages")	
end)

local entering = false
RegisterNetEvent("house:entering")
AddEventHandler("house:entering", function(entering)
	entering = true
	Citizen.Wait(10000)
	entering = false
end)


RegisterNetEvent("house:garagechanges")
AddEventHandler("house:garagechanges", function(table_id)
	if scannedPOIs["garage"..table_id] ~= nil then
		Citizen.Wait(math.random(1000,5000))
		TriggerEvent("house:entering")
		TriggerServerEvent("GarageData")
	end
end)


Citizen.CreateThread(function()
	--TriggerServerEvent("GarageData")
	
	while true do
		Citizen.Wait(1)
		local closest = 500.0
		if garagesRunning then
			for i,row in pairs(mygarages) do
				local dist = #(vector3(row["x"],row["y"],row["z"]) - GetEntityCoords(PlayerPedId()))
				if dist < 40.0 and scannedPOIs["garage"..row["table_id"]] == nil and not resetting then
					scannedPOIs["garage"..row["table_id"]] = row
					CreateHouseGarages(row)

				elseif scannedPOIs["garage"..row["table_id"]] ~= nil and dist > 150.0 and not entering then
					scannedPOIs["garage"..row["table_id"]] = nil
					RemoveHouseGarages(row)
				end
				Citizen.Wait(1000)
			end
		end
		Citizen.Wait(math.ceil(closest * 20))
	end

end)

local addedGarages = {}
function CreateHouseGarages(currentRow)

	for x,row in pairs(currentRow["house_poi"]["garages"]) do

		local currentGarage = row
		local garageName = currentRow["house_name"] .. " #" .. x

		for _, g in ipairs(garages) do
			if g.garage == garageName and g.disabled then
				addedGarages[garageName] = true
				g.disabled = false
			end
		end

		if addedGarages[garageName] == nil then
			garages[#garages+1] =
			{ 
				loc = { currentGarage["x"],currentGarage["y"],currentGarage["z"],currentGarage["h"] },
				spawn = { currentGarage["x"],currentGarage["y"],currentGarage["z"],currentGarage["h"] }, 
				garage = garageName
			}
		end

	end

	garagesRunning = false
	TriggerEvent("RecreateGarages")
end

function RemoveHouseGarages(currentRow)
	
	local todelete = {}

	for x,row in pairs(currentRow["house_poi"]["garages"]) do
		local currentGarage = row
		local garageName = currentRow["house_name"] .. " #" .. x
		for i = 1, #garages do
			if garageName == garages[i].garage and not garages[i].disabled then
				garages[i].disabled = true
			end
		end
	end

	garagesRunning = false
	TriggerEvent("RecreateGarages")	
end

local myroomtype = 0
local showGarages = false

local blips = {}
RegisterNetEvent('Garages:ShowGarageBlip')
AddEventHandler('Garages:ShowGarageBlip', function(rec)
	local currentBlipCount = #blips
	print(json.encode(garages))
	for _, item in pairs(garages) do
		currentBlipCount = currentBlipCount + 1
		blips[currentBlipCount] = AddBlipForCoord(item.loc[1], item.loc[2], item.loc[3])
		if item.garage == "Garage Impound Lot" then
			SetBlipSprite(blips[currentBlipCount], 524)
			AddTextComponentString('Impound Lot')
			SetBlipColour(blips[currentBlipCount], 47)
		else
			SetBlipSprite(blips[currentBlipCount], 595)
			SetBlipColour(blips[currentBlipCount], 0)
			AddTextComponentString(item.garage)
		end
		SetBlipScale(blips[currentBlipCount], 0.75)
		SetBlipAsShortRange(blips[currentBlipCount], true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString(item.garage)
		EndTextCommandSetBlipName(blips[currentBlipCount])
	end
end)






--





VEHICLES = {}
local vente_location = {-45.228, -1083.123, 25.816}
local inrangeofgarage = false
local currentlocation = nilwaaaaaaaaaa
local garage = {title = "garage", currentpos = nil, marker = { r = 0, g = 155, b = 255, a = 200, type = 1 }}
local selectedGarage = false


--[[Functions]]--

function MenuGarage()
	
    ped = PlayerPedId();
    MenuTitle = "Garage"
    ClearMenu()
    Menu.addButton("Store Vehicle","RentrerVehicule",nil)
    Menu.addButton("Vehicle List","ListeVehicule",nil)
    Menu.addButton("Close Menu","CloseMenu",nil) 
end

function RentrerVehicule()
	TriggerServerEvent('garages:CheckForVeh',garages[selectedGarage].garage)
	CloseMenu()
end
carCount = 0
firstCar = 0



function doCarDamages(eh, bh, Fuel, veh)
	smash = false
	damageOutside = false
	damageOutside2 = false 
	local engine = eh + 0.0
	local body = bh + 0.0
	if engine < 200.0 then
		engine = 200.0
	end

	if body < 150.0 then
		body = 150.0
	end
	if body < 950.0 then
		smash = true
	end

	if body < 920.0 then
		damageOutside = true
	end

	if body < 920.0 then
		damageOutside2 = true
	end

	local currentVehicle = (veh and IsEntityAVehicle(veh)) and veh or GetVehiclePedIsIn(PlayerPedId(), false)

	Citizen.Wait(100)
	SetVehicleEngineHealth(currentVehicle, engine)
	if smash then
		SmashVehicleWindow(currentVehicle, 0)
		SmashVehicleWindow(currentVehicle, 1)
		SmashVehicleWindow(currentVehicle, 2)
		SmashVehicleWindow(currentVehicle, 3)
		SmashVehicleWindow(currentVehicle, 4)
	end
	if damageOutside then
		SetVehicleDoorBroken(currentVehicle, 1, true)
		SetVehicleDoorBroken(currentVehicle, 6, true)
		SetVehicleDoorBroken(currentVehicle, 4, true)
	end
	if damageOutside2 then
		SetVehicleTyreBurst(currentVehicle, 1, false, 990.0)
		SetVehicleTyreBurst(currentVehicle, 2, false, 990.0)
		SetVehicleTyreBurst(currentVehicle, 3, false, 990.0)
		SetVehicleTyreBurst(currentVehicle, 4, false, 990.0)
	end
	if body < 1000 then
		SetVehicleBodyHealth(currentVehicle, 985.0)
	end
	DecorSetInt(currentVehicle, "CurrentFuel", Fuel)


end


local myspawnedhousecars = {}
nearHouseGarage = false
local currentGarage = GetEntityCoords(PlayerPedId())
RegisterNetEvent('Garages:SpawnHouseGarage')
AddEventHandler('Garages:SpawnHouseGarage', function(z)
	currentGarage = z

	ListeVehicule()

end)
local dntdelete = "none"
RegisterNetEvent('Garages:HouseRemoveVehicle')
AddEventHandler('Garages:HouseRemoveVehicle', function(veh)
	for i = 1, #myspawnedhousecars do
		if myspawnedhousecars[i] == veh then
			table.remove(myspawnedhousecars,i)
		end
	end

	local plate = GetVehicleNumberPlateText(veh)
	SetVehicleHasBeenOwnedByPlayer(veh,true)
	
	local id = NetworkGetNetworkIdFromEntity(veh)
	SetNetworkIdCanMigrate(id, true)
	dntdelete = plate
	TriggerEvent("keys:addNew",veh,plate)
	TriggerServerEvent('garages:SetVehOut', veh, plate)
	TriggerEvent("Garages:ToggleHouse",false)
	TriggerEvent("veh.PlayerOwned",veh)
	Citizen.Wait(1000)
	TriggerServerEvent("garages:CheckGarageForVeh")
end)



RegisterNetEvent('hotel:myroomtype')
AddEventHandler('hotel:myroomtype', function(roomtype)
	myroomtype = roomtype
end)

RegisterNetEvent('Garages:ToggleHouse')
AddEventHandler('Garages:ToggleHouse', function(tg)

	nearHouseGarage = tg
	if nearHouseGarage then

	else
		for i=1,#myspawnedhousecars do
			local plate = GetVehicleNumberPlateText(myspawnedhousecars[i])
			if plate ~= dntdelete then

				--SetEntityAsMissionEntity(myspawnedhousecars[i],false,true)
				--DeleteEntity(myspawnedhousecars[i])
				Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(myspawnedhousecars[i]))
			end
		end
	end

	myspawnedhousecars = {}
	dntdelete = "none"

end)




function AddonsHouseCars(veh,v)
	
	
	SetVehicleOnGroundProperly(veh)
	SetEntityInvincible(veh, false) 

	SetVehicleModKit(veh, 0)
	local plate = v.license_plate
	SetVehicleNumberPlateText(veh, plate)
	local customized = v.data
	local plate = GetVehicleNumberPlateText(vehicle)
	TriggerEvent("chop:plateoff",plate)	


	if customized ~= nil then
		SetVehicleWheelType(veh, customized.wheeltype)
		SetVehicleNumberPlateTextIndex(veh, 3)

		for i = 0, 16 do
			SetVehicleMod(veh, i, customized.mods[tostring(i)])
		end

		for i = 17, 22 do
			ToggleVehicleMod(veh, i, customized.mods[tostring(i)])
		end

		for i = 23, 48 do
			SetVehicleMod(veh, i, customized.mods[tostring(i)])
		end

		for i = 0, 3 do
			SetVehicleNeonLightEnabled(veh, i, customized.neon[tostring(i)])
		end

		SetVehicleColours(veh, customized.colors[1], customized.colors[2])
		SetVehicleExtraColours(veh, customized.extracolors[1], customized.extracolors[2])
		SetVehicleNeonLightsColour(veh, customized.lights[1], customized.lights[2], customized.lights[3])
		SetVehicleTyreSmokeColor(veh, customized.smokecolor[1], customized.smokecolor[2], customized.smokecolor[3])
		SetVehicleWindowTint(veh, customized.tint)
	else
		SetVehicleColours(veh, 0, 0)
		SetVehicleExtraColours(veh, 0, 0)
	end
	--SetEntityAsMissionEntity(veh,false,true)

	TriggerEvent("keys:addNew",veh,plate)
	SetVehicleHasBeenOwnedByPlayer(veh,true)
	

	local id = NetworkGetNetworkIdFromEntity(veh)
	SetNetworkIdCanMigrate(id, true)
	
	if GetEntityModel(veh) == `rumpo4` then
		SetVehicleLivery(veh,0)
	end

	if GetEntityModel(veh) == `rumpo` then
		SetVehicleLivery(veh,0)
	end

	if GetEntityModel(veh) == `taxi` then

		SetVehicleExtra(veh, 8, 1)
		SetVehicleExtra(veh, 9, 1)
		SetVehicleExtra(veh, 6, 0)

	end
	doCarDamages(v.engine_damage, v.body_damage, v.fuel, veh)
	myspawnedhousecars[#myspawnedhousecars+1]=veh
end

function spawnCars(houseCars)
 	myspawnedhousecars = {}
 	local spawnStart = {}
 	spawnStart.x = currentGarage.x-4.1
 	spawnStart.y = currentGarage.y-14.2
 	spawnStart.z = currentGarage.z+1.0
 	local fuckyou = 1
	for i, v in pairs(houseCars) do
		if i < 7 then

	 		RequestModel(v.model)
			while not HasModelLoaded(v.model) do
				Citizen.Wait(1)
			end
	 		local vehicle = CreateVehicle(v.model,spawnStart.x,spawnStart.y + (i*4.5),spawnStart.z,235.0,true,false)
			SetModelAsNoLongerNeeded(v.model)
	 		AddonsHouseCars(vehicle,v)	
	
		else
	 		RequestModel(v.model)
			while not HasModelLoaded(v.model) do
				Citizen.Wait(1)
			end
			SetModelAsNoLongerNeeded(v.model)

	 		if i < 12 then
	 			local vehicle = CreateVehicle(v.model,spawnStart.x+7.7,spawnStart.y + (fuckyou*4.5),spawnStart.z,115.0,true,false)	
	 			AddonsHouseCars(vehicle,v)

	 			fuckyou = fuckyou + 1
	 		end		
		end
	end 

 end


function ListeVehicule()
    ped = PlayerPedId();
    MenuTitle = "My Vehicles :"
    ClearMenu()
    carCount = 0
    for ind, value in pairs(VEHICLES) do
		carCount = carCount + 1
    end 
    
    estimate = 15 * (carCount * carCount * 2)

    if firstCar == 0 then estimate = 0 end


	if nearHouseGarage then
		curGarage = "House"
		local houseCars = {}
	    for ind, v in pairs(VEHICLES) do
	    	enginePercent = v.engine_damage / 10
	    	bodyPercent = v.body_damage / 10
	  		curGarage = v.current_garage

	  		if curGarage == nil then
	  			curGarage = "Any"
	  		end
			if (curGarage == "House") and v.vehicle_state == "In" then
				houseCars[#houseCars+1]= { ["id"] = v.id, ["model"] = v.model, ["name"] = v.name, ["license_plate"] = v.license_plate, ["vehicle_state"] = v.vehicle_state, ["engine_damage"] = v.engine_damage, ["body_damage"] = v.body_damage, ["current_garage"] = v.current_garage, ["data"] = v.data, ["fuel"] = v.fuel }
			end
	    end 
	    spawnCars(houseCars)
	else
	    for ind, value in pairs(VEHICLES) do
	    	enginePercent = value.engine_damage / 10
	    	bodyPercent = value.body_damage / 10
	  		curGarage = value.current_garage

	  		if curGarage == nil then
	  			curGarage = "Any"
	  		end

			if value.vehicle_state == "Standard Impound" then
				Menu.addButton(tostring(value.name), "OptionVehicle", value.id, "Impounded: R500", " Engine %:" .. enginePercent .. "", " Body %:" .. bodyPercent .. "")
			elseif value.vehicle_state == "Police Impound" then
				Menu.addButton(tostring(value.name), "OptionVehicle", value.id, "Impounded: R1500", " Engine %:" .. enginePercent .. "", " Body %:" .. bodyPercent .. "")
			elseif curGarage ~= current_used_garage and curGarage ~= "Any" and value.vehicle_state ~= "Out" then
				Menu.addButton(tostring(value.name), "OptionVehicle", value.id, "Garage " .. curGarage, " Engine %:" .. enginePercent .. "", " Body %:" .. bodyPercent .. "")
			else
				Menu.addButton(tostring(value.name), "OptionVehicle", value.id, tostring(value.vehicle_state) , " Engine %:" .. enginePercent .. "", " Body %:" .. bodyPercent .. "")
			end
	    end    
	end

    TriggerEvent("DoLongHudText", "It will cost R" .. estimate .. " to R" .. estimate + (estimate * 0.15) .. " @ R15 x (carCount x carCount x 2)",1)
		
    Menu.addButton("Return","MenuGarage",nil)
end


RegisterNetEvent('Garages:PhoneUpdate')
AddEventHandler('Garages:PhoneUpdate', function()
	TriggerEvent("phone:Garage",VEHICLES)
end)


function OptionVehicle(vehID)
	local vehID = vehID
    MenuTitle = "Options :"
    ClearMenu()
    Menu.addButton("Take Out", "SortirVehicule", vehID)
	--Menu.addButton("Supprimer", "SupprimerVehicule", vehID)
    Menu.addButton("Return", "ListeVehicule", nil)
end

function SortirVehicule(vehID)

	local vehID = vehID
	if firstCar == 0 then carCount = 0 end
	local impound = false
	local dist = #(vector3(550.09,-55.45,71.08) - GetEntityCoords(LocalPed()))

	local rooster = exports["isPed"]:GroupRank("rooster_academy")

	if dist < 15.0 then
		impound = true
	end
	local garagecount = 1
	for i = 1, #garages do
		if current_used_garage == garages[i] then
			garagecount = i
		end
	end
	local garagefree = false

	if addedGarages[current_used_garage] ~= nil then
		garagefree = true
	end
	for i = 1, #garages do
		if garages[i].garage == "House" then
			garagefree = true
		end
	end

	TriggerServerEvent('garages:CheckForSpawnVeh', vehID, carCount,impound, current_used_garage,garagefree)
	firstCar = 1
	CloseMenu()

end


function CloseMenu()
	TriggerEvent("inmenu",false)
    Menu.hidden = true
end

function LocalPed()
	return PlayerPedId()
end

function IsPlayerInRangeOfGarage()
	return inrangeofgarage
end

function Chat(debugg)
    TriggerEvent("chatMessage", '', { 0, 0x99, 255 }, tostring(debugg))
end

isJudge = false
RegisterNetEvent("isJudge")
AddEventHandler("isJudge", function()
	isJudge = true
end)

RegisterNetEvent("isJudgeOff")
AddEventHandler("isJudgeOff", function()
    isJudge = false
end)
RegisterNetEvent("car:dopayment")
AddEventHandler("car:dopayment", function(plate)
	local rankCarshop = exports["isPed"]:GroupRank("car_shop")
    local rankImport = exports["isPed"]:GroupRank("illegal_carshop")
    local salesman = false
	if rankCarshop > 0 or rankImport > 0 then
		salesman = true
	end
	TriggerServerEvent('car:dopayment', plate, salesman)
end)

RegisterNetEvent("car:carpaymentsowed")
AddEventHandler("car:carpaymentsowed", function()
    TriggerServerEvent("car:Outstanding")
end)

current_used_garage = "Any"

Citizen.CreateThread(function()
	Citizen.Wait(1000)
	TriggerEvent("RecreateGarages")
end) 

function spawnJudgeCar(model,garage)

		RequestModel(model)
		while not HasModelLoaded(model) do
		  Wait(1)
		end 

		local playerPed = PlayerPedId()
		local spawnPos = garages[garage].spawn
		local spawned_car = CreateVehicle(model, spawnPos[1], spawnPos[2], spawnPos[3], spawnPos[4], true, false)

		local plate = "TRU ".. GetRandomIntInRange(1000, 9000)

		SetVehicleOnGroundProperly(spawned_car)

		SetVehicleNumberPlateText(spawned_car, plate)
		TriggerEvent("keys:addNew",spawned_car,plate)
		TriggerServerEvent('garges:addJobPlate', plate)
		SetModelAsNoLongerNeeded(model)
		SetPedIntoVehicle(playerPed, spawned_car, -1)
		--SetEntityAsMissionEntity(spawned_car,false,true)

end

Controlkey = {["generalUse"] = {38,"E"},["generalUseSecondary"] = {18,"Enter"},["generalUseThird"] = {47,"G"}} 
RegisterNetEvent('event:control:update')
AddEventHandler('event:control:update', function(table)
	Controlkey["generalUse"] = table["generalUse"]
	Controlkey["generalUseSecondary"] = table["generalUseSecondary"]
	Controlkey["generalUseThird"] = table["generalUseThird"]
end)

RegisterNetEvent("RecreateGarages")
AddEventHandler("RecreateGarages", function()
	Citizen.Wait(5000)
	if not garagesRunning then
		garagesRunning = true
		local rooster = exports["isPed"]:GroupRank("rooster_academy")
		for k,v in ipairs(garages) do
			Citizen.CreateThread(function()
				if v.disabled then return end
				local pos = v.loc
				local gar = v.garage
				while garagesRunning do
					Citizen.Wait(1)

					local dist = #(vector3(pos[1],pos[2],pos[3]) - GetEntityCoords(LocalPed()))
					if dist < 35.0 then
						dist = math.floor(200 - (dist * 10))
						if dist < 0 then dist = 0 end
						if myroomtype ~= 3 and gar == "House" then

						else
							if gar == "Rooster Cab" and rooster <= 0 then

							else
								DrawMarker(20,pos[1],pos[2],pos[3],0,0,0,0,0,0,0.701,1.0001,0.3001,markerColor.Red,markerColor.Green, markerColor.Blue,dist,0,0,0,0)
							end
							
						end
					end

					if #(vector3(pos[1],pos[2],pos[3]) - GetEntityCoords(LocalPed())) < 3.0 and IsPedInAnyVehicle(LocalPed(), true) == false then
						if gar == "Rooster Cab" and rooster <= 0 then
						else
							if Menu.hidden then
								current_used_garage = garages[k].garage
							else
								DisplayHelpText('~g~'..Controlkey["generalUse"][2]..'~s~ or ~g~'..Controlkey["generalUseSecondary"][2]..'~s~ Accepts ~g~Arrows~s~ Move ~g~Backspace~s~ Exit')
							end

							if IsControlJustPressed(1, 177) and not Menu.hidden then
							--	MenuCallFunction(Menu.GUI[Menu.selection -1]["func"], Menu.GUI[Menu.selection -1]["args"])
								CloseMenu()
								PlaySound(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET", 0, 0, 1)
							end
							if (myroomtype ~= 3 and gar == "House") then

							elseif ( IsControlJustPressed(1,Controlkey["generalUse"][1]) or IsControlJustPressed(1,Controlkey["generalUseSecondary"][1]) ) and Menu.hidden then
								TriggerServerEvent("garages:CheckGarageForVeh")
								Citizen.Wait(150)
								MenuGarage()
								TriggerEvent("inmenu",true)
								selectedGarage = k
								Menu.hidden = not Menu.hidden
							elseif IsControlJustPressed(1,74)  and k == 17 and isJudge then -- H
								spawnJudgeCar("g65amg",k)
							end
							Menu.renderGUI()
						end
					end
					if dist > 200.0 then
						Citizen.Wait(2000)
					end
				end
			end)
		end
	end
end)





-- Citizen.CreateThread(function()
-- 	local loc = vente_location
-- 	pos = vente_location
-- 	local blip = AddBlipForCoord(pos[1],pos[2],pos[3])
-- 	SetBlipSprite(blip,207)
-- 	SetBlipColour(blip, 2)
-- 	BeginTextCommandSetBlipName("STRING")
-- 	AddTextComponentString('Sell Vehicle')
-- 	EndTextCommandSetBlipName(blip)
-- 	SetBlipAsShortRange(blip,true)
-- 	SetBlipAsMissionCreatorBlip(blip,true)
-- 	SetBlipScale(blip, 0.7)

-- 	checkgarage = 0
-- 	while true do
		
-- 		Wait(1)

-- 		DrawMarker(27,376.6728515625,-1612.5723876953,28.29195022583,0,0,0,0,0,0,3.001,3.0001,0.5001,0,155,255,50,0,0,0,0)
-- 		if isJudge and #(vector3(376.6728515625,-1612.5723876953,29.29195022583) - GetEntityCoords(LocalPed())) < 5 and IsPedInAnyVehicle(LocalPed(), true) == false then
-- 			DisplayHelpText('Press on ~g~'..Controlkey["generalUse"][2]..'~s~ to sell this vehicle for the state!')		
-- 			if IsControlJustPressed(1,Controlkey["generalUse"][1]) then
-- 				local caissei = GetClosestVehicle(376.6728515625,-1612.5723876953,29.29195022583, 3.000, 0, 70)
-- 				if not DoesEntityExist(caissei) then caissei = GetVehiclePedIsIn(PlayerPedId(), true) end
-- 				if DoesEntityExist(caissei) then
-- 					local plate = GetVehicleNumberPlateText(caissei)
-- 					TriggerServerEvent('garages:SelVehJudge',plate)
-- 					--SetEntityAsMissionEntity(caissei,false,true)
-- 					Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caissei))
-- 				else
-- 					TriggerEvent("DoLongHudText","No Vehicle",2)
-- 				end
-- 			end
-- 		end

-- 		wait2 = #(vector3(376.6728515625,-1612.5723876953,29.29195022583) - GetEntityCoords(LocalPed()))
-- 		if wait2 > 50.0 then
-- 			if wait2 then
-- 				Citizen.Wait(math.ceil(wait2) * 10)
-- 			end
-- 		end

-- 	end

-- end)




currentlyselling = false

AddEventHandler('garages:chopshopVehicle', function()
	if not currentlyselling then
		currentlyselling = true
		Citizen.CreateThread(function()		
			local allowsale = true
			local startTime = GetGameTimer()
			TriggerEvent("DoLongHudText","Attemping Vehicle Sale",1)
			count = 12000
			while count > 0 do
				Citizen.Wait(1)
				local curTime = GetGameTimer()
				if #(vector3(712.51171875,-723.117553710938,25.9337730407715) - GetEntityCoords(LocalPed())) > 15 then	
					allowsale = false
					count = -10	
				end
				 drawTxt(0.89, 1.44, 1.0,1.0,0.6, "Selling Vehicle: " .. math.floor(GetTimeDifference(curTime, startTime)/1000) .. " out of 120 seconds", 255, 255, 255, 255)
				if GetTimeDifference(curTime, startTime) > 120000 then
					count = -1
				end
			end
			local caissei = GetClosestVehicle(215.124, -791.377, 30.836, 3.000, 0, 70)
			if not DoesEntityExist(caissei) then caissei = GetVehiclePedIsIn(PlayerPedId(), true) end
		--	SetEntityAsMissionEntity(caissei,false,true)

			if DoesEntityExist(caissei) and #(vector3(712.51171875,-723.117553710938,25.9337730407715) - GetEntityCoords(LocalPed())) < 15 and allowsale then
				TriggerEvent('keys:remove', GetVehicleNumberPlateText(caissei))
				Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caissei))
				TriggerEvent("DoLongHudText","Vehicle sold",1)
				TriggerServerEvent("garages:SelHotVeh")

			else
				TriggerEvent("DoLongHudText","No Vehicle or moved too far..",2)


			end   
			currentlyselling = false

		end)
	else
		TriggerEvent("DoLongHudText","Already selling vehicle",2)
	end
end)

function drawTxt(x,y ,width,height,scale, text, r,g,b,a, outline)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(2, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

--[[Events]]--
AddEventHandler("garages:getVehicles", function(THEVEHICLES)
    VEHICLES = {}
	VEHICLES = THEVEHICLES
	TriggerEvent("Garages:PhoneUpdate")
end)

AddEventHandler("playerSpawned", function()
    TriggerServerEvent("garages:CheckGarageForVeh")
end)

AddEventHandler('garages:SpawnVehicle', function(oof, vehicle, plate, customized, state, Fuel)

	local customized = json.decode(customized)
	
	Citizen.CreateThread(function()			
		Citizen.Wait(100)

		-- if coordlocation == nil then
		-- 	local loc = garages[selectedGarage].loc
		-- 	local caisseo = GetClosestVehicle(loc[1], loc[2], loc[3], 3.000, 0, 70)
		-- 	if DoesEntityExist(caisseo) then
		-- 		TriggerEvent("DoLongHudText", "The area is crowded",2)
		-- 		TriggerServerEvent("garages:CheckGarageForVeh")
		-- 		return
		-- 	end
		-- end
		if oof == garages[selectedGarage].garage then
			if state == "Out" and coordlocation == nil then
				TriggerEvent("DoLongHudText","Not in garage",2)
			else
				if state == "Normal Impound" then
					TriggerServerEvent('ImpoundLot')
					TriggerEvent("DoLongHudText","This vehicle cost you R50.",1)
				end

				if state == "Police Impound" then
					TriggerServerEvent('ImpoundLot2')
					TriggerEvent("DoLongHudText","This vehicle cost you R200.",1)
				end

				local spawnPos
				if coordlocation ~= nil then
					spawnPos = coordLocation
					--veh = CreateVehicle(vehicle, coordlocation[1],coordlocation[2],coordlocation[3], 0.0, true, false)
				else
					spawnPos = garages[selectedGarage].spawn
					--veh = CreateVehicle(car, spawnPos[1], spawnPos[2], spawnPos[3], spawnPos[4], true, false)
				end
				
				exports['nsrp_util']:spawnVehicle(vehicle, spawnPos[1], spawnPos[2], spawnPos[3],
					function(veh) -- This is a big nest

						if Fuel < 5 then
							Fuel = 5
						end

						DecorSetInt(veh, "CurrentFuel", Fuel)
						DecorSetBool(veh, "PlayerVehicle", true)
						SetVehicleOnGroundProperly(veh)
						SetEntityInvincible(veh, false) 

						SetVehicleModKit(veh, 0)

						SetVehicleNumberPlateText(veh, plate)

						if customized then
							SetVehicleWheelType(veh, customized.wheeltype)
							SetVehicleNumberPlateTextIndex(veh, 3)

							for i = 0, 16 do
								SetVehicleMod(veh, i, customized.mods[tostring(i)])
							end

							for i = 17, 22 do
								ToggleVehicleMod(veh, i, customized.mods[tostring(i)])
							end

							for i = 23, 24 do
								local isCustom = customized.mods[tostring(i)]
								if (isCustom == nil or isCustom == "-1" or isCustom == false or isCustom == 0) then
									isSet = false
								else
									isSet = true
								end
								SetVehicleMod(veh, i, customized.mods[tostring(i)], isCustom)
							end

							for i = 23, 48 do
								SetVehicleMod(veh, i, customized.mods[tostring(i)])
							end

							for i = 0, 3 do
								SetVehicleNeonLightEnabled(veh, i, customized.neon[tostring(i)])
							end

							if customized.extras ~= nil then
								for i = 1, 12 do
									local onoff = tonumber(customized.extras[i])
									if onoff == 1 then
										SetVehicleExtra(veh, i, 0)
									else
										SetVehicleExtra(veh, i, 1)
									end
								end
							end

							if customized.oldLiveries ~= nil and customized.oldLiveries ~= 24  then
								SetVehicleLivery(veh, customized.oldLiveries)
							end

							if customized.plateIndex ~= nil and customized.plateIndex ~= 4 then
								SetVehicleNumberPlateTextIndex(veh, customized.plateIndex)
							end

							-- Xenon Colors
							SetVehicleXenonLightsColour(veh, (customized.xenonColor or -1))
							SetVehicleColours(veh, customized.colors[1], customized.colors[2])
							SetVehicleExtraColours(veh, customized.extracolors[1], customized.extracolors[2])
							SetVehicleNeonLightsColour(veh, customized.lights[1], customized.lights[2], customized.lights[3])
							SetVehicleTyreSmokeColor(veh, customized.smokecolor[1], customized.smokecolor[2], customized.smokecolor[3])
							SetVehicleWindowTint(veh, customized.tint)
							SetVehicleInteriorColour(veh, customized.dashColour)
							SetVehicleDashboardColour(veh, customized.interColour)
						else

							SetVehicleColours(veh, 0, 0)
							SetVehicleExtraColours(veh, 0, 0)

						end

						TriggerEvent("keys:addNew",veh,plate)
						SetVehicleHasBeenOwnedByPlayer(veh,true)
						
						local id = NetworkGetNetworkIdFromEntity(veh)
						SetNetworkIdCanMigrate(id, true)
						
						TriggerServerEvent('garages:SetVehOut', veh, plate)

						SetPedIntoVehicle(PlayerPedId(), veh, - 1)

						TriggerServerEvent('veh.getVehicles', plate, veh)
						
						if GetEntityModel(veh) == `rumpo4` then
							SetVehicleLivery(veh,0)
						end
						
						if GetEntityModel(veh) == `rumpo` then
							SetVehicleLivery(veh,0)
						end

						if GetEntityModel(veh) == `taxi` then
							SetVehicleExtra(veh, 8, 1)
							SetVehicleExtra(veh, 9, 1)
							SetVehicleExtra(veh, 6, 0)
						end

					--	SetEntityAsMissionEntity(veh,false,true)
						TriggerEvent("chop:plateoff",plate)
				end,
				-20.0, plate)
			end
				
			TriggerServerEvent("garages:CheckGarageForVeh")
		else
			TriggerEvent('DoLongHudText', 'Vehicle not in garage', 2)
		end
	end)
end)

AddEventHandler('garages:StoreVehicle', function(plates)
	plates = plates
	local pos = garages[selectedGarage].loc
	Citizen.CreateThread(function()		
		exports.veh:trackVehicleHealth()
		Citizen.Wait(3000)
		local caissei = GetClosestVehicle(pos[1], pos[2], pos[3], 3.000, 0, 70)
		current_used_garage = garages[selectedGarage].garage
		if not DoesEntityExist(caissei) then caissei = GetVehiclePedIsIn(PlayerPedId(), true) end

		if #(vector3(pos[1],pos[2],pos[3]) - GetEntityCoords(caissei)) > 5 then 
			TriggerEvent("DoLongHudText", "No vehicle",2)
			return
		end

		--SetEntityAsMissionEntity(caissei,false,true)		
		local platecaissei = GetVehicleNumberPlateText(caissei)
		local fuel = GetVehicleFuelLevel(caissei)
    	if current_used_garage == "House" then
    		local d1,d2 = GetModelDimensions(GetEntityModel(caissei))

    		if d2["y"] > 3.4 then
    			TriggerEvent("DoLongHudText","This vehicle is too big.",2)
    			return
    		end
		end
		
		if DoesEntityExist(caissei) then
			NetworkRequestControlOfEntity(caissei)

			local timeout = 0

			while timeout < 1000 and not NetworkHasControlOfEntity(caissei) do
				Citizen.Wait(100)
				timeout = timeout + 100
			end

			SetEntityAsMissionEntity(caissei, true, true)
			DeleteVehicle(caissei)
			DeleteEntity(caissei)

			TriggerEvent('keys:remove', platecaissei)

			TriggerServerEvent('garages:SetVehIn', platecaissei, current_used_garage, fuel)
		else
			TriggerEvent("DoLongHudText","No Vehicle",2)
		end   

		Citizen.Wait(5000)
		TriggerServerEvent("garages:CheckGarageForVeh")
	end)
end)

AddEventHandler('garages:SelVehicle', function(vehicle, plate)
	local car = GetHashKey(vehicle)	
	local plate = plate
	Citizen.CreateThread(function()		
		Citizen.Wait(0)
		local caissei = GetClosestVehicle(215.124, -791.377, 30.836, 3.000, 0, 70)
		if not DoesEntityExist(caissei) then caissei = GetVehiclePedIsIn(PlayerPedId(), true) end
		--SetEntityAsMissionEntity(caissei,false,true)
		local platecaissei = GetVehicleNumberPlateText(caissei)
		if DoesEntityExist(caissei) then	
			if plate ~= platecaissei then					
				TriggerEvent("DoLongHudText","It's not your vehicle",2)
			else
				TriggerEvent('keys:remove', platecaissei)
				Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caissei))
				TriggerServerEvent('garages:SelVeh', plate)
				TriggerServerEvent("garages:CheckGarageForVeh")
			end
		else
			TriggerEvent("DoLongHudText","No Vehicle",2)
		end   
	end)
end)

RegisterNetEvent('garages:SetWaypointToGarage')
AddEventHandler('garages:SetWaypointToGarage', function(garage)
	for i = 1, #garages do
		if garages[i] ~= nil then
			print(garages[i].garage)
			print(garage)
			if garages[i].garage == garage then
				local spawn = garages[i].loc
				SetNewWaypoint(spawn[1], spawn[2])
			end
		end
	end
end)


waitingForAccept = true
local isWanting = false
local previousPlayer = 0
RegisterNetEvent('garages:SellToPlayer')
AddEventHandler('garages:SellToPlayer', function(price,plate,player)
	if waitingForAccept then
		waitingForAccept = false
		TriggerServerEvent("CancelSale",previousPlayer)
		Citizen.Wait(1000)
	end

	waitingForAccept = true
	price = tonumber(price)
	player = tonumber(player)
	if price <= 0 then return end
	if player <= 0 then return end
	previousPlayer = player
	local car = GetHashKey(vehicle)	
	local plate = plate
	Citizen.CreateThread(function()		
		Citizen.Wait(0)
		local targetPed = GetPlayerFromServerId(player)
		if not DoesPlayerExist(player) or not DoesEntityExist(GetPlayerPed(targetPed)) then
			TriggerEvent("DoLongHudText","No Person",2)
			return
		end
		local pos = GetEntityCoords(PlayerPedId())
		local pos2 = GetEntityCoords(GetPlayerPed(targetPed))
		if #(vector3(pos.x,pos.y,pos.z) - vector3(pos2.x,pos2.y,pos2.z)) < 40 then
			local caissei = GetClosestVehicle(pos.x,pos.y,pos.z, 3.000, 0, 70)
			if not DoesEntityExist(caissei) then caissei = GetVehiclePedIsIn(PlayerPedId(), true) end
			local platecaissei = GetVehicleNumberPlateText(caissei)
			if DoesEntityExist(caissei) then	
				if plate ~= platecaissei then					
					TriggerEvent("DoLongHudText","It's not your vehicle",2)
				else
					TriggerServerEvent('garages:askRequest',player)
					while waitingForAccept do
						Wait(5)
						DisplayHelpText('Waiting for other person to decide.')
					end
					if isWanting then
						TriggerServerEvent('garages:SellToPlayerEnd', plate,player,price)
					else
						TriggerEvent("DoLongHudText","Person has declined.",2)
					end
				end
			else
				TriggerEvent("DoLongHudText","No Vehicle",2)
			end
		else
			TriggerEvent("DoLongHudText","Not close enough.",2)
		end
	end)
end)

local making = false
RegisterNetEvent('CancelNow')
AddEventHandler('CancelNow', function()
	making = false
end)	

RegisterNetEvent('garages:askingForVeh')
AddEventHandler('garages:askingForVeh', function(source)
	making = true
	Citizen.CreateThread(function()	
		while making do	
			Citizen.Wait(3)
			DisplayHelpText('~g~'..Controlkey["generalUse"][2]..'~s~ to Accept Car ,~g~'..Controlkey["generalUseSecondary"][2]..'~s~ to Refuse Car.')

			if ( IsControlJustPressed(1,Controlkey["generalUse"][1])) then
				TriggerServerEvent('garages:askResult',source,true)
				making = false
			elseif IsControlJustPressed(1,Controlkey["generalUseSecondary"][1]) then
				making = false
				TriggerServerEvent('garages:askResult',source,false)
			end
		end
	end)
end)

RegisterNetEvent('garages:clientResult')
AddEventHandler('garages:clientResult', function(result)
	waitingForAccept = false
	isWanting = result
end)

RegisterNetEvent('garages:ClientEnd')
AddEventHandler('garages:ClientEnd', function(plate)
	Citizen.CreateThread(function()		
		Citizen.Wait(0)
		local pos = GetEntityCoords(PlayerPedId())
		local caissei = GetClosestVehicle(pos.x,pos.y,pos.z, 3.000, 0, 70)
		if not DoesEntityExist(caissei) then caissei = GetVehiclePedIsIn(PlayerPedId(), true) end
		local platecaissei = GetVehicleNumberPlateText(caissei)
		if DoesEntityExist(caissei) then
			TriggerEvent('keys:remove', platecaissei)
			TriggerServerEvent("garages:CheckGarageForVeh")
		else
			TriggerEvent("DoLongHudText","No Vehicle",2)
		end   
	end)
end)

RegisterNetEvent('garages:PlayerEnd')
AddEventHandler('garages:PlayerEnd', function(plate)
	Citizen.CreateThread(function()		
		Citizen.Wait(0)
		local pos = GetEntityCoords(PlayerPedId())
		local caissei = GetClosestVehicle(pos.x,pos.y,pos.z, 3.000, 0, 70)
		if not DoesEntityExist(caissei) then caissei = GetVehiclePedIsIn(PlayerPedId(), true) end
		local platecaissei = GetVehicleNumberPlateText(caissei)
		if DoesEntityExist(caissei) then	
			TriggerEvent("keys:addNew",caissei,plate)
			TriggerServerEvent("garages:CheckGarageForVeh")
		else
			TriggerEvent("DoLongHudText","No Vehicle",2)
		end   
	end)
end)

local colorblind = false
RegisterNetEvent('option:colorblind')
AddEventHandler('option:colorblind',function()
	colorblind = not colorblind

	if colorblind then
		markerColor = 
		{
			["Red"] = 200,
			["Green"] = 200,
			["Blue"] = 0
		}
	else 
		markerColor = 
		{
			["Red"] = 222,
			["Green"] = 50,
			["Blue"] = 50
		}
	end
end)
