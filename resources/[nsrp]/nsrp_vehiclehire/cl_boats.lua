local boats = {
    {displayName = 'Dinghy', vehicle = 'dinghy', price = 100, coords = vector3(-1617.0656, 5270.983, 3.433)},
    {displayName = 'Jetmax', vehicle = 'jetmax', price = 400, coords = vector3(-1617.065, 5270.983, 3.433)}
}


RegisterNetEvent('boats:hireboatMenu')
AddEventHandler('boats:hireboatMenu', function()
    hireBoatMenu()
end)

function hireBoatMenu()
    local boatMenu = {
        {
            id = 1,
            header = "Boat Rental",
            txt = "",
        }
    }
    for i = 1, #boats do
        local count = i + 1
        table.insert(boatMenu,
            {
                id = count,
                header = boats[i].displayName,
                txt = 'R' .. boats[i].price,
                --txt = data[i].create_ts,
                params = {
                    event = "boats:hireboat",
                    args = {
                        boatId = i
                    },
                }
            }
        )
    end
    TriggerEvent('ethical-context:sendMenu', boatMenu)
end

local buyingBoat = false
RegisterNetEvent('boats:hireboat')
AddEventHandler('boats:hireboat', function(data)
    local boatId = data.boatId
    local boat = boats[boatId]
    buyingBoat = true
    TriggerServerEvent('boats:requestBoatHire', boat)
end)


RegisterNetEvent('boats:hireboatSuccess')
AddEventHandler('boats:hireboatSuccess', function(boat)
    exports['nsrp_util']:spawnVehicle(boat.vehicle, boat.coords[1], boat.coords[2], boat.coords[3],
        function()
        end,
    -20.0)
end)