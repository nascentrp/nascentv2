fx_version 'adamant'
games { 'rdr3', 'gta5' }
rdr3_warning 'I acknowledge that this is a prerelease build of RedM, and I am aware my resources *will* become incompatible once RedM ships.'

description 'NSRP VEHICLE HIRE'

version '1.0'

client_scripts {
  'cl_boats.lua'
}

server_scripts {
  'sv_boats.lua'
}