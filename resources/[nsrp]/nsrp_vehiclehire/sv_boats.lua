RegisterServerEvent('boats:requestBoatHire')
AddEventHandler("boats:requestBoatHire", function(boatObject)
    local src = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local amount = boatObject.price
    if (tonumber(user:getCash()) >= amount) then
        user:removeMoney(amount)
        TriggerClientEvent('boats:hireboatSuccess', src, boatObject)
    else
        TriggerClientEvent("DoLongHudText", src, 'You do not have enough money', 1)
    end
end)