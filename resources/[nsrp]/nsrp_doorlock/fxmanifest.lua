fx_version 'cerulean'
games { 'gta5' }

version '1.0.0'


server_scripts {
	'config.lua',
	'configs/**/*.lua',
	'server/main.lua'
}

client_scripts {
	'config.lua',
	'client/main.lua'
}


ui_page {
	'html/door.html',
}

files {
	'html/door.html',
	'html/main.js', 
	'html/style.css',

	'html/sounds/*.ogg',
}
