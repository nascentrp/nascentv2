RegisterServerEvent('nsrp_receipts:getreceipts')
AddEventHandler('nsrp_receipts:getreceipts', function(cid,job)
    local src = source
    local result = {}
    local recieptNumber = 1
    --Query Reciepts from table
    exports.ghmattimysql:execute('SELECT * FROM job_receipts WHERE `ispaid`= 0 AND job = @job AND sender = @cid', {
        ['@cid'] = cid,
        ['@job'] = job
    }, function(data)
        if data[1] then
            for i=1, #data do
               
                result[recieptNumber] = {
                    id = data[i].ID,
                    job = data[i].JOB,
                    type = data[i].TYPE,
                    amount = data[i].AMOUNT,
                    sender = data[i].SENDER,
                    receiver = data[i].RECEIVER,
                }
                recieptNumber = recieptNumber + 1
            end
            TriggerClientEvent("nsrp_receipts:updateMyPendingReciepts", src, result)
        else
            TriggerClientEvent("nsrp_receipts:updateMyPendingReciepts", src, result)
        end
    end)

end)

RegisterServerEvent('nsrp_receipts:payemployee')
AddEventHandler('nsrp_receipts:payemployee',function(amount)
	local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    user:addBank(amount)
end)

RegisterServerEvent('nsrp_receipts:payreceipt')
AddEventHandler('nsrp_receipts:payreceipt', function(id)

    --Update reciept to ISPAID = TRUE based on ID
    exports.ghmattimysql:execute("UPDATE job_receipts SET `ISPAID` = 1 WHERE `ID` = @ID ", {
        ['@ID'] = id
    })
end)

RegisterServerEvent('nsrp_receipts:insertreceipt')
AddEventHandler('nsrp_receipts:insertreceipt', function(job,type,amount,sender,receiver)

    --Insert new reciept

    exports.ghmattimysql:execute("INSERT INTO job_receipts (job, type, amount, sender, receiver, ispaid) VALUES (@job, @type, @amount,@sender,@receiver,@ispaid)", {
        ['@job'] = job,
        ['@type'] = type,
        ['@amount'] = amount,
        ['@sender'] = sender,
        ['@receiver'] = receiver,
        ['@ispaid'] = 0
        })

  -- if type == "STOCK" then
  --     TriggerEvent("bank:BussinessWithdraw",job,amount,sender,"STOCK PURCHASE","PURCHASE OF STOCK")
  -- elseif type == "CUSTOMER" then
  --     TriggerEvent("bank:BussinessDeposit",job,amount,sender,"INCOME","INCOME FROM CHARGING CITIZEN")
  -- end

end)