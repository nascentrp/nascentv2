--local cid = exports["isPed"]:isPed("cid")
--Job =  exports["isPed"]:isPed("myJob")
local recieptsPending = {}

--Uncomment below to debug
--RegisterCommand('createcreciept', function()
-- 
--  TriggerEvent('nsrp_receipts:debugcreate')
--
--end)

--RegisterCommand('payreciept', function()
-- 
-- TriggerEvent('nsrp_receipts:debugpayout')
--
--end)

RegisterNetEvent('nsrp_receipts:debugcreate')
AddEventHandler('nsrp_receipts:debugcreate',function()
  local cid = exports["isPed"]:isPed("cid")
  local job = exports["isPed"]:isPed("myjob")
  local amount = 100

  TriggerEvent('nsrp_receipts:createreceipt', amount,job,cid,cid,'STOCK')
  TriggerEvent('nsrp_receipts:createreceipt', amount,job,cid,cid,'CUSTOMER')
  print('Reciepts Created')
end)

RegisterNetEvent('nsrp_receipts:debugpayout')
AddEventHandler('nsrp_receipts:debugpayout',function()
  local cid = exports["isPed"]:isPed("cid")
  local job = exports["isPed"]:isPed("myjob")
  local commvalue = 5
   TriggerEvent('nsrp_receipts:payrecieptcl',job,cid,commvalue)
   print('Reciepts Payed Out')
end)

RegisterNetEvent('nsrp_receipts:getMyOwedReceipts')
AddEventHandler('nsrp_receipts:getMyOwedReceipts', function(job,cid)
  TriggerServerEvent('nsrp_receipts:getreceipts',cid,job)
  print('Owed Reciepts Fetched')
end)

RegisterNetEvent('nsrp_receipts:updateMyPendingReciepts')
AddEventHandler('nsrp_receipts:updateMyPendingReciepts', function(data)
  recieptsPending = data
  print('Reciepts Updated from Server')
end)

RegisterNetEvent('nsrp_receipts:payrecieptcl')
AddEventHandler('nsrp_receipts:payrecieptcl', function(job,cid,commisionvalue)
  local totalPayout = 0
  TriggerEvent('nsrp_receipts:getMyOwedReceipts',job,cid)
  local finished = exports["ethical-taskbar"]:taskBar(5000,"Fetching Reciepts to payout",false,false,nil) --This taskbar is to give time for the DB fetch
  
  if (finished == 100) then
    if recieptsPending[1] == nil or recieptsPending == {} then
      TriggerEvent('DoLongHudText', 'You dont have any pending reciepts to payout', 2)
    else
      for k,v in pairs(recieptsPending) do
        print(recieptsPending[k].type,recieptsPending[k].amount, recieptsPending[k].sender)
        if recieptsPending[k].type == 'STOCK' then --If item is stock like ingredients, refund the person exact amount spent
          totalPayout = totalPayout + recieptsPending[k].amount
          TriggerServerEvent('nsrp_receipts:payreceipt', recieptsPending[k].id)
        elseif recieptsPending[k].type == 'CUSTOMER' then --If item is customer i.e a sale, fine etc, give the person commission
          totalPayout = totalPayout + (recieptsPending[k].amount * (commisionvalue / 100))
          TriggerServerEvent('nsrp_receipts:payreceipt', recieptsPending[k].id)
        end
      end
      TriggerServerEvent('nsrp_receipts:payemployee',totalPayout)
    end
  end
  --Update local reciepts after is paid to avoid duplicating/exploiting
  TriggerEvent('nsrp_receipts:getMyOwedReceipts',job,cid)
  print('Pay Process Done')
end)

RegisterNetEvent('nsrp_receipts:createreceipt')
AddEventHandler('nsrp_receipts:createreceipt', function(amount,job,sender,receiver,type)
  TriggerServerEvent('nsrp_receipts:insertreceipt', job,type,amount,sender,receiver)
end)


RegisterNetEvent('nsrp_receipts:createstockreceiptcurrentjob') --TriggerEvent('burgershot:createstockreceipt',0)
AddEventHandler('nsrp_receipts:createstockreceiptcurrentjob', function(value)
  print('Stock reciept process',value)
  local jobName = exports["isPed"]:isPed("myjob")
  local amount = value
  local type = "STOCK"
  local sender = exports["isPed"]:isPed("cid")
  local receiver = exports["isPed"]:isPed("cid")
  TriggerEvent('nsrp_receipts:createreceipt',amount,jobName,sender,receiver,type)
end)

RegisterNetEvent('nsrp_receipts:createcustomerreceiptforcurrentjob') --TriggerEvent('burgershot:createstockreceipt',0)
AddEventHandler('nsrp_receipts:createcustomerreceiptforcurrentjob', function(value,target)
  print('Customer reciept process',value)
  local jobName = exports["isPed"]:isPed("myjob")
  local amount = value
  local type = "CUSTOMER"
  local sender = exports["isPed"]:isPed("cid")
  local receiver = target
  TriggerEvent('nsrp_receipts:createreceipt',amount,jobName,sender,receiver,type)
end)
------