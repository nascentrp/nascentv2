CREATE TABLE ethical.job_receipts (
	ID INT auto_increment NOT NULL COMMENT 'Unique ID of the receipt',
	JOB varchar(100) DEFAULT 'unemployed' NOT NULL COMMENT 'Job that this reciept relates to',
	`TYPE` varchar(100) DEFAULT 'STOCK' NOT NULL COMMENT 'Type of receipt(For stock purchased or customer sale)',
	AMOUNT INT DEFAULT 0 NOT NULL COMMENT 'Value of the receipt',
	SENDER varchar(100) DEFAULT '0' NOT NULL COMMENT 'The CID of the person who generated the receipt',
	RECEIVER varchar(100) NULL COMMENT 'Some sort of identifier for the payer for the receipt',
	ISPAID BOOL DEFAULT 0 NOT NULL COMMENT 'Has the receipt been paid',
	CONSTRAINT job_receipts_PK PRIMARY KEY (ID)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci
AUTO_INCREMENT=1;
