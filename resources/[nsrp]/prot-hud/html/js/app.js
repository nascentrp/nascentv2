$(function() {
    var isTalking = false;
    var carModeActive = false;
    var seatbelt = false;
    var currFuel = -1;
    window.addEventListener("message", function(event) {
        if (event.data.type == "updateStatusHud") {
            var data = event.data;
            loadStat("varHealth", data.varSetHealth);
            loadStat("varArmor", data.varSetArmor, true, function(armor) { return armor > 0 });
            loadStat("varHunger", data.varSetHunger);
            loadStat("varThirst", data.varSetThirst);
            loadStat("varSprint", data.sprint, true, function(sprint) { return sprint > 0 });
            loadStat("varOxygen", data.varSetOxy, true, function(oxygen) { return oxygen > 0 });
            loadStat("varStress", data.varSetStress);
            // console.log(data.isTalking)
            if(data.isTalking && !isTalking) {
                isTalking = true;
                $('#varVoice').addClass('active');
            } else if(!data.isTalking && isTalking) {
                isTalking = false;
                $('#varVoice').removeClass('active');
            }
            //console.log('stress', data.varSetStress)
            showEngineWarning(data.engine);
            if(carModeActive) {
                setSpeedo(data.speed);
            }
            if(data.seatbelt != seatbelt) {
                toggleSeatbelt(data.seatbelt);
                seatbelt = data.seatbelt;
            }
            setFuel(currFuel);
            currFuel = data.fuel;
            // $('#stressflash').fadeOut();
            // if(data.varSetStress > 0 && allowStressAnim) {
            //     $('#stressflash').fadeIn(500).delay(1000).fadeOut(500);
            //     setStressActive = true;
            //     allowStressAnim = false;
            //     setTimeout(function() {
            //         allowStressAnim = true;
            //     }, 10000)
            // }
            // let voice = event.data.varSetVoice;
            // if (voice == 1) {
            //     Progress(event.data.varSetVoice * 25, '.progress-voice')
            // }
            // if (voice == 2) {
            //     Progress(event.data.varSetVoice * 30, '.progress-voice')
            // }
            // if (voice == 3) {
            //     Progress(101, '.progress-voice')
            // }

            // if (event.data.hasParachute == true) {
            //     $("#parachute").removeClass("hidden");
            // } else {
            //     $("#parachute").addClass("hidden");
            // }
            
            // if (event.data.setOxy == false){
            //     $("#progress-oxygens").fadeOut();
            //     $("#oxygen-circle").fadeOut();
            //     document.getElementById("stress-circle").style.left = "88px";
            //     document.getElementById("stressLeft").style.left = "87%";

            // }else if(event.data.setOxy == true){
            //     $("#oxygen-circle").fadeIn();
            //     $("#progress-oxygens").fadeIn();
            //     document.getElementById("stressLeft").style.left = "106%";
            // }
            // if (event.data.setStress == false){
            //     $("#stress-circle").fadeOut();
            //     $("#stressLeft").fadeOut();
            // }else if(event.data.setStress == true && event.data.setOxy == false){
            //     $("#stress-circle").fadeIn();
            //     $("#stressLeft").fadeIn();
            //     document.getElementById("stress-circle").style.left = "88px";
            // }else if(event.data.setStress == true){
            //     $("#stress-circle").fadeIn();
            //     $("#stressLeft").fadeIn();
            //     document.getElementById("stress-circle").style.left = "108px";
            // }
            //  console.log("for shit",event.data.setStress,event.data.setOxy)
            // $("#progress-oxygen").addClass("hidden");
            // changeColor($(".progress-health"), event.data.varSetHealth, false)
            // changeColor($(".progress-armor"), event.data.varSetArmor, false)
            // changeColor($(".progress-burger"), event.data.varSetHunger, false)
            // changeColor($(".progress-water"), event.data.varSetThirst, false)
            // changeColor($(".progress-oxygen"), event.data.varSetOxy, false)
            // changeColor($(".progress-stress"), event.data.varSetStress, true)
            // Progress(event.data.varSetHealth, '.progress-health')
            // Progress(event.data.varSetHunger, '.progress-burger')
            // Progress(event.data.varSetThirst, '.progress-water')
            // Progress(event.data.varSetArmor, '.progress-armor')
            // Progress(event.data.varSetOxy, '.progress-oxygen')
            // Progress(event.data.varSetStress, '.progress-stress')

            // if (event.data.varSetHunger <= 15) {
            //     document.getElementById("progress-hungerr").style.fill = "#B82929";
            // } else {
            //     document.getElementById("progress-hungerr").style.fill = "#1c1c20";
            // }

            // if (event.data.varSetThirst <= 15) {
            //     document.getElementById("progress-thirstt").style.fill = "#B82929";
            // } else {
            //     document.getElementById("progress-thirstt").style.fill = "#1c1c20";
            // }

            // if (event.data.varSetHealth == 100) {
            //     Progress(event.data.varSetHealth + 1, '.progress-health')
            // } else {
            //     Progress(event.data.varSetHealth, '.progress-health')
            // }

            // if (event.data.varSetArmor == 100) {
            //     Progress(event.data.varSetArmor + 1, '.progress-armor')
            // } else {
            //     Progress(event.data.varSetArmor, '.progress-armor')
            // }

            // if (event.data.varSetHunger == 100) {
            //     Progress(event.data.varSetHunger + 1, '.progress-burger')
            // } else {
            //     Progress(event.data.varSetHunger, '.progress-burger')
            // }

            // if (event.data.varSetThirst == 100) {
            //     Progress(event.data.varSetThirst + 1, '.progress-water')
            // } else {
            //     Progress(event.data.varSetThirst, '.progress-water')
            // }
        } else if(event.data.type == 'street') {
            var data = event.data;
            if (data.street2 != "" && data.street2 != undefined) {
                loadStreetStat(data.street1, data.current_zone, data.nearestPostal, data.street2);
            } else {
                loadStreetStat(data.street1, data.current_zone, data.nearestPostal);
            }
        } else if(event.data.type == 'UpdateCompass') {
            UpdateCompass(event.data.compass)    
        } else if(event.data.type == 'hideUICauseDead') {
            if(event.data.hidden) {
                $('#statusHud').fadeOut(100);
            } else {
                $('#statusHud').fadeIn(100);
            }
        } else if(event.data.type == "car") {
            if(event.data.mode) {
                $("#carHud").css('visibility', 'visible');
                //$("#carHud").slideDown(800);
                $("#carStatus").fadeIn(1000);
                carModeActive = true;
                currFuel = -1;
                //$(".statback").css({'display': 'block'}, 200);
            } else {
                $("#carHud").css('visibility', 'hidden');
                //$("#carHud").slideUp(800);
                $("#carStatus").fadeOut(250);
                carModeActive = false;
                currFuel = -1;
                //$(".statback").css({'display': 'inline-block'}, 200);
            }
        } else if (event.data.type == "talkingStatus") {
            // if (event.data.is_talking) {
            //     document.getElementById("progress-voicer").style.stroke = "#D5CD31";
            //     document.getElementById("back-voicer").style.fill = "#B9B44E";
            // } else {
            //     document.getElementById("progress-voicer").style.stroke = "";
            //     document.getElementById("back-voicer").style.fill = "";
            // }
        } else if (event.data.type == "transmittingStatus") {
        //     var element = document.getElementById("progress-voicer");
        //     var backelement = document.getElementById("back-voicer");
        //     if (event.data.is_transmitting) {
        //         element.classList.add("transmitting");
        //         backelement.classList.add("transmitting2");
        //     } else {
        //         element.classList.remove("transmitting");
        //         backelement.classList.remove("transmitting2");
        //     }
        } else if (event.data.type = 'UpdateCompass') {
            var data = event.data;
            var amt = (data.heading * 0.1133333333333333);
            if (data.lookside == "left") {
                $(".compass-ui").css({
                    "right": (-30.6 - amt) + "vh"
                });
            } else {
                $(".compass-ui").css({
                    "right": (-30.6 + -amt) + "vh"
                });
            }
        }
    });

    var loadStat = function(stat, percentage, showOnCondition = false, evCb = function() { return true }, hideCompletely = false) {
        // console.log('setting', stat, 'to', percentage);
        percentage = Math.round(percentage);
        var el = $('#' + stat);
        if(hideCompletely || isNaN(percentage)) {
            if(el.css("display") != "none") {
                el.css("display", "none");
            }
            return;
        }
        if(!evCb(percentage) && showOnCondition) {
            if(!el.hasClass('m-fadeOut')) {
                el.addClass('m-fadeOut').delay(300).slideUp(300);
            }
        } else {
            if(el.hasClass('m-fadeOut')) {
            el.removeClass('m-fadeOut').delay(300).slideDown(300);
            }
        }
        $('#' + stat).find('.textstat').html(Math.round(percentage))
        const Horiz = [];
        if(Horiz.indexOf(stat) >= 0) {
            $('#' + stat).find('.bar').css({'height': percentage + '%', 'width':'100%' });
        } else {
            $('#' + stat).find('.bar').css({'width': percentage + '%', 'height':'100%' });
        }
        return;
    }

    var savedStreetStat = {street1: -1, street2: -1, areaZone: -1, nearestPostal: -1};
    var loadStreetStat = function(street1, areaZone, nearestPostal, street2 = false) {
        if(street2 != false && street1 != savedStreetStat.street1 && street2 != savedStreetStat.street2) {
            $('#varStreet > .text-location').html('<span id="streetName">' + street1 + ' &amp; ' + street2 + '</span><span id="areaZone">' + areaZone  + '</span>');
        } else if(street1 != savedStreetStat.street1) {
            $('#varStreet > .text-location').html('<span id="streetName">' + street1 + '</span><span id="areaZone">' + areaZone + '</span>');          
        } else {
            return;
        }
        savedStreetStat = {street1: street1, street2: street2, areaZone: areaZone};
    }

    var UpdateCompass = function(data) {
        var amt = (data.heading * 0.1133333333333333);
        if (data.lookside == "left") {
            $(".compass-ui").css({
                "right": (-30.6 - amt) + "vh"
            });
        } else {
            $(".compass-ui").css({
                "right": (-30.6 + -amt) + "vh"
            });
        }
    }

    var toggleSeatbelt = function(seatbelt) {
        if (seatbelt) {
            $('#seatBelt .on').hide();
            $('#seatBelt .off').show();
        } else {
            $('#seatBelt .off').hide();
            $('#seatBelt .on').show();
        }
    };

    var showEngineWarning = function(engineLevel) {
        var enginePerc = engineLevel/1000*100;
        if(enginePerc < 60 && enginePerc > 30) {
            $("#carHealth #half").show();
        } else if(enginePerc < 20) {
            $("#carHealth #half").hide();
            $("#carHealth #full").show();
        } else {
            $("#carHealth #half").hide();
            $("#carHealth #full").hide();
        }
    }
});