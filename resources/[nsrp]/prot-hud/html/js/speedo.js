
speedoGauge = false;
fuelGauge = false;

$(document).ready(function() {
    function generateSpeedoGauge() {
        var opts = {
            angle: 0.14, // The span of the gauge arc
            lineWidth: 0.02, // The line thickness
            radiusScale: 0.68, // Relative radius
            limitMax: false,     // If false, max value increases automatically if value > maxValue
            limitMin: false,     // If true, the min value of the gauge will be fixed
            colorStart: '#009900',   // Colors
            colorStop: '#8FC0DA',    // just experiment with them
            strokeColor: '#E0E0E0',  // to see which ones work best for you
            generateGradient: true,
            highDpiSupport: true     // High resolution support
        };
        var target = document.getElementById('speedo'); // your canvas element
        var gauge = new Donut(target).setOptions(opts); // create sexy gauge!
        gauge.maxValue = 300; // set max gauge value
        gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
        gauge.animationSpeed = 48; // set animation speed (32 is default value)
        gauge.set(0); // set actual value
        return gauge;
    }
    function generateFuelGauge() {
        var opts = {
            pointer: {
                length: 0.5, // // Relative to gauge radius
                strokeWidth: 0.03, // The thickness
                color: 'rgba(255,255,255,0.9)' // Fill color
            },
            angle: 0.2, // The span of the gauge arc
            lineWidth: 0.05, // The line thickness
            radiusScale: 0.50, // Relative radius
            limitMax: false,     // If false, max value increases automatically if value > maxValue
            limitMin: false,     // If true, the min value of the gauge will be fixed
            colorStart: '#990000',   // Colors
            colorStop: '#8FC0DA',    // just experiment with them
            strokeColor: '#E0E0E0',  // to see which ones work best for you
            generateGradient: true,
            highDpiSupport: true,     // High resolution support
            staticZones: [
                {strokeStyle: "#009900", min: 0, max: 60},
                {strokeStyle: "#999900", min: 60, max: 80},
                {strokeStyle: "#990000", min: 80, max: 100},
            ],
        };
        var target = document.getElementById('fuelo'); // your canvas element
        var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
        gauge.maxValue = 100; // set max gauge value
        gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
        gauge.animationSpeed = 32; // set animation speed (32 is default value)
        gauge.set(100); // set actual value
        return gauge;
    }

    speedoGauge = generateSpeedoGauge();
    fuelGauge = generateFuelGauge();
});

function setSpeedo(speed) {
    speedoGauge.set(speed);
    $("#speedo-value #units").html(speed);
}

function setFuel(fuel) {
    fuel = 100-fuel;
    fuelGauge.set(fuel);
}