carspawns = {
	-- [1] =  { ['x'] = -38.25,['y'] = -1104.18,['z'] = 26.43,['h'] = 14.46, ['inUse'] = false, ['info'] = 'Car Spot 1' },
	-- [2] =  { ['x'] = -36.36,['y'] = -1097.3,['z'] = 26.43,['h'] = 109.4, ['inUse'] = false, ['info'] = 'Car Spot 2' },
	[1] =  { ['x'] = -43.11,['y'] = -1095.02,['z'] = 26.43,['h'] = 67.77, ['inUse'] = false, ['info'] = 'Car Spot 3', ['inUseBy'] = nil },
	[2] =  { ['x'] = -50.45,['y'] = -1092.66,['z'] = 26.43,['h'] = 116.33, ['inUse'] = false, ['info'] = 'Car Spot 4', ['inUseBy'] = nil },
	[3] =  { ['x'] = -56.24,['y'] = -1094.33,['z'] = 26.43,['h'] = 157.08, ['inUse'] = false, ['info'] = 'Car Spot 5', ['inUseBy'] = nil },
	[4] =  { ['x'] = -49.73,['y'] = -1098.63,['z'] = 26.43,['h'] = 240.99, ['inUse'] = false, ['info'] = 'Car Spot 6', ['inUseBy'] = nil },
	[5] =  { ['x'] = -45.58,['y'] = -1101.4,['z'] = 26.43,['h'] = 287.3, ['inUse'] = false, ['info'] = 'Car Spot 7', ['inUseBy'] = nil }
}

RegisterServerEvent('nsrp_vehicleshop:buyCar')
AddEventHandler("nsrp_vehicleshop:buyCar", function(name, model, price)
    local src = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local char = user:getVar("character")
    if (tonumber(user:getBalance()) >= tonumber(price)) then
        user:removeBank(tonumber(price))
        exports.ghmattimysql:execute("INSERT INTO characters_cars (owner, cid, license_plate, name, model, purchase_price, vehicle_state, current_garage, fuel) VALUES ((SELECT owner from characters where id = @cid limit 1), @cid, (select SUBSTRING(UUID(), 1, 8)), @name, @model, @buy_price, 'In', 'Garage: Premium Deluxe Motorsport', 100)",{
            ['@cid'] = char.id,
            ['@name'] = name:upper(),
            ['@model'] = model,
            ['@buy_price'] = price
        }, function()
            TriggerClientEvent("DoLongHudText", src, "Payment Approved. Your car is ready for collection in the back.", 3)
        end)
    else
        TriggerClientEvent("DoLongHudText", src, "Your card was declined", 2)
    end
end)


RegisterServerEvent('nsrp_vehicleshop:openBuyMenu')
AddEventHandler("nsrp_vehicleshop:openBuyMenu", function()
    for i = 1, #carspawns do
        -- carspawns[i]
        if carspawns[i].inUse == false then
            carspawns[i].inUse = true
            carspawns[i].inUseBy = source
            TriggerClientEvent("nsrp_vehicleshop:allowOpenBuyMenu", source, carspawns[i])
            return
        end
    end
    TriggerClientEvent("nsrp_vehicleshop:OpenBuyMenuSlotsFull", source) 
end)


RegisterServerEvent('nsrp_vehicleshop:closeBuyMenu')
AddEventHandler("nsrp_vehicleshop:closeBuyMenu", function()
    for i = 1, #carspawns do
        if carspawns[i].inUse == true then
            if carspawns[i].inUseBy == source then
                carspawns[i].inUse = false
                carspawns[i].inUseBy = nil
                TriggerClientEvent("nsrp_vehicleshop:OpenBuyMenuSlotsEmpty", source) 
            end
        end
    end
end)