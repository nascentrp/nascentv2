vehshop = {
	opened = false,
    defaultMenu = "main",
    previewVehActive = false,
    previewVehUniqueId = 0,
    currVehiclePreview = {
        vehicle = "",
        menu = ""
    },
	title = "Vehicle Shop",
	currentmenu = "main",
	lastmenu = nil,
	currentpos = nil,
	selectedbutton = 0,
	marker = { r = 0, g = 155, b = 255, a = 250, type = 1 },
	menu = {
		x = 0.14,
		y = 0.15,
		width = 0.12,
		height = 0.03,
		buttons = 10,
		from = 1,
		to = 10,
		scale = 0.29,
		font = 0,
		["main"] = {
			title = "CATEGORIES",
			name = "main",
			buttons = {
				{model = "jobvehicles", name = "Job Vehicles", description = ""},
				{model = "vehicles", name = "Vehicles", description = ""},
				{model = "motorcycles", name = "Motorcycles", description = ""},
				{model = "cycles", name = "Cycles", description = ""},
			}
		},
		["vehicles"] = {
			title = "VEHICLES",
			name = "vehicles",
			buttons = {
				--{name = "Job Vehicles", description = ''},
				{model = "compacts", name = "Compacts", description = ''},
				{model = "coupes", name = "Coupes", description = ''},
				{model = "sedans", name = "Sedans", description = ''},
				{model = "super", name = "Super", description = ''},
				{model = "sports", name = "Sports", description = ''},
				{model = "sportsclassics", name = "Sports Classics", description = ''},
				--{model = "Imports", name = "Imports", description = ''},
				{model = "muscle", name = "Muscle", description = ''},
				{model = "offroad", name = "Off-Road", description = ''},
				{model = "suvs", name = "SUVs", description = ''},
				{model = "vans", name = "Vans", description = ''},
			}
		},
		["jobvehicles"] = {
			isPurchase = true,
			title = "Job Vehicles",
			name = "jobvehicles",
			buttons = {
				{name = "Taxi Cab", costs = 2500, description = {}, model = "taxi"},
				-- {name = "Flat Bed", costs = 2500, description = {}, model = "flatbed"},
				-- {name = "News Rumpo", costs = 10000, description = {}, model = "rumpo"},
				{name = "Taco Truck", costs = 2500, description = {}, model = "taco"},
			}
		},
		["super"] = {
            isPurchase = true,
			title = "super",
			name = "super",
			buttons = {			
				{name = "Reaper", costs = 175000, description = {}, model = "reaper"},
			}
		},
		["compacts"] = {
            isPurchase = true,
			title = "compacts",
			name = "compacts",
			buttons = {			
				{name = "Blista", costs = 28000, description = {}, model = "blista"},
				{name = "Issi", costs = 38000, description = {}, model = "issi2"},
				{name = "Brioso R/A", costs = 34000, description = {}, model = "brioso"},
				{name = "Kanjo", costs = 38000, description = {}, model = "kanjo"},
				--{name = "Dilettante", costs = 16000, description = {}, model = "Dilettante"},
				{name = "Issi Classic", costs = 20000, description = {}, model = "issi3"},
				{name = "Panto", costs = 22000, description = {}, model = "panto"},
				{name = "Prairie", costs = 30000, description = {}, model = "prairie"},
				{name = "Rhapsody", costs = 22000, description = {}, model = "rhapsody"},
	
			}
		},
		["coupes"] = {
            isPurchase = true,
			title = "coupes",
			name = "coupes",
			buttons = {
				{name = "Cognoscenti Cabrio", costs = 74000, description = {}, model = "cogcabrio"},
				{name = "Exemplar", costs = 85000, description = {}, model = "exemplar"},
				{name = "F620", costs = 78000, description = {}, model = "f620"},
				{name = "Felon", costs = 74000, description = {}, model = "felon"},
				{name = "Felon GT", costs = 84000, description = {}, model = "felon2"},
				{name = "Jackal", costs = 61000, description = {}, model = "jackal"},
				{name = "Oracle", costs = 40000, description = {}, model = "oracle"},
				{name = "Oracle XS", costs = 45000, description = {}, model = "oracle2"},
				{name = "Sentinel", costs = 50000, description = {}, model = "sentinel"},
				{name = "Sentinel XS", costs = 55000, description = {}, model = "sentinel2"},
				{name = "Windsor", costs = 83000, description = {}, model = "windsor"},
				{name = "Futo", costs = 40000, description = {}, model = "futo"},
				--{name = "Windsor Drop", costs = 34000, description = {}, model = "windsor2"},
				{name = "Zion", costs = 76000, description = {}, model = "zion"},
				{name = "Zion Cabrio", costs = 81000, description = {}, model = "zion2"},
			}
		},
		["sports"] = {
            isPurchase = true,
			title = "sports",
			name = "sports",
			buttons = {
				--{name = "Itali GTO", costs = 160000, description = {}, model = "italigto"},
				{name = "Comet Retro Custom", costs = 115000, description = {}, model = "comet3"},
				{name = "Dinka Jester Classic", costs = 85000, description = {}, model = "jester3"},
				-- {name = "Obey Drafter", costs = 82000, description = {}, model = "drafter"},
				-- {name = "Enus Paragon", costs = 84000, description = {}, model = "paragon"},
				--{name = "Imorgon", costs = 180000, description = {}, model = "imorgon"},
				{name = "Banshee 900R", costs = 100000, description = {}, model = "banshee2"},
				{name = "Revolter", costs = 130000, description = {}, model = "revolter"},
				{name = "Neon", costs = 160000, description = {}, model = "neon"},
				{name = "Schlagen GT", costs = 140000, description = {}, model = "schlagen"},
				--{name = "Sultan Classic", costs = 99000, description = {}, model = "sultan2"},
				{name = "9F", costs = 184000, description = {}, model = "ninef"},
				{name = "9F Cabrio", costs = 189000, description = {}, model = "ninef2"},
				{name = "Alpha", costs = 74000, description = {}, model = "alpha"},
				{name = "Banshee", costs = 235000, description = {}, model = "banshee"},
				{name = "Bestia GTS", costs = 80000, description = {}, model = "bestiagts"},
				--{name = "Buffalo", costs = 29000, description = {}, model = "buffalo"},
				{name = "Carbonizzare", costs = 98000, description = {}, model = "carbonizzare"},
				{name = "Comet", costs = 110000, description = {}, model = "comet2"},
				{name = "Coquette", costs = 204000, description = {}, model = "coquette"},
				{name = "Feltzer", costs = 76000, description = {}, model = "feltzer2"},
				{name = "Flash GT", costs = 96000, description = {}, model = "flashgt"},
				{name = "Furore GT", costs = 114000, description = {}, model = "furoregt"},
				{name = "Fusilade", costs = 52000, description = {}, model = "fusilade"},
				{name = "Jester", costs = 83000, description = {}, model = "jester"},
				{name = "Kuruma", costs = 42000, description = {}, model = "kuruma"},
				{name = "Lynx", costs = 206000, description = {}, model = "lynx"},
				{name = "Massacro", costs = 230000, description = {}, model = "massacro"},
				{name = "Omnis", costs = 44000, description = {}, model = "omnis"},
				{name = "Penumbra", costs = 38000, description = {}, model = "penumbra"},
				{name = "Rapid GT", costs = 87000, description = {}, model = "rapidgt"},
				{name = "Rapid GT Convertible", costs = 91000, description = {}, model = "rapidgt2"},
				{name = "Schafter V12", costs = 68000, description = {}, model = "schafter3"},
				{name = "Sultan", costs = 42000, description = {}, model = "sultan"},
				{name = "SultanRS", costs = 76000, description = {}, model = "sultanrs"},
				{name = "Surano", costs = 82000, description = {}, model = "surano"},
				{name = "Verlierer", costs = 89000, description = {}, model = "verlierer2"},
				{name = "Comet SR", costs = 235000, description = {}, model = "comet5"},
				{name = "Sentinel Classic", costs = 78000, description = {}, model = "sentinel3"},
				--{name = "Streiter", costs = 31000, description = {}, model = "streiter"},
				--{name = "Comet Safari", costs = 42000, description = {}, model = "comet4"},
				{name = "Pariah", costs = 216000, description = {}, model = "pariah"},
				{name = "Elegy", costs = 82000, description = {}, model = "elegy2"},
				{name = "Specter Custom", costs = 136000, description = {}, model = "specter2"},
			}
		},
		["sportsclassics"] = {
            isPurchase = true,
			title = "sports classics",
			name = "sportsclassics",
			buttons = {
				{name = "Cheburek", costs = 40000, description = {}, model = "cheburek"},
				{name = "Casco", costs = 45000, description = {}, model = "casco"},
				-- {name = "Zion Classic", costs = 33000, description = {}, model = "zion3"},
				{name = "Retinue", costs = 48000, description = {}, model = "retinue"},
				-- {name = "Nebula", costs = 130000, description = {}, model = "nebula"},
				-- {name = "Dynasty", costs = 20000, description = {}, model = "dynasty"},
				{name = "Coquette Classic", costs = 91000, description = {}, model = "coquette2"},
				{name = "Coquette BlackFin", costs = 230000, description = {}, model = "coquette3"},
				{name = "JB 700", costs = 190000, description = {}, model = "jb700"},
				{name = "Pigalle", costs = 38000, description = {}, model = "pigalle"},
				{name = "Stinger", costs = 161000, description = {}, model = "stinger"},
				{name = "Stinger GT", costs = 171000, description = {}, model = "stingergt"},
				{name = "Stirling GT", costs = 183000, description = {}, model = "feltzer3"},
				{name = "Tropos", costs = 265000, description = {}, model = "tropos"},
				{name = "Rapid GT Classic", costs = 53000, description = {}, model = "rapidgt3"},
				-- {name = "Retinue MKII", costs = 51000, description = {}, model = "retinue2"},
				-- {name = "Viseris", costs = 46000, description = {}, model = "viseris"}, 
				{name = "190z", costs = 74000, description = {}, model = "z190"},
				{name = "GT500", costs = 58000, description = {}, model = "gt500"},
				{name = "Savestra", costs = 64000, description = {}, model = "savestra"},
				{name = "Elegy Retro Custom", costs = 195000, description = {}, model = "elegy"},
			}
		},
		["muscle"] = {
            isPurchase = true,
			title = "muscle",
			name = "muscle",
			buttons = {
				--{name = "Blade", costs = 81000, description = {}, model = "blade"},
				--{name = "Gauntlet Classic", costs = 120000, description = {}, model = "gauntlet3"},
				--{name = "Gauntlet Hellfire", costs = 130000, description = {}, model = "gauntlet4"},
				--{name = "Yosmite Slamvan", costs = 75000, description = {}, model = "yosemite2"},
				{name = "Buccaneer", costs = 38000, description = {}, model = "buccaneer"},
				{name = "Buccaneer Lowrider", costs = 52000, description = {}, model = "buccaneer2"},
				--{name = "Chino", costs = 26000, description = {}, model = "chino"},
				{name = "Chino Lowrider", costs = 32000, description = {}, model = "chino2"},
				{name = "Impaler", costs = 58000, description = {}, model = "impaler"},
				{name = "Dominator", costs = 197000, description = {}, model = "dominator"},
				{name = "Buffalo S", costs = 45000, description = {}, model = "buffalo2"},
				{name = "Dukes", costs = 175000, description = {}, model = "dukes"},
				{name = "Gauntlet", costs = 185000, description = {}, model = "gauntlet"},
				{name = "Faction", costs = 41000, description = {}, model = "faction"},
				{name = "Faction Lowrider", costs = 51000, description = {}, model = "faction2"},
				{name = "Faction Lowrider DONK", costs = 55000, description = {}, model = "faction3"},
				{name = "Moonbeam Lowrider", costs = 38000, description = {}, model = "moonbeam2"},
				{name = "Minivan Lowrider", costs = 34000, description = {}, model = "minivan2"},
				--{name = "Picador", costs = 41000, description = {}, model = "picador"},
				{name = "Sabre Turbo", costs = 164000, description = {}, model = "sabregt"},
				{name = "SabreGT Lowrider", costs = 88000, description = {}, model = "sabregt2"},
				{name = "Slamvan Lowrider", costs = 46000, description = {}, model = "slamvan3"},
				{name = "Drift Tampa", costs = 96000, description = {}, model = "tampa2"},
				--{name = "Tornado Lowrider", costs = 25000, description = {}, model = "tornado5"},
				--{name = "Virgo", costs = 42000, description = {}, model = "virgo"},
				--{name = "Vigero", costs = 42000, description = {}, model = "vigero"},
				{name = "Ellie", costs = 36000, description = {}, model = "ellie"},
				{name = "Phoenix", costs = 48000, description = {}, model = "phoenix"},
				{name = "Primo Lowrider", costs = 37000, description = {}, model = "primo2"},
				{name = "Slam Van", costs = 42000, description = {}, model = "slamvan"},
				-- {name = "Yosemite", costs = 84000, description = {}, model = "yosemite"},
				{name = "Tulip", costs = 39000, description = {}, model = "tulip"},
				{name = "Vamos", costs = 44000, description = {}, model = "vamos"},
				{name = "Virgo Lowrider", costs = 48000, description = {}, model = "virgo2"},
				{name = "Voodoo Lowrider", costs = 28000, description = {}, model = "voodoo"},
			}
		},
		["offroad"] = {
            isPurchase = true,
			title = "off-road",
			name = "offroad",
			buttons = {
				{name = "Lifted Mesa", costs = 40000, description = {}, model = "mesa3"},
				{name = "Bifta", costs = 31000, description = {}, model = "bifta"},
				-- {name = "Outlaw", costs = 40000, description = {}, model = "outlaw"},
				{name = "Caracara", costs = 78000, description = {}, model = "caracara2"},
				-- {name = "Vagrant", costs = 41000, description = {}, model = "vagrant"},
				{name = "Guardian", costs = 32000, description = {}, model = "guardian"},
				{name = "GB200", costs = 48000, description = {}, model = "gb200"},
				{name = "Blazer Sport", costs = 12000, description = {}, model = "blazer4"},
				-- {name = "Everon", costs = 43000, description = {}, model = "everon"},
				{name = "Blazer", costs = 7000, description = {}, model = "blazer"},
				{name = "Brawler", costs = 84000, description = {}, model = "brawler"},
				{name = "Dune Buggy", costs = 20000, description = {}, model = "dune"},
				{name = "Rebel", costs = 21000, description = {}, model = "rebel2"},
				{name = "Sandking", costs = 26000, description = {}, model = "sandking"},
				{name = "Kamacho", costs = 31000, description = {}, model = "kamacho"},
				{name = "Dubsta 6x6", costs = 52000, description = {}, model = "dubsta3"},
				{name = "Vapid Trophy Truck", costs = 44000, description = {}, model = "trophytruck"},
				{name = "Vapid Desert Raid", costs = 55000, description = {}, model = "trophytruck2"},
			}
		},
		["suvs"] = {
            isPurchase = true,
			title = "suvs",
			name = "suvs",
			buttons = {
				{name = "Baller", costs = 40000, description = {}, model = "baller"},
				{name = "Baller 2", costs = 50000, description = {}, model = "baller2"},
				{name = "Baller LE", costs = 54000, description = {}, model = "baller3"},
				-- {name = "Baller LE LWB", costs = 45000, description = {}, model = "baller4"},
				{name = "Cavalcade", costs = 54000, description = {}, model = "cavalcade2"},
				{name = "Granger", costs = 34000, description = {}, model = "granger"},
				{name = "Novak", costs = 48000, description = {}, model = "novak"},
				{name = "Rebla", costs = 44000, description = {}, model = "rebla"},
				{name = "Huntley S", costs = 60000, description = {}, model = "huntley"},
				{name = "Landstalker", costs = 32000, description = {}, model = "landstalker"},
				{name = "Radius", costs = 32000, description = {}, model = "radi"},
				{name = "Rocoto", costs = 42000, description = {}, model = "rocoto"},
				{name = "Seminole", costs = 32000, description = {}, model = "seminole"},
				{name = "XLS", costs = 42000, description = {}, model = "xls"},
				{name = "Dubsta", costs = 34000, description = {}, model = "dubsta"},
				{name = "Patriot", costs = 36000, description = {}, model = "patriot"},
				{name = "Gresley", costs = 30000, description = {}, model = "gresley"},
				{name = "Toros", costs = 70000, description = {}, model = "toros"},
			}
		},
		["vans"] = {
            isPurchase = true,
			title = "vans",
			name = "vans",
			buttons = {
				{name = "Bison", costs = 22000, description = {}, model = "bison"},
				{name = "Bobcat XL", costs = 25000, description = {}, model = "bobcatxl"},
				{name = "The Lost Burrito", costs = 15000, description = {}, model = "gburrito"},
				{name = "SOA Burrito", costs = 15000, description = {}, model = "soaburrito"},
				{name = "Journey", costs = 15000, description = {}, model = "journey"},
				{name = "Minivan", costs = 20000, description = {}, model = "minivan"},
				{name = "Paradise", costs = 18000, description = {}, model = "paradise"},
				{name = "Surfer", costs = 15000, description = {}, model = "surfer"},
				{name = "Youga", costs = 24000, description = {}, model = "youga"},
				{name = "Moonbeam", costs = 20000, description = {}, model = "moonbeam"},
				{name = "Quantum", costs = 20000, description = {}, model = "quantum"},
				{name = "Camper", costs = 20000, description = {}, model = "camper"},
			}
		},
		["sedans"] = {
            isPurchase = true,
			title = "sedans",
			name = "sedans",
			buttons = {
				{name = "Emperor", costs = 15000, description = {}, model = "emperor2"},
				--{name = "Ocelot Jugular", costs = 88000, description = {}, model = "jugular"},
				{name = "Komoda", costs = 96000, description = {}, model = "komoda"},
				{name = "Sugoi", costs = 68000, description = {}, model = "sugoi"},
				{name = "VSTR", costs = 106000, description = {}, model = "vstr"},
				{name = "Stafford", costs = 86000, description = {}, model = "stafford"},
				{name = "Tornado", costs = 18000, description = {}, model = "tornado3"},
				--{name = "Bodhi", costs = 16000, description = {}, model = "bodhi2"},		
				{name = "Asea", costs = 20000, description = {}, model = "asea"},
				{name = "Asterope", costs = 26000, description = {}, model = "asterope"},
				{name = "Fugitive", costs = 30000, description = {}, model = "fugitive"},
				{name = "Glendale", costs = 24000, description = {}, model = "glendale"},
				{name = "Intruder", costs = 35000, description = {}, model = "intruder"},
				{name = "Premier", costs = 32000, description = {}, model = "premier"},
				{name = "Regina", costs = 15000, description = {}, model = "regina"},
				{name = "Schafter", costs = 38000, description = {}, model = "schafter2"},
				{name = "Stanier", costs = 32000, description = {}, model = "stanier"},
				{name = "Stratum", costs = 30000, description = {}, model = "stratum"},
				{name = "Super Diamond", costs = 54000, description = {}, model = "superd"},
				{name = "Warrener", costs = 28000, description = {}, model = "warrener"},
				{name = "Washington", costs = 26000, description = {}, model = "washington"},
				{name = "Tailgater", costs = 35000, description = {}, model = "tailgater"},
				{name = "Cognoscenti", costs = 40000, description = {}, model = "cognoscenti"},
			}
		},
		["motorcycles"] = {
            isPurchase = true,
			title = "MOTORCYCLES",
			name = "motorcycles",
			buttons = {
				{name = "Akuma", costs = 72000, description = {}, model = "AKUMA"}, 
				{name = "Bagger", costs = 51000, description = {}, model = "bagger"},
				{name = "Bati 801", costs = 68000, description = {}, model = "bati"},
				{name = "BF400", costs = 60000, description = {}, model = "bf400"},
				{name = "Carbon RS", costs = 66000, description = {}, model = "carbonrs"},
				{name = "Daemon", costs = 58000, description = {}, model = "daemon"},
				{name = "Enduro", costs = 50000, description = {}, model = "enduro"},
				{name = "Faggio", costs = 5000, description = {}, model = "faggio"},
				{name = "Gargoyle", costs = 66000, description = {}, model = "gargoyle"},
				{name = "Hexer", costs = 55000, description = {}, model = "hexer"},
				{name = "Innovation", costs = 65000, description = {}, model = "innovation"},
				{name = "Nemesis", costs = 58000, description = {}, model = "nemesis"},
				{name = "PCJ-600", costs = 48000, description = {}, model = "pcj"},
				{name = "Ruffian", costs = 66000, description = {}, model = "ruffian"},
				{name = "Sanchez", costs = 34000, description = {}, model = "sanchez"},
				{name = "Sovereign", costs = 64000, description = {}, model = "sovereign"},
				{name = "Zombiea", costs = 64000, description = {}, model = "zombiea"},
				{name = "Vespa", costs = 5000, description = {}, model = "faggio2"},
				{name = "Manchez", costs = 44000, description = {}, model = "manchez"},
				{name = "Vortex", costs = 64000, description = {}, model = "vortex"},
				{name = "Avarus", costs = 56000, description = {}, model = "avarus"},
				{name = "Vader", costs = 55000, description = {}, model = "vader"},
				{name = "Esskey", costs = 58000, description = {}, model = "esskey"},
				{name = "Defiler", costs = 70000, description = {}, model = "defiler"},
				{name = "Chimera", costs = 38000, description = {}, model = "chimera"},
				{name = "DaemonHigh", costs = 66000, description = {}, model = "daemon2"},
			}
		},
		["cycles"] = {
            isPurchase = true,
			title = "cycles",
			name = "cycles",
			buttons = {
				{name = "BMX", costs = 550, description = {}, model = "bmx"},
				--{name = "Unicycle", costs = 75, description = {}, model = "unicycle"},
				{name = "Cruiser", costs = 240, description = {}, model = "cruiser"},
				{name = "Fixter", costs = 270, description = {}, model = "fixter"},
				{name = "Scorcher", costs = 300, description = {}, model = "scorcher"},
				{name = "Pro 1", costs = 2500, description = {}, model = "tribike"},
				{name = "Pro 2", costs = 2600, description = {}, model = "tribike2"},
				{name = "Pro 3", costs = 2900, description = {}, model = "tribike3"},
			}
		},
	}
}

local carspawns = {}

-- Citizen.CreateThread(function()
-- 	for i = 1, #carspawns do
-- 		local x, y, z, h = carspawns[i].x, carspawns[i].y, carspawns[i].z, carspawns[i].h
-- 		exports["nsrp_util"]:spawnLocalVehicle(
--             'granger',
--             x, y, z,
--             function()
--             end,
--             h
--         )
-- 	end
-- end)

function showListFormArrayItem(item)
    local menuObj = vehshop.menu[item]
    local menu = {
        {
            id = 1,
            header = menuObj.title,
            txt = ""
        }
    }
    local isPurchase = false
    if menuObj.isPurchase ~= nil then
        isPurchase = menuObj.isPurchase
    end
    for i = 1, #menuObj.buttons do
        local button = menuObj.buttons[i]
        local txt = ""
        if button.costs ~= nil then
            txt = "R" .. button.costs
        end
        
        table.insert(menu, {
            id = i+1,
            header =  button.name,
            txt = txt,
            params = {
                event = "nsrp_vehicleshop:doMenuAction",
                args = {
                    currMenu = menuObj.name,
                    actionItem = button.model,
                    isPurchase = isPurchase
                },
            }
        })
    end
    
    TriggerEvent('ethical-context:sendMenu', menu)
end


function deletePreviewVehicle() 
    if vehshop.previewVehActive == true then
        while DoesEntityExist(vehshop.previewVehUniqueId) do
            DeleteVehicle(vehshop.previewVehUniqueId)
            Wait(100)
        end
        vehshop.previewVehUniqueId = 0
    end
end

allocatedSpot = nil
areAllSpotsFull = false

function allocateSpot() 
	allocatedSpot = nil
	TriggerServerEvent("nsrp_vehicleshop:openBuyMenu")
	while allocatedSpot == nil and not areAllSpotsFull do
		Wait(1)
	end
	if areAllSpotsFull then
		TriggerEvent("DoLongHudText", "There are no car dealers available to assist you right now.", 2)
		return false
	end
	return allocatedSpot
end
RegisterNetEvent('nsrp_vehicleshop:OpenBuyMenuSlotsFull')
AddEventHandler('nsrp_vehicleshop:OpenBuyMenuSlotsFull', function()
	areAllSpotsFull = true
end)

RegisterNetEvent('nsrp_vehicleshop:OpenBuyMenuSlotsEmpty')
AddEventHandler('nsrp_vehicleshop:OpenBuyMenuSlotsEmpty', function()
	areAllSpotsFull = false
end)

RegisterNetEvent('nsrp_vehicleshop:allowOpenBuyMenu')
AddEventHandler('nsrp_vehicleshop:allowOpenBuyMenu', function(carspawn)
	allocatedSpot = carspawn
end)

RegisterNetEvent('nsrp_vehicleshop:doMenuAction')
AddEventHandler('nsrp_vehicleshop:doMenuAction', function(data)
    if data.currMenu ~= nil then
        vehshop.lastmenu = data.currMenu
    end
    if data.isFromCancel ~= nil then
        deletePreviewVehicle()
    end
    if data.isPurchase == true then
        vehshop.currVehiclePreview = {
            vehicle = data.actionItem,
            menu = data.currMenu
        }
		print("spawn area check", allocatedSpot)
		local spawnArea = nil
		if allocatedSpot ~= nil then
			spawnArea = allocatedSpot
	        --local x, y, z, h = -60.134296417236, -1116.9968261719, 26.434827804565, 3.5816886425018
		else
			spawnArea = allocateSpot()
			if spawnArea == false then
				allocatedSpot = nil
				return
			end
		end
		print("allocated")
		print(json.encode(spawnArena))
		local x, y, z, h = spawnArea.x, spawnArea.y, spawnArea.z, spawnArea.h

        exports["nsrp_util"]:spawnLocalVehicle(
            vehshop.currVehiclePreview.vehicle,
            x, y, z,
            function(veh)
                TaskWarpPedIntoVehicle(PlayerPedId(), veh, -1)
                openBuyMenu(vehshop.currVehiclePreview)
                vehshop.previewVehActive = true
                vehshop.previewVehUniqueId = veh
            end,
            h
        )
        -- -60.134296417236, -1116.9968261719, 26.434827804565 Heading: 3.5816886425018
        -- previewCar()
        -- spawn car for purchase
    elseif data.actionItem ~= nil then
        vehshop.currentmenu = data.actionItem
        showListFormArrayItem(data.actionItem)
    end
end)


local vehicleShopVehiclePreviewIsActive = false
function openBuyMenu(data)
	print(json.encode(data))
	FreezeEntityPosition(PlayerPedId(),true)
	vehicleShopVehiclePreviewIsActive = true
    local vehicle = data.vehicle
    local menuLocation = data.menu

    local vehicleMenuArray = vehshop.menu[menuLocation].buttons
    local idx = 0
    local foundVehicle = false
    while not foundVehicle do
        idx = idx+1
        if vehicle == vehicleMenuArray[idx].model then
            foundVehicle = true
        end
    end
    local vehicleObject = vehicleMenuArray[idx]
    local menu = {
        {
            id = 1,
            header = vehicleObject.name,
            txt = "R" .. vehicleObject.costs
        },
        {
            id = 2,
            header = "Buy",
            txt = "Make this car all yours",
            params = {
                event = "nsrp_vehicleshop:doActionBuyPreview",
                args = vehicleObject
            }
        },
        {
            id = 3,
            header = "Cancel",
            txt = "You probably wouldn't like it anyway",
            params = {
                event = "nsrp_vehicleshop:doMenuAction",
                args = {
                    currMenu = "main",
                    actionItem = menuLocation,
                    isPurchase = isPurchase,
                    isFromCancel = true
                },
            }
        }
    }
    TriggerEvent('ethical-context:sendMenu', menu)
end


RegisterNetEvent('nsrp_vehicleshop:doActionBuyPreview')
AddEventHandler('nsrp_vehicleshop:doActionBuyPreview', function(obj)
	print(json.encode(obj))
    deletePreviewVehicle()
    TriggerServerEvent("nsrp_vehicleshop:buyCar", obj.name, obj.model, obj.costs)
	FreezeEntityPosition(PlayerPedId(),false)
end)

RegisterNetEvent('nsrp_vehicleshop:openShop')
AddEventHandler('nsrp_vehicleshop:openShop', function()
    vehshop.opened = true
    vehshop.currentmenu = "main"
	TriggerServerEvent("nsrp_vehicleshop:openBuyMenu")
    showListFormArrayItem(vehshop.currentmenu)
end)

RegisterNetEvent('nsrp_vehicleshop:closeShop')
AddEventHandler('nsrp_vehicleshop:closeShop', function()
    vehshop.opened = false
    vehshop.currentmenu = vehshop.defaultMenu
    vehshop.lastmenu = nil
    TriggerEvent('ethical-context:closeActiveMenu')
end)

RegisterNetEvent("ethical-context:closeActiveMenu")
AddEventHandler("ethical-context:closeActiveMenu", function()
    if vehshop.opened == true then
		local isPermaClose = true
        if vehshop.currentmenu ~= vehshop.defaultMenu and vehshop.lastmenu ~= nil then
            TriggerEvent("nsrp_vehicleshop:doMenuAction", {
                currMenu = "main",
                actionItem = vehshop.lastmenu
            })
			isPermaClose = false
        end
		if isPermaClose then
			TriggerServerEvent("nsrp_vehicleshop:closeBuyMenu")
		end
		if vehicleShopVehiclePreviewIsActive then
			deletePreviewVehicle()
			--carspawns[allocatedSpot].inUse = false
		end
		FreezeEntityPosition(PlayerPedId(),false)
    end
end)


-- RegisterCommand("testvehicle", function()
-- 	local vehicle = GetVehiclePedIsIn(PlayerPedId())
--     ToggleVehicleMod(vehicle,  18, true)
-- end)