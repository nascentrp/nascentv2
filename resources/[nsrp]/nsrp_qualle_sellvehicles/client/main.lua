PlayerData = {}
veh = nil


Citizen.CreateThread(function() 
	LoadSellPlace()
end)

RegisterNetEvent("esx-qalle-sellvehicles:refreshVehicles")
AddEventHandler("esx-qalle-sellvehicles:refreshVehicles", function()
	RemoveVehicles()

	Citizen.Wait(500)

	SpawnVehicles()
end)

function LoadSellPlace()
	local waitTime = PlayerId()*1000
	if(PlayerId() > 25) then
		waitTime = 2500 + (PlayerId()*10)
	end
	Wait(waitTime)
	Citizen.CreateThread(function()

		local carsAreLoaded = false
		local SellPos = Config.SellPosition

		local Blip = AddBlipForCoord(SellPos["x"], SellPos["y"], SellPos["z"])
		SetBlipSprite (Blip, 147)
		SetBlipDisplay(Blip, 4)
		SetBlipScale  (Blip, 0.8)
		SetBlipColour (Blip, 52)
		SetBlipAsShortRange(Blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Used Cars")
		EndTextCommandSetBlipName(Blip)

		while true do
			local sleepThread = 500

			local ped = PlayerPedId()
			local pedCoords = GetEntityCoords(ped)

			local dstCheck = GetDistanceBetweenCoords(pedCoords, SellPos["x"], SellPos["y"], SellPos["z"], true)

			if dstCheck <= 80 then
				if not carsAreLoaded then
					RemoveVehicles()
					Citizen.Wait(500)
					SpawnVehicles()
					carsAreLoaded = true
				end
			elseif carsAreLoaded == true then
				RemoveVehicles()
				carsAreLoaded = false
			end

			if dstCheck <= 10.0 then
				sleepThread = 5

				if dstCheck <= 4.2 then
					DrawText3Ds(SellPos, "[E] Open Menu", 0.4)
					if IsControlJustPressed(0, 38) then
						if IsPedInAnyVehicle(ped, false) then
							OpenSellMenu(GetVehiclePedIsUsing(ped))
						else
							TriggerEvent("DoLongHudText","You need to sit in a vehicle.",2)
						end
					end
				end
			end
			for i = 1, #Config.VehiclePositions, 1 do
				if Config.VehiclePositions[i]["entityId"] ~= nil then
					local pedCoords = GetEntityCoords(ped)
					local vehCoords = GetEntityCoords(Config.VehiclePositions[i]["entityId"])

					local dstCheck = GetDistanceBetweenCoords(pedCoords, vehCoords, true)

					if dstCheck <= 2.0 then
						sleepThread = 5

						DrawText3Ds(vehCoords, "[E] R" .. Config.VehiclePositions[i]["price"] .. "", 0.4)
						
						if IsControlJustPressed(0, 38) then
							if IsPedInVehicle(ped, Config.VehiclePositions[i]["entityId"], false) then
								OpenSellMenu(Config.VehiclePositions[i]["entityId"], Config.VehiclePositions[i]["price"], true, Config.VehiclePositions[i]["owner"])
							else
								TriggerEvent("DoLongHudText","You need to sit in a vehicle.",2)
							end
						end
					end
				end
			end

			Citizen.Wait(sleepThread)
		end
	end)
end

function DrawText3Ds(r, text)
	local x, y, z = r[1], r[2], r[3]
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    SetTextScale(0.35, 0.35)
    SetTextFont(4)
    SetTextProportional(1)
    SetTextColour(255, 255, 255, 215)
    SetTextEntry("STRING")
    SetTextCentre(1)
    AddTextComponentString(text)
    DrawText(_x,_y)
end

function OpenSellMenu(veh, price, buyVehicle, owner)
	local cid = exports["isPed"]:isPed("cid")
	local myCid = tonumber(cid)
	local carOwner = tonumber(owner)
	local elements = {}
	local count = 1
	if not buyVehicle then
		if price ~= nil then
			count = count + 1
			table.insert(elements, { 
				 id = count,
				 header = "Change Price",
				 txt = "Current Price: R"..price,
				 params = {
					event = "sellvehicles:menuResponse",
					args = {
						value = "price",
						veh = veh
					}
				}
			})
			count = count + 1
			table.insert(elements, { 
				 id = count,
				 header = "Confirm",
				 txt = "Accept and put up for sale",
				 params = {
					event = "sellvehicles:menuResponse",
					args = {
						value = "sell",
						veh = veh,
						price = price
					}
				}
			})
		else
			count = count + 1
			table.insert(elements, { 
				id = count,
				header = "Set Price for Current Vehicle",
				txt = "",
				params = {
				   event = "sellvehicles:menuResponse",
				   args = {
					   value = "price",
					   veh = veh
				   }
			   }
		   })
		end
	else
		if carOwner == myCid then
			count = count + 1
			table.insert(elements, { 
				id = count,
				header = "Remove From Dealership",
				txt = "",
				params = {
				event = "sellvehicles:menuResponse",
					args = {
						value = "remove",
						veh = veh
					}
				}
			})
		else
			count = count + 1
			table.insert(elements, { 
				id = count,
				header = "Buy for R" .. price .. "",
				txt = "",
				params = {
				event = "sellvehicles:menuResponse",
				args = {
					value = "buy",
					veh = veh,
					price = price
				}
			}
			})
		end
	end
	TriggerEvent('ethical-context:sendMenu', elements)
end
	
RegisterNetEvent("sellvehicles:menuResponse")
AddEventHandler("sellvehicles:menuResponse", function(args)
	local action = args.value
	veh = args.veh
	if action == "price" then
		local setPrice = exports["ethical-applications"]:KeyboardInput({
			header = "Set Vehicle Price",
			rows = {
				{
					id = 1,
					txt = "Vehicle Price"
				}
			}
		})
		if setPrice then
			local vehPrice = tonumber(setPrice[1].input)
			OpenSellMenu(veh, vehPrice)				
		end
	elseif action == "sell" then
		local numberPlate = GetVehicleNumberPlateText(GetVehiclePedIsIn(PlayerPedId()))
		TriggerServerEvent("esx-qalle-sellvehicles:isVehicleValid", "esx-qalle-sellvehicles:isVehicleValid:response", args.price, numberPlate)
	elseif action == "buy" then
		local numberPlate = GetVehicleNumberPlateText(GetVehiclePedIsIn(PlayerPedId()))
		TriggerServerEvent("esx-qalle-sellvehicles:buyVehicle", "esx-qalle-sellvehicles:buyVehicle:response", numberPlate, args.price)
	elseif action == "remove" then
		local numberPlate = GetVehicleNumberPlateText(GetVehiclePedIsIn(PlayerPedId()))
		TriggerServerEvent("esx-qalle-sellvehicles:buyVehicle:remove", numberPlate)
	end
end)

RegisterNetEvent("esx-qalle-sellvehicles:buyVehicle:remove:response")
AddEventHandler("esx-qalle-sellvehicles:buyVehicle:remove:response", function(isPurchasable)
	if(isPurchasable == true) then
		DeleteVehicle(GetVehiclePedIsIn(PlayerPedId()))
		TriggerEvent("DoLongHudText", "You delisted your vehicle")
	else
		TriggerEvent("DoLongHudText", "This is not your car")
	end
end)

RegisterNetEvent("esx-qalle-sellvehicles:buyVehicle:response")
AddEventHandler("esx-qalle-sellvehicles:buyVehicle:response", function(isPurchasable, price)
	if isPurchasable then
		DeleteVehicle(veh)
		TriggerEvent("DoLongHudText","You bought the vehicle for R" .. price)
	else
		TriggerEvent("DoLongHudText","You don't have enough cash")
	end
end)

RegisterNetEvent("esx-qalle-sellvehicles:isVehicleValid:response")
AddEventHandler("esx-qalle-sellvehicles:isVehicleValid:response", function(valid)
	if valid then
		DeleteVehicle(veh)
		TriggerEvent("DoLongHudText","You put out the vehicle for sale")
	else
		TriggerEvent("DoLongHudText","You do not appear to be the owner of this vehicle (Or the dealership is full).")
	end
end)

RegisterNetEvent("esx-qalle-sellvehicles:retrieveVehicles:response")
AddEventHandler("esx-qalle-sellvehicles:retrieveVehicles:response", function(vehicles)
	local VehPos = Config.VehiclePositions
	for i = 1, #vehicles do
		local vehs = GetClosestVehicle(VehPos[i]["x"], VehPos[i]["y"], VehPos[i]["z"], 5.0, 0, 70)
		local distance = GetDistanceBetweenCoords(GetEntityCoords(vehs), vector3(VehPos[i]["x"], VehPos[i]["y"], VehPos[i]["z"]))
		if distance > 1.0 then
			exports['nsrp_util']:spawnLocalVehicle(vehicles[i]["model"], VehPos[i]["x"], VehPos[i]["y"], VehPos[i]["z"] - 0.975,
					function(veh1) -- This is a big nest
						local custom = vehicles[i]["data"]
						DecorSetBool(veh1, "PlayerVehicle", true)
						SetVehicleOnGroundProperly(veh1)
						SetEntityInvincible(veh1, true)

						SetVehicleModKit(veh1, 0)

						SetVehicleNumberPlateText(veh1, vehicles[i]["license_plate"])

						if custom then
							local customized = json.decode(custom)
							SetVehicleWheelType(veh1, customized.wheeltype)
							SetVehicleNumberPlateTextIndex(veh1, 3)

							for i = 1, 16 do
								SetVehicleMod(veh1, i, customized.mods[tostring(i)])
							end

							for i = 17, 22 do
								ToggleVehicleMod(veh1, i, customized.mods[tostring(i)])
							end

							for i = 23, 24 do
								local isCustom = customized.mods[tostring(i)]
								if (isCustom == nil or isCustom == "-1" or isCustom == false or isCustom == 0) then
									isSet = false
								else
									isSet = true
								end
								SetVehicleMod(veh1, i, customized.mods[tostring(i)], isCustom)
							end

							for i = 23, 48 do
								SetVehicleMod(veh1, i, customized.mods[tostring(i)])
							end

							for i = 0, 3 do
								SetVehicleNeonLightEnabled(veh1, i, customized.neon[tostring(i)])
							end

							if customized.extras ~= nil then
								for i = 1, 12 do
									local onoff = tonumber(customized.extras[i])
									if onoff == 1 then
										SetVehicleExtra(veh1, i, 0)
									else
										SetVehicleExtra(veh1, i, 1)
									end
								end
							end

							if customized.oldLiveries ~= nil and customized.oldLiveries ~= 24  then
								SetVehicleLivery(veh1, customized.oldLiveries)
							end

							if customized.plateIndex ~= nil and customized.plateIndex ~= 4 then
								SetVehicleNumberPlateTextIndex(veh1, customized.plateIndex)
							end

							-- Xenon Colors
							SetVehicleXenonLightsColour(veh1, (customized.xenonColor or -1))
							SetVehicleColours(veh1, customized.colors[1], customized.colors[2])
							SetVehicleExtraColours(veh1, customized.extracolors[1], customized.extracolors[2])
							SetVehicleNeonLightsColour(veh1, customized.lights[1], customized.lights[2], customized.lights[3])
							SetVehicleTyreSmokeColor(veh1, customized.smokecolor[1], customized.smokecolor[2], customized.smokecolor[3])
							SetVehicleWindowTint(veh1, customized.tint)
							SetVehicleInteriorColour(veh1, customized.dashColour)
							SetVehicleDashboardColour(veh1, customized.interColour)
						else

							SetVehicleColours(veh1, 0, 0)
							SetVehicleExtraColours(veh1, 0, 0)

						end

						TriggerEvent("keys:addNew",veh1,plate)
						SetVehicleHasBeenOwnedByPlayer(veh1,true)
						
						local id = NetworkGetNetworkIdFromEntity(veh1)
						SetNetworkIdCanMigrate(id, true)
						
						TriggerServerEvent('garages:SetVehOut', veh1, plate)

						TriggerServerEvent('veh.getVehicles', plate, veh1)
						
						if GetEntityModel(veh1) == `rumpo4` then
							SetVehicleLivery(veh1,0)
						end
						
						if GetEntityModel(veh1) == `rumpo` then
							SetVehicleLivery(veh1,0)
						end

						if GetEntityModel(veh1) == `taxi` then
							SetVehicleExtra(veh1, 8, 1)
							SetVehicleExtra(veh1, 9, 1)
							SetVehicleExtra(veh1, 6, 0)
						end

					--	SetEntityAsMissionEntity(veh,false,true)
					--	TriggerEvent("chop:plateoff",plate)
					VehPos[i]["entityId"] = veh1

					FreezeEntityPosition(VehPos[i]["entityId"], true)
					SetEntityAsMissionEntity(VehPos[i]["entityId"], true, true)
					SetModelAsNoLongerNeeded(vehicles[i]["model"])
				end,
			VehPos[i]["h"])
			

			VehPos[i]["price"] = vehicles[i]["price"]
			VehPos[i]["owner"] = vehicles[i]["owner"]
		end
	end
end)

function RemoveVehicles()
	-- local VehPos = Config.VehiclePositions

	-- for i = 1, #VehPos, 1 do
	-- 	local vehs = GetClosestVehicle(VehPos[i]["x"], VehPos[i]["y"], VehPos[i]["z"], 5.0, 0, 70)
	-- 	local distance = GetDistanceBetweenCoords(GetEntityCoords(vehs), vector3(VehPos[i]["x"], VehPos[i]["y"], VehPos[i]["z"]))
	-- 	if DoesEntityExist(vehs) and distance <= 1.0 then
	-- 		DeleteEntity(vehs)
	-- 	end
	-- end
end

function SpawnVehicles()
	TriggerServerEvent("esx-qalle-sellvehicles:retrieveVehicles", "esx-qalle-sellvehicles:retrieveVehicles:response")
end

LoadModel = function(model)
	while not HasModelLoaded(model) do
		RequestModel(model)

		Citizen.Wait(1)
	end
end
