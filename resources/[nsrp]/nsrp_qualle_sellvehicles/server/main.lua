local VehiclesForSale = 0

RegisterServerEvent('esx-qalle-sellvehicles:buyVehicle:remove')
AddEventHandler("esx-qalle-sellvehicles:buyVehicle:remove", function(plate)
	local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    local plate = plate:lower()
	local price = price
	local car = exports.ghmattimysql:executeSync("select cid from characters_cars where license_plate = trim(@license_plate)",
		{
			["@license_plate"] = plate
		}
	)
	if #car > 0 and car[1].cid == character.id then
		exports.ghmattimysql:executeSync("update characters_cars set vehicle_state = 'In' and for_sale = false, for_sale_price = null where license_plate = trim(@license_plate) and cid = @cid",
			{
				["@cid"] = character.id,
				["@license_plate"] = plate
			}
		)

		TriggerClientEvent("esx-qalle-sellvehicles:refreshVehicles", src)
		TriggerClientEvent("esx-qalle-sellvehicles:buyVehicle:remove:response", src, true)
	else
		TriggerClientEvent("esx-qalle-sellvehicles:buyVehicle:remove:response", src, false)
	end
end)


RegisterServerEvent('nsrp_sellvehicles:carsAreLoaded')
AddEventHandler("nsrp_sellvehicles:carsAreLoaded", function()
	TriggerClientEvent("nsrp_sellvehicles:carsAreLoaded", -1)
end)

RegisterServerEvent('esx-qalle-sellvehicles:retrieveVehicles')
AddEventHandler("esx-qalle-sellvehicles:retrieveVehicles", function(cb)
	local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    
    exports.ghmattimysql:execute("SELECT for_sale_price as price, cid, license_plate, data, model FROM characters_cars WHERE for_sale is true ORDER BY for_sale_posted_date asc, model desc", {}, function(result)
        local vehicleTable = {}

        VehiclesForSale = 0

        if result[1] ~= nil then
            for i = 1, #result, 1 do
                VehiclesForSale = VehiclesForSale + 1
                table.insert(vehicleTable, { ["model"] = result[i]["model"], ["data"] = result[i]["data"], ["price"] = result[i]["price"], ["license_plate"] = result[i]["license_plate"], ["owner"] = result[i]["cid"] })
            end
        end

		TriggerClientEvent(cb, src, vehicleTable)
    end)
end)

RegisterServerEvent('esx-qalle-sellvehicles:isVehicleValid')
AddEventHandler('esx-qalle-sellvehicles:isVehicleValid', function(cb, price, plate)
	local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
	local plate = plate
	local price = price
    

	RetrievePlayerVehicles(character.id, function(ownedVehicles)

		local isFound = false
	
		for id, v in pairs(ownedVehicles) do
			if Trim(plate):upper() == Trim(v['plate']):upper() and #Config.VehiclePositions ~= VehiclesForSale then
                local result = exports.ghmattimysql:executeSync("update characters_cars set for_sale = true, for_sale_price = @price where cid = @sellerIdentifier and license_plate = @licensePlate",
                    {
						["@sellerIdentifier"] = character.id,
                        ["@price"] = price,
						["@licensePlate"] = v['plate']
                    }
                )
				if result.changedRows >= 1 then
					isFound = true
				end
                TriggerClientEvent("esx-qalle-sellvehicles:refreshVehicles", src)
			end
		end
		TriggerClientEvent(cb, src, isFound)

	end)
end)

RegisterServerEvent('esx-qalle-sellvehicles:buyVehicle')
AddEventHandler("esx-qalle-sellvehicles:buyVehicle", function(cb, plate, price)
	local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    local plate = plate
	local price = tonumber(price)
	exports.ghmattimysql:execute("select cid from characters_cars where license_plate = trim(@license_plate)",
		{
			["@license_plate"] = plate
		},
		function(car)
			if user:getBalance() >= price or price == 0 then
				user:removeBank(price)
				exports.ghmattimysql:executeSync("update characters_cars set for_sale = false, vehicle_state = 'In', purchase_price = for_sale_price, for_sale_price = null, cid = @cid where license_plate = trim(@license_plate) and cid = @ownerCid",
					{
						["@cid"] = character.id,
						["@license_plate"] = plate,
						["@ownerCid"] = car[1].cid
					}
				)

				TriggerClientEvent("esx-qalle-sellvehicles:refreshVehicles", src)

				exports.ghmattimysql:executeSync('update characters set bank = bank + @price where cid = @ownerCid', {
					["@price"] = price,
					["@ownerCid"] = car[1].cid
				})
				TriggerClientEvent(cb, src, true, price)
			else
				TriggerClientEvent(cb, src, false, 0)
			end
		end
	)
end)

function RetrievePlayerVehicles(cid, cb)
	local yourVehicles = {}

	exports.ghmattimysql:execute("SELECT license_plate FROM characters_cars WHERE cid = @identifier", {['@identifier'] = cid}, function(result) 

		for id, values in pairs(result) do

			local plate = values['license_plate']

			table.insert(yourVehicles, { plate = plate })
		end

		cb(yourVehicles)

	end)
end

Trim = function(word)
	if word ~= nil then
		return word:match("^%s*(.-)%s*$")
	else
		return nil
	end
end