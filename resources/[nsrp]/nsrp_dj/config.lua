Config = {}

Config.nightclubs = {
	 nightclubBahamas = {
		 dancefloor = {
			 Pos = {x = -1387.0628662109, y=  -618.31188964844, z = 30.81955909729},
			 Marker = { w= 25.0, h= 1.0,r = 204, g = 204, b = 0},
			 HelpPrompt = "Appuyer sur ~INPUT_PICKUP~ pour afficher le menu de danse",
		 }, 
		 djbooth = {
			 Pos = {x = -1384.628662109, y=  -627.31188964844, z = 30.81955909729}, 
			 Marker = { w= 1.0, h= 0.5,r = 204, g = 204, b = 0},
			 HelpPrompt = "Push ~INPUT_PICKUP~ to shredd these vinyls",
		 },
	 },	
	-- nightclubUnicorn = {
		-- dancefloor = {
			-- Pos = {x = 110.13, y=  -1288.70, z = 28.85},
			-- Marker = { w= 25.0, h= 1.0,r = 204, g = 204, b = 0},
			-- HelpPrompt = "Appuyer sur ~INPUT_PICKUP~ pour afficher le menu de danse",
		-- }, 
		-- djbooth = {
			-- Pos = {x = 118.6188, y=  -1288.85, z = 28.81955909729}, 
			-- Marker = { w= 1.0, h= 0.5,r = 204, g = 204, b = 0},
			-- HelpPrompt = "Appuyer sur ~INPUT_PICKUP~ pour prendre votre place de dj",
		-- },
	-- },	
	--nightclubunderground = {
	--	dancefloor = {
	--		Pos = {x = -1592.275, y=  -3012.131, z = -78.00},
	--		Marker = { w= 25.0, h= 1.0,r = 204, g = 204, b = 0},
	--		HelpPrompt = "Appuyer sur ~INPUT_PICKUP~ pour afficher le menu de danse",
	--	}, 
	--	djbooth = {
	--		Pos = {x = -1603.98, y=  -3012.802, z = -77.79}, 
	--		Marker = { w= 1.0, h= 0.5,r = 204, g = 204, b = 0},
	--		HelpPrompt = "Appuyer sur ~INPUT_PICKUP~ pour prendre votre place de dj",
	--	},
	--},
}

Config.Songs = {
	-- SONGS = Youtube Video ID Ex.: www.youtube.com/watch?v=((jfreFPe99GU)) catch only jfreFPe99GU
	-- SONGS = ID do video  do Youtube Exemplo: www.youtube.com/watch?v=((jfreFPe99GU)) pegue apenas jfreFPe99GU

	{song = "dbOQU6kBBwI", label ="EDM Royalty Free Mix 1"},
	{song = "D-FIfQ5bINo", label ="EDM Royalty Free Mix 1.1"},
	{song = "xsqn1zvZ6kI", label ="EDM Royalty Free Mix 2"},
	{song = "sb-BQN-z1_Q", label ="EDM Royalty Free Mix 3"},
	{song = "hIWhX0nHbjA", label ="Generic House Mix"},
	{song = "N_WgwSC4uEQ", label ="Deep House Mix"},
	{song = "4XJ9jndqUao", label ="Tech House Mix"}
	
}