--DO-NOT-EDIT-BELLOW-THIS-LINE--
local GUI                       = {}
GUI.Time                        = 0
local toggle = false
-- Fin init ESX

function exitmarkerdjbooth()
	SendNUIMessage(false,false)
end

RegisterCommand('testdj', function() 
OpenDjMenu()
end)


RegisterNetEvent("ft_libs:OnClientReady") --Call when if NetworkIsSessionStarted() is triggered somewhere drop dependency on starting thread using ft_libs
AddEventHandler('ft_libs:OnClientReady', function()
	for k,v in pairs (Config.nightclubs) do
		exports.ft_libs:AddArea("nsrp_dj_"..k.."_dancefloor", {
			trigger = {
				weight = v.dancefloor.Marker.w,
				active = { --polyzone enter
					callback = function()
						local ped = GetPlayerPed(-1)
						local coords      = GetEntityCoords(ped)
						local distance = GetDistanceBetweenCoords(coords, v.dancefloor.Pos.x, v.dancefloor.Pos.y, v.dancefloor.Pos.z, true)
						-- local number =distance/v.dancefloor.Marker.w
						-- local volume = round((1-number), 2)
						local number =distance/v.dancefloor.Marker.w
						local volume = round(((1-number)/10), 2)
						if not toggle then
							SendNUIMessage({setvolume = volume, dancefloor = k})	
						else
							SendNUIMessage({setvolume = 0.0, dancefloor = k})	
						end
						
					end,
				},
				exit = { --polyzone exit
					callback = function()
						SendNUIMessage({setvolume = 0.0, dancefloor = k})	
					end,
				},
			},
			locations = {
				{
					x = v.dancefloor.Pos.x,
					y = v.dancefloor.Pos.y,
					z = v.dancefloor.Pos.z,
				},
			},
		})
	end
end)

function OpenDjMenu(dancefloor)
	local rank = exports["isPed"]:isPed("jobrank")
	rank = tonumber(rank)
	
	local displaymenu = 
	{
		{
		id = 1,
		header    = 'Playlist ',
		txt = 'Manage the DJ Booth'
		},
		{
			id = 2,
			header    = 'Pause',
			txt = "Pause the currently playing song",
			params = {
                    event = "nsrp_dj:setcommandcl",
					args = {"pause","",dancefloor}
			}
		},
		{
			id = 3,
			header = 'Play',
			txt = "Play the last selected song",
			params = {
                    event = "nsrp_dj:setcommandcl",
					args = {"play","",dancefloor}
			}
		},
		{
			id = 4,
			header = 'Stop',
			txt = "Stop the currently playing song",
			params = {
                    event = "nsrp_dj:setcommandcl",
					args = {"stop","",dancefloor}
			}
		},
		{
			id = 5,
			header = 'Song List',
			txt = "All music is Royalty Free non-copyright",
		}
	}

	local count = 6
	for k,v in pairs (Config.Songs) do
		
		--table.insert(elements, {label = v.label, value = v.song})
		table.insert(displaymenu, {
			id = count,
			header = "Select Song " .. tostring(count-5),
			txt = v.label,
			params = {
				event = "nsrp_dj:setcommandcl",
				args = {"playsong",v.song,dancefloor}
		}
			})
		count = count + 1
	end

	if rank >= 3 then
		
		table.insert(displaymenu, {
			id = count,
			header = "Play Custom Song",
			txt = "Use a youtube video ID",
			params = {
				event = "nsrp_jd:customsong",
				args = {dancefloor}
				
			}
			})
		count = count + 1
	end

	
	
TriggerEvent('ethical-context:sendMenu',displaymenu)

	
end
exports("OpenDjMenu",OpenDjMenu)
--args = {"playsong",v.song,dancefloor} nsrp_dj:setcommandcl

RegisterNetEvent('nsrp_jd:customsong')
AddEventHandler('nsrp_jd:customsong', function(dancefloor)
    local songchosen = exports["ethical-applications"]:KeyboardInput({
        header = "Choose a Custom song <br><br>Input the video ID from the end of the youtube URL.<br><br>Example: www.youtube.com/watch?v=ABCDEFGH <br><br> Input: ABCDEFGH",
        rows = {
            {
                id = 0,
                txt = "Video ID"
            }
        }
    })
    if songchosen then
        if songchosen[1] ~= nil then
            TriggerServerEvent("nsrp_dj:setcommand", "playsong", songchosen[1].input,dancefloor[1])
        else
            TriggerEvent('DoLongHudText', 'No Video ID was input', 2)
        end
    end

end)



RegisterNetEvent('nsrp_jd:muteme:bahama')
AddEventHandler('nsrp_jd:muteme:bahama', function()
	if toggle then
	 toggle = false
	else
	 toggle = true
	end
end)



RegisterNetEvent('nsrp_jd:openmenubooth:bahama')
AddEventHandler('nsrp_jd:openmenubooth:bahama', function()
 TriggerEvent('nsrp_jd:openmenubooth',"nightclubBahamas")
end)

RegisterNetEvent('nsrp_jd:openmenubooth')
AddEventHandler('nsrp_jd:openmenubooth', function(args)
OpenDjMenu(args)
GUI.Time = GetGameTimer()
end)

RegisterNetEvent('nsrp_dj:setcommandcl')
AddEventHandler('nsrp_dj:setcommandcl', function(args)
	print(args[1],args[2],args[3])
	TriggerServerEvent('nsrp_dj:setcommand',args[1],args[2],args[3])
end)

RegisterNetEvent('nsrp_dj:setmusicforeveryone')
AddEventHandler('nsrp_dj:setmusicforeveryone', function(command, songname, dancefloor)
	 print(dancefloor)
	SendNUIMessage({musiccommand = command, songname = songname, dancefloor = dancefloor})
end)

function round(num, dec)
  local mult = 10^(dec or 0)
  return math.floor(num * mult + 0.5) / mult
end

function dump(o, nb)
  if nb == nil then
    nb = 0
  end
   if type(o) == 'table' then
      local s = ''
      for i = 1, nb + 1, 1 do
        s = s .. "    "
      end
      s = '{\n'
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
          for i = 1, nb, 1 do
            s = s .. "    "
          end
         s = s .. '['..k..'] = ' .. dump(v, nb + 1) .. ',\n'
      end
      for i = 1, nb, 1 do
        s = s .. "    "
      end
      return s .. '}'
   else
      return tostring(o)
   end
end
