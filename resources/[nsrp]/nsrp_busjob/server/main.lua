RegisterNetEvent('blarglebus:finishRoute')
AddEventHandler('blarglebus:finishRoute', function(amount)
    local source = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(source)
    user:addMoney(amount)  
end)

RegisterNetEvent('blarglebus:passengersLoaded')
AddEventHandler('blarglebus:passengersLoaded', function(amount)
    local source = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(source)
    user:addMoney(amount)
end)

RegisterNetEvent('blarglebus:abortRoute')
AddEventHandler('blarglebus:abortRoute', function(amount)
    local source = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(source)
    user:removeMoney(amount)
end)
