local E_KEY = 38

local playerPosition = nil
local playerPed = nil

local isBusDriver = false
local isOnDuty = false
local isRouteFinished = false
local isRouteJustStarted = false
local isRouteJustAborted = false

local activeRoute = nil
local activeRouteLine = nil
local busType = nil
local stopNumber = 1
local lastStopCoords = {}
local totalMoneyPaidThisRoute = 0

local pedsOnBus = {}
local pedsAtNextStop = {}
local pedsToDelete = {}
local numberDepartingPedsNextStop = 0

local busStops = {
    ['stop_banner_hotel']                   = 'Banner Hotel & Spa',
    ['stop_von_crastenburg']                = 'Von Crastenburg',
    ['stop_airport']                        = 'Airport',
    ['stop_elgin']                          = 'Elgin House',
    ['stop_venetian']                       = 'Venetian',
    ['stop_viceroy']                        = 'The Viceroy',
    ['stop_crastenburg']                    = 'Crastenburg',
    ['stop_opium_nights']                   = 'Opium Nights Hotel',
 
    ['stop_dashound']                       = 'Dashound Bus Center',
    ['stop_banham_canyon']                  = 'Banhan Canyon',
    ['stop_lago_zancudo']                   = 'Lago Zancudo',
    ['stop_north_chumash']                  = 'North Chumash',
    ['stop_chiliad']                        = 'Chiliad Mtn State Park',
    ['stop_paleto_bay']                     = 'Paleto Bay',
    ['stop_grapeseed']                      = 'Grapeseed',
    ['stop_sandy_shores']                   = 'Sandy Shores',
    ['stop_senora_park']                    = 'Senora National Park',
    ['stop_harmony']                        = 'Harmony',

    ['stop_metro_station_departures']       = 'this terminal',
    ['stop_metro_station_arrivals']         = 'the terminal',
    ['stop_metro_vespucci_sinner']          = 'Vespucci Blvd / Sinner St',
    ['stop_metro_popular']                  = 'Popular St',
    ['stop_metro_hawick_meteor']            = 'Hawick Ave / Meteor St',
    ['stop_metro_hawick_alta']              = 'Hawick Ave / Alta Pl',
    ['stop_metro_strawberry_ave']           = 'Strawberry Ave',
    ['stop_metro_peaceful_san_andreas']     = 'Peaceful St / San Andreas Blvd',
    ['stop_metro_vespucci_alta']            = 'Vespucci Blvd / Alta St',
    ['stop_metro_peaceful_vespucci']        = 'Peaceful St / Vespucci Blvd',
    ['stop_metro_business_center']          = 'Arcadius Business Center',
    ['stop_metro_rockford_plaza']           = 'Rockford Plaza',

    ['stop_metro_carcer_way']               = 'Carcer Way',
    ['stop_metro_rockford_eastbourne']      = 'Rockford Dr / Eastbourne Way',
    ['stop_metro_delperro_madwayne']        = 'Del Perro Blvd / Mad Wayne Thunder Dr',
    ['stop_metro_dorset_palomino']          = 'Dorset Dr / Palomino Ave',
    ['stop_metro_sanandreas_calais']        = 'San Andreas Ave / Calais Ave',
    ['stop_metro_vespucci_ginger']          = 'Vespucci Blvd / Ginger St',
    ['stop_metro_ginger']                   = 'Ginger St',
    ['stop_metro_sanandreas_ginger']        = 'San Andreas Ave / Ginger St',
    ['stop_metro_alta_gruppe']              = 'Alta St at Gruppe Sechs',
    ['stop_metro_fib']                      = 'San Andreas Ave at FIB',
    ['stop_metro_delperro_rockford']        = 'Del Perro Blvd / Rockford Dr',
    ['stop_metro_delperro_morningwood']     = 'Del Perro Blvd / Morningwood Blvd',
    ['stop_metro_delperro_plaza']           = 'Del Perro Plaza',
    ['stop_metro_marathon_baycity']         = 'Marathon Ave / Bay City Ave',
    ['stop_metro_vespucci_elgin']           = 'Vespucci Blvd / Elgin Ave',
    ['stop_metro_marathon_prosperity']      = 'Marathon Ave / Properity St',
    ['stop_metro_eclipse_cougar']           = 'W Eclipse Blvd / Cougar Ave',
    ['stop_metro_marathon']                 = 'Marathon Ave',
}

Citizen.CreateThread(function()
   -- waitForEsxInitialization()
    waitForPlayerJobInitialization()
    registerJobChangeListener()

    Overlay.Init()
    startAbortRouteThread()
    startPedCleanupThread()
    startMainLoop()
end)

function waitForPlayerJobInitialization()
    while true do
        local playerData = ESX.GetPlayerData()
        if playerData.job ~= nil then
            handleJobChange(playerData.job)
            break
        end
        Citizen.Wait(10)
    end
end

function registerJobChangeListener()
    RegisterNetEvent('esx:setJob')
    AddEventHandler('esx:setJob', handleJobChange)
end

function startAbortRouteThread()
    Citizen.CreateThread(function()
        while true do
            if isOnDuty and not isRouteFinished and not isRouteJustStarted and not isRouteJustAborted then
                handleAbortRoute()
                Citizen.Wait(15)
            else
                Citizen.Wait(1000)
            end
        end
    end)
end

function startMainLoop()
    while true do
        if isBusDriver and not isRouteJustAborted then
            playerPed = PlayerPedId()
            playerPosition = GetEntityCoords(playerPed)

            if not isOnDuty then
                for i = 1, #Config.Routes do
                    handleSpawnPoint(i)
                end
                Citizen.Wait(5)
            elseif isPlayerNotInBus() then
                ESX.ShowHelpNotification(_('get_back_in_bus'))
                Citizen.Wait(5)
            else
                handleActiveRoute()
                Citizen.Wait(100)
            end
        else
            Citizen.Wait(1000)
        end
    end
end

function isPlayerNotInBus()
    local vehiclePlayerIsIn = GetVehiclePedIsIn(playerPed, false)
    return vehiclePlayerIsIn ~= Bus.bus
end

function startPedCleanupThread()
    Citizen.CreateThread(function()
        while true do
            if #pedsToDelete > 0 and (not isOnDuty or playerDistanceFromCoords(lastStopCoords) > Config.DeleteDistance) then
                Peds.DeletePeds(pedsToDelete)
            end

            Citizen.Wait(5000)
        end
    end)
end
RegisterNetEvent('busDriverOnDuty')
AddEventHandler('busDriverOnDuty',function()
    isBusDriver = true
    handleNowBusDriver()   
end)

RegisterNetEvent('busDriverOffDuty')
AddEventHandler('busDriverOffDuty',function()
    isBusDriver = false
    handleNoLongerBusDriver()   
end)

function handleNowBusDriver()
    Markers.StartMarkers()
    Blips.StartBlips()
end

function handleNoLongerBusDriver()
    immediatelyEndRoute()
    Markers.StopMarkers()
    Blips.StopBlips()
end

function handleSpawnPoint(locationIndex)
    local route = Config.Routes[locationIndex]
    local coords = route.SpawnPoint;
    
    if playerDistanceFromCoords(coords) < Config.Markers.Size then
        TriggerEvent('DoLongHudText','press ~INPUT_PICKUP~ to start ~g~'..route.Name..'.')
        if IsControlJustPressed(1, E_KEY) then
            startRoute(locationIndex)
        end
    end
end

function startRoute(route)
    activeRouteLine = selectStartingLine(Config.Routes[route])
    if activeRouteLine == nil then
        TriggerEvent('DoLongHudText','Bus route selection canceled.')
        return
    end

    handleSettingRouteJustStartedAsync()
    isOnDuty = true
    isRouteFinished = false
    activeRoute = Config.Routes[route]
    busType = getBusType(activeRoute, activeRouteLine)
    totalMoneyPaidThisRoute = 0
    TriggerEvent('DoLongHudText','You have been assigned ~g~'..activeRouteLine.Name..'~s~.')
    ESX.ShowNotification(_U('route_assigned', _U(activeRouteLine.Name)))
    Events.RouteStarted(activeRouteLine.Name)
    Bus.CreateBus(activeRoute.SpawnPoint, busType.BusModel, activeRouteLine.BusColor)
    Blips.StartAbortBlip(activeRoute.Name, activeRoute.SpawnPoint)
    Markers.StartAbortMarker(activeRoute.SpawnPoint)
    Overlay.Start()

    stopNumber = 0
    setUpNextStop()
    stopNumber = 1

    local firstStopName = _U(activeRouteLine.Stops[1].name)
    TriggerEvent('Get in bus and drive to first stop at ~g~'..firstStopName..'.')
    ESX.ShowNotification(_U('drive_to_first_marker', firstStopName))
    updateOverlay(firstStopName)
end

function selectStartingLine(selectedRoute)
    if #selectedRoute.Lines == 1 then
        return selectedRoute.Lines[1]
    end 
    return selectedRoute.Lines[math.random(1, #selectedRoute.Lines)]
end

function getBusType(route, line)
    return line.BusOverride or route.Bus or getBackwardsCompatibleBusType(route)
end

function getBackwardsCompatibleBusType(route)
    return {
        BusModel = route.BusModel,
        Capacity = route.Capacity,
        Doors = route.Doors,
        FirstSeat = route.FirstSeat
    }
end

function handleSettingRouteJustStartedAsync()
    isRouteJustStarted = true
    Citizen.CreateThread(function()
        Citizen.Wait(5000)
        isRouteJustStarted = false
    end)
end

function handleActiveRoute()
    if isRouteFinished then
        handleReturningBus()
    else
        handleNormalStop()
    end
end

function handleReturningBus()
    local coords = getReturnPointCoords(activeRoute, activeRouteLine)

    if playerDistanceFromCoords(coords) < Config.Markers.Size then
        Bus.DisplayMessageAndWaitUntilBusStopped('stop_bus')

        TriggerServerEvent('blarglebus:finishRoute', activeRoute.Payment)
        Events.RouteEnded()
        immediatelyEndRoute()

        Markers.ResetMarkers()
        Blips.ResetBlips()
    end
end

function handleNormalStop()
    local currentStop = activeRouteLine.Stops[stopNumber]
    local currentStopNameKey = activeRouteLine.Stops[stopNumber].name

    if playerDistanceFromCoords(currentStop) < Config.Markers.Size then
        local nextStopNameKey = determineNextStopName()
        lastStopCoords = currentStop
        Events.ArrivedAtStop(currentStopNameKey, nextStopNameKey)
        handleUnloading(currentStop)
        handleLoading()
        payForEachPedLoaded(#pedsAtNextStop)

        local nextStopName = busStops[nextStopNameKey]

        if (isLastStop(stopNumber)) then
            local coords = getReturnPointCoords(activeRoute, activeRouteLine)
            isRouteFinished = true
            Markers.StopAbortMarker()
            Markers.SetMarkers({coords})
            Blips.SetBlipAndWaypoint(activeRoute.Name, coords.x, coords.y, coords.z)
            Blips.StopAbortBlip()
            TriggerEvent('DoLongHudText','Route is finished. Return to the terminal.')
            ESX.ShowNotification(_U('return_to_terminal'))
        else
            TriggerEvent('DoLongHudText','Continue to next stop at ~g~'..nextStopName..'~.')
            ESX.ShowNotification(_U('drive_to_next_marker', nextStopName))
            setUpNextStop()
            stopNumber = stopNumber + 1
        end

        Events.DepartingStop(currentStopNameKey, nextStopNameKey)
        updateOverlay(nextStopName)
    end
end

function determineNextStopName()
    if (isLastStop(stopNumber)) then
        return 'terminal'
    end

    return activeRouteLine.Stops[stopNumber + 1].name
end

function getReturnPointCoords(route, line)
    return line.BusReturnPointOverride or activeRoute.BusReturnPoint or activeRoute.SpawnPoint
end

function handleUnloading(stopCoords)
    Bus.DisplayMessageAndWaitUntilBusStopped(determineWaitForPassengersMessage())
    Bus.OpenDoorsAndActivateHazards(busType.Doors)

    local departingPeds = {}
    for i = 1, numberDepartingPedsNextStop do
        local ped = table.remove(pedsOnBus)
        table.insert(departingPeds, ped)
        table.insert(pedsToDelete, ped)
        Peds.LeaveVehicle(ped, Bus.bus)
    end

    waitUntilPedsOffBus(departingPeds)

    Peds.WalkPedsToLocation(departingPeds, stopCoords)
end

function determineWaitForPassengersMessage()
    if numberDepartingPedsNextStop == 0 and #pedsAtNextStop == 0 then
        return _U('no_passengers_loading_or_unloading')
    elseif numberDepartingPedsNextStop == 0 then
        return _U('no_passengers_unloading')
    elseif #pedsAtNextStop == 0 then
        return _U('no_passengers_loading')
    end

    return _U('wait_for_passengers')
end

function waitUntilPedsOffBus(departingPeds)
    local stop = activeRouteLine.Stops[stopNumber]

    if #departingPeds == 0 then
        return
    end

    local onCount = 0
    while onCount < #departingPeds do
        onCount = 0
        for i = 1, #departingPeds do
            if Peds.IsPedInVehicleOrDead(departingPeds[i], stop) then
                onCount = onCount + 1
            end
            Citizen.Wait(200)
        end
    end
end

function handleLoading()
    Citizen.Wait(Config.DelayBetweenChanges)

    if #pedsAtNextStop == 0 then
        return Bus.CloseDoorsAndDeactivateHazards()
    end

    local freeSeats = Bus.FindFreeSeats(busType.FirstSeat, busType.Capacity)

    for i = 1, #pedsAtNextStop do
        Peds.EnterVehicle(pedsAtNextStop[i], Bus.bus, freeSeats[i])
        table.insert(pedsOnBus, pedsAtNextStop[i])
    end

    waitUntilPedsOnBus()
    Bus.CloseDoorsAndDeactivateHazards()
end

function waitUntilPedsOnBus()
    local stop = activeRouteLine.Stops[stopNumber]

    if #pedsAtNextStop == 0 then 
        return
    end

    local onCount = 0
    while onCount < #pedsAtNextStop do
        onCount = 0
        for i = 1, #pedsAtNextStop do
            if Peds.IsPedInVehicleDeadOrTooFarAway(pedsAtNextStop[i], stop) then
                onCount = onCount + 1
            end
            Citizen.Wait(200)
        end
    end
end

function payForEachPedLoaded(numberOfPeds)
    local amountToPay = 0

    if Config.SpawnPeds == false then
        amountToPay = Config.MoneyPerStopNoPedsOverride
    elseif numberOfPeds > 0 then
        amountToPay = numberOfPeds * activeRoute.PaymentPerPassenger
    end

    if amountToPay > 0 then
        TriggerServerEvent('blarglebus:passengersLoaded', amountToPay)
        TriggerEvent('DoLongHudText', '~g~%d~s~ passengers paid for this stop. Collected ~g~R%d.')
        ESX.ShowNotification(_U('passengers_loaded', numberOfPeds, amountToPay))
        totalMoneyPaidThisRoute = totalMoneyPaidThisRoute + amountToPay
    end
end

function setUpNextStop()
    local nextStop = activeRouteLine.Stops[stopNumber + 1]
    local numberOfPedsToSpawn = 0
    local freeSeats = busType.Capacity - #pedsOnBus
    
    pedsAtNextStop = {}

    if Config.SpawnPeds == false then
        numberDepartingPedsNextStop = 0
    elseif isLastStop(stopNumber + 1) then
        numberOfPedsToSpawn, numberDepartingPedsNextStop = setUpLastStop()
    elseif nextStop.unloadType == UnloadType.All then
        numberOfPedsToSpawn, numberDepartingPedsNextStop = setUpAllStop()
    elseif nextStop.unloadType == UnloadType.Some then
        numberOfPedsToSpawn, numberDepartingPedsNextStop = setUpSomeStop(freeSeats)
    elseif nextStop.unloadType == UnloadType.None and freeSeats > 0 then
        numberOfPedsToSpawn, numberDepartingPedsNextStop = setUpNoneStop(freeSeats)
    end

    Citizen.CreateThread(function()
        for i = 1, numberOfPedsToSpawn do
            table.insert(pedsAtNextStop, Peds.CreateRandomPedInArea(nextStop))
            Citizen.Wait(100)
        end
    end)
    
    Markers.SetMarkers({nextStop})
    Blips.SetBlipAndWaypoint(activeRoute.Name, nextStop.x, nextStop.y, nextStop.z)
end

function isLastStop(stopNumber)
    return stopNumber == #activeRouteLine.Stops
end

function setUpLastStop()
    Log.debug('next stop is last, all peds should depart')
    return 0, #pedsOnBus
end

function setUpAllStop()
    Log.debug('next stop is All, all peds should unload, should spawn peds equal to capacity')
    return busType.Capacity, #pedsOnBus
end

function setUpSomeStop(freeSeats)
    local numberOfPedsToSpawn = math.random(1, busType.Capacity)
    local minimumDepartingPeds = 1

    if numberOfPedsToSpawn > freeSeats then
        minimumDepartingPeds = numberOfPedsToSpawn - freeSeats
    end

    local numberDeparting = math.random(minimumDepartingPeds, #pedsOnBus)

    Log.debug('next stop is Some, randomly decided to spawn ' .. numberOfPedsToSpawn .. ' peds and depart ' .. numberDeparting)
    return numberOfPedsToSpawn, numberDeparting
end

function setUpNoneStop(freeSeats)
    local numberOfPedsToSpawn = math.random(1, freeSeats)

    Log.debug('next stop is None, randomly deciding to spawn ' .. numberOfPedsToSpawn .. 'peds')
    return numberOfPedsToSpawn, 0
end

function handleAbortRoute()
    if playerDistanceFromCoords(activeRoute.SpawnPoint) < Config.Markers.Size then
        TriggerEvent('press ~INPUT_PICKUP~ to abort route. Will cost ~r~R%d.')
        ESX.ShowHelpNotification(_U('abort_route_help', totalMoneyPaidThisRoute))

        if IsControlJustPressed(1, E_KEY) then
            handleSettingRouteJustAbortedAsync()
            TriggerServerEvent('blarglebus:abortRoute', totalMoneyPaidThisRoute)

            immediatelyEndRoute()
            Blips.ResetBlips()
            Markers.ResetMarkers()
        end
    end
end

function handleSettingRouteJustAbortedAsync()
    isRouteJustAborted = true
    Citizen.CreateThread(function()
        Citizen.Wait(5000)
        isRouteJustAborted = false
    end)
end

function immediatelyEndRoute()
    isOnDuty = false
    activeRoute = nil
    activeRouteLine = nil
    Peds.DeletePeds(pedsToDelete)
    Peds.DeletePeds(pedsAtNextStop)
    Peds.DeletePeds(pedsOnBus)
    Bus.DeleteBus()
    Overlay.Stop()
end

function playerDistanceFromCoords(coords)
    return GetDistanceBetweenCoords(playerPosition, coords.x, coords.y, coords.z, true)
end

function updateOverlay(nextStopName)
    Overlay.Update(_U(activeRouteLine.Name), nextStopName, #activeRouteLine.Stops - stopNumber, totalMoneyPaidThisRoute)
end
