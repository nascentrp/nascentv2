fx_version 'bodacious'
game 'gta5'

name 'nsrp_busjob'
description 'Blarglebottoms Bus Route'

server_scripts {
    'config/config.lua',
    'server/main.lua'
}

ui_page 'html/index.html'

client_scripts {
    'config/unloadType.lua',
    'config/busType.lua',
    'config/routes/*.lua',
    'config/config.lua',
    'client/*.lua',
}

files {
    'html/*.*'
}