Config = {}

-- The currency prefix to be used
Config.CurrencyPrefix = 'R'

-- Enables or disables job restriction
Config.RestrictToCertainJobs = true

-- Determines which jobs are able to use the meter (if RestrictToCertainJobs is true)
Config.JobsThatCanUseMeter = {'taxi'}

-- Enables or Disables the vehicle restriction
Config.RestrictVehicles = true

-- Distance Measurement -- valid options are "mi" or "km". "mi" is default. If you
-- change this be sure to change RateSuffix as well
Config.DistanceMeasurement = 'km'

-- Rate Suffix
Config.RateSuffix = '/km'

-- Which vehicles can not use the meter (if RestrictVehicles= true). By default
-- Bicycles, OffRoad and Emergency vehicles are disallowed
Config.DisallowedVehicleClasses = {8, 9, 18}
