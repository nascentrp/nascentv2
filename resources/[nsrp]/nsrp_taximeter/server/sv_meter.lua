
RegisterServerEvent('nsrp_meter:updatePassengerMeters')
AddEventHandler('nsrp_meter:updatePassengerMeters', function(player, meterAttrs)
  TriggerClientEvent('nsrp_meter:updatePassenger', player, meterAttrs)
end)


RegisterServerEvent('nsrp_meter:resetPassengerMeters')
AddEventHandler('nsrp_meter:resetPassengerMeters', function(player)
  TriggerClientEvent('nsrp_meter:resetMeter', player)
end)

RegisterServerEvent('nsrp_meter:updatePassengerLocation')
AddEventHandler('nsrp_meter:updatePassengerLocation', function(player)
  TriggerClientEvent('nsrp_meter:updateLocation', player)
end)
