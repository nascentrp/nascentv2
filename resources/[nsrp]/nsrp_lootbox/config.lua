Config = {}
Config.UserLicense = "None"
Config["image_source"] = "nui://ethical-inventory/nui/icons/"

Config["chance"] = {
	[1] = { name = "Common", rate = 50 },
	[2] = { name = "Rare", rate = 30 },
	[3] = { name = "Epic", rate = 20 },
	[4] = { name = "Unique", rate = 40} ,
	[5] = { name = "Legendary", rate = 1 },
}

Config["5mscriptscom"] = {
	["bankcase"] = {
		name = "Robbery",
		list = {
			{ item = "cashroll", image = "np_cash-roll.png", amount=45, tier = 1 },
			{ item = "markedbills", image = "np_inked-money-bag.png", amount=2, tier = 4 },
			--{ item = "goldbar", image = "np_gold-bar.png", amount=5, tier = 1 },
			--{ item = "ironbar", image = "np_ironbar.png", amount=15, tier = 1 },
			{ item = "cashstack", image = "np_cash-stack.png", amount=40, tier = 3 },
			--{ item = "goldbar", image = "np_gold-bar.png", amount=15 , tier = 3 },
			{ item = "decryptersess", image = "np_decrypter-sess.png", amount=1 , tier = 5 },
			{ item = "securityred", image = "cr.png", amount=1, tier = 5 },
			{ item = "Gruppe6Card", image = "gruppe6.png", amount=1, tier = 5 },
			{ item = "pix1", image = "np_decrypter-fv2.png", amount=1, tier = 5 }
			--{ money = 1000, tier = 2 },
		}
	},
	["fishinglockbox"] = {
		name = "Fishing Lock Box",
		list = {
			{ item = "ironbar", image = "np_ironbar.png", amount=15, tier = 1 },
			{ item = "fishingboot", image = "np_fishingboot.png", amount=1, tier = 1 },
			{ item = "fishingtin", image = "np_fishingtin.png", amount=1, tier = 1 },
			{ item = "sushiroll", image = "np_sushi-rolls.png", amount=15, tier = 2 },
			{ item = "cutfish", image = "np_sushi-rolls.png", amount=20, tier = 2 },
			{ item = "decryptersess", image = "np_decrypter-sess.png", amount=1 , tier = 5 },
			{ item = "securitygreen", image = "cg.png", amount=1, tier = 5 },
			{ item = "ironbar", image = "np_ironbar.png", amount=25, tier = 4 },
			{ item = "Gruppe6Card", image = "gruppe6.png", amount=1, tier = 5 }, 
			{ item = "pix1", image = "np_decrypter-fv2.png", amount=1, tier = 3 }
			--{ money = 1000, tier = 2 },
		}
	},
	["scubaloot"] = {
		name = "Scuba Loot",
		list = {
			{ item = "ironbar", image = "np_ironbar.png", amount=5, tier = 1 },
			{ item = "ironbar", image = "np_ironbar.png", amount=7, tier = 2 },
			{ item = "ironbar", image = "np_ironbar.png", amount=10, tier = 3 },
			{ item = "ironbar", image = "np_ironbar.png", amount=12, tier = 3 },
			{ item = "ironbar", image = "np_ironbar.png", amount=20, tier = 3 },
			--{ item = "whitepearl", image = "np_ironbar.png", amount=20, tier = 3 },
			{ item = "decryptersess", image = "np_decrypter-sess.png", amount=1 , tier = 5 },
			{ item = "pix1", image = "np_decrypter-fv2.png", amount=1, tier = 5 }
			--{ money = 1000, tier = 2 },
		}
	},
	
	["vangelicoloot"] = {
		name = "Vangelico Heist",
		list = {
			{ item = "rolexwatch", image = "np_rolex-watch.png", amount=1, tier = 1 },
			{ item = "goldbar", image = "np_gold-bar.png", amount=1, tier = 1 },
			{ item = "bdiamond", image = "np_blue-diamond.png", amount=1, tier = 3 },
			{ item = "stolen8ctchain", image = "np_8ct-gold-chain.png", amount=1, tier = 1 },
			{ item = "stolen10ctchain", image = "np_10ct-gold-chain.png", amount=1, tier = 1 },
			{ item = "stolen2ctchain", image = "np_2ct-gold-chain.png", amount=1 , tier = 5 },
			{ item = "goldcoin", image = "np_gold-coin.png", amount=1, tier = 1 },
			{ item = "rolexwatch", image = "np_rolex-watch.png", amount=4, tier = 2 },
			{ item = "goldbar", image = "np_gold-bar.png", amount=2, tier = 2 },
			{ item = "stolen8ctchain", image = "np_8ct-gold-chain.png", amount=5, tier = 2 },
			{ item = "stolen10ctchain", image = "np_10ct-gold-chain.png", amount=3, tier = 2 },
			{ item = "stolen2ctchain", image = "np_2ct-gold-chain.png", amount=5 , tier = 2 },
			{ item = "goldcoin", image = "np_gold-coin.png", amount=5, tier = 2 },
			{ item = "erpring", image = "np_engagement-ring.png", amount=2, tier = 2 },
			{ item = "goldbar", image = "np_gold-bar.png", amount=4, tier = 3 },
			{ item = "stolen8ctchain", image = "np_8ct-gold-chain.png", amount=8, tier = 3 },
			{ item = "stolen10ctchain", image = "np_10ct-gold-chain.png", amount=6, tier = 3 },
			{ item = "stolen2ctchain", image = "np_2ct-gold-chain.png", amount=10 , tier = 3 },
			{ item = "goldcoin", image = "np_gold-coin.png", amount=5, tier = 3 },
			{ item = "erpring", image = "np_engagement-ring.png", amount=4, tier = 3 },
			{ item = "goldbar", image = "np_gold-bar.png", amount=10, tier = 5 },
			{ item = "goldcoin", image = "np_gold-coin.png", amount=15, tier = 5 }

		}
	},
}
