local script_active = true
local draw = {}

RegisterNUICallback('mkbuss:NUIoff', function()
    print("ofoff")
	SetNuiFocus(false,false)
end)

RegisterNetEvent("mkbuss:open5mscriptscom")
AddEventHandler("mkbuss:open5mscriptscom", function(data)
	local sum = 0
	draw = {}
	for k, v in pairs(Config["5mscriptscom"][data].list) do
		local rate = Config["chance"][v.tier].rate * 100
		for i=1,rate do 
			if v.item then
				if v.amount then
					table.insert(draw, {item = v.item ,amount = v.amount, tier = v.tier, image = v.image})
				else
					table.insert(draw, {item = v.item ,amount = 1, tier = v.tier, image = v.image})
				end
			elseif v.money then
				table.insert(draw, {money = v.money, tier = v.tier})
			end
			i = i + 1
		end
		sum = sum + rate
	end
	local random = math.random(1,sum)
	SetNuiFocus(true,true)
	SendNUIMessage({
        type = "ui",
		data = Config["5mscriptscom"][data].list,
		img = Config["image_source"],
		win = draw[random]
    })
	Wait(9000)
    print("draw", json.encode(draw[random]))
	if draw[random].item then
		TriggerEvent('player:receiveItem', draw[random].item, draw[random].amount)
	elseif draw[random].money then
        TriggerServerEvent("player:bankMoneyPayout")
		-- TriggerServerEvent('player:receiveItem', 'money',draw[random].money)
	end
    SetNuiFocus(false,false)
end)

local NumberCharset = {}
local Charset = {}

for i = 48,  57 do table.insert(NumberCharset, string.char(i)) end

for i = 65,  90 do table.insert(Charset, string.char(i)) end
for i = 97, 122 do table.insert(Charset, string.char(i)) end

function GetRandomNumber(length)
    Citizen.Wait(1)
    math.randomseed(GetGameTimer())
    if length > 0 then
        return GetRandomNumber(length - 1) .. NumberCharset[math.random(1, #NumberCharset)]
    else
        return ''
    end
end

function GetRandomLetter(length)
    Citizen.Wait(1)
    math.randomseed(GetGameTimer())
    if length > 0 then
        return GetRandomLetter(length - 1) .. Charset[math.random(1, #Charset)]
    else
        return ''
    end
end

-- RegisterCommand("test", function()
--     TriggerEvent("mkbuss:open5mscriptscom", "bankcase")
-- end)