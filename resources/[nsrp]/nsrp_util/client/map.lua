local garageLocations = {
    [1] = { ["spawnX"] = 229.700, ["spawnY"] = -800.1149, ["spawnZ"] = 30.5722, ["X"] = 229.700, ["Y"] = -800.1149, ["Z"] = 30.5722, ["info"] = "Garage: Legion"},
    [2] = { ["X"] = -2029.064, ["Y"] = -463.440, ["Z"] = 11.463, ["spawnX"] = -2013.73, ["spawnY"] = -471.6, ["spawnZ"] = 11.2,  ["info"] = "Garage: Pacific Bluffs"},
    [3] = { ["X"] = -1283.76, ["Y"] = 297.219, ["Z"] = 64.937, ["spawnX"] = -1293.11, ["spawnY"] = 281.49, ["spawnZ"] = 64.29, ["info"] = "Garage: Rockford Hills"},
    [4] = { ["X"] = 2528.31, ["Y"] = 2617.308, ["Z"] = 37.957, ["spawnX"] = 2540.97, ["spawnY"] = 2611.27, ["spawnZ"] = 38.45, ["info"] = "Garage: Senora Way"},
    [5] = { ["X"] = 2575.463, ["Y"] = -297.27, ["Z"] = 93.078, ["spawnX"] = 2533.33, ["spawnY"] = -279.83, ["spawnZ"] = 92.46, ["info"] = "Garage: Sustancia"},
    [6] = { ["X"] = 1037.83, ["Y"] = -763.93, ["Z"] = 57.913, ["spawnX"] = 1040.24, ["spawnY"] = -773.45, ["spawnZ"] = 58.02, ["info"] = "Garage: Mirror Park"},
    [7] = { ["X"] = 82.368, ["Y"] = 6418.86, ["Z"] = 31.478, ["spawnX"] = 75.591, ["spawnY"] = 6386.12, ["spawnZ"] = 31.23, ["info"] = "Garage: Paleto"},
    [8] = { ["X"] = -794.57, ["Y"] = -2020.84, ["Z"] = 8.8, ["spawnX"] = -779.279, ["spawnY"] = -2030.52, ["spawnZ"] = 8.8, ["info"] = "Garage: Autopia Parkway"},
    [9] = { ["X"] = 1507.858, ["Y"] = 3767.301, ["Z"] = 34.137, ["spawnX"] = 1501.2, ["spawnY"] = 3762.19, ["spawnZ"] = 33.0, ["info"] = "Garage: Sandy Shores"},
    [10] = { ["X"] = -1009.94, ["Y"] = -2746.25, ["Z"] = 13.758, ["spawnX"] = -977.2166, ["spawnY"] = -2710.3798, ["spawnZ"] = 12.8534, ["info"] = "Garage: Airport"},
    [11] = { ["X"] = 483.925, ["Y"] =  -1312.05, ["Z"] = 29.21, ["spawnX"] = 490.942, ["spawnY"] = -1313.067, ["spawnZ"] = 27.964, ["info"] = "Garage Impound Lot"},
    [12] = { ["X"] = -1464.421, ["Y"] =  -500.361, ["Z"] = 32.961, ["spawnX"] = -1475.102, ["spawnY"] = -502.428, ["spawnZ"] = 32.806, ["info"] = "Garage: Del Perro"},
    [13] = { ["X"] = -294.10827636719, ["Y"] =  -987.10534667969, ["Z"] = 31.080961227417, ["spawnX"] = -297.8857421875, ["spawnY"] = -990.28344726563, ["spawnZ"] = 31.080961227417, ["info"] = "Garage: Alta Street"},
    [14] = { ["X"] = -25.373727798462, ["Y"] =  -1086.2159423828, ["Z"] = 26.574460983276, ["spawnX"] = -13.856682777405, ["spawnY"] = -1090.0783691406, ["spawnZ"] = 26.672624588013, ["info"] = "Garage: Premium Deluxe Motorsport"}
}

exports("getGarageLocations", function(cb)
    cb(garageLocations)
end)