BankRobberyPdRequired = 3
CitPdRequired = 3
StoreRobberyPdRequired = 2
StoreCashRegister = 2
JewelryRobberyPdRequired = 3

function fetchRequiredPdForJob(var)
    if var == 'BankRobberyPdRequired' then
        return BankRobberyPdRequired
    end
    if var == 'CitPdRequired' then
        return CitPdRequired
    end
    if var == 'StoreRobberyPdRequired' then
        return StoreRobberyPdRequired
    end
    if var == 'StoreCashRegister' then
        return StoreCashRegister
    end
    if var == 'JewelryRobberyPdRequired' then
        return JewelryRobberyPdRequired
    end
    return 1 -- default to at least 1 cop
end

exports('fetchRequiredPdForJob', fetchRequiredPdForJob)