ServerCallbacks = {}
CurrentRequestId = 1

RegisterNetEvent('nsrp_u:serverCallback')
AddEventHandler('nsrp_u:serverCallback', function(requestId, ...)
	ServerCallbacks[requestId](...)
	ServerCallbacks[requestId] = nil
end)
exports('TriggerServerCallback', function (name, cb, ...)
	ServerCallbacks[CurrentRequestId] = cb

	TriggerServerEvent('nsrp_u:triggerServerCallback', name, CurrentRequestId, ...)

	if CurrentRequestId < 65535 then
		CurrentRequestId = CurrentRequestId + 1
	else
		CurrentRequestId = 0
	end
end)