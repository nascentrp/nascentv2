local ETHICAL = ETHICAL or {}

RegisterNetEvent("nsrp_util:spawnVehicle")
AddEventHandler("nsrp_util:spawnVehicle", function(model, x,y,z, cb, heading)
    spawnVehicle(model, x,y,z, cb, heading)
end)

exports('spawnVehicle', function(model, x,y,z, cb, heading, numberplate)
    spawnVehicle(model, x,y,z, cb, heading, false, numberplate)
end)
exports('spawnLocalVehicle', function(model, x,y,z, cb, heading)
    spawnVehicle(model, x,y,z, cb, heading, true)
end)
function spawnVehicle(model, x,y,z, cb, heading, client_only, numberplate)
    Citizen.CreateThread(function()
        local hash = GetHashKey(model)

        if not IsModelAVehicle(hash) then return end
        if not IsModelInCdimage(hash) or not IsModelValid(hash) then return end
        
        RequestModel(hash)
        
        while not HasModelLoaded(hash) do
            Citizen.Wait(0)
        end

        local localped = PlayerPedId()
        local coords = vector3(x,y,z)
        local vehicle
        local isServer = true
        if client_only ~= nil then 
            isServer = not client_only
        end
        if heading ~= nil then
            vehicle = CreateVehicle(hash, coords, heading, true, false)
        else
            vehicle = CreateVehicle(hash, coords, GetEntityHeading(localped), true, false)
        end
        if numberplate ~= nil then
            SetVehicleNumberPlateText(vehicle, numberplate)
        end
        SetVehicleModKit(vehicle, 0)
        SetVehicleMod(vehicle, 11, 3, false)
        SetVehicleMod(vehicle, 12, 2, false)
        SetVehicleMod(vehicle, 13, 2, false)
        SetVehicleMod(vehicle, 15, 3, false)
        SetVehicleMod(vehicle, 16, 4, false)

        local plate = GetVehicleNumberPlateText(vehicle)
        TriggerEvent("keys:addNew",vehicle,plate)
        TriggerServerEvent('garages:addJobPlate', plate)
        SetModelAsNoLongerNeeded(hash)
        
        SetVehicleDirtLevel(vehicle, 0)
        SetVehicleWindowTint(vehicle, 0)

        -- if livery ~= nil then
        --     SetVehicleLivery(vehicle, tonumber(livery))
        -- end
        if cb ~= nil then
            cb(vehicle)
        end
    end)
end

RegisterNetEvent("nsrp_util:deleteVehicle")
AddEventHandler("nsrp_util:deleteVehicle", function(vehicle)
    spawnVehicle(vehicle)
end)
exports('deleteVehicle', function(vehicle)
    spawnVehicle(vehicle)
end)
function deleteVehicle(vehicle)
	SetEntityAsMissionEntity(vehicle, false, true)
	DeleteVehicle(vehicle)
end


function makeBlip(id, name, x, y, z, sprite, color, scale) -- VOID
    -- if scale == nil then
    --     scale = 0.7
    -- end
    -- if color == nil then
    --     color = 1
    -- end
    -- if sprite == nil then
    --     sprite = 67
    -- end
    -- ETHICAL.BlipManager:CreateBlip(id, {
    --     id = id,
    --     name = name,
    --     scale = scale,
    --     sprite = sprite,
    --     color = color,
    --     x = x,
    --     y = y,
    --     z = z,
    -- })
end

local blips = {
    {id = "pcenter", name = "Payments & Internet Center", scale = 0.7, sprite = 351, color = 17, x=-1081.8293457031, y=-248.12872314453, z=37.763294219971},
    {id = "Bennys", name = "Bennys Motor Works", scale = 0.7, color = 1, sprite = 72, x=-1161.449, y=-2017.087, z=12.685},
    {id = "Auto Exotics", name = "Auto Exotics", scale = 0.7, color = 1, sprite = 446, x=535.5615234375, y=-179.61291503906, z=54.364429473877},
    {id = "Auto Bodies", name = "Auto Bodies", scale = 0.7, color = 1, sprite = 446, x=-1417.1550292969, y=-446.43911743164, z=35.909713745117},
    {id = "Tuner Shop", name = "Tuner Shop", scale = 0.7, color = 1, sprite = 446, x=937.23828125, y=-970.89343261719, z=39.543106079102},
    {id = "Paleto Performance", name = "Paleto Performance", scale = 0.7, color = 1, sprite = 446, x=108.35273742676, y=6625.1982421875, z= 31.787202835083},
    {id = "Burger Shot", name = "Burger Shot", scale = 0.7, color = 21, sprite = 106, x=-1191.6701660156, y=-889.74584960938, z= 14.508341789246},
    {id = "truckjob1", name = "Delivery Garage", scale = 0.7, color = 17, sprite = 67, x =165.22, y=-28.38,  z=67.94},
    {id = "weazel", name = "Weazel News", scale = 0.7, color = 6, sprite = 47, x = -598.248046875, y= -929.77307128906, z= 23.869129180908},
    {id = "taxi", name = "Taxi HQ", scale = 0.7, color = 46, sprite = 198, x = 895.32, y= -179.28, z= 23.86}, 
    {id = "beanmachine", name = "Bean Machine", scale = 0.7, color = 44, sprite = 536, x = -629.541015625, y= 233.69226074219, z= 81.881462097168},
    {id = "licenseshop", name = "License Shop", scale = 0.7, color = 0, sprite = 77, x = -550.56, y=-192.52, z= 38.219},
    {id = "courthouse", name = "Los Santos Courthouse", scale = 0.7, color = 5, sprite = 419, x=-543.438, y=-207.164, z=198.36},
    {id = "prison_cafe", name = "Prison Cafeteria", scale = 0.7, color = 0, sprite = 273, x=1782.12, y=2552.01, z=45.6726},
    {id = "prison_cells", name = "Prison Cells", scale = 0.7, color = 0, sprite = 285, x=1752.450, y=2493.318, z=50.423},
    --   {id = "bestbuds", name = "BestBuds", scale = 0.7, color = 61, sprite = 140, x = -1174.2535400391, y= -1573.9865722656, z=4.3736281394958},
}

-- local markers = {

-- }

local peds = {
    { model = 'ig_siemonyetarian', x =-33.224678039551, y=-1102.1027832031, z=26.422351837158, h = 1.0}
}

AddEventHandler("ethical-base:playerSessionStarted", function()
        for k,v in ipairs(blips) do
            ETHICAL.BlipManager:CreateBlip(v.id, v)
        end
        for k,v in ipairs(peds) do
            local modelHash = GetHashKey(v.model)
            RequestModel(modelHash)
            while not HasModelLoaded(modelHash) do
                Wait(1)
            end
            CreatePed(0, modelHash, v.x, v.y, v.z, v.h, false, false) 
        end
        -- while true do
        --     DrawMarker(20, -33.224678039551, -1102.1027832031, 26.422351837158, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.5, 255, 0, 0, 25, false, true, 2, nil, nil, false)
        --     Wait(0)
        -- end
        
        -- Citizen.CreateThread(function()
        --     for k,v in ipairs(markers) do
        --         DrawMarker(20, v.x, v.y, v.z, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.5, 255, 0, 0, 25, false, true, 2, nil, nil, false)
        --         Wait(0)
        --     end
        -- end)
end)
