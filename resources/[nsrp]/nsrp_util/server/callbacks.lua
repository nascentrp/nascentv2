ServerCallbacks = {}
CurrentRequestId = 0

local RegisterServerCallback = function(name, cb)
	ServerCallbacks[name] = cb
end

TriggerServerCallback = function(name, requestId, source, cb, ...)
	if ServerCallbacks[name] then
		ServerCallbacks[name](source, cb, ...)
	else
		print(('[^3WARNING^7] Server callback "%s" does not exist. Make sure that the server sided file really is loading, an error in that file might cause it to not load.'):format(name))
	end
end

RegisterServerEvent('nsrp_u:triggerServerCallback')
AddEventHandler('nsrp_u:triggerServerCallback', function(name, requestId, ...)
	local playerId = source

	TriggerServerCallback(name, requestId, playerId, function(...)
		TriggerClientEvent('nsrp_u:serverCallback', playerId, requestId, ...)
	end, ...)
end)

exports('RegisterServerCallback', RegisterServerCallback)
exports('TriggerServerCallback', TriggerServerCallback)