

-- bhost2ent
table.insert(Config.DoorList, {
	lockpick = false,
	authorizedJobs = { ['burgershot']=0 },
	maxDistance = 2.5,
	slides = false,
	audioRemote = false,
	doors = {
		{objHash = -1475798232, objHeading = 124.02835845947, objCoords = vector3(-1184.892, -883.3377, 14.25113)},
		{objHash = 1517256706, objHeading = 124.02835845947, objCoords = vector3(-1183.207, -885.8312, 14.25113)}
 },
	locked = true,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})