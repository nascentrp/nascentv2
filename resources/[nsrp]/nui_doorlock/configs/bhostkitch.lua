

-- bhostkitch
table.insert(Config.DoorList, {
	maxDistance = 2.0,
	objHeading = 124.02838897705,
	locked = true,
	lockpick = false,
	garage = false,
	objCoords = vector3(-1195.271, -897.9354, 14.24888),
	fixText = false,
	objHash = -1253427798,
	audioRemote = false,
	authorizedJobs = { ['burgershot']=0 },
	slides = false,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})