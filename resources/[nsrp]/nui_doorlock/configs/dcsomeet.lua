

-- dcsomeet
table.insert(Config.DoorList, {
	lockpick = false,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	slides = false,
	maxDistance = 2.5,
	audioRemote = false,
	locked = true,
	doors = {
		{objHash = -667323357, objHeading = 320.0, objCoords = vector3(359.9926, -1598.033, 33.5079)},
		{objHash = -667323357, objHeading = 139.99998474121, objCoords = vector3(358.0049, -1596.365, 33.5079)}
 },		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})