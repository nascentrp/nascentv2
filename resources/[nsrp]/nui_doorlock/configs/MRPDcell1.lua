

-- MRPDcell1
table.insert(Config.DoorList, {
	garage = false,
	objHash = 631614199,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	slides = false,
	objHeading = 0.0,
	fixText = false,
	maxDistance = 2.0,
	objCoords = vector3(464.5701, -992.6641, 25.06443),
	locked = true,
	lockpick = false,
	audioRemote = false,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})

-- MRPDcell1
table.insert(Config.DoorList, {
	garage = false,
	objHash = 631614199,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	slides = false,
	objHeading = 270.0,
	fixText = false,
	maxDistance = 2.0,
	objCoords = vector3(461.8065, -994.4086, 25.06443),
	locked = true,
	lockpick = false,
	audioRemote = false,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})