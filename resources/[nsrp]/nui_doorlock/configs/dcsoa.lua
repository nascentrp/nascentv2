

-- dcsoa
table.insert(Config.DoorList, {
	objCoords = vector3(364.647, -1601.292, 29.43974),
	slides = false,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	fixText = false,
	objHash = -1925177820,
	maxDistance = 2.0,
	audioRemote = false,
	lockpick = false,
	objHeading = 139.99998474121,
	locked = true,
	garage = false,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})