

-- dcsoa2
table.insert(Config.DoorList, {
	objCoords = vector3(371.9657, -1603.393, 29.4418),
	slides = false,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	fixText = false,
	objHash = -1925177820,
	maxDistance = 2.0,
	audioRemote = false,
	lockpick = false,
	objHeading = 320.0,
	locked = true,
	garage = false,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})