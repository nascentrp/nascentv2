

-- MRPDexit
table.insert(Config.DoorList, {
	doors = {
		{objHash = -2023754432, objHeading = 179.99998474121, objCoords = vector3(469.9679, -1014.452, 26.53623)},
		{objHash = -2023754432, objHeading = 0.0, objCoords = vector3(467.3716, -1014.452, 26.53623)}
 },
	slides = false,
	authorizedJobs = { ['police ']=0, ['sheriff']=0 },
	maxDistance = 2.5,
	lockpick = false,
	audioRemote = false,
	locked = true,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})