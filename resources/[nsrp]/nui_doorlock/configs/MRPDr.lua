

-- MRPDroof
table.insert(Config.DoorList, {
	audioRemote = false,
	maxDistance = 2.0,
	authorizedJobs = { ['police ']=0, ['sheriff']=0 },
	locked = true,
	lockpick = false,
	slides = false,
	garage = false,
	objCoords = vector3(461.2865, -985.3206, 30.83926),
	objHeading = 89.999992370605,
	fixText = false,
	objHash = 749848321,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})