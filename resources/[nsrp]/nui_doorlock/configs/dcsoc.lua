

-- dcsoc
table.insert(Config.DoorList, {
	objCoords = vector3(349.4737, -1600.773, 29.6148),
	slides = false,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	fixText = false,
	objHash = -1842288246,
	maxDistance = 2.0,
	audioRemote = false,
	lockpick = false,
	objHeading = 320.0,
	locked = true,
	garage = false,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})