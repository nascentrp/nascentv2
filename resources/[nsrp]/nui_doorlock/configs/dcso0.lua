

-- dcso0
table.insert(Config.DoorList, {
	objHeading = 139.99998474121,
	maxDistance = 2.0,
	garage = false,
	lockpick = false,
	locked = true,
	slides = false,
	objHash = -944738487,
	audioRemote = false,
	fixText = false,
	objCoords = vector3(364.1677, -1603.649, 33.63295),
	authorizedJobs = { ['police']=0, ['sheriff']=0 },		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})