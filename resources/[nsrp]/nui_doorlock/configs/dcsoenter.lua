

-- dcsoenter
table.insert(Config.DoorList, {
	doors = {
		{objHash = 471928866, objHeading = 320.0, objCoords = vector3(354.8611, -1592.875, 29.43909)},
		{objHash = -1360054856, objHeading = 320.0, objCoords = vector3(353.2121, -1594.865, 29.44267)}
 },
	maxDistance = 2.5,
	slides = false,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	audioRemote = false,
	locked = true,
	lockpick = false,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})