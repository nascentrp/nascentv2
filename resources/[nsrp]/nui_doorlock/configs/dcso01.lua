

-- dcso01
table.insert(Config.DoorList, {
	objHeading = 320.0,
	maxDistance = 2.0,
	garage = false,
	lockpick = false,
	locked = true,
	slides = false,
	objHash = 782481772,
	audioRemote = false,
	fixText = false,
	objCoords = vector3(366.2662, -1593.286, 33.63173),
	authorizedJobs = { ['police']=0, ['sheriff']=0 },		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})