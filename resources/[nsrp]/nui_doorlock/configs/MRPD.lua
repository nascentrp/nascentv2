

-- MRPDFront
table.insert(Config.DoorList, {
	lockpick = false,
	slides = false,
	audioRemote = false,
	locked = true,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	maxDistance = 2.5,
	doors = {
		{objHash = -1215222675, objHeading = 270.0, objCoords = vector3(434.7479, -980.6184, 30.83926)},
		{objHash = 320433149, objHeading = 270.0, objCoords = vector3(434.7479, -983.2151, 30.83926)}
 },		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})