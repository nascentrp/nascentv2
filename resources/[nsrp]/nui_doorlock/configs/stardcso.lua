

-- stairdcso
table.insert(Config.DoorList, {
	objHeading = 49.999984741211,
	maxDistance = 2.0,
	garage = false,
	lockpick = false,
	locked = true,
	slides = false,
	objHash = -1925177820,
	audioRemote = false,
	fixText = false,
	objCoords = vector3(384.3159, -1604.146, 29.4411),
	authorizedJobs = { ['police']=0, ['sheriff']=0 },		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})