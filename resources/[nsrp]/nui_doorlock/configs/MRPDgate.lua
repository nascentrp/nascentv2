

-- MRPDgate
table.insert(Config.DoorList, {
	objHeading = 90.0,
	maxDistance = 6.0,
	lockpick = false,
	objCoords = vector3(488.8948, -1017.212, 27.14935),
	audioRemote = false,
	fixText = false,
	slides = true,
	objHash = -1603817716,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	garage = false,
	locked = true,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})