

-- MRPDroof2
table.insert(Config.DoorList, {
	audioRemote = false,
	maxDistance = 2.0,
	authorizedJobs = { ['police ']=0, ['sheriff']=0 },
	locked = true,
	lockpick = false,
	slides = false,
	garage = false,
	objCoords = vector3(464.3613, -984.678, 43.83443),
	objHeading = 89.999992370605,
	fixText = false,
	objHash = -340230128,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})