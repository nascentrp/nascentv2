

-- bshotem
table.insert(Config.DoorList, {
	objHash = -1253427798,
	slides = false,
	audioRemote = false,
	garage = false,
	maxDistance = 2.0,
	objHeading = 34.028408050537,
	lockpick = false,
	fixText = false,
	objCoords = vector3(-1200.971, -892.8766, 14.24888),
	locked = true,
	authorizedJobs = { ['burgershot']=0 },		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})