

-- MRPDChief
table.insert(Config.DoorList, {
	objHeading = 180.00012207031,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	objCoords = vector3(446.5728, -980.0106, 30.8393),
	lockpick = false,
	fixText = false,
	audioRemote = false,
	garage = false,
	objHash = -1320876379,
	maxDistance = 2.0,
	slides = false,
	locked = true,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})

-- MRPDc
table.insert(Config.DoorList, {
	lockpick = false,
	slides = false,
	audioRemote = false,
	doors = {
		{objHash = 185711165, objHeading = 0.0, objCoords = vector3(446.0079, -989.4454, 30.8393)},
		{objHash = 185711165, objHeading = 179.99998474121, objCoords = vector3(443.4078, -989.4454, 30.8393)}
 },
	maxDistance = 2.5,
	authorizedJobs = { ['police ']=0, ['sheriff']=0 },
	locked = true,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})