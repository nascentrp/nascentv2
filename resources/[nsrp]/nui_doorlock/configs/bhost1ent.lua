

-- bhost1ent
table.insert(Config.DoorList, {
	lockpick = false,
	authorizedJobs = { ['burgershot']=0 },
	maxDistance = 2.5,
	slides = false,
	audioRemote = false,
	doors = {
		{objHash = 1517256706, objHeading = 214.02838134766, objCoords = vector3(-1196.54, -883.4852, 14.25259)},
		{objHash = -1475798232, objHeading = 214.02838134766, objCoords = vector3(-1199.033, -885.1699, 14.25259)}
 },
	locked = true,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})