

-- MRPDarmory
table.insert(Config.DoorList, {
	objHeading = 270.0,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	objCoords = vector3(453.0793, -983.1895, 30.83926),
	lockpick = false,
	fixText = false,
	audioRemote = false,
	garage = false,
	objHash = 749848321,
	maxDistance = 2.0,
	slides = false,
	locked = true,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})

-- MRPDarmory
table.insert(Config.DoorList, {
	objHeading = 89.999992370605,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	objCoords = vector3(450.1041, -985.7384, 30.8393),
	lockpick = false,
	fixText = false,
	audioRemote = false,
	garage = false,
	objHash = 1557126584,
	maxDistance = 2.0,
	slides = false,
	locked = true,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})

-- MRPDarmory
table.insert(Config.DoorList, {
	objHeading = 179.99998474121,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	objCoords = vector3(452.6248, -987.3626, 30.8393),
	lockpick = false,
	fixText = false,
	audioRemote = false,
	garage = false,
	objHash = -2023754432,
	maxDistance = 2.0,
	slides = false,
	locked = true,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})