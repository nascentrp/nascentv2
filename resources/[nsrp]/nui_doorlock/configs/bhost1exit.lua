

-- bhost2exit
table.insert(Config.DoorList, {
	maxDistance = 2.0,
	objHeading = 304.02841186523,
	locked = true,
	lockpick = false,
	garage = false,
	objCoords = vector3(-1199.296, -904.0258, 14.05047),
	fixText = false,
	objHash = -1877571861,
	audioRemote = false,
	authorizedJobs = { ['burgershot']=0 },
	slides = false,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})