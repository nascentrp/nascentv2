

-- dcso02
table.insert(Config.DoorList, {
	objHeading = 320.0,
	maxDistance = 2.0,
	garage = false,
	lockpick = false,
	locked = true,
	slides = false,
	objHash = 167807608,
	audioRemote = false,
	fixText = false,
	objCoords = vector3(370.7128, -1587.973, 33.63252),
	authorizedJobs = { ['police']=0, ['sheriff']=0 },		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})