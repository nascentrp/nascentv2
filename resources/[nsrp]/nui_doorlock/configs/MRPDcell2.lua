

-- MRPDcell2
table.insert(Config.DoorList, {
	objHeading = 89.999992370605,
	maxDistance = 2.0,
	garage = false,
	objHash = 631614199,
	fixText = false,
	slides = false,
	lockpick = false,
	objCoords = vector3(461.8065, -997.6583, 25.06443),
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	audioRemote = false,
	locked = true,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})