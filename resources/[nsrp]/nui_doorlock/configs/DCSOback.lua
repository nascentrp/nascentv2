

-- DCSOback
table.insert(Config.DoorList, {
	locked = true,
	maxDistance = 2.5,
	slides = false,
	lockpick = false,
	authorizedJobs = { ['police']=0, ['sheriff']=0 },
	doors = {
		{objHash = -1047370197, objHeading = 320.0, objCoords = vector3(368.2753, -1608.238, 29.44148)},
		{objHash = 1738519111, objHeading = 320.0, objCoords = vector3(369.9344, -1606.261, 29.44148)}
 },
	audioRemote = false,		
	-- oldMethod = true,
	-- audioLock = {['file'] = 'metal-locker.ogg', ['volume'] = 0.6},
	-- audioUnlock = {['file'] = 'metallic-creak.ogg', ['volume'] = 0.7},
	-- autoLock = 1000
})