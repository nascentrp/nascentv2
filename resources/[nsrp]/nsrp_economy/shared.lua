fish = {
    [1] = 'fishingcod',
    [2] = 'fishingflounder',
    [3] = 'fishingmackerel',
    [4] = 'fishingbluefish',
}

fishCutValue = {
    ['fishingcod'] = 1,
    ['fishingflounder'] = 2,
    ['fishingmackerel'] = 4,
    ['fishingbluefish'] = 8
}

badFish = {
    [1] = 'fishingboot',
    [2] = 'fishinglog',
    [3] = 'fishingtin',
    [4] = '-102323637', -- broken bottle
    [5] = 'fishingchest',
    [6] = 'fishinglockbox',
    [7] = 'pix1'
}