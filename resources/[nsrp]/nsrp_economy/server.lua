function payoutForWorkBasic(src, withoutCash) -- scuba payout
    local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local char = user:getCurrentCharacter()
    if withoutCash == nil then
        local randomMoney = math.random(1,8)
        user:addMoney(randomMoney)
    end
    local chanceofRareDrop = math.random(1, 80)
    -- local recycleMaterialCount = math.random(100,200)
    if chanceofRareDrop == 40 then
        TriggerClientEvent('player:receiveItem', src, 'decryptersess', 1)
    end
end

exports('payoutForWorkBasic', function(src, withoutCash)
    --print("payout for basic work received")
    payoutForWorkBasic(src, withoutCash)
end)


RegisterServerEvent('nsrp_economy:cutFish')
AddEventHandler('nsrp_economy:cutFish', function(fishName)
    local count = fishCutValue[fishName]
    local source = tonumber(source)
    TriggerClientEvent("player:receiveItem", source, "cutfish", count)
end)

function fishingLoot(src)
    local naughtyOrNice = math.random(1,50)
    if naughtyOrNice ~= 5 then -- nice
        local whichFish = math.random(1, #fish)
        if whichFish > 2 and math.random(1,2) == 1 then -- if it's a rare fish then we do a roll for a chance to lose it
            whichFish = math.random(1,2)
        end
        TriggerClientEvent('player:receiveItem', src, fish[whichFish], 1)
    else
        TriggerClientEvent('player:receiveItem', src, badFish[math.random(1, #badFish)], 1)
    end        
end

RegisterServerEvent('nsrp_economy:payoutForFishingCatch')
AddEventHandler('nsrp_economy:payoutForFishingCatch', function()
    local src = source
    fishingLoot(src)
end)

-- Car Search Loot
local ew = {
    [1] = 'humantongue',
    [2] = 'humantooth',
    [3] = 'humaneye',
    [4] = 'humanintestines',
    [5] = 'humankidney',
    [6] = 'humanliver',
    [7] = 'humanbone',
    [8] = 'humanbones',
    [9] = 'humanheart',
    [10] = 'humanfinger',
    [11] = 'humanskull',
    [12] = 'humanskull',
    [13] = 'humanpancreas',
}

local genericLoot = {
    [1] = 'foodingredient',
    [2] = 'fabric',
    [3] = 'plastic',
    [4] = 'recyclablematerial',
    [5] = 'ironbar',
    [6] = 'key1',
    [7] = 'pix1'
}

function payoutForCarSearch(src, luckyNumber)
    if luckyNumber < 10 then
        return
    end
    local myLuckyNumber = math.random(1, 80)
    if myLuckyNumber ~= luckyNumber then
        return
    end
    local naughtyOrNice = math.random(1,3)
    if naughtyOrNice > 1 then -- nice
        local howNice = math.random(1,10)
        if howNice == 10 then
            TriggerClientEvent('player:receiveItem', src, 'band', math.random(1,3)) -- dirty money found
        else
            local item = math.random(1, #genericLoot)
            TriggerClientEvent('player:receiveItem', src, genericLoot[item], 1)
        end
    end
    if naughtyOrNice == 1 then -- naughty
        local item = math.random(1, #ew)
        TriggerClientEvent('player:receiveItem', src, ew[item], 1)
    end
    return
end

RegisterServerEvent('nsrp_economy:payoutForCarSearch')
AddEventHandler('nsrp_economy:payoutForCarSearch', function(luckyNumber)
    local src = source
    payoutForCarSearch(src, luckyNumber)
end)