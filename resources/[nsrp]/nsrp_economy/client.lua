-- cashstack comes from bank
-- cashroll comes from bank
-- rollcash comes from drugs
-- band comes from drugs


local bandprice = 463 -- price is per
local bandcount = 10 -- count is minimum required to sell

local rollprice = 265
local rollcount = 10

local cashrollprice = 265
local cashrollcount = 10

local cashstackprice = 265
local cashstackcount = 10

local markedbillsprice = 265
local markedbillscount = 10

local currentlyCuttingFish = false

items = {
    ["rollcash"] = {
        count = 10,
        price = 1000
    },
    ["band"] = {
        count = 10,
        price = 3000
    },
    ["cashroll"] = {
        count = 10,
        price = 3000
    },
    ["cashstack"] = {
        count = 10,
        price = 4000
    },
    ["markedbills"] = {
        count = 1,
        price = 4000
    }
}

local inkedmoneybagprice = 0 -- item not used yet

local mafiaMultiplier = 2 -- mafia take 50% cut (not really, but we take it anyway)


RegisterNetEvent('nsrp_economy:dropOxyForOxyRun')
AddEventHandler('nsrp_economy:dropOxyForOxyRun', function() -- logic to change once balanced
    local drop = false
    if math.random(7) == 5 then
        drop = true
    end
    if math.random(100) > 35 then
        drop = true
    end
    if drop then
        TriggerEvent( "player:receiveItem", "oxy", math.random(1,3))
    end
end)

RegisterNetEvent('nsrp_economy:washmoney')
AddEventHandler('nsrp_economy:washmoney', function(thankYouString, initialPayment, failString, paymentEvent)
    local thanksString = thankYouString
    if initialPayment == nil then
        initialPayment = 0
    end
    local payment = initialPayment
    local addedBonus = 0


    local is_Mafia = exports["isPed"]:isPed("isMafia")

    local success = false
    for item, v in pairs(items) do
        if exports["ethical-inventory"]:hasEnoughOfItem(item,v.count) and not success then
            TriggerEvent("inventory:removeItem", item, v.count)   
            if is_Mafia == 1 then
                addedBonus = v.price
            else
                addedBonus = v.price/mafiaMultiplier
            end
            success = true
        end
    end
    if not success then
        TriggerEvent("DoLongHudText",failString)
    else
        if isMafia == 1 then
            TriggerEvent("DoLongHudText","Thank you. Here's your gang bonus.")
        else
            TriggerEvent("DoLongHudText",thanksString)
        end
    end
    payment = payment + addedBonus
    TriggerServerEvent(paymentEvent, payment)
end)

RegisterNetEvent('nsrp_economy:cutFishingLoot')
AddEventHandler('nsrp_economy:cutFishingLoot', function()
    cutFishingLoot()
end)

function cutFishingLoot()
    local foundValidFish = false
    if currentlyCuttingFish == true then
        return
    end
    print("here")
    for i = 1, #fish do
        local fishName = fish[i]
        if exports["ethical-inventory"]:getQuantity(fishName) >= 1 then
            foundValidFish = true
            TriggerEvent('inventory:removeItem', fishName, 1)
            currentlyCuttingFish = true
            local finished = exports["ethical-taskbar"]:taskBar(4000,"Cutting")
            if finished == 100 then
                currentlyCuttingFish = false
                ClearPedTasksImmediately(PlayerPedId())
                Citizen.Wait(1000)
                TriggerServerEvent('nsrp_economy:cutFish', fishName)
                TriggerEvent("fishing:clearCuttingProcess")
                return
            else
                currentlyCuttingFish = false
            end
        end
    end
    if not foundValidFish then
        TriggerEvent('DoLongHudText', 'You need to have fish to cut.', 2)
    end
    currentlyCuttingFish = false
end

