local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local CurrentAction     = nil
local CurrentActionMsg  = nil
local CurrentActionData = nil
local Licenses          = {}
local CurrentTest       = nil
local CurrentTestType   = nil
local CurrentVehicle    = nil
local CurrentCheckPoint = 0
local LastCheckPoint    = -1
local CurrentBlip       = nil
local CurrentZoneType   = nil
local DriveErrors       = 0
local IsAboveSpeedLimit = false
local LastVehicleHealth = nil

function DrawMissionText(msg, time)
  ClearPrints()
  SetTextEntry_2('STRING')
  AddTextComponentString(msg)
  DrawSubtitleTimed(time, 1)
end

function StartTheoryTest()

  CurrentTest = 'theory'

  SendNUIMessage({
    openQuestion = true
  })

  SetNuiFocus(true, true)
  TriggerServerEvent('esx_dmvschool:pay', Config.Prices['Driver'])

end

function StopTheoryTest(success)

  CurrentTest = nil

  SendNUIMessage({
    openQuestion = false
  })

  SetNuiFocus(false)

  if success then
    TriggerServerEvent('esx_dmvschool:addLicense', 'Learners')
    
    TriggerEvent("customNotification",'you passed the test')
    --TriggerServerEvent("account:information:sv", exports['isPed']:isPed('cid'))
  else
    TriggerEvent("customNotification",'you failed the test')
  end

end

function StartDriveTest(type)
  exports['nsrp_util']:spawnVehicle(Config.VehicleModels[type], Config.Zones.VehicleSpawnPoint.Pos.x, Config.Zones.VehicleSpawnPoint.Pos.y, Config.Zones.VehicleSpawnPoint.Pos.z, function(vehicle) 
    CurrentTest       = 'drive'
    CurrentTestType   = type
    CurrentCheckPoint = 0
    LastCheckPoint    = -1
    CurrentZoneType   = 'residence'
    DriveErrors       = 0
    IsAboveSpeedLimit = false
    CurrentVehicle    = vehicle
    LastVehicleHealth = GetEntityHealth(vehicle)
    playerPed   = GetPlayerPed(-1)
    TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
  end)
  TriggerServerEvent('esx_dmvschool:pay', Config.Prices[type])
end

function StopDriveTest(success)
  local vehicle = GetVehiclePedIsIn(PlayerPedId(), false)
  if success then
    TriggerServerEvent('esx_dmvschool:addLicense', 'Driver')
    TriggerEvent("customNotification",'You passed the test')
  else
    TriggerEvent("customNotification",'You failed the test')
  end
  if vehicle ~= 0 then
    DeleteEntity(vehicle)
  end
  CurrentTest     = nil
  CurrentTestType = nil
end

function SetCurrentZoneType(type)
  CurrentZoneType = type
end

function OpenDMVSchoolMenu()

  local ownedLicenses = {}

  for i=1, #Licenses, 1 do
    ownedLicenses[Licenses[i].type] = true
  end

  print("owned")
  print(json.encode(Licenses))
  if not ownedLicenses['Driver'] then
    StartDriveTest('Driver')
  end

  --StartDriveTest('Driver')
  -- if ownedLicenses['Learners'] then
  --   StartDriveTest('Driver')
  -- else
  --   StartTheoryTest()
  -- end
    --   if data.current.value == 'theory_test' then
    --     menu.close()
    --   end

    --   if data.current.value == 'drive_test' then
    --     menu.close()
    --     StartDriveTest(data.current.type)
    --   end

    -- end,
   

end

RegisterNetEvent("ethical-base:characterLoaded")
AddEventHandler("ethical-base:characterLoaded", function()
  TriggerServerEvent('nsrp_license:loadCharacterDriversLicense')
end)
RegisterNUICallback('question', function(data, cb)

  SendNUIMessage({
    openSection = 'question'
  })

  cb('OK')
end)

RegisterNUICallback('close', function(data, cb)
  StopTheoryTest(true)
  cb('OK')
end)

RegisterNUICallback('kick', function(data, cb)
  StopTheoryTest(false)
  cb('OK')
end)


AddEventHandler('esx_dmvschool:hasEnteredMarker', function(zone)

  if zone == 'DMVSchool' then

    CurrentAction     = 'dmvschool_menu'
    CurrentActionMsg  = 'press ~INPUT_CONTEXT~ to open the menu'
    CurrentActionData = {}

  end

end)

AddEventHandler('esx_dmvschool:hasExitedMarker', function(zone)
  CurrentAction = nil
  -- ESX.UI.Menu.CloseAll()
end)

-- Create Blips
Citizen.CreateThread(function()

  local blip = AddBlipForCoord(Config.Zones.DMVSchool.Pos.x, Config.Zones.DMVSchool.Pos.y, Config.Zones.DMVSchool.Pos.z)

  SetBlipSprite (blip, 408)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 0.7)
  SetBlipAsShortRange(blip, true)

  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString('Driving School')
  EndTextCommandSetBlipName(blip)

end)

-- Display markers
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(0)

    local coords = GetEntityCoords(GetPlayerPed(-1))

    for k,v in pairs(Config.Zones) do
      if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
        DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
      end
    end

  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(0)

    local coords      = GetEntityCoords(GetPlayerPed(-1))
    local isInMarker  = false
    local currentZone = nil

    for k,v in pairs(Config.Zones) do
      if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
        isInMarker  = true
        currentZone = k
      end
    end

    if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
      HasAlreadyEnteredMarker = true
      LastZone                = currentZone
      TriggerEvent('esx_dmvschool:hasEnteredMarker', currentZone)
    end

    if not isInMarker and HasAlreadyEnteredMarker then
      HasAlreadyEnteredMarker = false
      TriggerEvent('esx_dmvschool:hasExitedMarker', LastZone)
    end

  end
end)

-- Block UI
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(0)

    if CurrentTest == 'theory' then

      local playerPed = GetPlayerPed(-1)

      DisableControlAction(0, 1, true) -- LookLeftRight
      DisableControlAction(0, 2, true) -- LookUpDown
      DisablePlayerFiring(playerPed, true) -- Disable weapon firing
      DisableControlAction(0, 142, true) -- MeleeAttackAlternate
      DisableControlAction(0, 106, true) -- VehicleMouseControlOverride

    end

  end
end)

-- Key Controls
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(0)

    if CurrentAction ~= nil then

      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlJustReleased(0,  Keys['E']) then

        if CurrentAction == 'dmvschool_menu' then
          OpenDMVSchoolMenu()
        end

        CurrentAction = nil

      end

    end

  end
end)

-- Drive test
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(0)

    if CurrentTest == 'drive' then

      local playerPed      = GetPlayerPed(-1)
      local coords         = GetEntityCoords(playerPed)
      local nextCheckPoint = CurrentCheckPoint + 1

      if Config.CheckPoints[nextCheckPoint] == nil then
                  if DoesBlipExist(CurrentBlip) then
          RemoveBlip(CurrentBlip)
        end

        CurrentTest = nil

        TriggerEvent("customNotification",'driving test completed')

        if DriveErrors < Config.MaxErrors then
          StopDriveTest(true)
        else
          StopDriveTest(false)
        end
              else

        if CurrentCheckPoint ~= LastCheckPoint then

          if DoesBlipExist(CurrentBlip) then
            RemoveBlip(CurrentBlip)
          end

          CurrentBlip = AddBlipForCoord(Config.CheckPoints[nextCheckPoint].Pos.x, Config.CheckPoints[nextCheckPoint].Pos.y, Config.CheckPoints[nextCheckPoint].Pos.z)
          SetBlipRoute(CurrentBlip, 1)

          LastCheckPoint = CurrentCheckPoint

        end

        local distance = GetDistanceBetweenCoords(coords, Config.CheckPoints[nextCheckPoint].Pos.x, Config.CheckPoints[nextCheckPoint].Pos.y, Config.CheckPoints[nextCheckPoint].Pos.z, true)

        if distance <= 100.0 then
          DrawMarker(1, Config.CheckPoints[nextCheckPoint].Pos.x, Config.CheckPoints[nextCheckPoint].Pos.y, Config.CheckPoints[nextCheckPoint].Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 1.5, 1.5, 1.5, 102, 204, 102, 100, false, true, 2, false, false, false, false)
        end

        if distance <= 3.0 then
          Config.CheckPoints[nextCheckPoint].Action(playerPed, CurrentVehicle, SetCurrentZoneType)
          CurrentCheckPoint = CurrentCheckPoint + 1
        end

      end

    end

  end
end)

-- Speed / Damage control
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(0)

    if CurrentTest == 'drive' then

      local playerPed = GetPlayerPed(-1)

      if IsPedInAnyVehicle(playerPed,  false) then

        local vehicle      = GetVehiclePedIsIn(playerPed,  false)
        local speed        = GetEntitySpeed(vehicle) * Config.SpeedMultiplier
        local tooMuchSpeed = false

        for k,v in pairs(Config.SpeedLimits) do
          if CurrentZoneType == k and speed > v then

            tooMuchSpeed = true

            if not IsAboveSpeedLimit then

              DriveErrors       = DriveErrors + 1
              IsAboveSpeedLimit = true

              TriggerEvent("customNotification",'You\'re driving too fast! Speed Limit: ' .. v .. 'km/h')
              TriggerEvent("customNotification",'errors: ' .. DriveErrors .. '/' .. Config.MaxErrors)

            end

          end
        end

        if not tooMuchSpeed then
          IsAboveSpeedLimit = false
        end

        local health = GetEntityHealth(vehicle)

        if health < LastVehicleHealth then

          DriveErrors = DriveErrors + 1

          TriggerEvent("customNotification",'you damaged the vehicle')
          TriggerEvent("customNotification",'errors: ' .. DriveErrors .. '/' .. Config.MaxErrors)
            LastVehicleHealth = health

        end

      end

    end

  end

end)