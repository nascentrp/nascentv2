
RegisterNetEvent('esx_dmvschool:loadLicenseFromServer')
AddEventHandler('esx_dmvschool:loadLicenseFromServer', function()
  local _source = source
  local user = exports["ethical-base"]:getModule("Player"):GetUser(_source)
  local char = user:getCurrentCharacter()
  local cid = char.id
  exports['nsrp_license']:selectCharacterDriversLicense(_source, cid, function(licenses)
    TriggerClientEvent('nsrp_license:loadLicenses', _source, licenses)
  end)
end)

RegisterNetEvent('esx_dmvschool:addLicense')
AddEventHandler('esx_dmvschool:addLicense', function(type)
  local _source = source
  local user = exports["ethical-base"]:getModule("Player"):GetUser(_source)
  local char = user:getCurrentCharacter()
  local cid = char.id
  exports['nsrp_license']:giveCharacterLicense(cid, type, function()  
    exports['nsrp_license']:selectCharacterLicenses(cid, function(licenses)
      TriggerClientEvent('nsrp_license:loadLicenses', _source, licenses)
    end)
  end)
end)

RegisterNetEvent('esx_dmvschool:pay')
AddEventHandler('esx_dmvschool:pay', function(price)
	local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
  
  user:removeMoney(price)
  TriggerClientEvent("DoLongHudText", src, "You paid R" .. price .. " to the DMV",1)
end)