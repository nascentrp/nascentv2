Config                 = {}
Config.DrawDistance    = 100.0
Config.MaxErrors       = 5
Config.SpeedMultiplier = 3.6
Config.Locale          = 'en'

Config.Prices = {
  Learners     = 45,
  Driver       = 100,
  drive_bike  = 95,
  drive_truck = 200
}

Config.VehicleModels = {
  Driver       = 'dilettante',
  drive_bike  = 'faggio3',
  drive_truck = 'pounder'
}

Config.SpeedLimits = {
  residence = 50,
  town      = 80,
  freeway   = 120
}

Config.Zones = {

  DMVSchool = {
    Pos   = {x = 239.471, y = -1380.960, z = 32.741},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Color = {r = 204, g = 204, b = 0},
    Type  = 1
  },

  VehicleSpawnPoint = {
    Pos   = {x = 249.409, y = -1407.230, z = 30.4094},
    Size  = {x = 1.5, y = 1.5, z = 1.0},
    Color = {r = 204, g = 204, b = 0},
    Type  = -1
  },

}

Config.CheckPoints = {
  {
    Pos = {x = 255.139, y = -1400.731, z = 29.537},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('Proceed to the next waypoint. Speed Limit: ~y~' .. Config.SpeedLimits['residence'] .. 'km/h', 5000)
    end
  },
  {
    Pos = {x = 271.874, y = -1370.574, z = 30.932},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('Go to the next waypoint', 5000)
    end
  },
  {
    Pos = {x = 234.907, y = -1345.385, z = 29.542},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      Citizen.CreateThread(function()
        DrawMissionText('~r~Stop~s~ for the pedestrian ~y~crossing', 5000)
        PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
        FreezeEntityPosition(vehicle, true)
        Citizen.Wait(4000)
        FreezeEntityPosition(vehicle, false)
        DrawMissionText('Good. Continue.', 5000)

      end)
    end
  },
  {
    Pos = {x = 217.821, y = -1410.520, z = 28.292},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      setCurrentZoneType('town')
      Citizen.CreateThread(function()
        DrawMissionText('~r~Stop~s~ and look ~y~left~s~. Speed Limit:~y~ ' .. Config.SpeedLimits['town'] .. 'km/h', 5000)
        PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
        FreezeEntityPosition(vehicle, true)
        Citizen.Wait(6000)
        FreezeEntityPosition(vehicle, false)
        DrawMissionText('~g~Good~s~, turn ~y~right~s~ and follow the line', 5000)
      end)
    end
  },
  {
    Pos = {x = 178.550, y = -1401.755, z = 27.725},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('Watch traffic, and ~y~turn on your lights~s~!', 5000)
    end
  },
  {
    Pos = {x = 113.160, y = -1365.276, z = 27.725},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('You can buy basic essentials at the ~y~store~s~ ahead of you, on the ~y~right~s~.', 5000)
    end
  },
  {
    Pos = {x = -73.542, y = -1364.335, z = 27.789},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('~r~Stop!~s~ After 5 seconds of waiting, you can proceed through a red light.', 5000)
      PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
      FreezeEntityPosition(vehicle, true)
      Citizen.Wait(6000)
      FreezeEntityPosition(vehicle, false)
    end
  },
  {
    Pos = {x = -355.143, y = -1420.282, z = 27.868},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('Proceed to the next waypoint. ', 5000)
    end
  },
  {
    Pos = {x = -520.145, y = -1128.976, z = 20.433},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      -- setCurrentZoneType('freeway')
      DrawMissionText('~r~Stop!~s~ After 5 seconds of waiting, you can proceed through a red light.', 5000)
      PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
      FreezeEntityPosition(vehicle, true)
      Citizen.Wait(6000)
      FreezeEntityPosition(vehicle, false)
    end
  },
  {
    Pos = {x = -533.414, y =  -982.178, z = 22.744},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('Down the road, on your ~y~left~s~, you can purchase ~r~weapons~s~. Continue straight.', 5000)
      FreezeEntityPosition(vehicle, true)
      Citizen.Wait(6000)
      FreezeEntityPosition(vehicle, false)
    end
  },
  {
    Pos = {x = -502.578, y = -859.5765, z = 29.799},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('You can use your indicators by flexing your ~g~+~s~ and ~g~-~s~ muscle.', 5000)
      FreezeEntityPosition(vehicle, true)
      Citizen.Wait(2000)
      FreezeEntityPosition(vehicle, false)
      DrawMissionText('Take a left.', 5000)
    end
  },
  {
    Pos = {x = -836.402, y = -833.452, z = 18.909},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('Proceed to the next waypoint.', 5000)
    end
  },
  {
    Pos = {x = -1061.588, y = -774.922, z = 18.829},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      --setCurrentZoneType('town')
      DrawMissionText('~r~Burgershot~s~ is to your ~y~left~s~. Their burgers can relieve ~b~stress~s~ and cure starvation. Go ~y~straight~s~.', 10000)
      FreezeEntityPosition(vehicle, true)
      Citizen.Wait(5000)
      FreezeEntityPosition(vehicle, false)
    end
  },
  {
    Pos = {x = -1115.266, y = -705.267, z = 20.235},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      setCurrentZoneType('freeway')
      DrawMissionText('Take a right. It\'s time to drive on the highway. Speed Limit:~y~ ' .. Config.SpeedLimits['freeway'] .. 'km/h', 5000)
      PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
      FreezeEntityPosition(vehicle, true)
      Citizen.Wait(1000)
      FreezeEntityPosition(vehicle, false)
    end
  },
  {
    Pos = {x = 56.812, y = -538.310, z = 33.345},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      setCurrentZoneType('town')
      DrawMissionText('We\'re back in town, now. Speed Limit:~y~ ' .. Config.SpeedLimits['town'] .. 'km/h', 5000)
    end
  },
  {
    Pos = {x = 249.483, y = -550.585, z = 42.56},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      setCurrentZoneType('town')
      DrawMissionText('Straight ahead of you is ~r~Pillbox Hospital~s~. This is where you can go if you need a doctor. Take a ~y~right~s~.', 5000)
      PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
    end
  },
  {
    Pos = {x = 180.894, y = -795.943, z = 30.834},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('To your left is ~r~Legion Square~s~. Most parking lots are places that allow you to store/retrieve owned vehicles. Turn ~y~left~s~.', 5000)
      PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
    end
  },
  {
    Pos = {x = 364.556, y = -867.659, z = 28.646},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('On your left is ~g~Best Buds~s~, they sell the dankest, most legal, green. It really helps with stress. Turn ~y~right~s~.', 5000)
      PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
    end
  },
  {
    Pos = {x = 400.576, y = -937.954, z = 28.886},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('Ahead of you, to the left, is the ~b~Mission Row Police Department~s~.', 5000)
      PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
    end
  },
  {
    Pos = {x = 394.759, y = -1117.436, z = 28.890},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('Continue to the next point.', 3000)
      PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
    end
  },
  {
    Pos = {x = 485.803, y = -1134.424, z = 28.89},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('Turn ~y~right~s~.', 3000)
      PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
    end
  },
  {
    Pos = {x = 495.4456, y = -1243.137, z = 28.627},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('Ahead of you is the ~o~impound lot~s~. This is where your personal vehicle will go if it gets lost. Now turn ~y~right~s~.', 5000)
      PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
      FreezeEntityPosition(vehicle, true)
      Citizen.Wait(1000)
      FreezeEntityPosition(vehicle, false)
    end
  },
  {
    Pos = {x = 250.341, y = -1427.915, z = 28.720},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('Turn ~y~right~s~.', 3000)
      PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
    end
  },
  {
    Pos = {x = 226.419, y = -1407.875, z = 29.195},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      DrawMissionText('Congratulations! Please remember to stay ~r~alert~s~ whilst driving!', 5000)
      PlaySound(-1, 'RACE_PLACED', 'HUD_AWARDS', 0, 0, 1)
    end
  },
  {
    Pos = {x = 235.283, y = -1398.329, z = 28.921},
    Action = function(playerPed, vehicle, setCurrentZoneType)
      exports["nsrp_util"]:deleteVehicle(vehicle)
    end
  },
}
