class Item {
    constructor(io) {
        this.weight = io.weight ? io.weight : 0;
        this.name = io.name ? io.name : null;
        this.itemid = io.itemid ? io.itemid : null;
        this.metaData = io.metaData ? io.metaData : {};
        this.stackable = io.stackable ? io.stackable : true; // probably need to set this
        this.image = io.image ? io.image : 'fail.png';
        this.html = this.generateItemHTML();
    }

    setDisplayQty(quantity) {
        $(this.html).find('.information').text(
            quantity + ' (' + this.weight + ')'
        )
    }

    exportIO() {
        return {
            weight: this.weight,
            name: this.name,
            itemid: this.itemid,
            metaData: this.metaData,
            stackable: this.stackable,
            image: this.image
        }
    }

    generateItemHTML() {
        const quality = $('<div />', {class: 'perfect', style: 'width: 100%; background-position: 0% 0%;'});
        const itemName = $('<div />', {class:'itemname', text: this.name});
        const information = $('<div />', {class: 'information', text: 0 + ' (' + this.weight + ')'});
        const image = $('<img />',
            {
                class: 'itemimage',
                src: 'icons/' + this.image,
                style: 'left: 0; top:0px;'
            }
        );

        const container = $('<div />', {style: 'z-index:9999'}).append(
            [quality,itemName,information,image]
        )
        
        this.html = container;
        return this.html;
    }
}