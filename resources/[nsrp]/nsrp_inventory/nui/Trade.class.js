
class Trade {
    constructor() {
        this.draggingFrom = {isSecondary: false, slotId: -1};
        this.quantitySet = null;
        const that = this;
        $('#move-amount').change(function(e) {
            that.quantitySet = e.target.value
        });
    }

    onBeginMoveItem(slot) {
        this.draggingFrom = slot;
    }

    onDropSlot(slot) {
        console.log(this);
        this.transferBetweenSlots(slot, this.draggingFrom)
    }

    transferBetweenSlots(toSlotObject, fromSlotObject) {
        
        if(toSlotObject.secondary == fromSlotObject.secondary) { // Going from the same inventory to the same inventory
           if($(toSlotObject.html).attr('data-itemId') == $(fromSlotObject.html).attr('data-itemId')) { // going from the same slot to the same slot
                return;
           }
        } else { // moving from one (primary for eg) to another (secondary)
            // OnDuty
            // if(toSlotObject.secondary && !fromSlotObject.secondary) { // to secondary
            //     // transferring to secondary
            // } else { // from secondary
            //     // transfering from secondary to me
            // }
        }

        const itemProps = Object.assign({}, { properties: fromSlotObject.item.exportIO(), quantity: fromSlotObject.quantity});
        const newItem = new Item(itemProps.properties);
        if(toSlotObject.hasItem) {
            const oldItemProps = Object.assign({}, { properties: toSlotObject.item.exportIO(), quantity: toSlotObject.quantity});
            const newOldItem = new Item(oldItemProps.properties);
            if(toSlotObject.item.itemid == fromSlotObject.item.itemid && fromSlotObject.item.stackable) {
                const totalQty = parseInt(toSlotObject.quantity)+parseInt(fromSlotObject.quantity);
                toSlotObject.AddItem(newOldItem, totalQty);
                fromSlotObject.removeItem();
                return;
            } else if(!fromSlotObject.item.stackable || toSlotObject.item.itemid != fromSlotObject.item.itemid) {
                fromSlotObject.AddItem(newOldItem, oldItemProps.quantity);
                toSlotObject.AddItem(newItem, itemProps.quantity);
                return;
            }
        } else if(!toSlotObject.hasItem) {
            let quantity = this.quantitySet > 0 ? this.quantitySet : itemProps.quantity;
            let stayBehind = 0;
            if(quantity > itemProps.quantity) {
                quantity = itemProps.quantity
            } else { // We can assume the else will only be triggered if quantitySet is valid and stored in "quantity"
                stayBehind = itemProps.quantity - quantity;
            }
            if(stayBehind > 0) {
                const oldItemProps = Object.assign({}, { properties: fromSlotObject.item.exportIO(), quantity: stayBehind});
                const newOldItem = new Item(oldItemProps.properties);
                fromSlotObject.AddItem(newOldItem, oldItemProps.quantity);
            } else {
                fromSlotObject.removeItem();
            }
            toSlotObject.AddItem(newItem, quantity);
            return;
        } else {
            fromSlotObject.removeItem();
            toSlotObject.AddItem(newItem, itemProps.quantity);
            return;
        }

    }
}