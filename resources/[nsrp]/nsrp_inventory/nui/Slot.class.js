class Slot {
    constructor(onDropSlot, onBeginMoveItem, customHTMLObject = {}, secondary = false) {
        
        if(!secondary) {
            this.containerSelector = "#wrapmain";
        } else {
            this.containerSelector = "#wrapsecondary";
        }
        this.secondary = secondary;
        this.item = null;
        this.hasItem = false;
        this.html = this.generateHTMLData(customHTMLObject)
        this.quantity = 0;
        this.onBeginMoveItem = onBeginMoveItem;
        this.onDropSlot = onDropSlot;
        this.weAreBeingDragged = false;
    }

    AddItem(item, quantity = 1) { // Returns slot
        const that = this;
        this.quantity = quantity;
        this.item = item;
        this.item.setDisplayQty(quantity);
        $(this.item.html).draggable({
            start: function() {
                that.onBeginMoveItem(that);
            },
            scroll: false,
            appendTo: 'body',
            helper: 'clone',
        });
        $(this.html).html(this.item.html);
        this.hasItem = true;
        return this;
    }

    removeItem() { // Returns removed item
        this.hasItem = false;
        this.html.html(null);
        //this.item = null;
    }

    generateHTMLData(customHTMLObject = {}) {
        const html = $('<div />', {...customHTMLObject, class: 'item'});
        let that = this;
        
        $(html).droppable({
            drop: function( event, ui ) {
                that.onDropSlot(that);
            }
        });
        $(this.containerSelector).append(
            html
        );
        return html;
    }

    updateQuantity() {
        $(this.item.html).find('.information').text(this.quantity + ' ('+this.item.weight+')')
    }
}