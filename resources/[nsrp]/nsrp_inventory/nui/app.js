let inventory;
$(document).ready(function() {
    const trade = new Trade();
    inventory = new Inventory(
        {
            slotCount: 24,
            onBeginMoveItem: function(slot) { trade.onBeginMoveItem.call(trade, slot) },
            onDropSlot: function(slot) { trade.onDropSlot.call(trade, slot) }
        }, 
    true);

    let secondaryInventory = new Inventory(
        {
            slotCount: 24,
            onBeginMoveItem: function(slot) { trade.onBeginMoveItem.call(trade, slot) },
            onDropSlot: function(slot) { trade.onDropSlot.call(trade, slot) }
        }, 
    );
});

window.addEventListener('message', function (event) {
    let item = event.data;
    switch(item.action) {
        case 'PERSONAL_ADD_ITEM':
            // id = id,
            // slot = slot,
            // itemId = item_id,
            // quality = quality,
            // information = information
            // image = image
            console.log('i', item);
            inventory.giveItem(item.itemId, item.weight, item.name, item.quality, 1, item.meta, !item.nonStack, item.image, item.slot);
            break;

    }
});