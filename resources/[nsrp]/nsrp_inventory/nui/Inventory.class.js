class Inventory {
    constructor(config = {}, isPersonal) {
        this.slots = [];
        this.isDragging = false;
        this.draggingFrom = null;
        this.targetSlot = null;
        this.onBeginMoveItem = config.onBeginMoveItem ? config.onBeginMoveItem : function(){};
        this.onDropSlot = config.onDropSlot ? config.onDropSlot : function(){};
        const that = this;

        const slotCount = config.slotCount ? config.slotCount : 0;
        for(var i = 0; i < slotCount; i++) {
            let slot = null;
            if(isPersonal && i >= 0 && i <= 4) {
                slot = new Slot(
                    function(slot) { this.onDropSlot.call(that, slot) },
                    function(slod) { this.onBeginMoveItem.call(that, slot) },
                    {id: 'playerslot' + (i+1), 'data-itemId': i},
                    !isPersonal
                );
            } else {
                slot = new Slot(
                    function(slot) { this.onDropSlot.call(that, slot) },
                    function(slod) { this.onBeginMoveItem.call(that, slot) },
                    {'data-itemId': i},
                    !isPersonal
                );
            }
            this.slots[i] = slot;
        }  
    }

    giveItem(itemid, weight, name, quality, quantity = 1, metaData = {}, stackable = true, image = 'fail.png', slotId = -1) {
        try {
            let slot
            if(slotId == -1) {
                slot = this.getAvailableSlot();
            } else {
                slot = this.getSpecificSlot(slotId);
            }
            let item = new Item({
                name: name,
                weight: weight,
                itemid: itemid,
                metaData: metaData,
                quality: quality,
                stackable: stackable,
                image: image
            })
            slot.AddItem(item, quantity)
        } catch(e) {
            console.log('error', e);
        }
    }  

    getAvailableSlot() {
        for(var i = 0; i < this.slots.length; i++) {
            if(!this.slots[i].hasItem) {
                return this.slots[i];
            }
        }
        throw "There is no available slot capacity";
    }

    getSpecificSlot(slotId) {
        return this.slots[slotId];
    }
}