function addItem(id, weight, name, slot, item_id, quality, information, description, nonStack, degradable, image)
    SendNUIMessage({
        action = 'PERSONAL_ADD_ITEM',
        id = id,
        name = name,
        slot = slot,
        itemId = item_id,
        quality = quality,
        nonStack = nonStack,
        degradable = degradable,
        meta = information,
        description = description,
        image = image
    })
    SetNuiFocus(true, true)
end

RegisterNetEvent('nsrp_inventory:restoreInventory')
AddEventHandler('nsrp_inventory:restoreInventory', function(inventory)
    for i = 1, #inventory do
        local inv = inventory[i]
        print('load', json.encode(inv))
        addItem(inv.id, inv.weight, inv.display_name, inv.slot, inv.item_id, inv.quality, inv.information, inv.description, inv.can_stack, inv.degradable, inv.image)
    end
end)

RegisterCommand('disableInv', function()
    SetNuiFocus(false, false)
end)

AddEventHandler("ethical-base:spawnInitialized", function()
    
end)