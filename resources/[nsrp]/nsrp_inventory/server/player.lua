local playerInventory = {}
itemList = {}

AddEventHandler("ethical-base:spawnInitialized", function()
    local src = source
    playerConnect(src)
end)

function playerConnect(source)
    local src = source
    generatePlayerInventoryObject(src, function(inventoryObject)
        print("received", json.encode(inventoryObject))
        playerInventory[src] = inventoryObject
        TriggerClientEvent('nsrp_inventory:restoreInventory', src, playerInventory[src]["inventory"])
    end)
end

function playerDisconnect(source)
    local src = source
    table.remove(playerInventory, src)
end

function generatePlayerInventoryObject(source, success) 
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    print('cid', character.id)
    loadPersonalInventoryForPlayer(character.id, function(inventory)
        success({
            ['id'] = src,
            ['cid'] = character.id,
            ['inventory'] = inventory
        })
    end)
end

function loadPersonalInventoryForPlayer(cid, onLoadSuccess)
    exports.ghmattimysql:execute(
        "SELECT ui2.id, ui2.item_id, ui2.quality, ui2.information, ui2.slot, il.deg as degradable, il.image, il.description, il.craft, il.display_name, il.can_stack, il.weight from user_inventory2 ui2 INNER JOIN item_list il on ui2.item_id = il.item_id WHERE ui2.name = concat('ply-', @cid)",
        {['cid'] = cid},
        function(inventory) 
            onLoadSuccess(inventory)
            print("Successfully Loaded " .. #inventory .. " for " .. cid)
        end
    )
end

function savePlayerInventory(source, inventoryObject)
    local src = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
end

function moveItemInInventory(source, fromSlotId, toSlotId)
    local foundFromSlot, foundToSlot = nil, nil
    for i = 1, #playerInventory.inventory do
        local inv = playerInventory[i]
        if(inv.slot == fromSlotId) then
            foundFromSlot = inv.slot
            break
        elseif(inv.slot == toSlotId) then
            foundToSlot = inv.slot
        end
    end

end

function openSecondary(source)
    -- GROUND (Else clause)

end