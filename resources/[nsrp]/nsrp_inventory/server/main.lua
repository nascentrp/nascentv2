Citizen.CreateThread(function()
    print('Starting Up Inventory Script')
    fetchItems()
    print('Items Fetched', #itemList)
end)

function fetchItems()
    exports.ghmattimysql:execute(
        "SELECT * from item_list",
        {},
        function(items) 
            for i = 1, #items do
                local item = items[i];
                itemList[item.item_id] = {
                    fullyDegrades = item.fully_degrades,
                    decayrate = item.decay_rate,
                    displayname = item.display_name,
                    craft = {},
                    price = item.price,
                    weight = item.weight,
                    nonStack = item.can_stack == false,
                    model = item.model,
                    image = item.image,
                    deg = item.deg
                }
                if(item.craft ~= nil) then
                    itemList[item.item_id].craft = item.craft
                end
                if(item.information ~= nil) then
                    itemList[item.item_id].information = item.description
                end
            end
        end
    )
end