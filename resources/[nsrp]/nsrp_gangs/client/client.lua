invitations = {}
invitations_default = {
        id = 1,
        header = "Gang Invitations",
        txt = ""
    };
inviteCount = 0
isMenuOpen = false
isOnMainMenu = false

RegisterNetEvent("ethical-base:characterLoaded")
AddEventHandler("ethical-base:characterLoaded", function()
  TriggerServerEvent('ethical-territories:getgang')
end)

RegisterCommand('gang', function()
    TriggerEvent('nsrp_gangs:OpenGangMenu')
end)

RegisterCommand('gangdebug', function()
    TriggerServerEvent('ethical-territories:getgang')
end)

RegisterNetEvent('nsrp_gangs:OpenGangMenu')
AddEventHandler('nsrp_gangs:OpenGangMenu', function()
    isOnMainMenu = true
    isMenuOpen = true
    local gang = exports["isPed"]:isPed("gang")
    local gangRank = exports["isPed"]:isPed("gangrank")
    print("Menu open")
    print(gang)
    print(gangRank)
    local menu = {
        {
            id = 1,
            header = "Gang Actions",
            txt = ""
        },
    }
    if gang == nil or gang == 0 then
       table.insert(menu,
            {
                id = 2,
                header = "Create Gang",
                txt = "Be your own boss, prove yourself on the streets.",
                params = {
                    event = "nsrp_gangs:CreateGang"
                }
            }
        )
		table.insert(menu,
            {
                id = 3,
                header = "View Gang Invites",
                txt = "Got a connection? Make it official.",
                params = {
                    event = "nsrp_gangs:FetchGangInvitations"
                }
            }
       )
    else
        if gangRank > 0 then
            table.insert(menu,
                {
                    id = 2,
                    header = "Manage Members",
                    txt = "View and manage all members here",
                    params = {
                        event = "nsrp_gangs:ManageMembers",
                    }
                }
            )
            table.insert(menu, 
                {
                    id = 3,
                    header = "Invite New Members",
                    txt = "Add new members here",
                    params = {
                        event = "invitemembers"
                    }
                }
            )
        end
        table.insert(menu, 
            {
                id = 4,
                header = "Leave",
                txt = "Don't be too hasty",
                params = {
                    event = "nsrp_gangs:leave",
                    args = gang
                }
            }
        )
    end
    TriggerEvent('ethical-context:sendMenu', menu)
end)

RegisterNetEvent('nsrp_gangs:FetchGangInvitations')
AddEventHandler('nsrp_gangs:FetchGangInvitations', function()
    TriggerServerEvent('ethical-territories:viewganginvites');
end)

RegisterNetEvent('nsrp_gangs:leave')
AddEventHandler('nsrp_gangs:leave', function(gang_id)
    TriggerServerEvent('gangs:leave', gang_id);
end)


RegisterNetEvent('nsrp_gangs:UpdateAndShowGangInvitations')
AddEventHandler('nsrp_gangs:UpdateAndShowGangInvitations', function(data)
    invitations = {}
    inviteCount = 0
    isOnMainMenu = false
    if #data > 0 then
        table.insert(invitations, invitations_default)
        inviteCount = #data
        local ct = 2
        for i = 1, #data do
            ct = ct + i
            table.insert(invitations, {
                id = ct,
                header = data[i].gang_name,
                txt = "",
                --txt = data[i].create_ts,
                params = {
                    event = "nsrp_gangs:AcceptGang",
                    args = {
                        gang_member_id = data[i].gang_member_id,
                        gang_id = data[i].gang_id
                    },
                }
            })
        end
    else
        table.insert(invitations, invitations_default)
        table.insert(invitations, {
            id = 2,
            header = "N/A",
            txt = "No invitations found..."
        })
    end
    TriggerEvent('ethical-context:sendMenu', invitations)
end)

RegisterNetEvent('nsrp_gangs:AcceptGang')
AddEventHandler('nsrp_gangs:AcceptGang', function(data)
    local gmid = data.gang_member_id
    local gang_id = data.gang_id
    TriggerServerEvent('ethical-territories:acceptinvite', gmid, gang_id)
end)

RegisterNetEvent('nsrp_gangs:OpenInvitationMenu')
AddEventHandler('nsrp_gangs:OpenInvitationMenu', function()
    TriggerEvent('ethical-context:sendMenu', invitations)
end)

RegisterNetEvent('nsrp_gangs:CreateGang')
AddEventHandler('nsrp_gangs:CreateGang', function()
    local create = exports["ethical-applications"]:KeyboardInput({
        header = 'Gang Creator',
        rows = {
            {
                id = 0,
                txt = "Gang Name"
            }
        }
    })
    if create then
        TriggerServerEvent('setupgangdb', create[1].input)
    end
end)
AddEventHandler('ethical-context:closedMenu', function()
    if isMenuOpen then
        if isOnMainMenu then
            TriggerEvent('ethical-context:closeActiveMenu')
            isMenuOpen = false
        else
            TriggerEvent('nsrp_gangs:OpenGangMenu')
        end
    end
end)


-- function menuIsOpen() 
--     Citizen.CreateThread(function()
--         hasTriedToQuit = false
--         isMenuOpen = true
--         while isMenuOpen do
--             if IsControlJustReleased(0, 177) and not HasTriedToQuit then
--                 print("PRESSED ESC")
--                 hasTriedToQuit = true
--                 if isOnMainMenu then
--                     TriggerEvent('ethical-context:closeActiveMenu')
--                     isMenuOpen = false
--                     Citizen.Wait(5000)
--                     hasTriedToQuit = false
--                 else
--                     TriggerEvent('nsrp_gangs:OpenGangMenu')
--                 end
--             end
--             Citizen.Wait(0)
--         end
--     end)
-- end