RegisterNetEvent('nsrp_gangs:ManageSpecificMember')
AddEventHandler('nsrp_gangs:ManageSpecificMember', function(data)
    local gang_id = data.gang_id
    local cid = data.cid
    local ingang = data.ingang
    local rank = data.rank
    local gang_member_id = data.member_id
    local menu = {
        {
            id = 1,
            header = "Gang Members",
            txt = ""
        }
    }
    local nextId = 2
    if ingang ~= 0 then
        if rank == 0 then
            table.insert(menu, {
                id = nextId,
                header = "Promote to Leader",
                txt = "Yes, you can have multiple leaders",
                params = {
                    event = "nsrp_gangs:setRank",
                    args = {
                        cid = cid,
                        gang_id = gang_id,
                        gang_member_id = gang_member_id,
                        rank = 1
                    }
                }
            })
            nextId = nextId + 1
        else
            table.insert(menu, {
                id = nextId,
                header = "Demote to Member",
                txt = "Demote this leader down to member",
                params = {
                    event = "nsrp_gangs:setRank",
                    args = {
                        cid = cid,
                        gang_id = gang_id,
                        gang_member_id = gang_member_id,
                        rank = 0
                    }
                }
            })
            nextId = nextId + 1
            table.insert(menu, {
                id = nextId,
                header = "Transfer Ownership",
                txt = "Make this person the gang owner",
                params = {
                    event = "nsrp_gangs:giveGang",
                    args = {
                        cid = cid,
                        gang_id = gang_id,
                        gang_member_id = gang_member_id,
                    }
                }
            })
        end
        nextId = nextId + 1
        table.insert(menu, {
            id = nextId,
            header = "Remove Member",
            txt = "Irreversably",
            params = {
                event = "nsrp_gangs:removeSpecificMember",
                args = {
                    cid = cid,
                    gang_id = gang_id
                }
            }
        })
    else
        table.insert(menu, {
            id = nextId,
            header = "Cancel Invite",
            txt = "Prevent this member from being able to accept the invitation",
            params = {
                event = "nsrp_gangs:removeSpecificMember",
                args = {
                    cid = cid,
                    gang_id = gang_id
                }
            }
        })
        nextId = nextId + 1
    end
    TriggerEvent('ethical-context:sendMenu', menu)
end)

RegisterNetEvent('nsrp_gangs:removeSpecificMember')
AddEventHandler('nsrp_gangs:removeSpecificMember', function(data)
    local cid = data.cid
    local gang_id = data.gang_id
    TriggerServerEvent("gangs:removemembers", cid, gang_id)
    TriggerEvent('nsrp_gangs:ManageMembers')
end)

RegisterNetEvent('nsrp_gangs:setRank')
AddEventHandler('nsrp_gangs:setRank', function(data)
    print("demote to member")
    local gmid = data.gang_member_id
    local rank = data.rank
    TriggerServerEvent("ethical-territories:setRank", gmid, rank)
end)

RegisterNetEvent('nsrp_gangs:giveGang')
AddEventHandler('nsrp_gangs:giveGang', function(data)
    TriggerServerEvent('ethical-territories:transferownership', data);
end)

RegisterNetEvent('nsrp_gangs:receiveMemberList')
AddEventHandler('nsrp_gangs:receiveMemberList', function(data)
    local menu = {
        {
            id = 1,
            header = "Gang Members",
            txt = ""
        },
    }
    if data ~= nil and #data > 0 then
        local ct = 1
        for i = 1, #data do
            ct = ct + 1
            local title = 'Invited'
            if data[i].ingang ~= 0 then
                title = 'Member'
            end
            if data[i].rank == 1 and ingang ~= 0 then
                title = 'Leader'
            end
            table.insert(menu, {
                id = ct,
                header =  data[i].first_name .. " " .. data[i].last_name,
                txt = title,
                params = {
                    event = "nsrp_gangs:ManageSpecificMember",
                    args = {
                        gang_id = data[i].gang_id,
                        cid = data[i].cid,
                        ingang = data[i].in_gang,
                        rank = data[i].rank,
                        member_id = data[i].gang_member_id
                    },
                }
            })
        end
    else
        table.insert(menu, {
            id = 3,
            header = "You have no members",
            txt = "Members will appear here"
        })
    end
    isOnMainMenu = false
    TriggerEvent('ethical-context:sendMenu', menu)
end)

RegisterNetEvent('nsrp_gangs:ManageMembers')
AddEventHandler('nsrp_gangs:ManageMembers', function()
    TriggerServerEvent('ethical-territories:getmembers')
end)
