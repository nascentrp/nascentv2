fx_version 'adamant'
games { 'rdr3', 'gta5' }
rdr3_warning 'I acknowledge that this is a prerelease build of RedM, and I am aware my resources *will* become incompatible once RedM ships.'

description 'NSRP FIX DEBUGGER FOR PLAYERS'

version '1.0.3'

client_scripts {
  'client.lua'
}