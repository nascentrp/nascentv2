RegisterCommand("debug", function()
    local generateMenu = {
        {
            id = 1,
            header = "Debug Options",
            txt = "Please select an option to reset"
        },
        {
            id = 2,
            header = '3rd Eye',
            txt = 'Reset your 3rd Eye',
            params = {
                event = "ethical-target:debugReset"
            }
        },
        {
            id = 3,
            header = 'Voice Chat',
            txt = 'Reset your voice',
            params = {
                event = "ethical-voice:debugreset"
            }
        }       
    }
    TriggerEvent('ethical-context:sendMenu', generateMenu)
end)