licenses = {
    {header = "Firearms License", price = 600, id = "firearms", server_call = "police:grantSelfFirearms"},
    {header = "Hunting License", price = 500, id = "hunting", server_call = "police:grantSelfHunting"},
    {header = "Fishing License", price = 200, id = "fishing", server_call = "police:grantSelfFishing"}
}
generateMenu = {
    {
        id = 1,
        header = "Available Licenses",
        txt = ""
    }        
}

Citizen.CreateThread(function()
    for i,v in ipairs(licenses) do
        local index = i+1
        table.insert(generateMenu, {
            id = index,
            header = v.header,
            txt = "R" .. v.price,
            params = {
                event = "nsrp_license:giveLicense",
                args = v.id
            }
        })
    end
end)

Citizen.CreateThread(function()
    local modelHash = GetHashKey("a_f_y_femaleagent")
	RequestModel(modelHash)
	while not HasModelLoaded(modelHash) do
		Wait(1)
	end
	--local ped = CreatePed(0, modelHash, -550.91662597656, -188.85559082031, 38.219268798828, true)
	local ped = CreatePed(0, modelHash, -551.35, -190.55, 37.72, true)
	SetEntityHeading(ped,  225.0)
	SetEntityInvincible(ped, true)
	local obj = 0
    local sitAttempts = 0
    while obj == 0 do -- force the ped to sit down
        sitAttempts = sitAttempts + 1
        SetEntityCoords(ped, -551.35, -190.55, 37.72)
        local coords = GetEntityCoords(ped)
        obj = GetClosestObjectOfType(coords.x, coords.y, coords.z, 2.5, 536071214, false, true ,true)
        Citizen.Wait(1000)
    end
    print("Sit complete " .. sitAttempts)
	local objc = GetEntityCoords(obj)
	TaskStartScenarioAtPosition(ped, 'PROP_HUMAN_SEAT_BENCH', objc.x, objc.y, objc.z+(0.5), GetEntityHeading(object)-120.0, 0, true, true)

	local ped = PlayerPedId()
	while true do
		AttachEntityToEntity(ped, object, 20, 0.0, 0.0, 1.0, 0.0, 0.0, 180.0, false, false, false, false, 1, true)

		Citizen.Wait(0)
	end
    --TriggerEvent('animation:Chair', ped)
	--FreezeEntityPosition(ped, true)
    TriggerServerEvent('nsrp_license:loadCharacterDriversLicense')
    
end)

Citizen.CreateThread(function()
    while true do -- license department
        DrawMarker(20, -549.92, -192.26, 37.61, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.5, 0, 0, 255, 25, false, true, 2, nil, nil, false)
        Citizen.Wait(0)
    end

end)

AddEventHandler("nsrp_license:getAvailableLicenses", function()
    TriggerEvent('ethical-context:sendMenu', generateMenu)
end)

AddEventHandler("nsrp_license:giveLicense", function(type)
    print("give license!")
    print(json.encode(type))
    for i,v in ipairs(licenses) do
        if type == v.id then
            TriggerServerEvent(v.server_call)
        end
    end
end)
