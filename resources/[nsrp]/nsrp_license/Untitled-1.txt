1. Supporter Pack | R50
- Patreon Role on Discord

2. Priority Pack | R100
- All from supporter pack
- Connect Queue Priority

3. Personal Pack | R200
- Everything from the Priority pack
- Free number plate change

4. Personal Pack with a thank you | R300
- Everything from the Personal pack
- 1 extra Free number plate change
- Your name on the loading screen

5. VIP Packages | R500
- Everything from Personal Pack
- Your name on the building