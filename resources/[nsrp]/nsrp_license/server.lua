
RegisterServerEvent('nsrp_license:addLicense')
AddEventHandler('nsrp_license:addLicense', function(type)
  local _source = source
  local user = exports["ethical-base"]:getModule("Player"):GetUser(_source)
  local char = user:getCurrentCharacter()
  local cid = char.id
end)

exports('giveCharacterLicense', function(cid, type, cb)
    giveCharacterLicense(cid, type, cb)
end)
function giveCharacterLicense(cid, type, cb)
    exports.ghmattimysql:execute("INSERT INTO user_licenses (owner, `type`, status) VALUES(@cid, @type, 1)", {['cid'] = cid, ['type'] = type}, function()  
        cb()
    end)
end

RegisterServerEvent('nsrp_license:loadCharacterDriversLicense')
AddEventHandler('nsrp_license:loadCharacterDriversLicense', function(_source)
    if _source == nil then
        _source = source
    end
    local user = exports["ethical-base"]:getModule("Player"):GetUser(_source)
    local char = user:getCurrentCharacter()
    local cid = char.id
    selectCharacterLicenses(cid, function(licenses) 
        TriggerClientEvent('nsrp_license:loadLicenses', _source, licenses)
    end)
end)

exports('selectCharacterLicenses', function(cid, cb)
    selectCharacterLicenses(cid, cb)
end)
function selectCharacterLicenses(cid, cb)
    exports.ghmattimysql:execute("SELECT type from user_licenses WHERE owner = @cid", {['cid'] = cid}, function(license)  
        cb(license)
    end)
end