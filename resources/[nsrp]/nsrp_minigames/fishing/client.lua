

local threadHalted = false
local success = false

RegisterNUICallback('fishingComplete', function(data)
    success = data.success
    threadHalted = false
end)

function doSkill()
    SendNUIMessage({action = "startFishing"})
    threadHalted = true
    local controlIsJustPressed = false
    while threadHalted do
        if IsControlJustPressed(1, 38) then -- E
            if not controlIsJustPressed then
                controlIsJustPressed = true
                SendNUIMessage({action = "pressE"})
            end
        elseif controlIsJustPressed then
            controlIsJustPressed = false
        end
        Wait(0)
    end
    return success
end

exports("MiniGameFishingSkill", doSkill)