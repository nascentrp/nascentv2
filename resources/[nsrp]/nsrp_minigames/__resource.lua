resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

version '1.0'

ui_page "fishing/meter.html"

files {
	"fishing/assets/*",
  "fishing/meter.html",
  "fishing/script.js",
  "fishing/style.css"
}

client_scripts{
  'fishing/client.lua',
}
