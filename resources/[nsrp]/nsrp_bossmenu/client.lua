RegisterCommand('bossmenu', function() 
	local job = exports["isPed"]:isPed("myjob")
    if job ~= 'unemployed' and job ~= 'police' and job ~= 'ems' then
        TriggerEvent('nsrp_bossmenu:open', job)
    else
        TriggerEvent("DoLongHudText", "This menu is not available for your job '" .. job .. "'",2)
    end
end)

RegisterNetEvent('nsrp_bossmenu:receiveNewEmployeeData')
AddEventHandler('nsrp_bossmenu:receiveNewEmployeeData',function(data, myRank, myJob)
    SendNUIMessage({method = 'receiveNewData', data = data, myRank = myRank, job = myJob})
end);

RegisterNetEvent('nsrp_bossmenu:open')
AddEventHandler('nsrp_bossmenu:open',function(job)
    print("openining for", job)
    SendNUIMessage({method = 'open', job = job})
    TriggerServerEvent('nsrp_bossmenu:loadMenuForSpecificJob', job)
    SetNuiFocus(true, true)
end);

RegisterNUICallback('addNewUser', function(data, cb)
    TriggerServerEvent('nsrp_bossmenu:createNewWhitelistedJobUser', data.cid, data.job, data.rank)
    cb();
end)

RegisterNUICallback('onMenuClose', function(data, cb)
    SetNuiFocus(false, false);
    cb();
end)

RegisterNUICallback('deleteUser', function(data, cb)
    print("sending")
    TriggerServerEvent('nsrp_bossmenu:deleteUserFromJob', data.cid, data.job)
    cb()
end)