fx_version 'bodacious'
game 'gta5'

name 'nsrp_bossmneu'
description 'Boss Menu'

server_scripts {
    'server.lua'
}

ui_page 'boss_menu.html'

client_scripts {
    'client.lua',
}

files {
    'boss_menu.html'
}