



RegisterServerEvent('nsrp_bossmenu:createNewWhitelistedJobUser')
AddEventHandler('nsrp_bossmenu:createNewWhitelistedJobUser', function(cid,job,rank)
    local src = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local char = user:getCurrentCharacter()
    local myCid = char.id
    exports.ghmattimysql:execute('select rank from jobs_whitelist where cid = @cid and job = @job limit 1', {
        ['@cid'] = myCid,
        ['@job'] = job
    }, function(rankData)
        if tonumber(rankData[1].rank) > tonumber(rank) then
            exports.ghmattimysql:execute('INSERT INTO jobs_whitelist (cid, job, rank, assigned_by) VALUES (@cid, @job, @rank, @myCid)', {
                ['@myCid'] = myCid,
                ['@cid'] = cid,
                ['@job'] = job,
                ['@rank'] = rank
            }, function(data)
                TriggerEvent("nsrp_bossmenu:loadMenuForSpecificJob", job, src)
            end)
        end
    end)
end)

RegisterServerEvent('nsrp_bossmenu:deleteUserFromJob')
AddEventHandler('nsrp_bossmenu:deleteUserFromJob', function(cid,job) 
    print("sent", cid, job)
    local src = source
    exports.ghmattimysql:execute('delete from jobs_whitelist where cid = @cid and job = @job', {
        ['@cid'] = cid,
        ['@job'] = job
    }, function(data)
        TriggerEvent("nsrp_bossmenu:loadMenuForSpecificJob", job, src)
    end)
end)

RegisterServerEvent('nsrp_bossmenu:loadMenuForSpecificJob')
AddEventHandler('nsrp_bossmenu:loadMenuForSpecificJob', function(job, _src)
    local src = source
    if _src ~= nil then
        src = _src
    end
    local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local char = user:getCurrentCharacter()
    local cid = char.id
    exports.ghmattimysql:execute('select cid, job, last_name, first_name, rank from jobs_whitelist jw inner join characters c on c.id = jw.cid where job = @job and true = (select true from jobs_whitelist where cid = @cid and job = @job and rank > jw.rank limit 1) order by create_ts desc', {
        ['@cid'] = cid,
        ['@job'] = job
    }, function(data)
        exports.ghmattimysql:execute('select rank from jobs_whitelist where cid = @cid and job = @job', {
            ['@cid'] = cid,
            ['@job'] = job
        }, function(myRankData) 
            TriggerClientEvent("nsrp_bossmenu:receiveNewEmployeeData", src, data, myRankData[1].rank, job)
        end)
    end)
end);