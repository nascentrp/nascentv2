local ETHICAL = ETHICAL or {}
ETHICAL.Scoreboard = {}
ETHICAL._Scoreboard = {}
ETHICAL._Scoreboard.PlayersS = {}
ETHICAL._Scoreboard.RecentS = {}


RegisterServerEvent('player:getpingsv')
AddEventHandler('player:getpingsv', function()
local src = source
local pingreturn = GetPlayerPing(src)
TriggerClientEvent('player:setping', src, pingreturn)
end)

RegisterServerEvent('ethical-scoreboard:AddPlayer')
AddEventHandler("ethical-scoreboard:AddPlayer", function()

    local identifiers, steamIdentifier = GetPlayerIdentifiers(source), nil
    for _, v in pairs(identifiers) do
        if string.find(v, "steam") then
            steamIdentifier = v
            break
        end
    end

    local stid = HexIdToSteamId(steamIdentifier)
    local ply = GetPlayerName(source)
    local scomid = steamIdentifier:gsub("steam:", "")
    local data = { src = source, steamid = stid, comid = scomid, name = ply }

    TriggerClientEvent("ethical-scoreboard:AddPlayer", -1, data )
    ETHICAL.Scoreboard.AddAllPlayers()
end)

function ETHICAL.Scoreboard.AddAllPlayers(self)
    --local players = GetActivePlayers()

    for i, _PlayerId in pairs(GetPlayers()) do
        
        local identifiers, steamIdentifier = GetPlayerIdentifiers(_PlayerId)
        for _, v in pairs(identifiers) do
            if string.find(v, "steam") then
                steamIdentifier = v
                break
            end
        end

        local stid = HexIdToSteamId(steamIdentifier)
        local ply = GetPlayerName(_PlayerId)
        local scomid = steamIdentifier:gsub("steam:", "")
        local data = { src = tonumber(_PlayerId), steamid = stid, comid = scomid, name = ply }

        TriggerClientEvent("ethical-scoreboard:AddAllPlayers", source, data)

    end
end

function ETHICAL.Scoreboard.AddPlayerS(self, data)
    ETHICAL._Scoreboard.PlayersS[data.src] = data
end

AddEventHandler("playerDropped", function()
	local identifiers, steamIdentifier = GetPlayerIdentifiers(source)
    for _, v in pairs(identifiers) do
        if string.find(v, "steam") then
            steamIdentifier = v
            break
        end
    end
    if steamIdentifier == nil then
        steamIdentifier = 'steam:NULL'
    end
    local stid = HexIdToSteamId(steamIdentifier)
    local ply = GetPlayerName(source)
    local scomid = steamIdentifier:gsub("steam:", "")
    local plyid = source
    local data = { src = source, steamid = stid, comid = scomid, name = ply }

    TriggerClientEvent("ethical-scoreboard:RemovePlayer", -1, data )
    Wait(600000)
    TriggerClientEvent("ethical-scoreboard:RemoveRecent", -1, plyid)
end)

function HexIdToSteamId(hexId)
    return hexId
end