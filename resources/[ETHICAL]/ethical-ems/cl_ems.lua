
local commValue = 100

RegisterNetEvent("ems:bill")
AddEventHandler("ems:bill", function()
    local bill = exports["ethical-applications"]:KeyboardInput({
        header = "Bill Patient",
        rows = {
            {
                id = 0,
                txt = "U ID"
            },
            {
                id = 1,
                txt = "Amount"
            }
        }
    })
    if bill then
        if tonumber(bill[2].input) < 1001  and tonumber(bill[2].input) > 0 then
            TriggerServerEvent("ems:bill:player", bill[1].input, bill[2].input)
        else
            TriggerEvent('DoLongHudText', 'You can\'t overcharge people like that. Attempt reported.', 2)
        end
    end
end)


RegisterNetEvent('ems:createcustomerreceipt')
AddEventHandler('ems:createcustomerreceipt', function(value,receiver)
local jobName = exports["isPed"]:isPed("myjob")
local amount = value
local type = "CUSTOMER"
local sender = exports["isPed"]:isPed("cid")
local receiver = receiver

TriggerEvent('nsrp_receipts:createreceipt',amount,jobName,sender,receiver,type)

end)

RegisterNetEvent("ems:cash:in")
AddEventHandler("ems:cash:in", function()
    local job = exports["isPed"]:isPed("myjob")
    local cid = exports["isPed"]:isPed("cid")
	if job == 'ems' or job == 'doctor' then
        local qty = exports["ethical-inventory"]:getQuantity("receipt")
        if qty >= 1 then
            TriggerServerEvent('ems:payout:employee', qty)
        end
        TriggerEvent("nsrp_receipts:payrecieptcl",job,cid,commValue)
	end
end)