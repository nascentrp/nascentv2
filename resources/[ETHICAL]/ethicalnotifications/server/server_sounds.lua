--------------------------------------------------------------------------------------------------
--																				          	    --
--       ______ _______ _    _ _____ _____          _        _____  ________       _______      --
--      |  ____|__   __| |  | |_   _/ ____|   /\   | |      |  __ \|  ____\ \    / / ____|      --
--      | |__     | |  | |__| | | || |       /  \  | |      | |  | | |__   \ \  / / (___        --
--      |  __|    | |  |  __  | | || |      / /\ \ | |      | |  | |  __|   \ \/ / \___ \       --
--      | |____   | |  | |  | |_| || |____ / ____ \| |____  | |__| | |____   \  /  ____) |      --
--      |______|  |_|  |_|  |_|_____\_____/_/    \_\______| |_____/|______|   \/  |_____/       --
-- 																					            --
--                           joshua#5319 glacielgaming#6969 Woodz#1668                          --
--------------------------------------------------------------------------------------------------

RegisterServerEvent('ethicalnotifications:PlayOnOne')
AddEventHandler('ethicalnotifications:PlayOnOne', function(clientNetId, soundFile, soundVolume)
    TriggerClientEvent('ethicalnotifications:PlayOnOne', clientNetId, soundFile, soundVolume)
end)

RegisterServerEvent('ethicalnotifications:PlayOnSource')
AddEventHandler('ethicalnotifications:PlayOnSource', function(soundFile, soundVolume)
    local source = source 
    TriggerClientEvent('ethicalnotifications:PlayOnOne', source, soundFile, soundVolume)
end)

RegisterServerEvent('ethicalnotifications:PlayOnAll')
AddEventHandler('ethicalnotifications:PlayOnAll', function(soundFile, soundVolume)
    TriggerClientEvent('ethicalnotifications:PlayOnAll', -1, soundFile, soundVolume)
end)

RegisterServerEvent('ethicalnotifications:PlayWithinDistance')
AddEventHandler('ethicalnotifications:PlayWithinDistance', function(maxDistance, soundFile, soundVolume)
    TriggerClientEvent('ethicalnotifications:PlayWithinDistance', -1, source, maxDistance, soundFile, soundVolume)
end)

--------------------------------------------------------------------------------------------------
--																				          	    --
--       ______ _______ _    _ _____ _____          _        _____  ________       _______      --
--      |  ____|__   __| |  | |_   _/ ____|   /\   | |      |  __ \|  ____\ \    / / ____|      --
--      | |__     | |  | |__| | | || |       /  \  | |      | |  | | |__   \ \  / / (___        --
--      |  __|    | |  |  __  | | || |      / /\ \ | |      | |  | |  __|   \ \/ / \___ \       --
--      | |____   | |  | |  | |_| || |____ / ____ \| |____  | |__| | |____   \  /  ____) |      --
--      |______|  |_|  |_|  |_|_____\_____/_/    \_\______| |_____/|______|   \/  |_____/       --
-- 																					            --
--                           joshua#5319 glacielgaming#6969 Woodz#1668                          --
--------------------------------------------------------------------------------------------------