--------------------------------------------------------------------------------------------------
--																				          	    --
--       ______ _______ _    _ _____ _____          _        _____  ________       _______      --
--      |  ____|__   __| |  | |_   _/ ____|   /\   | |      |  __ \|  ____\ \    / / ____|      --
--      | |__     | |  | |__| | | || |       /  \  | |      | |  | | |__   \ \  / / (___        --
--      |  __|    | |  |  __  | | || |      / /\ \ | |      | |  | |  __|   \ \/ / \___ \       --
--      | |____   | |  | |  | |_| || |____ / ____ \| |____  | |__| | |____   \  /  ____) |      --
--      |______|  |_|  |_|  |_|_____\_____/_/    \_\______| |_____/|______|   \/  |_____/       --
-- 																					            --
--                           joshua#5319 glacielgaming#6969 Woodz#1668                          --
--------------------------------------------------------------------------------------------------

local standardVolumeOutput = 1.0;

RegisterNetEvent('ethicalnotifications:PlayOnOne')
AddEventHandler('ethicalnotifications:PlayOnOne', function(soundFile, soundVolume)
    SendNUIMessage({
        transactionType     = 'playSound',
        transactionFile     = soundFile,
        transactionVolume   = soundVolume
    })
end)

RegisterNetEvent('ethicalnotifications:PlayOnAll')
AddEventHandler('ethicalnotifications:PlayOnAll', function(soundFile, soundVolume)
    SendNUIMessage({
        transactionType     = 'playSound',
        transactionFile     = soundFile,
        transactionVolume   = soundVolume
    })
end)

RegisterNetEvent('ethicalnotifications:PlayWithinDistance')
AddEventHandler('ethicalnotifications:PlayWithinDistance', function(playerNetId, maxDistance, soundFile, soundVolume)
    local lCoords = GetEntityCoords(GetPlayerPed(-1))
    local eCoords = GetEntityCoords(GetPlayerPed(GetPlayerFromServerId(playerNetId)))
    local distIs  = Vdist(lCoords.x, lCoords.y, lCoords.z, eCoords.x, eCoords.y, eCoords.z)
    if(distIs <= maxDistance) then
        SendNUIMessage({
            transactionType     = 'playSound',
            transactionFile     = soundFile,
            transactionVolume   = soundVolume
        })
    end
end)

RegisterNetEvent('ethicalnotifications:PlayWithinDistanceOS')
AddEventHandler('ethicalnotifications:PlayWithinDistanceOS', function(playerCoords, maxDistance, soundFile, soundVolume)
    local lCoords = GetEntityCoords(GetPlayerPed(-1))
    local distIs  = Vdist(lCoords.x, lCoords.y, lCoords.z, playerCoords.x, playerCoords.y, playerCoords.z)
    if(distIs <= maxDistance) then
        SendNUIMessage({
            transactionType     = 'playSound',
            transactionFile     = soundFile,
            transactionVolume   = soundVolume
        })
    end
end)

--------------------------------------------------------------------------------------------------
--																				          	    --
--       ______ _______ _    _ _____ _____          _        _____  ________       _______      --
--      |  ____|__   __| |  | |_   _/ ____|   /\   | |      |  __ \|  ____\ \    / / ____|      --
--      | |__     | |  | |__| | | || |       /  \  | |      | |  | | |__   \ \  / / (___        --
--      |  __|    | |  |  __  | | || |      / /\ \ | |      | |  | |  __|   \ \/ / \___ \       --
--      | |____   | |  | |  | |_| || |____ / ____ \| |____  | |__| | |____   \  /  ____) |      --
--      |______|  |_|  |_|  |_|_____\_____/_/    \_\______| |_____/|______|   \/  |_____/       --
-- 																					            --
--                           joshua#5319 glacielgaming#6969 Woodz#1668                          --
--------------------------------------------------------------------------------------------------