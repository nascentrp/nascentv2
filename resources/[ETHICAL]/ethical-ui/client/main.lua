RegisterNetEvent('ethical-ui:ShowUI')
AddEventHandler('ethical-ui:ShowUI', function(action, text)
	SendNUIMessage({
		action = action,
		text = text,
	})
end)

RegisterNetEvent('ethical-ui:HideUI')
AddEventHandler('ethical-ui:HideUI', function()
	SendNUIMessage({
		action = 'hide'
	})
end)

