local commValue = 60

RegisterNetEvent("bestbuds:dutyOn")
AddEventHandler("bestbuds:dutyOn", function()
    TriggerServerEvent("jobssystem:jobs", "best_buds")
end)

RegisterNetEvent('bestbuds:dutyMenu')
AddEventHandler('bestbuds:dutyMenu', function()
    TriggerEvent('ethical-context:sendMenu', {
        {
            id = 1,
            header = "Sign In / Off ",
            txt = ""
        },
        {
            id = 2,
            header = "Sign On Duty",
            txt = "Use this to sign in",
            params = {
                event = "bestbuds:dutyOn"
            }
        },
        {
            id = 3,
            header = "Sign Off Duty",
            txt = "Use this to sign off",
            params = {
                event = "bestbuds:dutyOff"
            }
        },
    })
end)

RegisterNetEvent("bestbuds:dutyOff")
AddEventHandler("bestbuds:dutyOff", function()
    TriggerServerEvent("jobssystem:jobs", "unemployed")
end)


RegisterNetEvent("ethical-bestbuds:bill")
AddEventHandler("ethical-bestbuds:bill", function()
    local bill = exports["ethical-applications"]:KeyboardInput({
        header = "Create Receipt",
        rows = {
            {
                id = 0,
                txt = "U ID"
            },
            {
                id = 1,
                txt = "Amount"
            }
        }
    })
    if bill then
        if tonumber(bill[2].input) < 500  and tonumber(bill[2].input) > 0 then
            TriggerServerEvent("ethical-bestbuds:bill:player", bill[1].input, bill[2].input)
        else
            TriggerEvent('DoLongHudText', 'You can\'t overcharge people like that. Attempt reported.', 2)
        end
    end
end)


RegisterNetEvent('ethical-bestbuds:createcustomerreceipt')
AddEventHandler('ethical-bestbuds:createcustomerreceipt', function(value,receiver)
local jobName = exports["isPed"]:isPed("myjob")
local amount = value
local type = "CUSTOMER"
local sender = exports["isPed"]:isPed("cid")
local receiver = receiver

TriggerEvent('nsrp_receipts:createreceipt',amount,jobName,sender,receiver,type)

end)

RegisterNetEvent("ethical-bestbuds:cash:in")
AddEventHandler("ethical-bestbuds:cash:in", function()
    local job = exports["isPed"]:isPed("myjob")
    local cid = exports["isPed"]:isPed("cid")
	if job == 'best_buds' then
        local qty = exports["ethical-inventory"]:getQuantity("receipt")
        if qty >= 1 then
            TriggerServerEvent('ethical-bestbuds:payout:employee', qty)
        end
        TriggerEvent("nsrp_receipts:payrecieptcl",job,cid,commValue)
	end
end)

RegisterNetEvent('ethical-bestbuds:pickup')
AddEventHandler('ethical-bestbuds:pickup', function()
	TriggerEvent("server-inventory-open", "1", "best_buds_counter");	
	Wait(1000)
end)

