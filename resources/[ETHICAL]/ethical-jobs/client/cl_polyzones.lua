NearHunting, NearHuntingSelling, NearScubaBoatRental, NearReturnBoat, IronSellSpot, SellUnknown, ProcessFish, NearWeazelNews,NearTaxiSignon = false, false, false, false, false, false, false,false,false
CutFish,SellSpotF,inGarbage = false, false, false
OnFishingDock = false

Citizen.CreateThread(function()
	exports["ethical-polyzone"]:AddBoxZone("start_hunting", vector3(-773.43, 5598.26, 33.61), 1.6, 2, {
        name="hunting",
        heading=346,
        --debugPoly=true,
        minZ=30.81,
        maxZ=34.81
    }) 
    exports["ethical-polyzone"]:AddBoxZone("hunting_sell", vector3(960.75, -2111.42, 31.95), 2.5, 2.5, {
        name="hunting_sell",
        heading=265,
        --debugPoly=true,
        minZ=30.95,
        maxZ=32.95
    })

    --Scuba
    exports["ethical-polyzone"]:AddBoxZone("scuba_return_boat", vector3(-1602.02, 5260.31, 0.11), 10, 10, {
        name="scuba_return_boat",
        heading=25,
        --debugPoly=true,
        minZ=-5.0,
        maxZ=5.0
    })  
    exports["ethical-polyzone"]:AddBoxZone("sell_iron_bars", vector3(2341.04, 3128.18, 48.21), 1.8, 1, {
        name="sell_iron_bars",
        heading=260,
        --debugPoly=true,
        minZ=47.21,
        maxZ=49.21
    }) 
    exports["ethical-polyzone"]:AddBoxZone("sell_unknown_material", vector3(-1459.32, -413.59, 35.75), 1.8, 1, {
        name="sell_unknown_material",
        heading=75,
        --debugPoly=true,
        minZ=34.55,
        maxZ=36.95
    }) 

    -- Fishing 
    exports["ethical-polyzone"]:AddBoxZone("fishing_dock", vector3(-3428.42, 965.99, 8.34), 1.0, 27.0, {
        name="fishing_dock",
        heading=90,
        --debugPoly=true,
        minZ=7.35,
        maxZ=9.2
    })

    exports["ethical-polyzone"]:AddBoxZone("fishing_sushi", vector3(-3248.21, 992.68, 12.49), 8.4, 5, {
        name="fishing_sushi",
        heading=85,
        --debugPoly=true,
        minZ=11.49,
        maxZ=13.44
    }) 

    exports["ethical-polyzone"]:AddBoxZone("fishing_cut", vector3(-3425.90, 974.31, 8.34), 3.0, 4.0, {
        name="fishing_cut",
        heading=270,
        --debugPoly=true,
        minZ=7.35,
        maxZ=9.2
    })

    exports["ethical-polyzone"]:AddBoxZone("fishing_sell", vector3(-1039.5, -1396.63, 5.42), 4.8, 4, {
        name="fishing_sell",
        heading=345,
        --debugPoly=true,
        minZ=4.42,
        maxZ=6.42
    }) 

    exports["ethical-polyzone"]:AddBoxZone("idcard", vector3(256.27, -369.48, -44.14), 2.4, 1, {
        name="idcard",
        heading=70,
        --debugPoly=true,
        minZ=-45.14,
        maxZ=-43.54
    }) 

    exports["ethical-polyzone"]:AddBoxZone("garbagetruck", vector3(-332.44903564453, -1516.421875, 27.542633056641), 2.4, 2.4, {
        name="garbagetruck",
        heading=70,
        -- debugPoly=true,
        minZ=24.14,
        maxZ=28.54
    }) 

    -- Weazel News Job
    exports["ethical-polyzone"]:AddBoxZone("Weazel", vector3(-598.17, -929.75, 25.34), 2.9, 5, {
        name="Weazel",
        heading=91,
        --debugPoly=true,
        minZ=22.14,
        maxZ=24.74
      })
end)

RegisterNetEvent('ethical-polyzone:enter')
AddEventHandler('ethical-polyzone:enter', function(name)
    if name == "start_hunting" then
        NearHunting = true
        AtHuntingSpot()
        if OnGoingHuntSession == true then
            TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("Return Sniper"))
        else
            TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("Hunting (R500 deposit)"))
        end
    elseif name == "hunting_sell" then
        NearHuntingSelling = true
        AtSellingSpotHunting()
        TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("Hunting (Sell Meat)"))
    elseif name == 'scuba_return_boat' then
        local vehicle = GetVehiclePedIsIn(PlayerPedId(), false)
        local vehicleclass = GetVehicleClass(vehicle)
        if vehicle ~= 0 and vehicleclass == 14 then
            TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("Return Boat")) 
            NearReturnBoat = true
            AtReturnSpot()
        end
    elseif name == "sell_iron_bars" then
        IronSellSpot = true
        TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("Sell Items")) 
        IronSell()
    elseif name == "sell_unknown_material" then
        SellUnknown = true
        TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("Sell Items")) 
        SellUnknownSpot()
    elseif name == "fishing_dock" then
        OnFishingDock = true
        TriggerEvent('ethical-ui:ShowUI', 'show', ("[G] %s"):format("Begin fishing"))
    elseif name == "fishing_sushi" then
        ProcessFish = true
        TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("Process Cut Fish")) 
        ProcessFishSpot()
    elseif name == "fishing_cut" then
        CutFish = true
        TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("Cut Fish")) 
        CutFishSpot()
    elseif name == "fishing_sell" then
        SellSpotF = true
        TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("Sell Sushi")) 
        SellSpotFish()
    elseif name == "Weazel" then
        NearWeazelNews = true
        NearWeazelNews2()
        TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("Record The News!"))
    elseif name == "Taxi" then
        NearTaxiSignon = true
        NearTaxiSignOn()
        TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("Go on duty as a Taxi Driver"))
    elseif name == "garbagetruck" then
        if exports["isPed"]:isPed("myjob") == 'garbage' then
            inGarbage = true
            TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("To spawn a garbage truck"))
            allowSpawnGarbageTruck()
        end
    end
end)

RegisterNetEvent('ethical-polyzone:exit')
AddEventHandler('ethical-polyzone:exit', function(name)
    if name == "start_hunting" then
        NearHunting = false
        atRefundSpot = false
    elseif name == "hunting_sell" then
        NearHuntingSelling = false
    elseif name == 'scuba_return_boat' then
        NearReturnBoat = false
    elseif name == "sell_iron_bars" then
        IronSellSpot = false
    elseif name == "sell_unknown_material" then
        SellUnknown = false
    elseif name == "fishing_dock" then
        OnFishingDock = false
    elseif name == "fishing_sushi" then
        ProcessFish = false
    elseif name == "fishing_cut" then
        CutFish = false
    elseif name == "fishing_sell" then
        SellSpotF = false
    elseif name == "Weazel" then
        NearWeazelNews = false
    elseif name == "Taxi" then
        NearTaxiSignon = false
    elseif name == "idcard" then
        NearCourthouse = false
    elseif name == "garbagetruck" then
        inGarbage = false
    end
    TriggerEvent('ethical-ui:HideUI')
end)

function allowSpawnGarbageTruck()
    local garbageTruckHasTriggered = false
    Citizen.CreateThread(function()
        while inGarbage do
            if IsControlJustPressed(0, 38) and not garbageTruckHasTriggered then
                TriggerEvent('ethical-garbage:starting')
                garbageTruckHasTriggered = true
                Wait(500)
            end
            Wait(0)
        end
    end)
end

CanHunt = false
RegisterNetEvent("ethical-hunting:allowed")
AddEventHandler("ethical-hunting:allowed", function(allowed)
    CanHunt = allowed
end)


isHuntingSpotRunning = false
function AtHuntingSpot()
    Citizen.CreateThread(function()
        isHuntingSpotRunning = true
        local busyWithHuntingLoop = false
        local CanHunt = exports["isPed"]:isPed("huntinglicense")
        while NearHunting do
            Citizen.Wait(0)
            if IsControlJustReleased(0, 38) and not busyWithHuntingLoop then
                busyWithHuntingLoop = true

                if CanHunt and allowedHunting and not exports["ethical-inventory"]:hasEnoughOfItem("100416529",1,false) then --You Dont have a rifle that needs to be returned, you can hunt and timeout is over
                    TriggerServerEvent('ethical:huntingdeposit')
                    Citizen.Wait(2500)
                elseif exports["ethical-inventory"]:hasEnoughOfItem("100416529",1,false) and not OnGoingHuntSession then --If you still have the rifle and your session has ended, allow to return rifle
                    TriggerServerEvent('ethical-hunting:removeloadout')
                    TriggerServerEvent('ethical:huntingreturnree')
                    TriggerEvent("DoLongHudText", "Thank you for returning the hunting rifle, here is some of your deposit back.", 2)
                    Citizen.Wait(2500)
                elseif not allowedHunting and OnGoingHuntSession then
                    Citizen.CreateThread(function() 
                        StartHuntingSession()
                    end)
                    Citizen.Wait(5000)
                elseif not allowedHunting and not OnGoingHuntSession then
                    TriggerEvent("DoLongHudText", "Hunting is on cooldown, you may hunt again when 30 minutes have passed since the start of your last hunting trip.", 2)
                else
                    if not CanHunt then
                        TriggerEvent("DoLongHudText", "You need a hunting license.", 2)
                    end
                end
            end
        end
        busyWithHuntingLoop = false
        isHuntingSpotRunning = false
    end)
end

function AtSellingSpotHunting()
    local isRunning = false
    Citizen.CreateThread(function()
        while NearHuntingSelling do
            Citizen.Wait(5)
            if IsControlJustReleased(0, 38) and not isRunning then
                if exports["ethical-inventory"]:getQuantity("deer") >= 2 then
                    isRunning = true
                    playerAnim()
                    TriggerEvent('inventory:removeItem', 'deer', 2)
                    local finished = exports["ethical-taskbar"]:taskBar(4000,"Selling Meat",false)
                    if finished == 100 then
                        ClearPedTasksImmediately(PlayerPedId())
                        TriggerServerEvent('ethical-hunting:sell')
                        isRunning = false
                    end
                else
                    TriggerEvent("DoLongHudText", "You need at least 2 meat.", 2)
                end
            end
        end
    end)
end

RegisterNetEvent("ethical-hunting:allowed")
AddEventHandler("ethical-hunting:allowed", function(allowed)
    CanHunt = allowed
end)

function AtReturnSpot()
    Citizen.CreateThread(function()
        while NearReturnBoat do
            Citizen.Wait(5)
            local vehicle = GetVehiclePedIsIn(PlayerPedId(), false)
            if IsControlJustReleased(0, 38) and vehicle ~= 0 then
                DeleteEntity(vehicle)
                vehicle = 0
                TriggerEvent('DoLongHudText', 'Thanks For Returning The Vehicles, Heres some of the Money Back!', 1)
                RemoveBlip(allBlips)
                RemoveBlip(allBlipsSprite)
                TriggerServerEvent('ethical-scuba:returnDepo')
                SetEntityCoords(GetPlayerPed(-1), -1605.7166748047, 5259.1162109375, 2.0883903503418)
                SetEntityHeading(GetPlayerPed(-1), 23.752769470215)
                Citizen.Wait(2000)
                canSpawn = true
            end
        end
    end)
end


function IronSell()
    Citizen.CreateThread(function()
        while IronSellSpot do
            Citizen.Wait(5)
            if IsControlJustReleased(0, 38) then
                if exports["ethical-inventory"]:hasEnoughOfItem('ironbar',10,false) then
                    TriggerEvent("inventory:removeItem", "ironbar", 10)
                    local finished = exports["ethical-taskbar"]:taskBar(2000,"Selling Iron")
                    if (finished == 100) then
                        TriggerServerEvent('ethical-scuba:paySalvage', math.random(250, 300))
                    end
                else
                    TriggerEvent('DoLongHudText', 'You\'re missing something.', 2)
                end
            end
        end
    end)
end

function SellUnknownSpot()
    Citizen.CreateThread(function()
        while SellUnknown do
            Citizen.Wait(5)
            if IsControlJustReleased(0, 38) then
                if exports["ethical-inventory"]:hasEnoughOfItem('umetal',10,false) then
                    local finished = exports["ethical-taskbar"]:taskBar(5000,"Trading")
                    if (finished == 100 and payed == false) then
                        payed = true
                        TriggerServerEvent('ethical-scuba:makeGold')
                        Citizen.Wait(500)
                        payed = false
                    end
                else
                    TriggerEvent('DoLongHudText', 'You\'re missing something.', 2)
                end
            end 
        end
    end)
end

local DoingTask = false
function ProcessFishSpot()
    Citizen.CreateThread(function()
        while ProcessFish do
            Citizen.Wait(5)
            if IsControlJustReleased(0, 38) then
                if not DoingTask then
                    if exports["ethical-inventory"]:getQuantity("cutfish") >= 1 then
                        playerAnim()
                        DoingTask = true
                        TriggerEvent('inventory:removeItem', 'cutfish', 1)
                        local finished = exports["ethical-taskbar"]:taskBar(4000,"Processing")
                        if finished == 100 then
                            ClearPedTasksImmediately(PlayerPedId())
                            Citizen.Wait(1000)
                            TriggerServerEvent('ethical-fish:process')
                            DoingTask = false
                        else
                            DoingTask = false
                            TriggerEvent('player:receiveItem', 'cutfish', 1)
                        end
                    else 
                        TriggerEvent('DoLongHudText', 'You are missing cut fish.', 2)
                    end
                end
            end
        end
        DoingTask = false
    end)
end

local DoingTask2 = false
function CutFishSpot()
    Citizen.CreateThread(function()
        while CutFish do
            Citizen.Wait(5)
            if IsControlJustReleased(0, 38) and not DoingTask2 then
                DoingTask2 = true
                TriggerEvent('nsrp_economy:cutFishingLoot')
                Citizen.Wait(1000)
            end
        end
        DoingTask2 = false
    end)
end

RegisterNetEvent('fishing:clearCuttingProcess')
AddEventHandler('fishing:clearCuttingProcess', function()
    DoingTask2 = false
end)

local DoingTask3 = false
function SellSpotFish()
   Citizen.CreateThread(function()
       while SellSpotF do
           Citizen.Wait(0)
            if IsControlJustReleased(0, 38) and not DoingTask3 then
                if exports["ethical-inventory"]:getQuantity("sushiroll") >= 5 then
                    playerAnim()
                    DoingTask3 = true 
                    TriggerEvent("inventory:removeItem", "sushiroll", 5)
                    local finished = exports["ethical-taskbar"]:taskBar(4000,"Selling Sushi")
                    if finished == 100 then
                        local sushi = math.random(30, 50)
                        DoingTask3 = false
                        TriggerServerEvent('ethical-fish:sellSushi', sushi)
                    else
                        DoingTask3 = false
                    end
                else
                    TriggerEvent("DoLongHudText", "You need at least 5 Sushi", 2)
                end
           end
       end
   end)
end

function NearWeazelNews2()
    Citizen.CreateThread(function()
        local myJob = exports["isPed"]:isPed("myJob")
        local buttonPressed = false
        while NearWeazelNews do
            Citizen.Wait(5)
            
            if IsControlJustReleased(0, 38) and not buttonPressed then
                buttonPressed = true
                print(myJob)
                if canSpawn and myJob ~= "news" then
                    canSpawn = false
                    TriggerEvent('ethical-news:startshift')
                    --register weazel news job
                    TriggerEvent('ethical-news:signon')
                end
            end
        end
        buttonPressed = false
    end)
end

function NearTaxiSignOn()
    Citizen.CreateThread(function ()
        
        local myJob = exports["isPed"]:isPed("myJob")
        local buttonPressed = false
        while NearTaxiSignon do
            Citizen.Wait(5)
            
            if IsControlJustReleased(0, 38) and not buttonPressed then
                buttonPressed = true
                print(myJob)
                TriggerEvent('ethical-taxi:signonmenu')
            end
        end
        buttonPressed = false
    end)
end