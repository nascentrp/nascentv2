local commValue = 100
local menuDegredation = {}

RegisterNetEvent("ethical-mechanic:bill")
AddEventHandler("ethical-mechanic:bill", function()
    local bill = exports["ethical-applications"]:KeyboardInput({
        header = "Create Receipt",
        rows = {
            {
                id = 0,
                txt = "U ID"
            },
            {
                id = 1,
                txt = "Amount"
            }
        }
    })
    if bill then
        if (tonumber(bill[2].input) < 10000 and tonumber(bill[2].input) > 0) then
            TriggerServerEvent("ethical-mechanic:bill:player", bill[1].input, bill[2].input)
        else
            TriggerEvent('DoLongHudText', 'You can\'t overcharge people like that. Attempt reported.', 2)
        end
    end
end)


RegisterNetEvent('ethical-mechanic:createcustomerreceipt')
AddEventHandler('ethical-mechanic:createcustomerreceipt', function(value,receiver)
local jobName = exports["isPed"]:isPed("myjob")
local amount = value
local type = "CUSTOMER"
local sender = exports["isPed"]:isPed("cid")
local receiver = receiver

TriggerEvent('nsrp_receipts:createreceipt',amount,jobName,sender,receiver,type)

end)

RegisterNetEvent("ethical-mechanic:cash:in")
AddEventHandler("ethical-mechanic:cash:in", function()
    local job = exports["isPed"]:isPed("myjob")
    local cid = exports["isPed"]:isPed("cid")
	if job == 'tuner_shop' or job == 'auto_exotics' or job == 'paleto_mech' or job == 'auto_bodies' then
        local qty = exports["ethical-inventory"]:getQuantity("receipt")
        if qty >= 1 then
            TriggerServerEvent('ethical-mechanic:payout:employee', qty)
        end
        TriggerEvent("nsrp_receipts:payrecieptcl",job,cid,commValue)
	end
end)

RegisterNetEvent("ethical-menu:senddegredation")
AddEventHandler("ethical-menu:senddegredation", function(degredation)
 menuDegredation = degredation

end)



RegisterNetEvent("ethical-mechanic:repairpart")
AddEventHandler("ethical-mechanic:repairpart", function(partname)
    local partValue = 0
    --print(json.encode(menuDegredation))
    if menuDegredation ~= nil then
        if partname[1] == "fuel" then
            partValue = menuDegredation.fuel_tank
        elseif partname[1] == "injector" then
            partValue = menuDegredation.fuel_injector
        elseif partname[1] == "axle" then
            partValue = menuDegredation.axle
        elseif partname[1] == "radiator" then
            partValue = menuDegredation.radiator
        elseif partname[1] == "clutch" then
            partValue = menuDegredation.clutch
        elseif partname[1] == "transmission" then
            partValue = menuDegredation.transmission
        elseif partname[1] == "electronics" then
            partValue = menuDegredation.electronics
        end
    end

    local heading = "Repair " .. partname[1] .. "<br>" .. "Last known degredation was: " .. tostring(partValue/10)
    local partToFix = partname[1]
    local amounttofix = exports["ethical-applications"]:KeyboardInput({
        header = heading,
        rows = {
            {
                id = 0,
                txt = "Amount to Repair (Out of 10)"
            }
        }
    })
    if amounttofix then
        if (tonumber(amounttofix[1].input) <= 10 and tonumber(amounttofix[1].input) > 0) then
            local args = {
                [1] = partToFix,
                [2] = tonumber(amounttofix[1].input)
            }
            TriggerEvent("ethical-mechanic:repairredirect", args)
        else
            TriggerEvent('DoLongHudText', 'Invalid amount to repair, please use a number between 0 and 10', 2)
        end
    end
end)