local truckplate = false
local truckcoords
local inTruck
local missionBlip = nil
local binCoords = false
local maxruns = 0
local runs = 0
local arrived 
local jobBlip
local submitBlip
local submitCoords = vector3(-342.11, -1530.78, 27.43)
local clockRoom = vector3(-321.70, -1545.94, 31.02)
local doingGarbage = false
local jobCompleted = false
local garbageHQBlip = 0
local truckTaken = false

local JobCoords = {
    {x = 114.83280181885, y = -1462.3127441406, z = 29.295083999634},
    {x = -6.0481648445129, y = -1566.2338867188, z = 29.209197998047},
    {x = -1.8858588933945, y = -1729.5538330078, z = 29.300233840942},
    {x = 159.09, y = -1816.69, z = 27.9},
    {x = 358.94696044922, y = -1805.0723876953, z = 28.966590881348},
    {x = 481.36560058594, y = -1274.8297119141, z = 29.64475440979},
    {x = 254.70010375977, y = -985.32482910156, z = 29.196590423584},
    {x = 240.08079528809, y = -826.91204833984, z = 30.018426895142},
    {x = 342.78308105469, y = -1036.4720458984, z = 29.194206237793},
    {x = 462.17517089844, y = -949.51434326172, z = 27.959424972534},
    {x = 317.53698730469, y = -737.95416259766, z = 29.278547286987},
    {x = 410.22503662109, y = -795.30517578125, z = 29.20943069458},
    {x = 398.36038208008, y = -716.35577392578, z = 29.282489776611},
    {x = 443.96984863281, y = -574.33978271484, z = 28.494501113892},
    {x = -1332.53, y = -1198.49, z = 4.62},
    {x = -45.443946838379, y = -191.32261657715, z = 52.161594390869},
    {x = -31.948055267334, y = -93.437454223633, z = 57.249073028564},
    {x = 283.10873413086, y = -164.81878662109, z = 60.060565948486},
    {x = 441.89678955078, y = 125.97653198242, z = 99.887702941895},
}

local Dumpsters = {
    "prop_dumpster_01a",
    "prop_dumpster_02a",
    "prop_dumpster_02b",
    "prop_dumpster_3a",
    "prop_dumpster_4a",
    "prop_dumpster_4b",
    "prop_skip_01a",
    "prop_skip_02a",
    "prop_skip_06a",
    "prop_skip_05a",
    "prop_skip_03",
    "prop_skip_10a"
}

Citizen.CreateThread(function()
	local modelHash = GetHashKey("s_m_y_garbage")
	RequestModel(modelHash)
	while not HasModelLoaded(modelHash) do
		Wait(1)
	end
	local ped = CreatePed(0, modelHash , -322.04, -1545.80, 30.0699, true)
	SetEntityHeading(ped,  274.31)
	SetEntityInvincible(ped, true)
	FreezeEntityPosition(ped, true)
    
	local modelHash2 = GetHashKey("ig_patricia")
	RequestModel(modelHash2)
	while not HasModelLoaded(modelHash2) do
		Wait(1)
	end
	local ped2 = CreatePed(0, modelHash2 , -355.79, -1541.99, 26.72, true)
	--SetBlockingOfNonTemporaryEvents(ped2, true)
	SetEntityHeading(ped2,  274.31)
	SetEntityInvincible(ped2, true)
	FreezeEntityPosition(ped2, true)
    -- TriggerEvent("server-inventory-open", "103", "Shop")

    while true do
        Citizen.Wait(2000)
        local myJob = exports["isPed"]:isPed("myjob")
        if garbageHQBlip == nil or garbageHQBlip == 0 then
            garbageHQBlip = AddBlipForCoord(clockRoom)
            SetBlipSprite(garbageHQBlip, 467)
            SetBlipDisplay(garbageHQBlip, 4)
            SetBlipScale(garbageHQBlip, 1.0)
            SetBlipColour(garbageHQBlip, 25)
            SetBlipAsShortRange(garbageHQBlip, true)
            BeginTextCommandSetBlipName("STRING")
            AddTextComponentString("Garbage HQ")
            EndTextCommandSetBlipName(garbageHQBlip)
        end
    end
end)

RegisterNetEvent('ethical-garbage:openReyclingShop')
AddEventHandler('ethical-garbage:openReyclingShop', function()
    TriggerEvent("server-inventory-open", "103", "Craft")
end)

RegisterNetEvent('ethical-garbage:signon')
AddEventHandler('ethical-garbage:signon', function()
	TriggerEvent('ethical-context:sendMenu', {
        {
            id = 1,
            header = "Sign In / Off ",
            txt = ""
        },
        {
            id = 2,
            header = "Signing On Duty",
			txt = "Use this to sign in",
			params = {
                event = "ethical-garbage:duty"
            }
        },
		{
            id = 3,
            header = "Signing Off Duty",
			txt = "Use this to sign off",
			params = {
                event = "ethical-garbage:dutyOff"
            }
        },
    })
end)

RegisterNetEvent('ethical-garbage:duty')
AddEventHandler('ethical-garbage:duty', function()
    havingGarbageJob = true    
    TriggerServerEvent("jobssystem:jobs", "garbage")
    TriggerEvent("DoLongHudText","You are now on duty, go grab a garbage truck!",1)
end)

RegisterNetEvent('ethical-garbage:dutyOff')
AddEventHandler('ethical-garbage:dutyOff', function()
    havingGarbageJob = false    
    TriggerServerEvent("jobssystem:jobs", "unemployed")
    TriggerEvent("DoLongHudText","You are now off duty.",1)
end)

RegisterNetEvent('ethical-garbage:starting')
AddEventHandler('ethical-garbage:starting', function()
    --truckTaken = false -- temp for test
    if havingGarbageJob then
        if not truckTaken then 
            truckTaken = true
            local random = math.random(1, #JobCoords)
            local coordVec = vector3(JobCoords[random].x, JobCoords[random].y, JobCoords[random].z)
            local engine = not GetIsVehicleEngineRunning(veh)
            local car = GetHashKey('trash')
            inTruck = false
            RequestModel(car)
            while not HasModelLoaded(car) do
                Citizen.Wait(0)
            end
            vehicle = CreateVehicle(car, -323.53, -1523.58, 27.00, 269.7, true, false)
            truckplate = GetVehicleNumberPlateText(vehicle)
            inTruck = true
            SetEntityAsMissionEntity(vehicle, true, true)
            TaskWarpPedIntoVehicle(GetPlayerPed(-1), vehicle, -1) 
            TriggerEvent("keys:addNew", vehicle, truckplate)
            SetVehicleEngineOn(vehicle, engine, false, true)
            print("turning vehicle on from ethical-jobs 163")
            Citizen.Wait(1000)
            missionStart(coordVec,vehicle)
        else
            TriggerEvent('DoShortHudText', 'You have already taken a truck for the job!', 2)
        end
    end
end)

function submit()
    Citizen.CreateThread(function()
        local helpTextActive = false
        local pressed = false
        local wait = 100
        while true do
            Citizen.Wait(wait)
            local playerPed = GetPlayerPed(-1)
            local plyCoords = GetEntityCoords(playerPed)
            local distance = GetDistanceBetweenCoords(plyCoords,submitCoords, true) 
            if distance < 20 then
                wait = 5
                if IsPedInAnyVehicle(playerPed) then
                    DrawMarker(2, submitCoords+vector3(0.0,0.0,2.0), 0.0, 0.0, 0.0, 0, 0.0, 0.0, 1.0, 1.0, 1.0, 255, 0, 0, 150, false, true, 2, false, false, false, false)
                    local truck = GetVehiclePedIsIn(playerPed, false)
                    local plate = GetVehicleNumberPlateText(truck)
                    if distance < 2.0 then
                        DisplayHelpText(("[E] %s"):format("to drop off trash"))
                        helpTextActive = true
                        if IsControlJustReleased(1,46) and not pressed then
                            truckTaken = false
                            pressed = true
                            RemoveBlip(submitBlip)
                            HideHelpText()
                            helpTextActive = false;
                            if plate == truckplate then
                                jobCompleted = true
                                exports["ethical-taskbar"]:taskBar(5000, "Dropping off trash")
                                TriggerServerEvent('ethical-garbage:pay', jobCompleted)
                                jobCompleted = false
                                while DoesEntityExist(truck) do
                                    deleteCar(truck)
                                    Citizen.Wait(5)
                                end
                                truckplate = false
                                Citizen.Wait(1000)
                                pressed = false    
                                return
                            else
                                TriggerEvent('DoShortHudText', 'This is not our vehicle!', 2)
                                Citizen.Wait(1000)
                                pressed = false
                            end
                            Citizen.Wait(1000)
                            pressed = false
                        end
                    elseif helpTextActive then
                        HideHelpText()
                        helpTextActive = false;
                    end
                end
            else
                wait = 100
            end
        end
    end)
end

function missionStart(coordVec,xtruck)
    local vehicle = xtruck
    arrived = false
    missionBlip = AddBlipForCoord(coordVec)
    SetBlipRoute(missionBlip, true)
    SetBlipRouteColour(missionBlip, 25)
    SetBlipColour(missionBlip, 25)
    Citizen.CreateThread(function()
        local wait = 100
        while not arrived do
            Citizen.Wait(wait)
            local tempdist = GetDistanceBetweenCoords(coordVec, GetEntityCoords(GetPlayerPed(-1)),true)
            if  tempdist < 80 then
                wait = 5
                TriggerEvent('DoShortHudText', 'Proceed to the refuse bin marked on your map.', 3)
                arrived = true
                DrawMarker(20, coordVec + vector3(0.0,0.0,3.5), 0.0, 0.0, 0.0, 0, 0.0, 0.0, 2.0, 2.0, 1.0, 0, 120, 0, 200, false, true, 2, false, false, false, false)
                maxruns  = math.random(6,10)
                Citizen.Wait(1000)
                SetBlipRoute(missionBlip, false)
                RemoveBlip(missionBlip)
                findtrashbins(coordVec,vehicle,0)
            else
                wait = 100
            end
        end
    end)      
end

function findtrashbins(coordVec,xtruck,pickup)
    doingGarbage = true
    local location = coordVec
    local vehicle = xtruck
    local playerPed = GetPlayerPed(-1)
    local boneindex = GetPedBoneIndex(playerPed, 57005)
    local helpTextActive = false
    runs = pickup

    if not HasAnimDictLoaded("anim@heists@narcotics@trash") then
        RequestAnimDict("anim@heists@narcotics@trash")
    end
    while not HasAnimDictLoaded("anim@heists@narcotics@trash") do
        Citizen.Wait(0)
    end

    if runs < maxruns then
        angle = math.random()*math.pi*2;
        r = math.sqrt(math.random());
        x = coordVec.x + r * math.cos(angle) * 100;     
        y = coordVec.y + r * math.sin(angle) * 100;
        for i = 0, #Dumpsters, 1 do 
            local NewBin = GetClosestObjectOfType(x, y, coordVec.z, 100.0, GetHashKey(Dumpsters[i]), false)
            if NewBin ~= 0 then
                local dumpCoords = GetEntityCoords(NewBin)
                jobBlip = AddBlipForCoord(dumpCoords)
                SetBlipSprite(jobBlip, 420)
                SetBlipScale(jobBlip, 0.7)
                SetBlipColour(jobBlip, 25)
                while true do
                    Wait(5) 
                    local userDist = GetDistanceBetweenCoords(dumpCoords,GetEntityCoords(GetPlayerPed(-1)),true) 
                    if userDist < 50 then
                        DrawMarker(20, dumpCoords + vector3(0.0,0.0,2.5), 0.0, 0.0, 0.0, 0, 0.0, 0.0, 2.0, 2.0, 1.0, 120, 0, 0, 200, false, true, 2, false, false, false, false)
                        if userDist < 5 then
                            -- chopshop:removevehicle
                            DisplayHelpText(("[E] %s"):format("to collect trash"))
                            helpTextActive = true
                            if IsControlJustReleased(1,46) then
                                local geeky = CreateObject(GetHashKey("hei_prop_heist_binbag"), 0, 0, 0, true, true, true)
                                AttachEntityToEntity(geeky, playerPed, boneindex, 0.12, 0.0, 0.00, 25.0, 270.0, 180.0, true, true, false, true, 1, true)
                                TaskPlayAnim(PlayerPedId(-1), 'anim@heists@narcotics@trash', 'walk', 1.0, -1.0,-1,49,0,0, 0,0)
                                RemoveBlip(jobBlip)
                                collectedtrash(geeky,vehicle,location,runs)
                                HideHelpText()
                                helpTextActive = false
                                return
                            end
                        elseif helpTextActive then
                            HideHelpText()
                            helpTextActive = false
                        end
                    end
                end
                return
            end
        end
    else
        submit()
        doingGarbage = false
        TriggerEvent('DoShortHudText', 'Suburb is now clean. Return to the dispatch for your pay.', 3)
        submitBlip = AddBlipForCoord(submitCoords)
        SetBlipColour(submitBlip, 25)
    end
end

local trashCollected = false

function collectedtrash(geeky,vehicle,location,pickup)
    local wait = 100
    local trashbag = geeky
    local pressed = false     
    local helpTextActive = false                    
    while true do
        Wait(wait)
        local trunkcoord = GetWorldPositionOfEntityBone(vehicle, GetEntityBoneIndexByName(vehicle, "platelight"))
        local tdistance = GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)),trunkcoord)
        local runs = pickup
        if tdistance < 20 then
            wait = 5
            DrawMarker(20, trunkcoord + vector3(0.0,0.0,0.5), 0.0, 0.0, 0.0, 0, 0.0, 0.0, 1.0, 1.0, 1.0, 0, 120, 0, 200, false, true, 2, false, false, false, false)
            if tdistance < 2 then
                DisplayHelpText(("[E] %s"):format("to load trash"))
                helpTextActive = true
                if IsControlJustReleased(1, 46) and not pressed then
                    if GetVehicleDoorAngleRatio(vehicle, 5) ~= 0 then
                        HideHelpText()
                        helpTextActive = false
                        pressed = true
                        trashCollected = false
                        ClearPedTasksImmediately(GetPlayerPed(-1))
                        TaskPlayAnim(GetPlayerPed(-1), 'anim@heists@narcotics@trash', 'throw_b', 1.0, -1.0,-1,2,0,0, 0,0)
                        Citizen.Wait(100)
                        DeleteObject(trashbag)
                        Citizen.Wait(3000)
                        ClearPedTasksImmediately(GetPlayerPed(-1))
                        TriggerEvent('DoShortHudText', 'Trash loaded, on to the next one.', 3)        
                        findtrashbins(location,vehicle,runs+1)
                        pressed = false
                        return
                    else 
                        TriggerEvent('DoShortHudText', 'The Rear Loader is closed.', 2)                                    
                    end
                end
            elseif helpTextActive then
                HideHelpText()
                helpTextActive = false
            end
        end
    end
end

function deleteCar( entity )
    Citizen.InvokeNative( 0xEA386986E786A54F, Citizen.PointerValueIntInitialized( entity ) )
end

function LocalPed()
	return PlayerPedId()
end

function DisplayHelpText(str)
    TriggerEvent('ethical-ui:ShowUI', 'show', str)
end

function HideHelpText()
    TriggerEvent('ethical-ui:HideUI')
end