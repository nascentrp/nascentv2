local commValue = 60

RegisterNetEvent('ethical-beanmachine:signonmenu')
AddEventHandler('ethical-beanmachine:signonmenu', function()
  TriggerEvent('ethical-context:sendMenu', {
    {
        id = 1,
        header = "Sign In / Off ",
        txt = ""
    },
    {
        id = 2,
        header = "Sign On Duty",
        txt = "Use this to sign in",
        params = {
          event = "ethical-beanmachine:dutycl"
        }
    },
    {
        id = 3,
        header = "Sign Off Duty",
        txt = "Use this to sign off",
        params = {
            event = "ethical-beanmachine:offdutycl"
        }
    }
  })
end)

RegisterNetEvent('ethical-beanmachine:dutycl')
AddEventHandler('ethical-beanmachine:dutycl', function()
    TriggerServerEvent("ethical-beanmachine:duty")
    TriggerEvent("DoLongHudText","You are now on Duty",1)

end)

RegisterNetEvent('ethical-beanmachine:offdutycl')
AddEventHandler('ethical-beanmachine:offdutycl', function()
    TriggerServerEvent("ethical-beanmachine:offduty")
    TriggerEvent("DoLongHudText","You are no longer on duty, enjoy the break.",1)
end)

RegisterNetEvent("ethical-beanmachine:bill")
AddEventHandler("ethical-beanmachine:bill", function()
    local bill = exports["ethical-applications"]:KeyboardInput({
        header = "Create Receipt",
        rows = {
            {
                id = 0,
                txt = "U ID"
            },
            {
                id = 1,
                txt = "Amount"
            }
        }
    })
    if bill then
        if tonumber(bill[2].input) < 500  and tonumber(bill[2].input) > 0 then
            TriggerServerEvent("ethical-beanmachine:bill:player", bill[1].input, bill[2].input)
        else
            TriggerEvent('DoLongHudText', 'You can\'t overcharge people like that. Attempt reported.', 2)
        end
    end
end)


RegisterNetEvent('ethical-beanmachine:createcustomerreceipt')
AddEventHandler('ethical-beanmachine:createcustomerreceipt', function(value,receiver)
local jobName = exports["isPed"]:isPed("myjob")
local amount = value
local type = "CUSTOMER"
local sender = exports["isPed"]:isPed("cid")
local receiver = receiver

TriggerEvent('nsrp_receipts:createreceipt',amount,jobName,sender,receiver,type)

end)

RegisterNetEvent("ethical-beanmachine:cash:in")
AddEventHandler("ethical-beanmachine:cash:in", function()
    local job = exports["isPed"]:isPed("myjob")
    local cid = exports["isPed"]:isPed("cid")
	if job == 'bean_machine' then
        local qty = exports["ethical-inventory"]:getQuantity("receipt")
        if qty >= 1 then
            TriggerServerEvent('ethical-beanmachine:payout:employee', qty)
        end
        TriggerEvent("nsrp_receipts:payrecieptcl",job,cid,commValue)
	end
end)

RegisterNetEvent('ethical-beanmachine:pickup')
AddEventHandler('ethical-beanmachine:pickup', function()
	TriggerEvent("server-inventory-open", "1", "bean_tray");	
	Wait(1000)
end)