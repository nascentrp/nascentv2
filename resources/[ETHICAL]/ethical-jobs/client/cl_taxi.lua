local taxiRequestOnCooldown = false
local requesterNumber = 'Number Unavailable'
local commValue = 100

RegisterNetEvent('ethical-taxi:signonmenu')
AddEventHandler('ethical-taxi:signonmenu', function()
  TriggerEvent('ethical-context:sendMenu', {
    {
        id = 1,
        header = "Sign In / Off ",
        txt = ""
    },
    {
        id = 2,
        header = "Sign On Duty",
        txt = "Use this to sign in",
        params = {
          event = "ethical-taxi:dutycl"
        }
    },
    {
        id = 3,
        header = "Sign Off Duty",
        txt = "Use this to sign off",
        params = {
            event = "ethical-taxi:offdutycl"
        }
    },
      {  id = 4,
        header = "Cash out Receipts",
        txt = "Get paid for all those customers you billed",
        params = {
            event = "taxi:cash:in"
        }
      }
  })
end)

RegisterNetEvent('ethical-taxi:dutycl')
AddEventHandler('ethical-taxi:dutycl', function()
    TriggerEvent("toggletaxi",true)
    TriggerServerEvent("ethical-taxi:duty")
    TriggerEvent("DoLongHudText","You are now on Duty, please use the official vehicle available from the Car Dealer",1)

end)

RegisterNetEvent('ethical-taxi:offdutycl')
AddEventHandler('ethical-taxi:offdutycl', function()
    TriggerEvent("toggletaxi",false)
    TriggerServerEvent("ethical-taxi:offduty")
    TriggerEvent("DoLongHudText","You are no longer on duty, enjoy the break.",1)

end)

RegisterNetEvent('ethical-taxi:updatesendernumber')
AddEventHandler('ethical-taxi:updatesendernumber', function(input)
 requesterNumber = input
end)

RegisterNetEvent("ethical-taxi:notify")
AddEventHandler("ethical-taxi:notify", function()


  local pos = GetEntityCoords(PlayerPedId(),  true)
  

 

  if not taxiRequestOnCooldown then
    TriggerEvent("DoLongHudText","Your taxi has been requested, please wait here.",1)
    taxiRequestOnCooldown = true
    local cid = exports["isPed"]:isPed("cid")
    TriggerServerEvent('ethical-taxi:getsendernumber',cid)
    
    while requesterNumber == 'Number Unavailable' do
      Citizen.Wait(0)
    end
    local stringMessage = "Pickup Request\n Call: " .. requesterNumber

    TriggerServerEvent("dispatch:svNotify", {
      dispatchCode = "Taxi Service",
      firstStreet = GetStreetAndZone(),
      isImportant = false,
      priority = 0,
      dispatchMessage = stringMessage,
      recipientList = {
        taxi = "taxi"
      },
      origin = {
        x = pos.x,
        y = pos.y,
        z = pos.z
      }
    })

    TriggerEvent('ethical-alerts:taxicaller')
    Citizen.Wait(45000)
    taxiRequestOnCooldown = false
  else
    TriggerEvent("DoLongHudText", "You cannot request another taxi so soon after requesting one, please wait.", 2)
  end
end)

RegisterNetEvent('ethical-taxi:meter')
AddEventHandler('ethical-taxi:meter', function()
  TriggerEvent('nsrp_meter:triggermeter')
end)

function GetStreetAndZone()
    local plyPos = GetEntityCoords(PlayerPedId(), true)
    local s1, s2 = Citizen.InvokeNative( 0x2EB41072B4C1E4C0, plyPos.x, plyPos.y, plyPos.z, Citizen.PointerValueInt(), Citizen.PointerValueInt() )
    local street1 = GetStreetNameFromHashKey(s1)
    local street2 = GetStreetNameFromHashKey(s2)
    local zone = GetLabelText(GetNameOfZone(plyPos.x, plyPos.y, plyPos.z))
    local street = street1 .. ", " .. zone
    return street
  end


  Citizen.CreateThread(function()
    CreateTaxiPed()
  end)
  
  
  function CreateTaxiPed()
  
    modelHash = GetHashKey("a_m_y_business_03")
    RequestModel(modelHash)
    while not HasModelLoaded(modelHash) do
       Wait(1)
    end
    created_ped5 = CreatePed(0, modelHash , 894.96, -179.28, 74.70,true)
    FreezeEntityPosition(created_ped5, true)
    SetEntityHeading(created_ped5,  238.85)
    SetEntityInvincible(created_ped5, true)
    SetBlockingOfNonTemporaryEvents(created_ped5, true)
  
  end


  
RegisterNetEvent("ethical-taxi:bill")
AddEventHandler("ethical-taxi:bill", function()
    local bill = exports["ethical-applications"]:KeyboardInput({
        header = "Bill Customer",
        rows = {
            {
                id = 0,
                txt = "U ID"
            },
            {
                id = 1,
                txt = "Amount"
            }
        }
    })
    if bill then
        if tonumber(bill[2].input) < 201  and tonumber(bill[2].input) > 0 then
            TriggerServerEvent("taxi:bill:player", bill[1].input, bill[2].input)
        else
            TriggerEvent('DoLongHudText', 'You can\'t overcharge people like that. Attempt reported.', 2)
        end
    end
end)


RegisterNetEvent('taxi:createcustomerreceipt')
AddEventHandler('taxi:createcustomerreceipt', function(value,receiver)
local jobName = exports["isPed"]:isPed("myjob")
local amount = value
local type = "CUSTOMER"
local sender = exports["isPed"]:isPed("cid")
local receiver = receiver

TriggerEvent('nsrp_receipts:createreceipt',amount,jobName,sender,receiver,type)

end)

RegisterNetEvent("taxi:cash:in")
AddEventHandler("taxi:cash:in", function()
    local job = exports["isPed"]:isPed("myjob")
    local cid = exports["isPed"]:isPed("cid")
	if job == 'taxi' then
        local qty = exports["ethical-inventory"]:getQuantity("receipt")
        if qty >= 1 then
            TriggerServerEvent('taxi:payout:employee', qty)
        end
        TriggerEvent("nsrp_receipts:payrecieptcl",job,cid,commValue)
	end
end) 