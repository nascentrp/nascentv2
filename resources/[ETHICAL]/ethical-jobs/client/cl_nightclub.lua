local commValue = 60

RegisterNetEvent('ethical-nightclub:signonmenu')
AddEventHandler('ethical-nightclub:signonmenu', function()
  TriggerEvent('ethical-context:sendMenu', {
    {
        id = 1,
        header = "Sign In / Off ",
        txt = ""
    },
    {
        id = 2,
        header = "Sign On Duty",
        txt = "Use this to sign in",
        params = {
          event = "ethical-nightclub:dutycl"
        }
    },
    {
        id = 3,
        header = "Sign Off Duty",
        txt = "Use this to sign off",
        params = {
            event = "ethical-nightclub:offdutycl"
        }
    }
  })
end)

RegisterNetEvent('ethical-nightclub:dutycl')
AddEventHandler('ethical-nightclub:dutycl', function()
    TriggerServerEvent("ethical-nightclub:duty")
    TriggerEvent("DoLongHudText","You are now on Duty",1)

end)

RegisterNetEvent('ethical-nightclub:offdutycl')
AddEventHandler('ethical-nightclub:offdutycl', function()
    TriggerServerEvent("ethical-nightclub:offduty")
    TriggerEvent("DoLongHudText","You are no longer on duty, enjoy the break.",1)
end)

RegisterNetEvent("ethical-nightclub:bill")
AddEventHandler("ethical-nightclub:bill", function()
    local bill = exports["ethical-applications"]:KeyboardInput({
        header = "Create Receipt",
        rows = {
            {
                id = 0,
                txt = "U ID"
            },
            {
                id = 1,
                txt = "Amount"
            }
        }
    })
    if bill then
        if tonumber(bill[2].input) < 500  and tonumber(bill[2].input) > 0 then
            TriggerServerEvent("ethical-nightclub:bill:player", bill[1].input, bill[2].input)
        else
            TriggerEvent('DoLongHudText', 'You can\'t overcharge people like that. Attempt reported.', 2)
        end
    end
end)


RegisterNetEvent('ethical-nightclub:createcustomerreceipt')
AddEventHandler('ethical-nightclub:createcustomerreceipt', function(value,receiver)
local jobName = exports["isPed"]:isPed("myjob")
local amount = value
local type = "CUSTOMER"
local sender = exports["isPed"]:isPed("cid")
local receiver = receiver

TriggerEvent('nsrp_receipts:createreceipt',amount,jobName,sender,receiver,type)

end)

RegisterNetEvent("ethical-nightclub:cash:in")
AddEventHandler("ethical-nightclub:cash:in", function()
    local job = exports["isPed"]:isPed("myjob")
    local cid = exports["isPed"]:isPed("cid")
	if job == 'night_club' then
        local qty = exports["ethical-inventory"]:getQuantity("receipt")
        if qty >= 1 then
            TriggerServerEvent('ethical-nightclub:payout:employee', qty)
        end
        TriggerEvent("nsrp_receipts:payrecieptcl",job,cid,commValue)
	end
end)

RegisterNetEvent('ethical-nightclub:pickup1')
AddEventHandler('ethical-nightclub:pickup1', function()
	TriggerEvent("server-inventory-open", "1", "nc_tray1");	
	Wait(1000)
end)

RegisterNetEvent('ethical-nightclub:pickup2')
AddEventHandler('ethical-nightclub:pickup2', function()
	TriggerEvent("server-inventory-open", "1", "nc_tray2");	
	Wait(1000)
end)

--Bar in and out teleporters
RegisterNetEvent('ethical-nightclub:gobehindbar1')
AddEventHandler("ethical-nightclub:gobehindbar1", function()
    print('attempting tp')
local destination = vector3( -1376.7279052734, -628.99884033203, 30.833566665649)
SetPedCoordsKeepVehicle(PlayerPedId(),destination)
end)

RegisterNetEvent('ethical-nightclub:gobehindbar2')
AddEventHandler("ethical-nightclub:gobehindbar2", function()
    print('attempting tp')
local destination = vector3( -1386.0496826172, -607.27935791016, 30.309268951416)


SetPedCoordsKeepVehicle(PlayerPedId(),destination)

end)

RegisterNetEvent('ethical-nightclub:exitbar1')
AddEventHandler("ethical-nightclub:exitbar1", function()
    print('attempting tp')
    local destination = vector3( -1382.2214355469, -632.85028076172, 30.819581985474)
SetPedCoordsKeepVehicle(PlayerPedId(),destination)
end)

RegisterNetEvent('ethical-nightclub:exitbar2')
AddEventHandler("ethical-nightclub:exitbar2", function()
print('attempting tp')
    local destination = vector3( -1389.5946044922, -610.86126708984, 30.319551467896)
SetPedCoordsKeepVehicle(PlayerPedId(),destination)
end)

--Open DJ menu
RegisterNetEvent('ethical-nightclub:opendjmenu')
AddEventHandler("ethical-nightclub:opendjmenu", function()
 exports["nsrp_dj"]:OpenDjMenu()
end)