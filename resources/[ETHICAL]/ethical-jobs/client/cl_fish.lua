
local fishingRod
TryToFish = function()
    if not hasPoleOut then return TriggerEvent("DoLongHudText","You need a fishing rod.",2) end
    if IsPedSwimming(PlayerPedId()) then return TriggerEvent("DoLongHudText","You can't fish while in the water.",2) end
    if IsPedInAnyVehicle(PlayerPedId()) then return TriggerEvent("DoLongHudText","Exit your vehicle first before fishing.",2) end
    local waterValidated, castLocation = IsInWater()

    if waterValidated then
        poleTimer = 5
        if baitTimer == 0 then
            CastBait(fishingRod, castLocation)
        end
    else
        TriggerEvent("DoLongHudText","You need to aim for the water.",2)
    end
end

poleTimer = 0
baitTimer = 0

function timerCount()
    if poleTimer ~= 0 then
        poleTimer = poleTimer - 1
    end
    if baitTimer ~= 0 then
        baitTimer = baitTimer - 1
    end
    SetTimeout(1000, timerCount)
end

rodHandle = ""
timerCount()

CastBait = function(rodHandle, castLocation)
    baitTimer = 5
    local startedCasting = GetGameTimer()
    while not IsControlJustPressed(0, 47) do
        Citizen.Wait(5)
        DisableControlAction(0, 311, true)
        DisableControlAction(0, 157, true)
        DisableControlAction(0, 158, true)
        DisableControlAction(0, 160, true)
        DisableControlAction(0, 164, true)
        if not hasPoleOut then
            return
        end
    end
    
    -- check that the player is in the dock polyzone
    if not OnFishingDock then
        TriggerEvent("DoLongHudText","There are no fish here!",2)
        return
    end

    PlayAnimation(PlayerPedId(), "mini@tennis", "forehand_ts_md_far", {
        ["flag"] = 48
    })
    while IsEntityPlayingAnim(PlayerPedId(), "mini@tennis", "forehand_ts_md_far", 3) do
        Citizen.Wait(0)
    end
    PlayAnimation(PlayerPedId(), "amb@world_human_stand_fishing@idle_a", "idle_c", {
        ["flag"] = 11
    })   
    local startedBaiting = GetGameTimer()
    local randomBait = math.random(10000, 30000)
    DisableControlAction(0, 311, true)
    DisableControlAction(0, 157, true)
    DisableControlAction(0, 158, true)
    DisableControlAction(0, 160, true)
    DisableControlAction(0, 164, true)
    local interupted = false
    Citizen.Wait(1000)
    while GetGameTimer() - startedBaiting < randomBait do
        Citizen.Wait(5)
        if not IsEntityPlayingAnim(PlayerPedId(), "amb@world_human_stand_fishing@idle_a", "idle_c", 3) or not hasPoleOut then
            interupted = true
            break
        end
    end
    RemoveLoadingPrompt()
    if interupted then
        ClearPedTasks(PlayerPedId())
        CastBait(rodHandle, castLocation)
        return
    end

    local caughtFish = exports["nsrp_minigames"]:MiniGameFishingSkill()
    ClearPedTasks(PlayerPedId())
    if caughtFish then
        TriggerServerEvent("nsrp_economy:payoutForFishingCatch")
        TriggerEvent("DoLongHudText","You caught a fish",1)
    else
        TriggerEvent("DoLongHudText","The fish got loose",2)
    end
end

IsInWater = function()
    local startedCheck = GetGameTimer()
    local ped = PlayerPedId()
    local pedPos = GetEntityCoords(ped)
    local forwardVector = GetEntityForwardVector(ped)
    local forwardPos = vector3(pedPos["x"] + forwardVector["x"] * 10, pedPos["y"] + forwardVector["y"] * 10, pedPos["z"])
    local fishHash = GetHashKey('a_c_fish')
    RequestModel(fishHash)
    if not HasModelLoaded(fishHash) then
        Citizen.Wait(0)
    end
    local waterHeight = GetWaterHeight(forwardPos["x"], forwardPos["y"], forwardPos["z"])
    local fishHandle = CreatePed(1, fishHash, forwardPos, 0.0, false)
    SetEntityAlpha(fishHandle, 0, true)
    TriggerEvent("DoLongHudText","Checking Fishing Location",1)
    while GetGameTimer() - startedCheck < 3000 do
        Citizen.Wait(0)
    end
    RemoveLoadingPrompt()
    local fishInWater = IsEntityInWater(fishHandle)
    DeleteEntity(fishHandle)
    SetModelAsNoLongerNeeded(fishHash)
    TriggerEvent("DoLongHudText", "[G] - To Cast")
    return fishInWater, fishInWater and vector3(forwardPos["x"], forwardPos["y"], waterHeight) or false
end

PlayAnimation = function(ped, dict, anim, settings)
	if dict then
        Citizen.CreateThread(function()
            RequestAnimDict(dict)

            while not HasAnimDictLoaded(dict) do
                Citizen.Wait(100)
            end

            if settings == nil then
                TaskPlayAnim(ped, dict, anim, 1.0, -1.0, 1.0, 0, 0, 0, 0, 0)
            else 
                local speed = 1.0
                local speedMultiplier = -1.0
                local duration = 1.0
                local flag = 0
                local playbackRate = 0
                if settings["speed"] then
                    speed = settings["speed"]
                end

                if settings["speedMultiplier"] then
                    speedMultiplier = settings["speedMultiplier"]
                end

                if settings["duration"] then
                    duration = settings["duration"]
                end
                if settings["flag"] then
                    flag = settings["flag"]
                end
                if settings["playbackRate"] then

                    playbackRate = settings["playbackRate"]

                end
                TaskPlayAnim(ped, dict, anim, speed, speedMultiplier, duration, flag, playbackRate, 0, 0, 0)
            end
            RemoveAnimDict(dict)
		end)
	else
		TaskStartScenarioInPlace(ped, anim, 0, true)
	end
end

FadeOut = function(duration)
    DoScreenFadeOut(duration)
    while not IsScreenFadedOut() do
        Citizen.Wait(0)
    end
end

FadeIn = function(duration)
    DoScreenFadeIn(500)
    while not IsScreenFadedIn() do
        Citizen.Wait(0)
    end
end

WaitForModel = function(model)
    if not IsModelValid(model) then
        return
    end
	if not HasModelLoaded(model) then
		RequestModel(model)
	end
	while not HasModelLoaded(model) do
		Citizen.Wait(0)
	end
end

DrawBusySpinner = function(text)
    SetLoadingPromptTextEntry("STRING")
    AddTextComponentSubstringPlayerName(text)
    ShowLoadingPrompt(3)
end

-- Process
function loadAnimDict( dict )
    while ( not HasAnimDictLoaded( dict ) ) do
        RequestAnimDict( dict )
        Citizen.Wait( 5 )
    end
end 

function playerAnim()
	loadAnimDict( "mp_safehouselost@" )
    TaskPlayAnim( PlayerPedId(), "mp_safehouselost@", "package_dropoff", 8.0, 1.0, -1, 16, 0, 0, 0, 0 )
end
-- Cut Fish

local DoingTask3 = false
RegisterNetEvent("sellfish")
AddEventHandler("sellfish", function()
    if not DoingTask3 then
        DoingTask3 = true
        if exports["ethical-inventory"]:hasEnoughOfItem("sushiroll",5,false) then 
            local finished = exports["ethical-taskbar"]:taskBar(4000,"Selling Fish")
            if finished == 100 then
                local sushi = math.random(30, 50)
                DoingTask2 = false
                TriggerEvent("inventory:removeItem", "sushiroll", 5)
                TriggerServerEvent('ethical-fish:sellSushi', sushi)
            else
                DoingTask2 = false
            end
        else
            TriggerEvent("DoLongHudText", "You need at least 5 Sushi", 2)
        end
    end
end)



-- Blips
local blips = {
    {title="Fishing Pier & Processing", colour=62, id=68, scale=0.7, x = -3427.2314453125, y = 967.49719238281, z = 8.3466892242432},
    {title="Fishing Sushi Roll Processing", colour=41, id=501, scale=0.7, x = -3250.2976074219, y = 992.57708740234, z = 12.489754676819},
    {title="Fishing Sell Spot", colour=9, id=356, scale=0.7, x = -1037.6729736328, y = -1397.1528320312, z = 5.5531921386719}
}

Citizen.CreateThread(function()
    for _, info in pairs(blips) do
      info.blip = AddBlipForCoord(info.x, info.y, info.z)
      SetBlipSprite(info.blip, info.id)
      SetBlipDisplay(info.blip, 4)
      SetBlipScale(info.blip, info.scale)
      SetBlipColour(info.blip, info.colour)
      SetBlipAsShortRange(info.blip, true)
	  BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(info.title)
      EndTextCommandSetBlipName(info.blip)
    end
end)

hasPoleOut = false
goAgain = true
RegisterNetEvent("ethical-fish:toggleRod")
AddEventHandler("ethical-fish:toggleRod", function()
    hasPoleOut = not hasPoleOut
    if hasPoleOut == true then
        local fishingRodHash = GetHashKey('prop_fishing_rod_01')
        RequestModel(fishingRodHash)
        if not HasModelLoaded(fishingRodHash) then
            Citizen.Wait(0)
        end
        local pedPos = GetEntityCoords(PlayerPedId())
        rodHandle = CreateObject(fishingRodHash, pedPos, true)
        AttachEntityToEntity(rodHandle, PlayerPedId(), GetPedBoneIndex(PlayerPedId(), 18905), 0.1, 0.05, 0, 80.0, 120.0, 160.0, true, true, false, true, 1, true)
        SetModelAsNoLongerNeeded(fishingRodHash)
        while hasPoleOut do
            CastBait()
            Wait(100)
        end
    else
        if DoesEntityExist(rodHandle) then
            DeleteEntity(rodHandle)
        end
    end
end)

CanFish = true
RegisterNetEvent("ethical-fish:tryToFish")
AddEventHandler("ethical-fish:tryToFish", function()
    if CanFish then
        if poleTimer == 0 then 
            TryToFish()
        end
    else
        TriggerEvent("DoLongHudText", "You need a fishing license, contact the police", 2)
    end
end)

RegisterNetEvent("ethical-fish:allowed")
AddEventHandler("ethical-fish:allowed", function(allowed)
    CanFish = allowed
end)

-- Notification
ShowHelpNotification = function(msg)
    BeginTextCommandDisplayHelp('STRING')
    AddTextComponentSubstringPlayerName(msg)
    EndTextCommandDisplayHelp(0, false, true, -1)
end