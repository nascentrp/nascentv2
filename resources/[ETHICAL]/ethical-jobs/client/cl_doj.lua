local dojjobRequestOnCooldown = false
local requesterNumber = 'Number Unavailable'
local commValue = 100

RegisterNetEvent('ethical-dojjob:signonmenu')
AddEventHandler('ethical-dojjob:signonmenu', function()
  TriggerEvent('ethical-context:sendMenu', {
    {
        id = 1,
        header = "Sign In / Off ",
        txt = ""
    },
    {
        id = 2,
        header = "Sign On Duty",
        txt = "Use this to sign in",
        params = {
          event = "ethical-dojjob:dutycl"
        }
    },
    {
        id = 3,
        header = "Sign Off Duty",
        txt = "Use this to sign off",
        params = {
            event = "ethical-dojjob:offdutycl"
        }
    }
  })
end)

RegisterNetEvent('ethical-dojjob:dutycl')
AddEventHandler('ethical-dojjob:dutycl', function()
    TriggerEvent("toggledojjob",true)
    TriggerServerEvent("ethical-dojjob:duty")
    TriggerEvent("DoLongHudText","You are now on Duty",1)

end)

RegisterNetEvent('ethical-dojjob:offdutycl')
AddEventHandler('ethical-dojjob:offdutycl', function()
    TriggerEvent("toggledojjob",false)
    TriggerServerEvent("ethical-dojjob:offduty")
    TriggerEvent("DoLongHudText","You are no longer on duty, enjoy the break.",1)

end)

RegisterNetEvent("ethical-dojjob:notify")
AddEventHandler("ethical-dojjob:notify", function()


  local pos = GetEntityCoords(PlayerPedId(),  true)
  

 

  if not dojjobRequestOnCooldown then
    TriggerEvent("DoLongHudText","Your Lawyer has been requested, please expect a call.",1)
    dojjobRequestOnCooldown = true
    local cid = exports["isPed"]:isPed("cid")
    TriggerServerEvent('ethical-dojjob:getsendernumber',cid)
    
    while requesterNumber == 'Number Unavailable' do
      Citizen.Wait(0)
    end
    local stringMessage = "Legal Assistance Request\n Call: " .. requesterNumber

    TriggerServerEvent("dispatch:svNotify", {
      dispatchCode = "Law on Call",
      firstStreet = GetStreetAndZone(),
      isImportant = false,
      priority = 0,
      dispatchMessage = stringMessage,
      recipientList = {
        DOJ = "DOJ"
      },
      origin = {
        x = pos.x,
        y = pos.y,
        z = pos.z
      }
    })

    TriggerEvent('ethical-alerts:dojjobcaller')
    Citizen.Wait(45000)
    dojjobRequestOnCooldown = false
  else
    TriggerEvent("DoLongHudText", "You cannot request another Lawyer so soon after requesting one, please wait.", 2)
  end
end)

RegisterNetEvent('ethical-dojjob:updatesendernumber')
AddEventHandler('ethical-dojjob:updatesendernumber', function(input)
 requesterNumber = input
end)


--Add billing here



RegisterNetEvent("ethical-dojjob:bill")
AddEventHandler("ethical-dojjob:bill", function()
    local bill = exports["ethical-applications"]:KeyboardInput({
        header = "Bill Client",
        rows = {
            {
                id = 0,
                txt = "U ID"
            },
            {
                id = 1,
                txt = "Amount"
            }
        }
    })
    if bill then
        if (tonumber(bill[2].input) < 50000 and tonumber(bill[2].input) > 0) then
            TriggerServerEvent("ethical-dojjob:bill:player", bill[1].input, bill[2].input)
        else
            TriggerEvent('DoLongHudText', 'You can\'t overcharge people like that. Attempt reported.', 2)
        end
    end
end)


RegisterNetEvent('ethical-dojjob:createcustomerreceipt')
AddEventHandler('ethical-dojjob:createcustomerreceipt', function(value,receiver)
local jobName = exports["isPed"]:isPed("myjob")
local amount = value
local type = "CUSTOMER"
local sender = exports["isPed"]:isPed("cid")
local receiver = receiver

TriggerEvent('nsrp_receipts:createreceipt',amount,jobName,sender,receiver,type)

end)

RegisterNetEvent("ethical-dojjob:cash:in")
AddEventHandler("ethical-dojjob:cash:in", function()
    local job = exports["isPed"]:isPed("myjob")
    local cid = exports["isPed"]:isPed("cid")
	if job == 'DOJ' or job == 'judge' then
        local qty = exports["ethical-inventory"]:getQuantity("receipt")
        if qty >= 1 then
            TriggerServerEvent('ethical-dojjob:payout:employee', qty)
        end
        TriggerEvent("nsrp_receipts:payrecieptcl",job,cid,commValue)
	end
end)
