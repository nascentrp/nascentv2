RegisterNetEvent('ethical-beanmachine:duty')
AddEventHandler('ethical-beanmachine:duty', function()
    local player = source
    local target = exports["ethical-base"]:getModule("Player"):GetUser(player)
    exports["ethical-base"]:getModule("JobManager"):SetJob(target, "bean_machine", false)
end)

RegisterNetEvent('ethical-beanmachine:offduty')
AddEventHandler('ethical-beanmachine:offduty', function()
    local player = source
    local target = exports["ethical-base"]:getModule("Player"):GetUser(player)
    exports["ethical-base"]:getModule("JobManager"):SetJob(target, "unemployed", false)
end)


RegisterServerEvent('ethical-beanmachine:bill:player')
AddEventHandler("ethical-beanmachine:bill:player", function(TargetID, amount)
	local src = source
	local sender = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local senderCharacterId = sender:getCurrentCharacter().id
	local target = tonumber(TargetID)
	local fine = tonumber(amount)
	local user = exports["ethical-base"]:getModule("Player"):GetUser(target)
	local characterId = user:getCurrentCharacter().id
	if user ~= false then
		if user:getBalance() >= fine then
			user:removeBank(fine)
			
			TriggerClientEvent("player:receiveItem", src, "receipt", 1, {
				Price = fine,
				Creator = senderCharacterId,
				Billed = characterId,
				Comment = "BeanMachine"
			})
            
			TriggerClientEvent("ethical-beanmachine:createcustomerreceipt",src,fine,characterId)
			TriggerClientEvent("DoLongHudText", target, "You have been billed for R"..fine, 1)
			TriggerClientEvent("DoLongHudText", src, "You have charged customer R"..fine, 1)
			TriggerEvent("bank:addlog", characterId, fine, "BeanMachine", false)
		else
			TriggerClientEvent("DoLongHudText", target, "Card Declined - Insufficient funds", 2)
			TriggerClientEvent("DoLongHudText", src, "Card Declined", 2)
		end
	else
		TriggerClientEvent("DoLongHudText", src, "Card Invalid", 2)
	end
end)


RegisterServerEvent('ethical-beanmachine:payout:employee')
AddEventHandler("ethical-beanmachine:payout:employee", function(qty)
	local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local job = user:getVar("Job")
	TriggerClientEvent("inventory:removeItem", src, "receipt", qty)	
end)