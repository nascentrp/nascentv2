RegisterNetEvent('ethical-taxi:duty')
AddEventHandler('ethical-taxi:duty', function()
    local player = source
    local target = exports["ethical-base"]:getModule("Player"):GetUser(player)
    exports["ethical-base"]:getModule("JobManager"):SetJob(target, "taxi", false)
end)

RegisterNetEvent('ethical-taxi:offduty')
AddEventHandler('ethical-taxi:offduty', function()
    local player = source
    local target = exports["ethical-base"]:getModule("Player"):GetUser(player)
    exports["ethical-base"]:getModule("JobManager"):SetJob(target, "unemployed", false)
end)

RegisterNetEvent('ethical-taxi:getsendernumber')
AddEventHandler('ethical-taxi:getsendernumber',function(cid) 
    local src = source
    local phoneNumber = getNumberPhone(cid)
    TriggerClientEvent('ethical-taxi:updatesendernumber',src, phoneNumber)
end)

function getNumberPhone(cid)
    local thenumber
    exports.ghmattimysql:execute("SELECT phone_number FROM characters WHERE id = @id", {
        ['id'] = cid
    }, function(result)
        if result[1] ~= nil then
            thenumber = result[1].phone_number
        else
            thenumber = nil
        end
    end)
    Wait(200)
    if thenumber ~= nil then
        return thenumber
    else
        return nil
    end
end


RegisterServerEvent('taxi:bill:player')
AddEventHandler("taxi:bill:player", function(TargetID, amount)
	local src = source
	local sender = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local senderCharacterId = sender:getCurrentCharacter().id
	local target = tonumber(TargetID)
	local fine = tonumber(amount)
	local user = exports["ethical-base"]:getModule("Player"):GetUser(target)
	local characterId = user:getCurrentCharacter().id
	if user ~= false then
        if user:getBalance() >= fine then
			user:removeBank(fine)
			
			TriggerClientEvent("player:receiveItem", src, "receipt", 1, {
				Price = fine,
				Creator = senderCharacterId,
				Billed = characterId,
				Comment = "Taxi"
			})
            
			TriggerClientEvent("taxi:createcustomerreceipt",src,fine,characterId)
			TriggerClientEvent("DoLongHudText", target, "You have been billed for R"..fine, 1)
			TriggerClientEvent("DoLongHudText", src, "You have charged the patient R"..fine, 1)
			TriggerEvent("bank:addlog", characterId, fine, "Taxi", false)
        else
			TriggerClientEvent("DoLongHudText", target, "Card Declined - Insufficient funds", 2)
			TriggerClientEvent("DoLongHudText", src, "Card Declined", 2)
		end
	else
		TriggerClientEvent("DoLongHudText", src, "Card Invalid", 2)
	end
end)


RegisterServerEvent('taxi:payout:employee')
AddEventHandler("taxi:payout:employee", function(qty)
	local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local job = user:getVar("Job")
	TriggerClientEvent("inventory:removeItem", src, "receipt", qty)	
end)