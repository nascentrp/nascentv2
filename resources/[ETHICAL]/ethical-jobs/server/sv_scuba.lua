RegisterServerEvent('ethical-scuba:checkAndTakeDepo')
AddEventHandler('ethical-scuba:checkAndTakeDepo', function()
local src = source
local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    user:removeMoney(400)
end)

RegisterServerEvent('ethical-scuba:returnDepo')
AddEventHandler('ethical-scuba:returnDepo', function()
local src = source
local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    user:addMoney(80)
end)

RegisterServerEvent('ethical-scuba:findTreasure')
AddEventHandler('ethical-scuba:findTreasure', function()
    --print("received treasure")
    local src = source
    local roll = math.random(1,3)
    --print(roll)
    if roll == 1 then
        exports["nsrp_economy"]:payoutForWorkBasic(src)
    elseif roll == 2 then
        exports["nsrp_economy"]:payoutForWorkBasic(src, false)
        TriggerClientEvent("mkbuss:open5mscriptscom",src, "scubaloot")
        -- TriggerClientEvent('player:receiveItem', source, "ironbar", math.random(4,8))
    elseif roll == 3 then
        TriggerClientEvent("mkbuss:open5mscriptscom",src, "scubaloot")
        -- TriggerClientEvent('player:receiveItem', source, "ironbar", math.random(7,12))
    end


end)

RegisterServerEvent('ethical-scuba:paySalvage')
AddEventHandler('ethical-scuba:paySalvage', function(money)
    local src = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    if money ~= nil then
        user:addMoney(tonumber(money))
    end
end)

RegisterServerEvent('ethical-scuba:makeGold')
AddEventHandler('ethical-scuba:makeGold', function()
 local source = source
 TriggerClientEvent('inventory:removeItem', source, 'umetal', 10)
 TriggerClientEvent("player:receiveItem", source, "goldbar", 1)
end)