---570.12615966797, -198.43711853027, 38.259475708008 Heading: 198.71934509277

RegisterNetEvent('ethical-dojjob:duty')
AddEventHandler('ethical-dojjob:duty', function()
    local player = source
    local target = exports["ethical-base"]:getModule("Player"):GetUser(player)
    exports["ethical-base"]:getModule("JobManager"):SetJob(target, "DOJ", false)
end)

RegisterNetEvent('ethical-dojjob:offduty')
AddEventHandler('ethical-dojjob:offduty', function()
    local player = source
    local target = exports["ethical-base"]:getModule("Player"):GetUser(player)
    exports["ethical-base"]:getModule("JobManager"):SetJob(target, "unemployed", false)
end)

RegisterNetEvent('ethical-dojjob:getsendernumber')
AddEventHandler('ethical-dojjob:getsendernumber',function(cid) 
    local src = source
    local phoneNumber = getNumberPhone(cid)
    TriggerClientEvent('ethical-dojjob:updatesendernumber',src, phoneNumber)
end)

function getNumberPhone(cid)
    local thenumber
    exports.ghmattimysql:execute("SELECT phone_number FROM characters WHERE id = @id", {
        ['id'] = cid
    }, function(result)
        if result[1] ~= nil then
            thenumber = result[1].phone_number
        else
            thenumber = nil
        end
    end)
    Wait(200)
    if thenumber ~= nil then
        return thenumber
    else
        return nil
    end
end



RegisterServerEvent('ethical-dojjob:bill:player')
AddEventHandler("ethical-dojjob:bill:player", function(TargetID, amount)
	local src = source
	local sender = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local senderCharacterId = sender:getCurrentCharacter().id
	local target = tonumber(TargetID)
	local fine = tonumber(amount)
	local user = exports["ethical-base"]:getModule("Player"):GetUser(target)
	local characterId = user:getCurrentCharacter().id
	if user ~= false then
        if user:getBalance() >= fine then
			user:removeBank(fine)
			
			TriggerClientEvent("player:receiveItem", src, "receipt", 1, {
				Price = fine,
				Creator = senderCharacterId,
				Billed = characterId,
				Comment = "DOJ"
			})
            
			TriggerClientEvent("ethical-dojjob:createcustomerreceipt",src,fine,characterId)
			TriggerClientEvent("DoLongHudText", target, "You have been billed for R"..fine, 1)
			TriggerClientEvent("DoLongHudText", src, "You have charged customer R"..fine, 1)
			TriggerEvent("bank:addlog", characterId, fine, "DOJ", false)
        else
			TriggerClientEvent("DoLongHudText", target, "Card Declined - Insufficient funds", 2)
			TriggerClientEvent("DoLongHudText", src, "Card Declined", 2)
		end
	else
		TriggerClientEvent("DoLongHudText", src, "Card Invalid", 2)
	end
end)


RegisterServerEvent('ethical-dojjob:payout:employee')
AddEventHandler("ethical-dojjob:payout:employee", function(qty)
	local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local job = user:getVar("Job")
	TriggerClientEvent("inventory:removeItem", src, "receipt", qty)	
end)