RegisterServerEvent('ethical:huntingreturnree')
AddEventHandler('ethical:huntingreturnree', function()
local src = source
local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    user:addMoney(400)
end)

-- RegisterServerEvent('crowded')
-- AddEventHandler('crowded', function()
--     local src = source
--     local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
--     user:addMoney(1000)
-- end)

RegisterServerEvent('ethical:huntingdeposit')
AddEventHandler('ethical:huntingdeposit', function()
    local src = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    if (tonumber(user:getCash()) >= 500) then
        user:removeMoney(500)
        TriggerClientEvent('player:receiveItem', src, '100416529', 1)
        TriggerClientEvent('hunting:start', src)
    else
        OnGoingHuntSession = false
        TriggerClientEvent("DoLongHudText", src, "You need R500", 2)
    end
end)

RegisterServerEvent('ethical-hunting:sell')
AddEventHandler('ethical-hunting:sell', function()
local src = source
local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
local randompayout = math.random(30, 40)
    user:addMoney(randompayout)
end)

RegisterServerEvent('ethical-hunting:giveloadout')
AddEventHandler('ethical-hunting:giveloadout', function()
    --TriggerClientEvent('player:receiveItem', source, '100416529', 1)
end)

RegisterServerEvent("ethical-hunting:removeSniper", function()
    local src = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local randompayout = math.random(300, 400)
    user:addMoney(randompayout)
    TriggerClientEvent('inventory:removeItem', source, '100416529', 1)
end)

RegisterServerEvent('ethical-hunting:removeloadout')
AddEventHandler('ethical-hunting:removeloadout', function()
    TriggerClientEvent('inventory:removeItem', source, '100416529', 1)
end)


RegisterServerEvent("ethical-hunting:retreive:license")
AddEventHandler("ethical-hunting:retreive:license", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT * FROM user_licenses WHERE `owner`= ? AND `type` = ? AND `status` = ?', {character.id, "Hunting", "1"}, function(data)
		if data[1] then
            TriggerClientEvent("ethical-hunting:allowed", src, true)
        end
    end)
end)