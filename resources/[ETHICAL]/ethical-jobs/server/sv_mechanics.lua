
RegisterServerEvent('ethical-mechanic:bill:player')
AddEventHandler("ethical-mechanic:bill:player", function(TargetID, amount)
	local src = source
	local sender = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local senderCharacterId = sender:getCurrentCharacter().id
	local target = tonumber(TargetID)
	local fine = tonumber(amount)
	local user = exports["ethical-base"]:getModule("Player"):GetUser(target)
	local characterId = user:getCurrentCharacter().id
	if user ~= false then
		if user:getBalance() >= fine then
			user:removeBank(fine)
			
			TriggerClientEvent("player:receiveItem", src, "receipt", 1, {
				Price = fine,
				Creator = senderCharacterId,
				Billed = characterId,
				Comment = "Mechanics"
			})
            
			TriggerClientEvent("ethical-mechanic:createcustomerreceipt",src,fine,characterId)
			TriggerClientEvent("DoLongHudText", target, "You have been billed for R"..fine, 1)
			TriggerClientEvent("DoLongHudText", src, "You have charged customer R"..fine, 1)
			TriggerEvent("bank:addlog", characterId, fine, "Mechanics", false)
		else
			TriggerClientEvent("DoLongHudText", target, "Card Declined - Insufficient funds", 2)
			TriggerClientEvent("DoLongHudText", src, "Card Declined", 2)
		end
	else
		TriggerClientEvent("DoLongHudText", src, "Card Invalid", 2)
	end
end)


RegisterServerEvent('ethical-mechanic:payout:employee')
AddEventHandler("ethical-mechanic:payout:employee", function(qty)
	local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local job = user:getVar("Job")
	TriggerClientEvent("inventory:removeItem", src, "receipt", qty)	
end)