-- Stress
local stresslevel = 0
local isBlocked = false

RegisterNetEvent("client:updateStress")
AddEventHandler("client:updateStress",function(newStress)
    print("stress updated")
    stresslevel = (newStress/1000)*100
end)

RegisterNetEvent("client:blockShake")
AddEventHandler("client:blockShake",function(isBlockedInfo)
    isBlocked = isBlockedInfo
end)


RegisterNetEvent("ethical-admin:currentDevmode")
AddEventHandler("ethical-admin:currentDevmode", function(devmode)
    isBlocked = devmode
end)

Citizen.CreateThread(function()
    while true do
        if not isBlocked then
            if stresslevel >= 250 and stressLevel < 500 then
                print('stress found 250')
                ShakeGameplayCam('SMALL_EXPLOSION_SHAKE', 0.1)
            elseif stresslevel >= 500 and stressLevel < 900 then
                print('stress found 500')
                ShakeGameplayCam('SMALL_EXPLOSION_SHAKE', 0.07)
            elseif stresslevel >= 900 then
                print('stress found 900')
                ShakeGameplayCam('SMALL_EXPLOSION_SHAKE', 0.02)
            end
        end 
        Citizen.Wait(2000)
    end
end)