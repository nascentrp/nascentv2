--------------------------------------------------------------------------------------------------
--																				          	    --
--       ______ _______ _    _ _____ _____          _        _____  ________       _______      --
--      |  ____|__   __| |  | |_   _/ ____|   /\   | |      |  __ \|  ____\ \    / / ____|      --
--      | |__     | |  | |__| | | || |       /  \  | |      | |  | | |__   \ \  / / (___        --
--      |  __|    | |  |  __  | | || |      / /\ \ | |      | |  | |  __|   \ \/ / \___ \       --
--      | |____   | |  | |  | |_| || |____ / ____ \| |____  | |__| | |____   \  /  ____) |      --
--      |______|  |_|  |_|  |_|_____\_____/_/    \_\______| |_____/|______|   \/  |_____/       --
-- 																					            --
--                           joshua#5319 glacielgaming#6969 Woodz#1668                          --
--------------------------------------------------------------------------------------------------


Citizen.CreateThread(
    function()

        -------- Hotel --------
        AddBoxZone("hotel_entermenu", vector3(-268.83721923828, -962.29449462891, 31.227432250977), 2.4, 3, {
            name="hotel_entermenu",
            -- debugPoly=true,
        }, {
            options = {
                {
                    event = "hotel:entermenu",
                    icon = "fas fa-building",
                    label = "Enter Elevator",

                },
             },
             job = {"all"},
            distance = 2.5
        })
        
        AddBoxZone("hotel_entermenu1", vector3(-270.9504, -957.824, 31.2274), 2.4, 3, {
            name="hotel_entermenu1",
            -- debugPoly=true,
        }, {
            options = {
                {
                    event = "hotel:entermenu",
                    icon = "fas fa-building",
                    label = "Enter Elevator",

                },
             },
             job = {"all"},
            distance = 2.5
        })

        AddBoxZone("hotel_entermenu2", vector3(-265.887, -955.5065, 31.2274), 2.4, 3, {
            name="hotel_entermenu2",
            -- debugPoly=true,
        }, {
            options = {
                {
                    event = "hotel:entermenu",
                    icon = "fas fa-building",
                    label = "Enter Elevator",

                },
             },
             job = {"all"},
            distance = 2.5
        })

        AddBoxZone("hotel_entermenu3", vector3(-263.6370, -959.6098, 31.2274), 2.4, 3, {
            name="hotel_entermenu3",
            -- debugPoly=true,
        }, {
            options = {
                {
                    event = "hotel:entermenu",
                    icon = "fas fa-building",
                    label = "Enter Elevator",

                },
             },
             job = {"all"},
            distance = 2.5
        })
        
        local leave = {
            `prop_dart_bd_01`

        }

        AddTargetModel(leave,
        {
            options = {
                {
                    event = "hotel:leavemenu",
                    icon = "fas fa-building",
                    label = "Leave Apartment"
                }
            },
            job = {"all"},
            distance = 2.5
        }
    )

        local storage = {
            -1705306415
            
        }

      
        
        AddTargetModel(storage,
        {
            options = {
                {
                    event = "hotel:stash",
                    icon = "fas fa-archive",
                    label = "Open Stash"
                },
                {
                    event = "raid_clothes:outfits",
                    icon = "fas fa-tshirt",
                    label = "Change Clothes"
                },
                {
                    event = "spawn:logout",
                    icon = "fas fa-bed",
                    label = "Get Some Rest"
                }
            },
            job = {"all"},
            distance = 1.5
        }
        
    )

    -- local beds = {
    --     -1992223063
    -- }


        -- local tvremote = {
        --     `prop_cs_remote_01`
            
        -- }

        -- local change = {
        --     64076466
            
        -- }

    local pc = {
        `prop_monitor_li`

    }

    AddTargetModel(pc,
    {
        options = {
            {
                event = "tab:pc",
                icon = "fas fa-desktop",
                label = "Use PC"
            }
        },
        job = {"all"},
        distance = 2.5
    }
)

      -------- Burger Shot --------

    AddBoxZone("rentBoat", vector3(-1612.522, 5261.800, 3.97), 1, 1.2, {
        name="rentBoat",
        heading=32,
        debugPoly=false,
        minZ=1.0,
        maxZ=5.0
    }, {
        options = {
            {
                event = "boats:hireboatMenu",
                icon = "fas fa-anchor",
                label = "Boat Shop"
            },
            {
                event = "scuba:getScubaMission",
                icon = "fas fa-clipboard",
                label = "Get Mission (R400)"
            }
        },
        job = {"all"},
        distance = 1.5
    })

    AddBoxZone("ethicalburgershotduty", vector3(-1196.95, -902.69, 14.0), 1, 1.2, {
      name="ethicalburgershotduty",
      heading=32,
      --debugPoly=true,
      minZ=14.0,
      maxZ=18.0
  }, {
      options = {
          {
              event = "ethicalburgershot:dutymenu",
              icon = "far fa-clipboard",
              label = "Clock On"
          },
      },
      job = {"all"},
      distance = 1.5
  })


    AddBoxZone("burger_tray_1", vector3(-1195.29, -892.31, 14.0), 1.05, 1, {
        name="burger_tray_1",
        heading=35,
        --debugPoly=true,
        minZ=13.8,
        maxZ=14.3,
    }, {
        options = {
            {
                event = "ethicalburgershot:pickup",
                parms = "ethicalburgershot_pickup",
                icon = "fas fa-hand-holding",
                label = "Take the Items",
            },
        },
        job = {"all"},
        distance = 1.5
    })
    
    AddBoxZone("burger_tray_2", vector3(-1193.87, -894.38, 14.0), 0.5, 0.7, {
        name="burger_tray_2",
        heading=318,
        --debugPoly=true,
        minZ=14.0,
        maxZ=14.4,
    }, {
        options = {
            {
                event = "ethicalburgershot:pickup2",
                parms = "ethicalburgershot_pickup2",
                icon = "fas fa-hand-holding",
                label = "Take the Items",
            },
        },
        job = {"all"},
        distance = 1.5
    })
        

    AddBoxZone("burger_tray_3", vector3(-1193.88, -906.98, 14.0), 1, 1, {
        name="burger_tray_3",
        heading=350,
        --debugPoly=true,
        minZ=13.2,
        maxZ=14.2,
    }, {
        options = {
            {
                event = "ethicalburgershot:pickup3",
                parms = "ethicalburgershot_pickup3",
                icon = "fas fa-hand-holding",
                label = "Take the Items",
            },
        },
        job = {"all"},
        distance = 3.5
    })

    AddBoxZone("ethicalburgershotcooker2 ", vector3(-1202.94, -897.38, 14.0), 1.7, 1, {
        name="ethicalburgershotcooker2 ",
        heading=34,
        --debugPoly=true,
        minZ=13.0,
        maxZ=14.3,
    }, {
        options = {
            {
                event = "ethical-ethicalburgershot:startprocess3",
                icon = "fas fa-hamburger",
                label = "Fry Patty",
            },
         },
         job = {"BurgerShot"},
        distance = 2.5
    })

    -- AddBoxZone("ethicalburgershotcooker", vector3(-1200.54, -900.92, 14.0), 1.8, 1.0, {
    --     name="ethicalburgershotcooker",
    --     heading=34,
    --     --debugPoly=true,
    --     minZ=13.0,
    --     maxZ=14.4,
    -- }, {
    --     options = {
    --         {
    --             event = "ethicalburgershot:cookmenu",
    --             icon = "fas fa-cheeseburger",
    --             label = "Cook Burger",
    --         },
    --      },
    --      job = {"BurgerShot"},
    --     distance = 2.5
    -- })

    AddBoxZone("ethicalburgershotfryer", vector3(-1202.01, -899.27, 14.0), 2.5, 1.5, {
        name="ethicalburgershotfryer",
        heading=35,
        --debugPoly=true,
        minZ=13.0,
        maxZ=14.4,
    }, {
        options = {
            {
                event = "ethical-ethicalburgershot:startfryer",
                icon = "fas fa-french-fries",
                label = "Make Fries",
            },
         },
         job = {"BurgerShot"},
        distance = 2.5
    })
        
    AddBoxZone("ethicalburgershotself", vector3(-1199.54, -895.52, 14.0), 2.2, 0.6, {
        name="ethicalburgershotself",
        heading=34,
        --debugPoly=true,
        minZ=13.8,
        maxZ=14.8,
    }, {
        options = {
            {
                event = "ethicalburgershot:updatecraftdrink",
                icon = "fas fa-glass",
                label = "Make Some Drinks",

            },

         },
         job = {"BurgerShot"},
        distance = 2.5
    })

    AddBoxZone("ethicalburgershotself2", vector3(-1189.08, -905.28, 14.0), 1.15, 0.7, {
        name="ethicalburgershotself2",
        heading=33,
        --debugPoly=true,
        minZ=13.8,
        maxZ=14.8,
    }, {
        options = {
            {
                event = "ethicalburgershot:updatecraftdrink",
                icon = "fas fa-glass",
                label = "Make Some Drinks",

            },
         },
         job = {"BurgerShot"},
        distance = 2.5
    })

    AddBoxZone("burgerfridge", vector3(-1203.71, -895.86, 14.0), 1.6, 1, {
        name="burgerfridge",
        heading=35,
        --debugPoly=true,
        minZ=13.0,
        maxZ=15.6,
    }, {
        options = {
            {
                event = "ethicalburgershot:ordermenu",
                icon = "fas fa-laptop",
                label = "Order Ingredients!",
            },
         },
         job = {"BurgerShot"},
        distance = 2.5
    })
    AddBoxZone("ethicalburgershotdisplay", vector3(-1197.78, -894.45, 14.0), 4.6, 1.2, {
        name="ethicalburgershotdisplay",
        heading=34,
        --debugPoly=true,
        minZ=13.0,
        maxZ=14.8,
    }, {
        options = {
            {
                event = "open:burgerstorage",
                parms = "BurgerShot_front",
                icon = "fas fa-hand-holding",
                label = "Put the Food on the Rack!",

            },
         },
         job = {"BurgerShot"},
        distance = 2.5
    })


    AddBoxZone("craftburger", vector3(-1197.57, -899.41, 14.0), 1.8, 0.7, {
        name="craftburger",
        heading=304,
        --debugPoly=true,
        minZ=13.0,
        maxZ=14.4,
    }, {
        options = {
            {
                event = "ethicalburgershot:updatecraft",
                icon = "fas fa-cheeseburger",
                label = "Make Burger",
            },
         },
         job = {"BurgerShot"},
        distance = 3.5
    })
    
    AddBoxZone("BurgerShot_register_1", vector3(-1196.01, -891.34, 14.0), 0.5, 0.4, {
        name="BurgerShot_register_1",
        heading=125,
        minZ=14.0,
        maxZ=14.5,
    }, {
        options = {

            {
                event = "ethicalburgershot:bill",
                parms = "1",
                icon = "fas fa-credit-card",
                label = "Charge Customer",
            },
         },
         job = {"BurgerShot"},
        distance = 1.5
    })


    AddBoxZone("BurgerShot_register_2", vector3(-1194.65, -893.3, 14.0), 0.6, 0.5, {
        name="BurgerShot_register_2",
        heading=302,
        minZ=14.1,
        maxZ=14.5,
    }, {
        options = {

            {
                event = "ethicalburgershot:bill",
                parms = "2",
                icon = "fas fa-credit-card",
                label = "Charge Customer",
            },
         },
         job = {"BurgerShot"},
        distance = 1.5
    })

    AddBoxZone("BurgerShot_register_3", vector3(-1193.39, -895.22, 14.0), 0.6, 0.4, {
        name="BurgerShot_register_3",
        heading=125,
        minZ=14.0,
        maxZ=14.4,
    }, {
        options = {

            {
                event = "ethicalburgershot:bill",
                parms = "3",
                icon = "fas fa-credit-card",
                label = "Charge Customer",
            },
         },
         job = {"BurgerShot"},
        distance = 1.5
    })
    
    AddBoxZone("BurgerShot_register_4", vector3(-1192.52, -906.65, 14.0), 0.5, 0.5, {
        name="BurgerShot_register_4",
        heading=0,
        --debugPoly=true,
        minZ=13.8,
        maxZ=14.2,
    }, {
        options = {
            {
                event = "ethicalburgershot:bill",
                parms = "4",
                icon = "fas fa-credit-card",
                label = "Charge Customer",
            },
         },
         job = {"BurgerShot"},
        distance = 1.5
    })
    
    AddBoxZone("BurgerShot_trade_receipts", vector3(-1192.01, -900.95, 14.0), 1.6, 1, {
        name="BurgerShot_trade_receipts",
        heading=35,
        --debugPoly=true,
        minZ=13.8,
        maxZ=14.6,
    }, {
        options = {
            {
                event = "ethicalburgershot:cash:in",
                icon = "fas fa-cash-register",
                label = "Cash in receipts",
            },
         },
         job = {"BurgerShot"},
        distance = 1.5
    })

    
        -------- Police --------
        AddBoxZone("buy_licenses", vector3(-550.53, -192.507, 38.219), 1.2, 2.2, {
            name="buy_licenses",
            heading=35.0,
            -- debugPoly=true,
            minZ=35.49,
            maxZ=38.9
        }, {
            options = {
                {
                    event = "nsrp_license:getAvailableLicenses",
                    icon = "far fa-clipboard",
                    label = "View Available Licenses"
                },
                {
                    event = "cid:buynew",
                    icon = "fas fa-id-card",
                    label = "Purchase New Citizen Card"
                },
                {
                    event = "ethical-jobs:getjobcardid",
                    icon = "fas fa-address-card",
                    label = "Purchase New Job Identification"
                }
            },
            job = {"all"},
            distance = 1.0
        })

        AddBoxZone("duty_police", vector3(441.87, -981.92, 30.69), 0.9, 0.5, {
            name="duty_police",
            heading=0,
            --debugPoly=true,
            minZ=30.49,
            maxZ=31.24
        }, {
            options = {
                {
                    event = "Police:duty",
                    icon = "far fa-clipboard",
                    label = "Sign On/Off Duty"
                },
            },
            job = {"all"},
            distance = 1.5
        })

        AddBoxZone("duty_police2", vector3(361.42, -1598.41, 29.29), 2.2, 1, {
            name="duty_police2",
            heading=320,
            --debugPoly=true,
            minZ=28.29,
            maxZ=29.89
        }, {
            options = {
                {
                    event = "Police:duty",
                    icon = "far fa-clipboard",
                    label = "Sign On/Off Duty"
                },
            },
            job = {"all"},
            distance = 1.5
        })

        AddBoxZone("lspd_evidence",vector3(475.78, -993.94, 26.27), 0.8, 2, {
            name="lspd_evidence",
            heading=90,
            --debugPoly=true,
            minZ=25.27,
            maxZ=27.62
        }, {
            options = {
                {
                    event = "evidence:general",
                    icon = "fas fa-trash",
                    label = "Trash Locker (Clears On Tsunami)"
                },
            },
            job = {"police"},
            distance = 1.5
        })


        AddBoxZone("lspd_armory", vector3(483.47, -994.7, 30.69), 7.2, 1, {
            name="lspd_armory",
            heading=90,
            --debugPoly=true,
            minZ=29.49,
            maxZ=32.49
        }, {
            options = {
                {
                    event = "police:general",
                    icon = "fas fa-clipboard",
                    label = "PD Armory"
                },
            },
            job = {"police"},
            distance = 2.5
        })

        AddBoxZone("lspd_armory2", vector3(369.36, -1598.7, 29.29), 2.6, 1, {
            name="lspd_armory2",
            heading=50,
            --debugPoly=true,
            minZ=29.09,
            maxZ=30.49
        }, {
            options = {
                {
                    event = "police:general",
                    icon = "fas fa-clipboard",
                    label = "Davis Armory     "
                },
                {
                    event = "evidence:general",
                    icon = "fas fa-trash",
                    label = "Trash Locker"
                },
                {
                    event = "serial:search",
                    icon = "fas fa-hand-holding",
                    label = "Search Weapon Serial"
                },
            },
            job = {"police"},
            distance = 2.5
        })


        AddBoxZone("serial_search", vector3(485.5, -989.33, 30.69), 0.5, 0.5, {
            name="serial_search",
            heading=290,
            --debugPoly=true,
            minZ=30.69,
            maxZ=31.29
        }, {
            options = {
                {
                    event = "serial:search",
                    icon = "fas fa-hand-holding",
                    label = "Search Weapon Serial"
                },
            },
            job = {"police"},
            distance = 1.5
        })

        local doors = {
            --"seat_dside_f"
            "engine"
        }

        AddTargetVehicle(doors,
            {
                options = {
                    {
                        event = "police:forceEnter",
                        icon = "fas fa-sign-in-alt",
                        label = "Seat Player"
                    },
                    {
                        event = "unseatPlayer",
                        icon = "fas fa-sign-out-alt",
                        label = "Unseat Player"
                    },
                    {
                        event = "keys:give",
                        icon = "fas fa-key",
                        label = "Give The Car Key"
                    },
                    {
                        event = "FlipVehicle",
                        icon = "fas fa-undo",
                        label = "Flip the Vehicle"
                    },
                    {
                        event = "clean:cleanvehicle",
                        icon = "fas fa-mitten",
                        label = "Clean the car"
                    },
                    {
                        event = "veh:requestUpdate",
                        icon = "fas fa-wrench",
                        label = "Examine Internals"
                    },
                    {
                        event = "impoundVeh",
                        icon = "fas fa-lock",
                        label = "Impound Vehicle"
                    }
                },
                job = {"all"},
                distance = 2.5
            }
        )


        local Payphones = {
            1158960338,
            -78626473,
           1281992692,
           -1058868155,
           -429560270,
           -2103798695,
            295857659,
           -1559354806
        }

        AddTargetModel(Payphones,
            {
                options = {
                    {
                        event = "payphone:ui",
                        icon = "fas fa-phone-square-alt",
                        label = "Make a call! (Anonymous)"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        
        -------- BLACK MARKET --------
        --3522.5563964844, 3704.8408203125, 22.357629776001 Heading: 164.59127807617
        
        AddCircleZone(
            "blackmarketcraft",
            vector3(3522.55, 3704.84, 22.35),
            1,
            {
                name = "blackmarketcraft",
                --debugPoly = true,
                useZ = true
            },
            {
                options = {
                    {
                        event = "blackmarket:general", --96666
                        icon = "far fa-shopping-basket",
                        label = "Open Replimat"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        
        -------- EMS --------

        AddCircleZone(
            "emsdutys",
            vector3(310.22, -597.56, 43.28),
            0.2,
            {
                name = "emsdutys",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "emsduty",
                        icon = "far fa-clipboard",
                        label = "Sign On/Off Duty"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )

        AddCircleZone(
            "pillbox",
            vector3(306.54, -602.32, 43.53),
            1.11,
            {
                name = "pillbox",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "ems:general",
                        icon = "fas fa-shopping-basket",
                        label = "EMS Supply Closet"
                    }
                },
                job = {"ems"},
                distance = 1.5
            }
        )

        -------- Check In --------

        AddCircleZone(
            "checkin",
            vector3(307.53, -595.28, 43.08),
            0.33,
            {
                name = "checkin",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "ethical-hospitalization:checkin",
                        icon = "far fa-clipboard",
                        label = "Check In"
                    }
                    ---,
                    ---{
                    ---   event = "ethical-hospitalization:page",
                    ---   icon = "far fa-phone",
                    ---    label = "Call a Doctor"
                    ---}
                },
                job = {"all"},
                distance = 2.5
            }
        )


        AddCircleZone(
            "checkinpolito",
            vector3(-254.28, 6330.53, 32.6),
            0.33,
            {
                name = "checkinpolito",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "ethical-hospitalization:checkinpolito",
                        icon = "far fa-clipboard",
                        label = "Check In"
                    }
                    ---,
                    ---{
                    ---   event = "ethical-hospitalization:page",
                    ---   icon = "far fa-phone",
                    ---    label = "Call a Doctor"
                    ---}
                },
                job = {"all"},
                distance = 2.5
            }
        )
        -------- ATMS --------

        local atms = {
            -1126237515,
            506770882,
            -870868698,
            150237004,
            -239124254,
            -1364697528,
            506770882
        }
        AddTargetModel(
            atms,
            {
                options = {
                    {
                        event = "bank:checkATM",
                        icon = "fas fa-credit-card",
                        label = "Use ATM"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )

        -------- Vending Machines --------

        local food = {
            -654402915,
            -1034034125
        }
        local coffee = {
            690372739,
            -1318035530,
            -2015792788
        }
        local water = {
            1099892058
        }
        local soda = {
            1114264700,
            -504687826,
            992069095,
            -1741437518,
            -1317235795
        }
        AddTargetModel(
            food,
            {
                options = {
                    {
                        event = "shops:food",
                        icon = "fas fa-cookie-bite",
                        label = "Buy your self a nice snack!"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddTargetModel(
            coffee,
            {
                options = {
                    {
                        event = "shops:coffee",
                        icon = "fas fa-mug-hot",
                        label = "Make a nice cup of Coffee!"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddTargetModel(
            water,
            {
                options = {
                    {
                        event = "shops:water",
                        icon = "fas fa-tint",
                        label = "Drink a Refreshing Bottle Of Water!"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddTargetModel(
            soda,
            {
                options = {
                    {
                        event = "shops:soda",
                        icon = "fas fa-wine-bottle",
                        label = "Drink a Refreshing Can of Soda!"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )

        --------  General Stores --------
        AddCircleZone(
            "Store1",
            vector3(25.08, -1347.41, 29.5),
            0.55,
            {
                name = "Store1",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store1s",
            vector3(25.11, -1345.0, 29.5),
            0.55,
            {
                name = "Store1s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store2",
            vector3(-706.7, -915.54, 19.22),
            0.55,
            {
                name = "Store2",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store2s",
            vector3(-706.68, -913.55, 19.22),
            0.55,
            {
                name = "Store2s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store3",
            vector3(-1222.31, -907.74, 12.33),
            0.55,
            {
                name = "Store3",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store4",
            vector3(-47.22, -1757.5, 29.42),
            0.55,
            {
                name = "Store4",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store4s",
            vector3(-48.42, -1759.02, 29.42),
            0.55,
            {
                name = "Store4s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store5",
            vector3(1134.9, -982.46, 46.42),
            0.55,
            {
                name = "Store5",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Use"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store6",
            vector3(1164.08, -322.73, 69.21),
            0.55,
            {
                name = "Store6",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store6s",
            vector3(1164.49, -324.74, 69.21),
            0.55,
            {
                name = "Store6s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store7",
            vector3(373.12, 326.21, 103.57),
            0.55,
            {
                name = "Store7",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store7s",
            vector3(373.62, 328.58, 103.57),
            0.55,
            {
                name = "Store7s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store8",
            vector3(-1820.68, 793.82, 138.14),
            0.55,
            {
                name = "Store8",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store8s",
            vector3(-1819.29, 792.38, 138.14),
            0.55,
            {
                name = "Store8s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store9",
            vector3(-2967.1, 390.97, 15.04),
            0.55,
            {
                name = "Store9",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store10",
            vector3(-3039.1, 585.11, 7.91),
            0.55,
            {
                name = "Store10",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store10s",
            vector3(-3041.36, 584.35, 7.91),
            0.55,
            {
                name = "Store10s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store11",
            vector3(-3242.17, 1000.57, 12.83),
            0.55,
            {
                name = "Store11",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store11s",
            vector3(-3244.56, 1000.69, 12.83),
            0.55,
            {
                name = "Store11s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )

        AddCircleZone(
            "Store12",
            vector3(2557.28, 381.38, 108.62),
            0.55,
            {
                name = "Store12",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store12s",
            vector3(2554.88, 381.49, 108.62),
            0.55,
            {
                name = "Store12s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store13",
            vector3(548.46, 2671.31, 42.16),
            0.55,
            {
                name = "Store13",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store13s",
            vector3(548.81, 2668.96, 42.16),
            0.55,
            {
                name = "Store13s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store14",
            vector3(1165.89, 2710.13, 38.16),
            0.55,
            {
                name = "Store14",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store15",
            vector3(1393.04, 3605.85, 34.98),
            0.55,
            {
                name = "Store15",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store15s",
            vector3(2678.45, 3279.905, 55.24),
	    0.55,
            {
                name = "Store15s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store15ss",
            vector3(2676.20, 3280.919, 55.24),
	    0.55,
            {
                name = "Store15ss",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store16",
            vector3(1960.62, 3740.19, 32.34),
            0.55,
            {
                name = "Store16",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store16s",
            vector3(1959.34, 3742.35, 32.34),
            0.55,
            {
                name = "Store16s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store17",
            vector3(1696.83, 4924.53, 42.06),
            0.55,
            {
                name = "Store17",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store17s",
            vector3(1698.46, 4923.44, 42.06),
            0.55,
            {
                name = "Store17s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store18",
            vector3(1728.38, 6414.87, 35.04),
            0.55,
            {
                name = "Store18",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store18s",
            vector3(1729.47, 6417.11, 35.04),
            0.55,
            {
                name = "Store18s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "Store18ss",
            vector3(189.082, -890.027, 31.035),
            0.55,
            {
                name = "Store18s",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "shop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Goods"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )

        -------- Mega Mall --------

        local megamall = {
            [1] = -1453933154
        }

        AddTargetModel(
            megamall,
            {
                options = {
                    {
                        event = "toolshop:general",
                        icon = "fas fa-shopping-basket",
                        label = "Purchase Tools"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )

        -------- Weapon Shop --------

        -- local weaponshop = {
        --     [1] = -1643617475
        -- }

        -- AddTargetModel(
        --     weaponshop,
        --     {
        --         options = {
        --             {
        --                 event = "weapon:general",
        --                 icon = "fas fa-wrench",
        --                 label = "Purchase Weapons"
        --             }
        --         },
        --         job = {"all"},
        --         distance = 3
        --     }
        -- )

        AddBoxZone("weapon_shop_1", vector3(20.22, -1106.09, 29.8), 2, 2, {
            name="weapon_shop_1",
            heading=340,
            --debugPoly=true,
            minZ=28.8,
            maxZ=30.8
        }, {
            options = {
                {
                    event = "weapon:general",
                    icon = "fas fa-wrench",
                    label = "Purchase Weapons"
                }
            },
            job = {"all"},
            distance = 1.5
        })


        AddBoxZone("weapon_shop_2", vector3(253.23, -48.21, 69.94), 2, 2, {
            name="weapon_shop_2",
            heading=340,
            --debugPoly=true,
            minZ=68.94,
            maxZ=70.94
        }, {
            options = {
                {
                    event = "weapon:general",
                    icon = "fas fa-wrench",
                    label = "Purchase Weapons"
                }
            },
            job = {"all"},
            distance = 1.5
        })


        AddBoxZone("weapon_shop_3", vector3(-1305.11, -392.29, 36.7), 2.0, 2, {
            name="weapon_shop_3",
            heading=345,
            --debugPoly=true,
            minZ=35.7,
            maxZ=37.7
        }, {
            options = {
                {
                    event = "weapon:general",
                    icon = "fas fa-wrench",
                    label = "Purchase Weapons"
                }
            },
            job = {"all"},
            distance = 1.5
        })


        AddBoxZone("weapon_shop_4", vector3(-664.26, -934.41, 21.83), 2.0, 1, {
            name="weapon_shop_4",
            heading=90,
            --debugPoly=true,
            minZ=20.83,
            maxZ=22.83
        }, {
            options = {
                {
                    event = "weapon:general",
                    icon = "fas fa-wrench",
                    label = "Purchase Weapons"
                }
            },
            job = {"all"},
            distance = 1.5
        })


        AddBoxZone("weapon_shop_4", vector3(-664.26, -934.41, 21.83), 2.0, 1, {
            name="weapon_shop_4",
            heading=90,
            --debugPoly=true,
            minZ=20.83,
            maxZ=22.83
        }, {
            options = {
                {
                    event = "weapon:general",
                    icon = "fas fa-wrench",
                    label = "Purchase Weapons"
                }
            },
            job = {"all"},
            distance = 1.5
        })


        AddBoxZone("weapon_shop_5", vector3(844.4, -1034.54, 28.19), 2.0, 1, {
            name="weapon_shop_5",
            heading=90,
            --debugPoly=true,
            minZ=27.19,
            maxZ=28.99
        }, {
            options = {
                {
                    event = "weapon:general",
                    icon = "fas fa-wrench",
                    label = "Purchase Weapons"
                }
            },
            job = {"all"},
            distance = 1.5
        })

        AddBoxZone("weapon_shop_6", vector3(812.24, -2158.1, 29.62), 1.8, 1, {
            name="weapon_shop_6",
            heading=90,
            --debugPoly=true,
            minZ=28.62,
            maxZ=30.42
        }, {
            options = {
                {
                    event = "weapon:general",
                    icon = "fas fa-wrench",
                    label = "Purchase Weapons"
                }
            },
            job = {"all"},
            distance = 1.5
        })


        AddBoxZone("weapon_shop_7", vector3(2569.96, 293.18, 108.73), 1.8, 1, {
            name="weapon_shop_7",
            heading=90,
            --debugPoly=true,
            minZ=107.73,
            maxZ=109.53
        }, {
            options = {
                {
                    event = "weapon:general",
                    icon = "fas fa-wrench",
                    label = "Purchase Weapons"
                }
            },
            job = {"all"},
            distance = 1.5
        })

        AddBoxZone("weapon_shop_8", vector3(1691.51, 3758.9, 34.71), 1.65, 1, {
            name="weapon_shop_8",
            heading=135,
            --debugPoly=true,
            minZ=33.71,
            maxZ=35.51
        }, {
            options = {
                {
                    event = "weapon:general",
                    icon = "fas fa-wrench",
                    label = "Purchase Weapons"
                }
            },
            job = {"all"},
            distance = 1.5
        })

        AddBoxZone("weapon_shop_9", vector3(-1119.85, 2697.74, 18.55), 1.6, 1, {
            name="weapon_shop_9",
            heading=310,
            --debugPoly=true,
            minZ=17.55,
            maxZ=19.35
        }, {
            options = {
                {
                    event = "weapon:general",
                    icon = "fas fa-wrench",
                    label = "Purchase Weapons"
                }
            },
            job = {"all"},
            distance = 1.5
        })


        AddBoxZone("weapon_shop_10", vector3(-3173.59, 1086.26, 20.84), 1.6, 1, {
            name="weapon_shop_10",
            heading=335,
            --debugPoly=true,
            minZ=19.84,
            maxZ=21.64
        }, {
            options = {
                {
                    event = "weapon:general",
                    icon = "fas fa-wrench",
                    label = "Purchase Weapons"
                }
            },
            job = {"all"},
            distance = 1.5
        })


        AddBoxZone("weapon_shop_11", vector3(-332.5, 6083.09, 31.45), 1.8, 1, {
            name="weapon_shop_11",
            heading=315,
            --debugPoly=true,
            minZ=30.45,
            maxZ=32.25
        }, {
            options = {
                {
                    event = "weapon:general",
                    icon = "fas fa-wrench",
                    label = "Purchase Weapons"
                }
            },
            job = {"all"},
            distance = 1.5
        })

        -------- Banks --------
        AddCircleZone(
            "bank1",
            vector3(149.66, -1041.52, 29.38),
            0.9,
            {
                name = "bank1",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "bank:openbank",
                        icon = "fas fa-university",
                        label = "Open Bank"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "bank2",
            vector3(313.99, -279.77, 54.17),
            0.9,
            {
                name = "bank2",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "bank:openbank",
                        icon = "fas fa-university",
                        label = "Open Bank"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "bank3",
            vector3(241.61, 226.15, 106.29),
            0.9,
            {
                -- Add Payhecks to this
                name = "bank3",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "bank:openbank",
                        icon = "fas fa-university",
                        label = "Open Bank"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "bank4",
            vector3(243.43, 225.42, 106.29),
            0.9,
            {
                -- Add Payhecks to this
                name = "bank4",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "bank:openbank",
                        icon = "fas fa-university",
                        label = "Open Bank"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "bank5",
            vector3(-351.21, -50.77, 49.04),
            0.9,
            {
                name = "bank5",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "bank:openbank",
                        icon = "fas fa-university",
                        label = "Open Bank"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "bank6",
            vector3(-1212.27, -331.37, 37.78),
            0.9,
            {
                name = "bank6",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "bank:openbank",
                        icon = "fas fa-university",
                        label = "Open Bank"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "bank7",
            vector3(-2961.81, 482.95, 15.7),
            0.9,
            {
                name = "bank7",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "bank:openbank",
                        icon = "fas fa-university",
                        label = "Open Bank"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "bank8",
            vector3(-112.09, 6470.12, 31.63),
            0.9,
            {
                name = "bank8",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "bank:openbank",
                        icon = "fas fa-university",
                        label = "Open Bank"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "bank9",
            vector3(1175.0703125, 2706.7980957031, 38.0940284729),
            0.9,
            {
                name = "bank8",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "bank:openbank",
                        icon = "fas fa-university",
                        label = "Open Bank"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )
            -------- Prison --------
        AddCircleZone(
            "prison1",
            vector3(1778.8070068359, 2557.5014648438, 45.67293548584),
            0.81,
            {
                name = "prison1",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "slushy:general",
                        icon = "fas fa-ice-cream",
                        label = "Make Slushy"
                    }
                },
                job = {"prisoner"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "prison2",
            vector3(1783.098, 2558.45, 45.672),
            0.7,
            {
                name = "prison2",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "pfood:general",
                        icon = "fas fa-bread-slice",
                        label = "Prison Food"
                    }
                },
                job = {"prisoner"},
                distance = 1.5
            }
        )
        
        AddCircleZone(
            "prisonmeth",
            vector3(1742.563, 2488.837, 50.421),
            0.7,
            {
                name = "prisonmeth",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "pmeth:general",
                        icon = "fas fa-hand-sparkles",
                        label = "Clean this toilet"
                    }
                },
                job = {"prisoner"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "prison3",
            vector3(1784.970, 2565.210, 45.672),
            1.0,
            {
                name = "prison3",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "prisonap:general",
                        icon = "fas fa-question",
                        label = "Search Boxes"
                    }
                },
                job = {"prisoner"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "prison4",
            vector3(1776.299, 2488.488, 45.820),
            0.8,
            {
                name = "prison4",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "prisonlp:general",
                        icon = "fas fa-question",
                        label = "Craft"
                    }
                },
                job = {"prisoner"},
                distance = 1.5
            }
        )
        -- AddCircleZone(
        --     "reclaim",
        --     vector3(1847.654296875, 2584.6437988281, 45.672019958496),
        --     1.0,
        --     {
        --         name = "reclaim",
        --         debugPoly = false,
        --         useZ = true
        --     },
        --     {
        --         options = {
        --             {
        --                 event = "jailbreak:reclaimitems",
        --                 icon = "fas fa-undo",
        --                 label = "Reclaim Possessions"
        --             }
        --         },
        --         job = {"all"},
        --         distance = 1.5
        --     }
        -- )


        -- Bean Machine
        AddBoxZone("bean_tray", vector3(-634.69, 236.56, 81.91), 0.6, 1, {
            name="bean_tray",
            heading=5,
            --debugPoly=true,
            minZ=82.11,
            maxZ=82.51
        }, {
            options = {
                {
                    event = "ethical-beanmachine:pickup",
                    parms = "bean_tray",
                    icon = "fas fa-hand-holding",
                    label = "Take the Items",
                },
            },
                job = {"all"},
                distance = 1.5
        })

        AddBoxZone("bean_storage", vector3(-631.859375, 227.91253662109, 81.881507873535), 1.6, 1, {
            name="bean_storage",
            heading=356,
            --debugPoly=true,
            minZ=79.43,
            maxZ=83.43
        }, {
            options = {
                {
                    event = "open:storage",
                    parms = "bean_machine",
                    icon = "fas fa-box-open",
                    label = "Open Bean Machine's Stash!"
                },
             },
             job = {"bean_machine"},
            distance = 2.5
        })

        
        AddBoxZone("bean_frig", vector3(-635.31030273438, 233.62026977539, 81.881538391113), 1.4, 1, {
            name="bean_frig",
            heading=160,
            --debugPoly=true,
            minZ=80.88,
            maxZ=85.93
        }, {
            options = {
                {
                    event = "bean:order",
                    icon = "fas fa-box-open",
                    label = "Open Bean Machine's Fridge!"
                },
             },
             job = {"bean_machine"},
            distance = 2.5
        })
       

        AddBoxZone("bean_craft", vector3(-635.22253417969, 235.8413848877, 81.88330078125), 1.1, 0.5, {
            name="bean_craft",
            heading=40,
            --debugPoly=true,
            minZ=80.08,
            maxZ=83.63
        }, {
            options = {
                {
                    event = "bean:craft",
                    icon = "fas fa-box-open",
                    label = "Make Coffee!"
                },
             },
             job = {"bean_machine"},
            distance = 2.5
        })

        AddBoxZone("bean_craft2", vector3(-629.4317, 222.9399, 82.0472), 1.1, 0.5, {
            name="bean_craft2",
            heading=40,
            --debugPoly=true,
            minZ=80.08,
            maxZ=84.63
        }, {
            options = {
                {
                    event = "bean:craft2",
                    icon = "fas fa-box-open",
                    label = "Make Food!"
                },
             },
             job = {"bean_machine"},
            distance = 2.5
        })


        AddBoxZone("bean_chargecustomer",vector3(-634.33929443359, 235.42379760742, 83.117523193359), 0.5, 0.5, {
            name="bean_chargecustomer",
            heading=270,
            --debugPoly=true,
            minZ=80.68,
            maxZ=90.88
        }, {
            options = {
                {
                    event = "ethical-beanmachine:bill",
                    icon = "fas fa-cash-register",
                    label = "Charge Customer",
                },
             },
             job = {"bean_machine"},
            distance = 2.5
        })
        
        -- -635.62237548828, 227.03076171875, 82.35
        AddBoxZone("bean_redeemreceipts",vector3(-635.62, 227.03, 82.357523193359), 1, 1, {
            name="bean_redeemreceipts",
            heading=270,
            --debugPoly=true,
            minZ=80.68,
            maxZ=90.88
        }, {
            options = {
                {
                    event = "ethical-beanmachine:cash:in",
                    icon = "fas fa-cash-register",
                    label = "Redeem Receipts",
                },
             },
             job = {"bean_machine"},
            distance = 2.5
        })

        AddBoxZone("bean_duty",vector3(-634.4058, 225.5780, 81.88), 1, 1, {
            name="bean_duty",
            heading=270,
            --debugPoly=true,
            minZ=80.68,
            maxZ=90.88
        }, {
            options = {
                {
                    event = "ethical-beanmachine:signonmenu",
                    icon = "fas fa-clipboard",
                    label = "Sign On/Off Duty",
                },
             },
             job = {"all"},
            distance = 2.5
        })
        
        
        AddBoxZone("bean_vehicle",vector3(-633.918, 228.617, 81.88145), 0.5, 0.5, {
            name="bean_vehicle",
            heading=270,
            -- debugPoly=true,
            minZ=80.68,
            maxZ=81.98
        }, {
            options = {
                {
                    event = "requestBeanMachineVehicles",
                    icon = "fas fa-key",
                    label = "Take Car Keys",
                },
             },
             job = {"bean_machine"},
            distance = 2.5
        })

        -------- Courthouse (TELEPORTERS) -------- Removed due to lag/fall through floor issues when using

        --[[ AddCircleZone(
            "courthouseenter",
            vector3(235.26, -412.24, 48.11),
            5.0,
            {
                name = "courthouseenter",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "courthouse:enter",
                        icon = "fas fa-door-open",
                        label = "Enter the Courthouse"
                    }
                },
                job = {"all"},
                distance = 10.0
            }
        )

        AddCircleZone(
            "courthouseexit",
            vector3(269.21, -371.89, -44.14),
            1.5,
            {
                name = "courthouseexit",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "courthouse:exit",
                        icon = "fas fa-door-open",
                        label = "Exit the Courthouse"
                    }
                },
                job = {"all"},
                distance = 10.0
            }
        )

        AddCircleZone(
            "courtroomenter",
            vector3(249.17, -364.77, -44.14),
            1.5,
            {
                name = "courtroomenter",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "courtroom:enter",
                        icon = "fas fa-door-open",
                        label = "Enter the Courtroom"
                    }
                },
                job = {"all"},
                distance = 10.0
            }
        )

        AddCircleZone(
            "courtroomexit",
            vector3(313.18, -1611.2, -66.79),
            1.5,
            {
                name = "courtroomexit",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "courtroom:exit",
                        icon = "fas fa-door-open",
                        label = "Exit the Courtroom"
                    }
                },
                job = {"all"},
                distance = 10.0
            }
        ) ]]

        AddBoxZone(
            "TacoCraft",
            vector3(16.27, -1597.89, 29.38),
            2.6,
            0.8,
            {
                name = "TacoCraft",
                heading = 230,
                debugPoly = false,
                minZ = 28.18,
                maxZ = 29.58
            },
            {
                options = {
                    {
                        event = "taco:craft",
                        icon = "fas fa-utensils",
                        label = "Cook up some grub!"
                    }
                },
                job = {"all"},
                distance = 3.5
            }
        )

        AddBoxZone(
            "TacoList",
            vector3(8.17, -1607.24, 29.37),
            0.4,
            0.2,
            {
                name = "TacoList",
                heading = 230,
                debugPoly = false,
                minZ = 29.52,
                maxZ = 29.92
            },
            {
                options = {
                    {
                        event = "taco:list",
                        icon = "fas fa-laptop",
                        label = "Check the Latest Orders"
                    }
                },
                job = {"all"},
                distance = 3.5
            }
        )

        -- AddBoxZone(
        --     "Tacodelivery",
        --     vector3(10.33, -1605.26, 29.39),
        --     2.4,
        --     1.0,
        --     {
        --         name = "Tacodelivery",
        --         heading = 320,
        --         debugPoly = false,
        --         minZ = 28.69,
        --         maxZ = 31.29
        --     },
        --     {
        --         options = {
        --             {
        --                 event = "taco:deliver2",
        --                 icon = "fas fa-shopping-bag",
        --                 label = "Begin Delivery"
        --             }
        --         },
        --         job = {"all"},
        --         distance = 3.5
        --     }
        -- )

        -------- Auto Exotics --------
        AddBoxZone(
            "AutoExoticsStorage",
            vector3(-196.511, -1314.715, 31.271), -- -196.51168823242, -1314.7152099609, 31.271062850952 Heading: 351.17071533203
            2.0,
            1,
            {
                name = "Storage",
                heading = 75,
                debugPoly = false,
                minZ = 30.19,
                maxZ = 32.5
            },
            {
                options = {
                    {
                        event = "mech:check:internal:storage",
                        parms = "auto_exotics",
                        icon = "fas fa-hammer",
                        label = "Check AutoExotic's Internals's Supply"
                    },
                    {
                        event = "open:storage",
                        parms = "auto_exotics",
                        icon = "fas fa-box-open",
                        label = "Open AutoExotic's Stash!"
                    },
                    {
                        event = "mech:craft",
                        icon = "fas fa-wrench",
                        label = "Open AutoExotic's Crafting Book!"
                    }
                },
                job = {"auto_exotics"},
                distance = 3
            }
        )
        
        AddBoxZone(
            "AutoExoticsDutyStation",
            vector3(-206.664, -1341.821, 34.894),
            1.2,
            1.5,
            {
                name = "AutoExoticsDutyStation",
                heading = 276.0,
                debugPoly = false,
                minZ = 30.96,
                maxZ = 36.0
            },
            {
                options = {
                    {
                        event = "mech:dutyAutoExoticsMenu",
                        parms = "auto_exotics",
                        icon = "fas fa-address-card",
                        label = "Check Duty Status"
                    }
                },
                job = {"all"},
                distance = 3
            }
        )
        
        -------- Auto Bodies --------
        AddBoxZone(
            "AutoBodiesDutyStation",
            vector3(-1426.50, -458.31, 36.1),
            1.5,
            1.5,
            {
                name = "AutoBodiesDutyStation",
                heading = 18.0,
                debugPoly = false,
                minZ = 30.96,
                maxZ = 36.6
            },
            {
                options = {
                    {
                        event = "mech:dutyAutoBodiesMenu",
                        parms = "auto_bodies",
                        icon = "fas fa-address-card",
                        label = "Check Duty Status"
                    }
                },
                job = {"all"},
                distance = 3
            }
        )

        AddBoxZone(
            "AutoBodies",
            vector3(-1421.2340087891, -455.22863769531, 35.909713745117),
            3.5,
            1,
            {
                name = "AutoBodies",
                heading = 302,
                debugPoly = false,
                minZ = 34.86,
                maxZ = 37.26
            },
            {
                options = {
                    {
                        event = "mech:check:internal:storage",
                        parms = "auto_bodies",
                        icon = "fas fa-hammer",
                        label = "Check Hayes's Internals's Supply"
                    },
                    {
                        event = "open:storage",
                        parms = "auto_bodies",
                        icon = "fas fa-box-open",
                        label = "Open Hayes's Stash!"
                    },
                    {
                        event = "mech:craft",
                        icon = "fas fa-wrench",
                        label = "Open Hayes's Crafting Book!"
                    }
                },
                job = {"auto_bodies"},
                distance = 3
            }
        )

         
        AddBoxZone("auto_bodies_register", vector3(-1430.00, -453.87, 35.90), 1, 1, {
            name="auto_bodies_register",
            heading=210.03,
            debugPoly=false,
            minZ=35.34,
            maxZ=37.54
        }, {
                options = {
                    {
                        event = "ethical-mechanic:bill",
                        icon = "fas fa-cash-register",
                        label = "Charge Customer"
                    }, 
                    {
                        event = "ethical-mechanic:cash:in",
                        icon = "fas fa-handshake",
                        label = "Redeem Receipts"
                    }
                },
                job = {"auto_bodies"},
                distance = 2.5
            }
        )
        -------- Tuner Shop --------

        AddBoxZone(
            "Tuner",
            vector3(948.03, -969.7, 39.5),
            3.9,
            1,
            {
                name = "Tuner",
                heading = 4,
                debugPoly = false,
                minZ = 38.5,
                maxZ = 40.1
            },
            {
                options = {
                    {
                        event = "mech:check:internal:storage",
                        parms = "tuner_shop",
                        icon = "fas fa-hammer",
                        label = "Check Tuner Shop's Internals's Supply"
                    },
                    {
                        event = "open:storage",
                        parms = "Tuner",
                        icon = "fas fa-box-open",
                        label = "Open Tuner Shop's Stash!"
                    },
                    {
                        event = "mech:craft",
                        icon = "fas fa-wrench",
                        label = "Open Tuner Shop's Crafting Book!"
                    }
                },
                job = {"tuner_shop"},
                distance = 3
            }
        )

        AddBoxZone(
            "TunerShopDutyStation",
            vector3(950.149, -968.570, 39.506),
            1.2,
            1.5,
            {
                name = "TunerShopDutyStation",
                heading = 276.0,
                debugPoly = false,
                minZ = 38.96,
                maxZ = 40.0
            },
            {
                options = {
                    {
                        event = "mech:dutyTunerShopMenu",
                        parms = "auto_exotics",
                        icon = "fas fa-address-card",
                        label = "Check Duty Status"
                    }
                },
                job = {"all"},
                distance = 3
            }
        )

        -------- Paleto Mech --------
        AddBoxZone(
            "Paletoperf",
            vector3(106.16, 6629.43, 31.79),
            3.8,
            1,
            {
                name = "Paletoperf",
                heading = 315,
                debugPoly = false,
                minZ = 30.79,
                maxZ = 32.59
            },
            {
                options = {
                    {
                        event = "mech:check:internal:storage",
                        parms = "Paletoperf",
                        icon = "fas fa-hammer",
                        label = "Check Paleto Performance's Internals's Supply"
                    },
                    {
                        event = "open:storage",
                        parms = "Paletoperf",
                        icon = "fas fa-box-open",
                        label = "Open Paleto Performance's Stash!"
                    },
                    {
                        event = "mech:craft",
                        icon = "fas fa-wrench",
                        label = "Open Paleto Performance's Crafting Book!"
                    }
                },
                job = {"paleto_mech"},
                distance = 3
            }
        )

        -------- Best Buds --------
   --759654580
        local bestbudsregisters = {
            [1] = 759654580
        }

        AddTargetModel(
            bestbudsregisters,
            {
                options = {
                    {
                        event = "ethical-bestbuds:bill",
                        icon = "fas fa-cash-register",
                        label = "Charge Customer"
                    }, 
                    {
                        event = "ethical-bestbuds:cash:in",
                        icon = "fas fa-file-text",
                        label = "Redeem Receipts"
                    }
                },
                job = {"best_buds"},
                distance = 1.5
            }
        )
        AddCircleZone(
            "bestbudsstorage", vector3(380.84420776367, -820.04235839844, 29.30261039733), 1.8,
            {
                name = "bestbudsstorage",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "open:storage",
                        parms = "best_buds",
                        icon = "fas fa-box-open",
                        label = "Open Bestbuds Storage"
                    }
                },
                job = {"best_buds"},
                distance = 1.5
            }
        )

        AddBoxZone("best_buds_shop", vector3(377.12396240234, -826.14831542969, 29.302156448364), 1, 2.0, {
            name="best_buds_shop",
            heading=353,
            debugPoly=false,
            minZ=10,
            maxZ=30
        }, {
         
            options = {
                {
                    event = "bestbuds:shop",
                    icon = "fas fa-shopping-basket",
                    label = "Open BestBuds Weed Shop"
                }
            },
         
             job = {"best_buds"},
            distance = 3.5
        })

        AddBoxZone("best_buds_duty", vector3(375.8619, -825.101, 30.0879), 0.8, 1.0, {
            name="best_buds_duty",
            heading=353,
            debugPoly=false,
            minZ=10,
            maxZ=30.5
        }, {
            options = {
                {
                    event = "bestbuds:dutyMenu",
                    icon = "fas fa-prescription",
                    label = "Open Duty Roster"
                }
            },
            job = {"all"},
            distance = 3.5
        })

        -- AddBoxZone("best_buds_register_1", vector3(380.16445922852, -827.08619384766, 29.302227020264), 0.6, 1, {
        --     name="best_buds_register_1",
        --     heading=300,
        --     debugPoly=true,
        --     minZ=10,
        --     maxZ=30
        -- }, {
        --     options = {
        --         {
        --             event = "Best_buds:get:receipt",
        --             parms = "1",
        --             icon = "fas fa-cash-register",
        --             label = "Make Payment",
        --         },
        --         {
        --             event = "Best_buds:register",
        --             parms = "1",
        --             icon = "fas fa-credit-card",
        --             label = "Charge Customer",
        --         },
        --      },
        --      job = {"all"},
        --     distance = 1.5
        -- })


        -- AddBoxZone("best_buds_register_2", vector3(375.4921875, -827.08566894531, 29.302255630493), 0.6, 1, {
        --     name="best_buds_register_2",
        --     heading=30,
        --     debugPoly=true,
        --     minZ=10,
        --     maxZ=30
        -- }, {
        --     options = {
        --         {
        --             event = "Best_buds:get:receipt",
        --             parms = "2",
        --             icon = "fas fa-cash-register",
        --             label = "Make Payment",
        --         },
        --         {
        --             event = "Best_buds:register",
        --             parms = "2",
        --             icon = "fas fa-credit-card",
        --             label = "Charge Customer",
        --         },
        --      },
        --      job = {"all"},
        --     distance = 1.5
        -- })


        -- AddBoxZone("best_buds_receipts", vector3(374.3544921875, -824.07855224609, 29.302980422974), 2.15, 1, {
        --     name="best_buds_receipts",
        --     heading=30,
        --     debugPoly=true,
        --     minZ=35.19,
        --     maxZ=35.49
        -- }, {
        --     options = {
        --         {
        --             event = "Best_buds:cash:in",
        --             icon = "fas fa-cash-register",
        --             label = "Cash in receipts",
        --         },
        --      },
        --      job = {"best_buds"},
        --     distance = 1.5
        -- })
        
        AddBoxZone("best_buds_counter", vector3(378.66, -827.49, 29.48), 1, 1, {
             name="best_buds_counter",
             heading=90,
             --debugPoly=true,
             minZ=28.34,
             maxZ=29.54
         }, {
             options = {
                 {
                    event = "ethical-bestbuds:pickup",
                     parms = "best_buds_counter",
                     icon = "fas fa-hand-holding",
                     label = "Take the Items",
                 },
             },
             job = {"all"},
             distance = 1.5
         })

         --Mech shop charge and redeem
         AddBoxZone("auto_exotics_register", vector3(-203.52, -1313.70, 30.89), 1, 1, {
            name="auto_exotics_register",
            heading=90,
            --debugPoly=true,
            minZ=29.34,
            maxZ=32.54
        }, {
                options = {
                    {
                        event = "ethical-mechanic:bill",
                        icon = "fas fa-cash-register",
                        label = "Charge Customer"
                    }, 
                    {
                        event = "ethical-mechanic:cash:in",
                        icon = "fas fa-handshake",
                        label = "Redeem Receipts"
                    }
                },
                job = {"auto_exotics"},
                distance = 2.5
            }
        )

        AddBoxZone("tuner_shop_register", vector3(952.00, -968.32, 39.50), 1, 1, {
            name="tuner_shop_register",
            heading=90,
            --debugPoly=true,
            minZ=38.34,
            maxZ=41.54
        }, {
                options = {
                    {
                        event = "ethical-mechanic:bill",
                        icon = "fas fa-cash-register",
                        label = "Charge Customer"
                    }, 
                    {
                        event = "ethical-mechanic:cash:in",
                        icon = "fas fa-handshake",
                        label = "Redeem Receipts"
                    }
                },
                job = {"tuner_shop"},
                distance = 2.5
            }
        )
        -------- Tequaila --------

        AddCircleZone("Tequila", vector3(-562.31, 287.77, 82.18), 2.2,
            {
                name = "Tequila ",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "tequila:shop",
                        icon = "fas fa-shopping-basket",
                        label = "Open the Bar's Menu"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )

        -------- Smelter --------

        AddCircleZone(
            "smelter",
            vector3(1109.83, -2008.2, 31.06),
            2.0,
            {
                name = "smelter",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "smelter-smelt",
                        icon = "fas fa-shopping-basket",
                        label = "Sell a Gold Bar"
                    },
                    {
                        event = "smelter-smeltjewellery",
                        icon = "fas fa-shopping-basket",
                        label = "Melt down Items into Gold Bars"
                    }
                },
                job = {"all"},
                distance = 10.0
            }
        )

        -------- Hospital (TELEPORTERS) --------


        AddBoxZone("hospitalroof1", vector3(332.35, -597.32, 43.28), 0.2, 1, {
            name="hospitalroof2",
            heading=340,
            --debugPoly=true,
            minZ=43.48,
            maxZ=43.78
        }, {
            options = {
                {
                    event = "hospital:roof",
                    icon = "fas fa-arrow-alt-circle-up",
                    label = "Enter the Elevator to the Roof"
                },
            },
            job = {"all"},
            distance = 5.0
        })

        AddBoxZone("hospitalroof", vector3(330.3, -602.77, 43.28), 0.2, 1, {
            name="hospitalroof",
            heading=340,
            --debugPoly=true,
            minZ=43.48,
            maxZ=43.88
        }, {
            options = {
                {
                    event = "hospital:lower",
                    icon = "fas fa-arrow-alt-circle-down",
                    label = "Enter the Elevator to the Bottom Floor"
                }
            },
            job = {"all"},
            distance = 10.0
        })


        AddBoxZone("hospitallowerup", vector3(344.32, -584.58, 28.8), 0.15, 1, {
            name="hospitallowerup",
            heading=340,
            --debugPoly=true,
            minZ=29.0,
            maxZ=29.25
        }, {
            options = {
                {
                    event = "hospital:elevatorup",
                    icon = "fas fa-arrow-alt-circle-up",
                    label = "Enter the Elevator to the Main Hospital"
                }
            },
            job = {"all"},
            distance = 5.0
        })

        AddBoxZone("hospitalroof2", vector3(345.71, -580.91, 28.8), 0.2, 1, {
            name="hospitalroof2",
            heading=340,
            --debugPoly=true,
            minZ=29.0,
            maxZ=29.35
        }, {
            options = {
                {
                    event = "hospital:roof",
                    icon = "fas fa-arrow-alt-circle-up",
                    label = "Enter the Elevator to the Roof"
                }
            },
            job = {"all"},
            distance = 5.0
        })

        AddBoxZone("hospitalrooftomain", vector3(338.23, -583.74, 74.16), 3.0, 1, {
            name="hospitalrooftomain",
            heading=340,
            --debugPoly=true,
            minZ=73.16,
            maxZ=75.36
        }, {
            options = {
                {
                    event = "hospital:roofexit",
                    icon = "fas fa-arrow-alt-circle-down",
                    label = "Enter the Door to elevator to leave the roof!"
                }
            },
            job = {"all"},
            distance = 5.0
        })
    
        local pdheliped = {
            [1] = -1422914553
        }
       
        AddTargetModel(
            pdheliped,
            {
                options = {
                    {
                        event = "spawn:hei:pd",
                        icon = "fas fa-helicopter",
                        label = "Police Air Garage"
                    }
                },
                job = {"police"},
                distance = 1.5
            }
        )

        local emsheliped = {
            [1] = -163714847
        }

        --351.26876831055, -588.04632568359, 74.161773681641 Heading: 218.46687316895
         AddBoxZone("hospitalroofhelipad", vector3(351.26, -588.04, 74.16), 3.0, 3.0, {
                name="hospitalroofhelipad",
                heading=340,
                --debugPoly=true,
                minZ=73.16,
                maxZ=75.36
            }, {
                options = {
                    {
                        event = "spawn:hei:ems",
                        icon = "fas fa-helicopter",
                        label = "EMS Air Garage"
                    },
                    {
                        event = "impoundVeh",
                        icon = "fas fa-trash",
                        label = "Return Chopper"
                    },
                },
                job = {"ems"},
                distance = 1.5
            }
        )

        AddBoxZone("emsreciepts", vector3(339.92, -589.83, 43.39), 1.0, 1.0, {
            name="emsreciepts",
            heading=340,
            --debugPoly=true,
            minZ=42.16,
            maxZ=45.36
        }, {
            options = {
                {
                    event = "ems:cash:in",
                    icon = "fas fa-cash-register",
                    label = "Redeem Receipts"
                }
            },
            job = {"ems"},
            distance = 2.5
        }
    )


    AddBoxZone("dojsignon", vector3(-570.12, -198.43, 38.25), 1.0, 1.0, {
        name="dojsignon",
        heading=340,
        --debugPoly=true,
        minZ=37.16,
        maxZ=39.36
    }, {
        options = {
            {
                event = "ethical-dojjob:signonmenu",
                icon = "fas fa-gavel",
                label = "Sign on Duty"
            }, 
            {
                event = "ethical-dojjob:cash:in",
                icon = "fas fa-cash-register",
                label = "Collect Pay"
            }
        },
        job = {"all"},
        distance = 2.5
    }
)
        local emsvehped = {
            [1] = 2139205821
        }

        AddTargetModel(
            emsvehped,
            {
                options = {
                    {
                        event = "EMSSpawnVeh",
                        icon = "fas fa-car",
                        label = "EMS Garage"
                    }
                },
                job = {"ems"},
                distance = 1.5
            }
        )

        local pdvehped = {
            [1] = -994634286
        }

        AddTargetModel(
            pdvehped,
            {
                options = {
                    {
                        event = "PDSpawnVeh",
                        icon = "fas fa-car",
                        label = "Police Garage"
                    }
                },
                job = {"police"},
                distance = 1.5
            }
        )

        local newsStands = {
            [1] = 1211559620,
            [2] = -1186769817,
            [3] = -756152956,
            [4] = 720581693,
            [5] = -838860344
        }

        AddTargetModel(
            newsStands,
            {
                options = {
                    {
                        event = "NewsStandCheck",
                        icon = "fas fa-newspaper",
                        label = "Read The News!"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )

        local CHAIRS = {
            [1] = -99500382,
            [2] = -1118419705,
            [3] = 538002882,
            [4] = 525667351,
            [5] = 1805980844,
            [6] = 826023884,
            [7] = 764848282,
            [8] = -741944541,
            [9] = -377849416,
            [10] = -992710074,
            [11] = 867556671,
            [12] = 1716133836,
            [13] = 146905321,
            [14] = 603897027
        }

        AddTargetModel(
            CHAIRS,
            {
                options = {
                    {
                        event = "animation:Chair",
                        icon = "fas fa-chair",
                        label = "Sit Down!"
                    }
                },
                job = {"all"},
                distance = 1.5
            }
        )

        AddBoxZone("jail_options", vector3(1832.03, 2592.68, 46.01), 0.5, 0.5, {
            name="jail_options",
            heading=0,
            --debugPoly=true,
            minZ=46.21,
            maxZ=46.81
        }, {
            options = {
                {
                    event = "check:time",
                    icon = "fas fa-clock",
                    label = "Check Remaining Time"
                },
                {
                    event = "swappingCharsLoop",
                    icon = "fas fa-exchange-alt",
                    label = "Change Characters"
                }
            },
            job = {"all"},
            distance = 2.0
        })

        AddCircleZone(
            "civ-mdt",
            vector3(260.5163269043, -371.86566162109, -44.137672424316),
            0.5,
            {
                name = "civ-mdt",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "civ:mdt:open",
                        icon = "fas fa-laptop",
                        label = "Public Records"
                    }
                },
                job = {"all"},
                distance = 2.0
            }
        )

      
        AddBoxZone(
            "Jewel Robbery",
            vector3(-636.11, -213.18, 53.54),
            2.3,
            0.5,
            {
                name = "Jewel Robbery",
                heading = 119,
                debugPoly = false,
                minZ = 49.99,
                maxZ = 54.39
            },
            {
                options = {
                    {
                        event = "jewel:thermite",
                        icon = "fas fa-bomb",
                        label = "Place Thermite"
                    }
                },
                job = {"all"},
                distance = 3
            }
        )


        AddBoxZone(
            "recycle",
            vector3(995.19, -3100.02, -39.0),
            0.4,
            0.5,
            {
                name = "recycle",
                heading = 0,
             --   debugPoly = true,
                minZ = -39.2,
                maxZ = -38.9
            },
            {
                options = {
                    {
                        event = "recycle:trade",
                        icon = "far fa-clipboard",
                        label = "Trade Your Materials!"
                    }
                },
                job = {"all"},
                distance = 3
            }
        )
        

        AddCircleZone(
            "taxizone",
            vector3(894.96, -179.28, 74.70),
            0.5,
            {
                name = "taxizone",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "ethical-taxi:signonmenu",
                        icon = "fas fa-laptop",
                        label = "Taxi HQ"
                    }
                },
                job = {"all"},
                distance = 2.0
            }
        )

        local GasPumps = {
            [1] = 1694452750,
            [2] = -2007231801,
            [3] = 1933174915,
            [4] = 1339433404,
            [5] = -462817101
        }

        AddTargetModel(
            GasPumps,
            {
                options = {
                    {
                        event = "RefuelCarServerReturn",
                        icon = "fas fa-gas-pump",
                        label = "Fuel Up Your Car!"
                    }
                },
                job = {"all"},
                distance = 5
            }
        )

        AddCircleZone(
            "methstart",
            vector3(2292.2185, 4843.8306, 31.78186),
            0.5,
            {
                name = "methstart",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "meth:start",
                        icon = "fas fa-atom",
                        label = "Turn The Cooker On!"
                    }
                },
                job = {"all"},
                distance = 2.5
            }
        )

        AddCircleZone(
            "lsdstart",
            vector3(1390.5190, 3604.7275, 38.9419),
            1.8,
            {
                name = "lsdstart",
                debugPoly = false,
                useZ = true
            },
            {
                options = {
                    {
                        event = "lsd:start",
                        icon = "fas fa-atom",
                        label = "Start growing weird fungus"
                    }
                },
                job = {"all"},
                distance = 2.5
            }
        )

        AddBoxZone("towstart", vector3(409.07, -1623.01, 29.29), 0.6, 1, {
            name="towstart",
            heading=51,
            --debugPoly=true,
            minZ=28.29,
            maxZ=30.09
        }, 
        {
            options = {
                {
                    event = "start:tow",
                    icon = "far fa-clipboard",
                    label = "Pull out a tow truck!"
                },
                {
                    event = "end:tow",
                    icon = "far fa-clipboard",
                    label = "Put away your tow truck!"
                },
            },
            job = {"tow"},
            distance = 1.5
        }
    )

        AddCircleZone(
            "methsell",
            vector3(203.00288391113, -2016.6826171875, 17.5706615448),
            0.5,
            {
                name = "methsell",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "meth:sell",
                        icon = "far fa-handshake",
                        label = "Sling Some Ice"
                    }
                },
                job = {"all"},
                distance = 2.5
            }
        )
        AddCircleZone(
            "lsdsell",
            vector3(-255.40719604492, -1542.0688476562, 30.936147689819),
            0.5,
            {
                name = "lsdsell",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "lsd:sell",
                        icon = "far fa-handshake",
                        label = "Sling Some Dots"
                    }
                },
                job = {"all"},
                distance = 2.5
            }
        )
        AddCircleZone(
            "methsell4",
            vector3(1235.3387451172, -413.82913208008, 67.928161621094),
            0.5,
            {
                name = "methsell",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "meth:sell",
                        icon = "far fa-handshake",
                        label = "Sling Trash"
                    },
                    {
                        event = "lsd:sell",
                        icon = "far fa-handshake",
                        label = "Sling Dots"
                    }
                },
                job = {"all"},
                distance = 2.5
            }
        )

        AddCircleZone(
            "oxysell1",
            vector3(56.782905578613, 3690.2495117188, 39.921279907227),
            0.5,
            {
                name = "oxysell",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "oxy:sell",
                        icon = "far fa-handshake",
                        label = "Sell Oxy"
                    },
                    {
                        event = "oxy:selljewelery",
                        icon = "far fa-handshake",
                        label = "Sell Stolen Jewells"
                    }
                },
                job = {"all"},
                distance = 2.5
            }
        )
        -- AddCircleZone(
        --     "gangbouncer",
        --     vector3(114.94241333008, 170.44744873047, 112.45175933838),
        --     0.5,
        --     {
        --         name = "gangbouncer",
        --         debugPoly = false,
        --         useZ = false
        --     },
        --     {
        --         options = {
        --             {
        --                 event = "gangteleport",
        --                 icon = "fas fa-question",
        --                 label = "Request Access To Enter"
        --             }
        --         },
        --         job = {"all"},
        --         distance = 2.5
        --     }
        -- )

        -- AddCircleZone(
        --     "gangleave",
        --     vector3(-1570.5743408203, -3017.6284179688, -75.406188964844),
        --     0.5,
        --     {
        --         name = "gangleave",
        --         debugPoly = false,
        --         useZ = false
        --     },
        --     {
        --         options = {
        --             {
        --                 event = "gangleave",
        --                 icon = "fas fa-question",
        --                 label = "Leave The Club"
        --             }
        --         },
        --         job = {"all"},
        --         distance = 2.5
        --     }
        -- )

        -- AddCircleZone(
        --     "talktoboss",
        --     vector3(-1610.5, -3012.0046386719, -75.205032348633),
        --     0.8,
        --     {
        --         name = "talktoboss",
        --         debugPoly = false,
        --         useZ = false
        --     },
        --     {
        --         options = {
        --             {
        --                 event = "boss1",
        --                 icon = "fas fa-question",
        --                 label = "Talk To Boss"
        --             }
        --         },
        --         job = {"all"},
        --         distance = 5.0
        --     }
        -- )
        AddCircleZone(
            "garbagestart",
            vector3(-321.70, -1545.94, 31.02),
            1.0,
            {
                name = "garbagestart",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "ethical-garbage:signon",
                        icon = "fas fa-clipboard",
                        label = "Sign On/Off Duty"
                    }
                },
                job = {"all"},
                distance = 2.5
            }
        )
        AddCircleZone(
            "garbagerecycle",
            vector3(-355.79, -1541.99, 26.72),
            1.0,
            {
                name = "garbagerecycle",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "ethical-garbage:openReyclingShop",
                        icon = "fas fa-prescription",
                        label = "Recycling Center"
                    }
                },
                job = {"all"},
                distance = 2.5
            }
        )
        -- AddCircleZone(
        --     "autoexoticssignon",
        --     vector3(540.507, -196.617, 54.49),
        --     1.0,
        --     {
        --         name = "autoexoticssignon",
        --         debugPoly = true,
        --         useZ = false
        --     },
        --     {
        --         options = {
        --             {
        --                 event = "ethical-garbage:openReyclingShop",
        --                 icon = "fas fa-prescription",
        --                 label = "Recycling Center"
        --             }
        --         },
        --         job = {"all"},
        --         distance = 2.5
        --     }
        -- )
        AddCircleZone(
            "truckerstart",
            vector3(173.12, -26.38, 68.34),
            1.0,
            {
                name = "truckerstart",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "ethical-trucker:signon",
                        icon = "fas fa-clipboard",
                        label = "Sign On/Off Duty"
                    }
                },
                job = {"all"},
                distance = 2.5
            }
        )

        ---1382.4615478516, -615.14398193359, 32.044719696045 Heading: 122.42754364014
        
        --DJ Booths and Jukeboxes
        
        AddCircleZone(
            "bahamabooth",
            vector3(-1382.4615478516, -615.14398193359, 32.044719696045),
            1.0,
            {
                name = "bahamabooth",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "nsrp_jd:openmenubooth:bahama",
                        icon = "fas fa-headphones",
                        label = "Open DJ Menu"
                    }
                },
                job = {"night_club"},
                distance = 2.5
            }
        )

        AddCircleZone(
            "bahamamute",
            vector3(-1392.8041, -593.7432, 30.3195),
            1.0,
            {
                name = "bahamamute",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "nsrp_jd:muteme:bahama",
                        icon = "fas fa-headphones",
                        label = "Toggle Audio"
                    }
                },
                job = {"all"},
                distance = 2.5
            }
        )
        

        --Raid locaitons
        -- -1477.8033447266, -519.74127197266, 34.736682891846 Heading: 214.53973388672
        AddCircleZone(
            "raidapartments",
            vector3(-1477.8033447266, -519.74127197266, 34.736682891846),
            1.0,
            {
                name = "raidapartments",
                debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "hotel:raidstash",
                        icon = "fas fa-bed",
                        label = "Raid Apartment"
                    }
                },
                job = {"police"},
                distance = 2.5
            }
        )


        --Nightclub stuff
        -- -1396.8034667969, -596.34027099609, 30.319570541382
        AddCircleZone(
            "nightclubduty",
            vector3(-1396.8034, -596.3402, 30.31),
            1.0,
            {
                name = "nightclubduty",
                --debugPoly = false,
                useZ = false
            },
            {
                options = {
                    {
                        event = "ethical-nightclub:signonmenu",
                        icon = "fas fa-headphones",
                        label = "Sign On/Off Duty"
                    }
                },
                job = {"all"},
                distance = 2.5
            }
        )

        -- -1392.3704, -606.2625, 30.8106  (Tray 1)
        AddBoxZone("nclub_tray1", vector3(-1392.3704, -606.2625, 30.8106), 1, 1, {
            name="nclub_tray1",
            heading=33,
           -- debugPoly=true,
            minZ=29,
            maxZ=32
        }, {
            options = {
                {
                    event = "ethical-nightclub:pickup1",
                    parms = "night_clubtray",
                    icon = "fas fa-hand-holding",
                    label = "Take your drinks",
                },
            },
                job = {"all"},
                distance = 2.5
        })
        -- -1377.7958, -628.4124, 31.1274 (Tray 2)
        AddBoxZone("nclub_tray2", vector3(-1377.7958, -628.4124, 31.1274), 1, 1, {
            name="nclub_tray2",
            heading=33,
            --debugPoly=true,
            minZ=29,
            maxZ=32
        }, {
            options = {
                {
                    event = "ethical-nightclub:pickup2",
                    parms = "night_clubtray",
                    icon = "fas fa-hand-holding",
                    label = "Take your drinks",
                },
            },
                job = {"all"},
                distance = 2.5
        })
        -- -1380.9604, -632.1744, 30.9838 (Enter Bar 2) -1376.7279052734, -628.99884033203, 30.833566665649 Heading: 306.19345092773 (Teleport)
        AddBoxZone("nightclub_tpin1", vector3(-1380.9604, -632.1744, 30.9838), 0.8, 0.8, {
            name="nightclub_tpin1",
            heading=40,
            --debugPoly=true,
            minZ=29,
            maxZ=33
        }, {
            options = {
                {
                    event = "ethical-nightclub:gobehindbar1",
                    icon = "fas fa-location-arrow",
                    label = "Get Behind Bar"
                },
             },
             job = {"night_club"},
            distance = 1
        })
        -- -1380.2384, -631.4810, 31.1796 (Exit Bar 2)  -1382.2214355469, -632.85028076172, 30.819581985474 Heading: 15.586097717285 (Teleport)
        AddBoxZone("nightclub_tpout1", vector3(-1380.2384, -631.4810, 31.1796), 0.8, 0.8, {
            name="nightclub_tpout1",
            heading=40,
            --debugPoly=true,
            minZ=29,
            maxZ=33
        }, {
            options = {
                {
                    event = "ethical-nightclub:exitbar1",
                    icon = "fas fa-location-arrow",
                    label = "Exit Behind Bar"
                },
             },
             job = {"night_club"},
            distance = 1
        })
        -- -1388.4959, -610.3179, 30.62599 (Enter Bar 1) -1386.0496826172, -607.27935791016, 30.309268951416 Heading: 109.33673095703 (Teleport)
        AddBoxZone("nightclub_tpin2", vector3(-1388.4959, -610.3179, 30.62599), 1, 1, {
            name="nightclub_tpin2",
            heading=40,
          --debugPoly=true,
            minZ=29,
            maxZ=33
        }, {
            options = {
                {
                    event = "ethical-nightclub:gobehindbar2",
                    icon = "fas fa-location-arrow",
                    label = "Get Behind Bar"
                },
             },
             job = {"night_club"},
            distance = 2.5
        })
        -- -1385.1480, -606.4208, 30.3195 (Exit Bar 2)  -1389.5946044922, -610.86126708984, 30.319551467896 Heading: 113.97560882568 (Teleport)
        AddBoxZone("nightclub_tpout2", vector3(-1385.1480, -606.4208, 30.3195), 1, 1, {
            name="nightclub_tpout2",
            heading=40,
            --debugPoly=true,
            minZ=29,
            maxZ=33
        }, {
            options = {
                {
                    event = "ethical-nightclub:exitbar2",
                    icon = "fas fa-location-arrow",
                    label = "Exit Behind Bar"
                },
             },
             job = {"night_club"},
            distance = 2.5
        })
        -- -1390.5347, -605.2766, 30.8885 (Bill Bar 1)
        AddBoxZone("nightclub_chargecustomer1",vector3(-1390.5347, -605.2766, 30.8885), 0.5, 0.5, {
            name="nightclub_chargecustomer1",
            heading=270,
            --debugPoly=true,
            minZ=29.68,
            maxZ=32.88
        }, {
            options = {
                {
                    event = "ethical-nightclub:bill",
                    icon = "fas fa-cash-register",
                    label = "Charge Customer",
                },
             },
             job = {"night_club"},
            distance = 1.5
        })
        -- -1391.4499, -601.8580, 30.8316 (Craft Bar 1)
        AddBoxZone("nightclub_craft1", vector3(-1391.4499, -601.8580, 30.8316), 0.5, 0.5, {
            name="nightclub_craft1",
            heading=40,
           -- debugPoly=true,
            minZ=29,
            maxZ=32
        }, {
            options = {
                {
                    event = "nightclub:craft",
                    icon = "fas fa-glass",
                    label = "Mix Drinks"
                },
             },
             job = {"night_club"},
            distance = 2.5
        })
        -- -1386.4078, -608.9677, 30.8147 (Fridge Bar 1)
        AddBoxZone("nightclubfridge1", vector3(-1386.4078, -608.9677, 30.8147), 1, 1, {
            name="nightclubfridge1",
            heading=160,
           -- debugPoly=true,
            minZ=29,
            maxZ=32
        }, {
            options = {
                {
                    event = "nightclub:order",
                    icon = "fas fa-box-open",
                    label = "Open Fridge"
                },{
                    event = "open:storage",
                    parms = "night_club",
                    icon = "fas fa-box-open",
                    label = "Open Storage"
                }
             },
             job = {"night_club"},
            distance = 1.5
        })
        -- -1379.3524, -629.8880, 31.3252 (Bill Bar 2)
        AddBoxZone("nightclub_chargecustomer2",vector3(-1379.3524, -629.8880, 31.3252), 0.5, 0.5, {
            name="nightclub_chargecustomer2",
            heading=270,
           -- debugPoly=true,
            minZ=30.68,
            maxZ=32.88
        }, {
            options = {
                {
                    event = "ethical-nightclub:bill",
                    icon = "fas fa-cash-register",
                    label = "Charge Customer",
                },
             },
             job = {"night_club"},
            distance = 1.5
        })
        -- -1373.2802, -627.5522, 30.5645(Fridge Bar 2)
        AddBoxZone("nightclubfridge2", vector3(-1373.2802, -627.5522, 30.5645), 1, 1, {
            name="nightclubfridge2",
            heading=160,
           -- debugPoly=true,
            minZ=29,
            maxZ=32
        }, {
            options = {
                {
                    event = "nightclub:order",
                    icon = "fas fa-box-open",
                    label = "Open Fridge"
                }, {
                    event = "open:storage",
                    parms = "night_club",
                    icon = "fas fa-box-open",
                    label = "Open Storage"
                }
             },
             job = {"night_club"},
            distance = 1.5
        })
        -- -1376.4300, -630.0565, 31.1844 (Craft Bar 2)
        AddBoxZone("nightclub_craft2", vector3(-1376.4300, -630.0565, 31.1844), 0.5, 0.5, {
            name="nightclub_craft2",
            heading=40,
          --  debugPoly=true,
            minZ=29,
            maxZ=33
        }, {
            options = {
                {
                    event = "nightclub:craft",
                    icon = "fas fa-glass",
                    label = "Mix Drinks"
                },
             },
             job = {"night_club"},
            distance = 2.5
        })

        -- -1386.5001220703, -589.79034423828, 30.639583587646 (Cash out)
        AddBoxZone("nightclubticketcounter", vector3(-1386.5001220703, -589.79034423828, 30.639583587646 ), 1.6, 1, {
            name="nightclubticketcounter",
            heading=35,
           -- debugPoly=true,
            minZ=29,
            maxZ=32,
        }, {
            options = {
                {
                    event = "ethical-nightclub:cash:in",
                    icon = "fas fa-cash-register",
                    label = "Cash in receipts",
                }
             },
             job = {"night_club"},
            distance = 1.5
        })
    end
)

--------------------------------------------------------------------------------------------------
--																				          	    --
--       ______ _______ _    _ _____ _____          _        _____  ________       _______      --
--      |  ____|__   __| |  | |_   _/ ____|   /\   | |      |  __ \|  ____\ \    / / ____|      --
--      | |__     | |  | |__| | | || |       /  \  | |      | |  | | |__   \ \  / / (___        --
--      |  __|    | |  |  __  | | || |      / /\ \ | |      | |  | |  __|   \ \/ / \___ \       --
--      | |____   | |  | |  | |_| || |____ / ____ \| |____  | |__| | |____   \  /  ____) |      --
--      |______|  |_|  |_|  |_|_____\_____/_/    \_\______| |_____/|______|   \/  |_____/       --
-- 																					            --
--                           joshua#5319 glacielgaming#6969 Woodz#1668                          --
--------------------------------------------------------------------------------------------------
