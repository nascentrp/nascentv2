RegisterServerEvent('ethical-cid:createID')
AddEventHandler('ethical-cid:createID', function(first, last, job, sex, dob)
    information = {
        ["identifier"] = math.random(1,999),
        ["Name"] = tostring(first),
        ["Surname"] = tostring(last),
        ["Sex"] = tostring(sex),
        ["DOB"] = tostring(dob),
    }
    TriggerClientEvent("player:receiveItem", source,"idcard",1,true,information)
end)

RegisterServerEvent('ethical-cid:createJobCard')
AddEventHandler('ethical-cid:createJobCard', function(first,cid, job, callsign)
    local info = {
        ["identifier"] = tostring(cid),
        ["NameSurname"] = tostring(first),
        ["Job"] = tostring(job),
        ["Position"] = tostring(callsign),
    }
    TriggerClientEvent("player:receiveItem", source,"jobcard",1,true,info)
    --id, amount, generateInformation, itemdata, returnData = '{}', devItem = false
    --TriggerClientEvent("player:receiveItem", source,"jobcard",1,true)
end)