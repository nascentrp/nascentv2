local blips = {
    {id = "pcenter", name = "Payments & Internet Center", scale = 0.7, sprite = 351, color = 17, x=-1081.8293457031, y=-248.12872314453, z=37.763294219971},
    {id = "Bennys", name = "Bennys Motor Works", scale = 0.7, color = 1, sprite = 72, x=-33.228, y=-1052.799, z=28.076}, 
    {id = "Auto Exotics", name = "Auto Exotics", scale = 0.7, color = 1, sprite = 446, x=-212.28814697266, y=-1324.95703125, z=30.890},
    {id = "Auto Bodies", name = "Hayes Auto", scale = 0.7, color = 1, sprite = 446, x=-1417.1550292969, y=-446.43911743164, z=35.909713745117},
    {id = "Tuner Shop", name = "Tuner Shop", scale = 0.7, color = 1, sprite = 446, x=937.23828125, y=-970.89343261719, z=39.543106079102},
    {id = "Paleto Performance", name = "Paleto Performance", scale = 0.7, color = 1, sprite = 446, x=108.35273742676, y=6625.1982421875, z= 31.787202835083},
    {id = "Burger Shot", name = "Burger Shot", scale = 0.7, color = 21, sprite = 106, x=-1191.6701660156, y=-889.74584960938, z= 14.508341789246},
    {id = "truckjob1", name = "Delivery Garage", scale = 0.7, color = 17, sprite = 67, x =165.22, y=-28.38,  z=67.94},
    {id = "weazel", name = "Weazel News", scale = 0.7, color = 6, sprite = 47, x = -598.248046875, y= -929.77307128906, z= 23.869129180908},
    {id = "taxi", name = "Taxi HQ", scale = 0.7, color = 46, sprite = 198, x = 895.32, y= -179.28, z= 23.86},
    {id = "beanmachine", name = "Bean Machine", scale = 0.7, color = 44, sprite = 536, x = -629.541015625, y= 233.69226074219, z= 81.881462097168},
    {id = "vangelico", name = "Jewellery Store", scale = 0.5, color = 0, sprite = 617, x = -631.6218, y= -237.4207, z= 38.069},
    {id = "licenseshop", name = "License Shop", scale = 0.7, color = 0, sprite = 77, x = -550.56, y=-192.52, z= 38.219},
    {id = "courthouse", name = "Los Santos Courthouse", scale = 0.7, color = 5, sprite = 419, x=-543.438, y=-207.164, z=198.36},
    {id = "prison_cafe", name = "Prison Cafeteria", scale = 0.7, color = 0, sprite = 273, x=1782.12, y=2552.01, z=45.6726},
    {id = "prison_cells", name = "Prison Cells", scale = 0.7, color = 0, sprite = 285, x=1752.450, y=2493.318, z=50.423},
    {id = "bestbuds", name = "BestBuds", scale = 0.7, color = 61, sprite = 140, x = 377.802, y= -829.0853, z=29.3025}, 
    {id = "nightclub", name = "Bahama Mama's", scale = 0.7, color = 50, sprite = 93, x = -1386.5001, y= -589.7903, z=30.6395},
}
AddEventHandler("ethical-base:playerSessionStarted", function()
    Citizen.CreateThread(function()
        for k,v in ipairs(blips) do
            ETHICAL.BlipManager:CreateBlip(v.id, v)
        end
    end)
end)