let ButtonsData = [];
let Buttons = [];
let Button = [];

const OpenMenu = (data) => {
    DrawButtons(data)
}

const OpenTextInput = (header) => {
    var el = $(`
    <div class="container">
        <div class="input">
            <label for="input-value" class="input-header">Please enter the value below</label>
            <input type="text" name="input-value" id="input-value">
            <button class="done button" onclick="inputSubmit()">Done</button>
        </div>
    </div>
    `);
    $('body').append(el);
}

const inputSubmit = () => {
    var value = jQuery('#input-value').val();
    $('.container').empty().remove();
    TextBox(value);
}

const CloseMenu = () => {
    for (let i = 0; i < ButtonsData.length; i++) {
        let id = ButtonsData[i].id
        $(".button").remove();
    }
    ButtonsData = [];
    Buttons = [];
    Button = [];
};

const DrawButtons = (data) => {
    ButtonsData = data
    for (let i = 0; i < ButtonsData.length; i++) {
        let header = ButtonsData[i].header
        let message = ButtonsData[i].txt
        let id = ButtonsData[i].id
        let element

        element = $(`
            <div class="button" id=`+id+`>
              <div class="header" id=`+id+`>`+header+`</div>
              <div class="txt" id=`+id+`>`+message+`</div>
            </div>`
        );
        $('#buttons').append(element);
        Buttons[id] = element
        if (ButtonsData[i].params) {
            Button[id] = ButtonsData[i].params
        }
    }
};

$(document).click(function(event){
    let $target = $(event.target);
    if ($target.closest('.button').length && $('.button').is(":visible")) {
        let id = event.target.id;
        if (!Button[id]) return
        PostData(id)
    }
})

const PostData = (id) => {
    $.post(`https://ethical-context/dataPost`, JSON.stringify(Button[id]))
    return CloseMenu();
}

const CancelMenu = () => {
    $.post(`https://ethical-context/cancel`)
    return CloseMenu();
}

const TextBox = (input) => {
    $.post(`https://ethical-context/receivedInput`, input)
}

window.addEventListener("message", (evt) => {
    // const evt = { data: { data: [{id: 1, header: 'test'}], action: 'INPUT_TEXT'} };
    const data = evt.data
    const info = data.data
    const action = data.action
    switch (action) {
        case "OPEN_MENU":
            return OpenMenu(info);
        case "CLOSE_MENU":
            return CloseMenu();
        case "INPUT_TEXT":
            return OpenTextInput(data);
        default:
            return;
    }
});


document.onkeyup = function (event) {
    event = event || window.event;
    var charCode = event.keyCode || event.which;
    if (charCode == 27) {
        CancelMenu();
    }
};