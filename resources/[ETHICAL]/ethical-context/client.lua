RegisterNUICallback("dataPost", function(data, cb)
    SetNuiFocus(false)
    TriggerEvent(data.event, data.args)
    cb('ok')
    PlaySoundFrontend(-1, 'Highlight_Cancel','DLC_HEIST_PLANNING_BOARD_SOUNDS', 1)
end)

RegisterNUICallback("cancel", function()
    closeActiveMenu()
    TriggerEvent("ethical-context:closeActiveMenu")
end)

RegisterNetEvent("ethical-context:closeActiveMenu")
AddEventHandler("ethical-context:closeActiveMenu", function()
    closeActiveMenu()    
end)

function closeActiveMenu()
    SetNuiFocus(false)
    PlaySoundFrontend(-1, 'Highlight_Cancel','DLC_HEIST_PLANNING_BOARD_SOUNDS', 1)
end

RegisterNetEvent('ethical-context:sendMenu', function(data)
    if not data then return end
    SetNuiFocus(true, true)
    PlaySoundFrontend(-1, 'Highlight_Cancel','DLC_HEIST_PLANNING_BOARD_SOUNDS', 1)
    SendNUIMessage({
        action = "OPEN_MENU",
        data = data
    })
end)

local isOpen = false
local setCallback = '';
RegisterNetEvent('ethical-context:inputText', function(instruction_text, cb)
    if isOpen then
        print("trying to open input with input already open")
        return
    end
    SetNuiFocus(true, true)
    setCallback = cb
    SendNUIMessage({
        action = "INPUT_TEXT",
        text = instruction_text
    })
    isOpen = true
end)

RegisterNUICallback("receivedInput", function(data, cb)
    SetNuiFocus(false)
    cb('ok')
    TriggerEvent(setCallback, data)
    PlaySoundFrontend(-1, 'Highlight_Cancel','DLC_HEIST_PLANNING_BOARD_SOUNDS', 1)
end)