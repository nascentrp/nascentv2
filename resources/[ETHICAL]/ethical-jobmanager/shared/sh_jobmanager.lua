ETHICAL = {}
ETHICAL.Jobs = {}
ETHICAL.Jobs.ValidJobs = {
    ["unemployed"] = {
        name = "Unemployed",
        paycheck = 10,
        ranks = {
            [1] = "Vagrant"
        }
    },
    ["prisoner"] = {
        name = "Prisoner",
        paycheck = 5,
        ranks = {
            [1] = "Criminal Scum"
        }
    },
    ["busdriver"] = {
        name = "Bus Driver",
        paycheck = 10,
        ranks = {
            [1] = "Driver"
        }
    },
    ["garbage"] = {
        name = "Garbage",
        paycheck = 10,
        ranks = {
            [1] = "Trash Collector",
            [2] = "Owner"
        }
    },
    ["tuner_shop"] = {
        name = "Tuner Shop",
        whitelisted = true,
        paycheck = 10,
        ranks = {
            [1] = "Mechanic",
            [2] = "Owner"
        }
    },
    ["auto_exotics"] = {
        name = "Auto Exotics",
        paycheck = 10,
        whitelisted = true,
        ranks = {
            [1] = "Mechanic",
            [2] = "Owner"
        }
    },
    ["paleto_mech"] = {
        name = "Paleto Mechanic",
        whitelisted = true,
        paycheck = 10,
        ranks = {
            [1] = "Mechanic",
            [2] = "Owner"
        }
    },
    ["auto_bodies"] = {
        name = "Auto Bodies",
        whitelisted = true,
        paycheck = 10,
        ranks = {
            [1] = "Mechanic",
            [2] = "Owner"
        }
    },
    ["bean_machine"] = {
        name = "Bean Machine",
        whitelisted = true,
        paycheck = 10,
        ranks = {
            [1] = "Customer Service Representative",
            [2] = "Manager",
            [3] = "Owner"
        }
    },
    ["best_buds"] = {
        name = "Best Buds",
        paycheck = 10,
        whitelisted = true,
        ranks = {
            [1] = "Customer Service Representative",
            [2] = "Manager",
            [3] = "Owner"
        }
    },
    ["ems"] = {
        name = "EMS",
        paycheck = 200,
        whitelisted = true,
        ranks = {
            [1] = "BLS",
            [2] = "ILS",
            [3] = "ALS",
            [4] = "CHIEF"
        }
    },
    ["doc"] = {
        name = "Doctor",
        paycheck = 150,
        whitelisted = true,
        ranks = {
            [1] = "GP",
            [2] = "SPECIALIST",
            [3] = "SURGEON",
            [4] = "HEAD SURGEON"
        }
    },
    ["police"] = {
        name = "Police officer",
        paycheck = 200,
        whitelisted = true,
        ranks = {
            [1] = "BCSO Probationary Cadet",
            [2] = "LSPD Probationary Officer",
            [3] = "BCSO Deputy",
            [4] = "LSPD Officer",
            [5] = "SAHP Trooper",
            [6] = "BCSO Sergeant",
            [7] = "LSPD Sergeant",
            [8] = "SAHP Senior Trooper",
            [9] = "BCSO Lieutenant",
            [10] = "LSPD Lieutenant",
            [11] = "SAHP Pursuit Trooper",
            [12] = "BCSO Deputy Sheriff",
            [13] = "LSPD Captain",
            [14] = "BCSO Sheriff",
            [15] = "LSPD Chief",
            [16] = "Police Comissioner"
        }
    },
    ["towtruck"] = {
        name = "Tow Trucker",
        paycheck = 20,
        ranks = {
            [1] = "Tow Service Technician"
        }
    },
    ["foodtruck"] = {
        name = "Food Truck",
        paycheck = 50,
        ranks = {
            [1] = "Customer Service Representative"
        }
    },
    ["taxi"] = {
        name = "Taxi driver",
        paycheck = 20,
        ranks = {
            [1] = "Driver"
        }
    },
    ["trucker"] = {
        name = "Delivery Job",
        paycheck = 10,
        ranks = {
            [1] = "Delivery Driver"
        }
    },
    ["entertainer"] = {
        name = "Entertainer",
        paycheck = 10,
        ranks = {
            [1] = "Customer Service Representative"
        }
    },
    ["news"] = {
        name = "News Reporter",
        paycheck = 60,
        whitelisted = true,
        ranks = {
            [1] = "Camera Operator",
            [2] = "Reporter",
            [3] = "News Anchor",
            [4] = "Editor in Chief",
        }
    },
    ["defender"] = {
        name = "Public Defender",
        paycheck = 100,
        whitelisted = true,
        ranks = {
            [1] = "Lawyer"
        }
    },
    ["district attorney"] = {
        name = "District Attorney",
        whitelisted = true,
        paycheck = 70,
        ranks = {
            [1] = "Attorney at Law"
        }
    },
    ["judge"] = {
        name = "Judge",
        whitelisted = true,
        paycheck = 50,
        ranks = {
            [1] = "Judge"
        }
    },
    ["broadcaster"] = {
        name = "Broadcaster",
        paycheck = 50,
        whitelisted = true,
        ranks = {
            [1] = "Host"
        }
    },
    ["doctor"] = {
        name = "Doctor",
        whitelisted = true,
        paycheck = 25,
        ranks = {
            [1] = "General Practitioner"
        }
    },
    ["therapist"] = {
        name = "Therapist",
        whitelisted = true,
        paycheck = 25,
        ranks = {
            [1] = "Consultant"
        }
    },
    ["driving instructor"] = {
        name = "Driving Instructor",
        whitelisted = true,
        paycheck = 10,
        ranks = {
            [1] = "Instructor"
        }
    },
    ["foodtruckvendor"] = {
        name = "Food Truck Vendor",
        paycheck = 10,
        ranks = {
            [1] = "Customer Service Representative"
        }
    },
    ["pdm"] = {
        name = "Car Dealer",
        whitelisted = true,
        paycheck = 50,
        ranks = {
            [1] = "Customer Service Representative"
        }
    },
    ["OffBurgerShot"] = {
        name = "Burger Shot Clock Off",
        paycheck = 50,
        ranks = {
            [1] = "Customer Service Representative",
            [2] = "Manager",
            [3] = "Owner"
        }
    },
    ["BurgerShot"] = {
        name = "Burger Shot Clock On",
        whitelisted = true,
        paycheck = 50,
        ranks = {
            [1] = "Customer Service Representative",
            [2] = "Manager",
            [3] = "Owner"
        }
    },
    ["burger_shot"] = {
        name = "Burger Shot Clock On",
        paycheck = 50,
        ranks = {
            [1] = "Customer Service Representative",
            [2] = "Manager",
            [3] = "Owner"
        }
    },
    ["DOJ"] = {
        name = "DOJ",
        whitelisted = true,
        paycheck = 50,
        ranks = {
            [1] = "Lawyer",
            [2] = "Public Defender",
            [3] = "Prosecutor",
            [4] = "Judge"
        }
    },
    ["strip_club"] = {
        name = "Strip Club",
        whitelisted = true,
        paycheck = 50,
        ranks = {
            [1] = "Waiter",
            [2] = "Dancer",
            [3] = "DJ",
            [4] = "Owner"
        }
    },
    ["night_club"] = {
        name = "Night Club",
        whitelisted = true,
        paycheck = 50,
        ranks = {
            [1] = "Bartender",
            [2] = "Bouncer",
            [3] = "DJ",
            [4] = "Owner"
        }
    },
    
    ["realestate"] = {
        name = "Estate Agent",
        paycheck = 50,
        whitelisted = true,
        ranks = {
            [1] = "Agent"
        }
    },
}