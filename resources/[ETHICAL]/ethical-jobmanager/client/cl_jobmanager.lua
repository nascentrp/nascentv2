
RegisterNetEvent("ethical-jobmanager:playerBecameJob")
AddEventHandler("ethical-jobmanager:playerBecameJob", function(job, name, notify)
    local LocalPlayer = exports["ethical-base"]:getModule("LocalPlayer")
    LocalPlayer:setVar("job", job)
    if notify ~= false then 

    end
    if name == "Entertainer" then
	    TriggerEvent('DoLongHudText',"College DJ and Comedy Club pay per person around you",1)
	end
    if name == "Broadcaster" then
        TriggerEvent('DoLongHudText',"(RadioButton + LeftCtrl for radio toggle)",3)
        TriggerEvent('DoLongHudText',"Broadcast from this room and give out the vibes to los santos on 1982.9",1)
    end  
	if job == "unemployed"  then
		SetPedRelationshipGroupDefaultHash(PlayerPedId(),`PLAYER`)
        SetPoliceIgnorePlayer(PlayerPedId(),false)
        TriggerEvent("ResetRadioChannel");
        TriggerEvent('DoLongHudText',"You're now unemployed!",1)
	end
    
    if job == "trucker" then

    end

    if job == "towtruck" then
        TriggerEvent("DoLongHudText","Use /tow to tow cars to your truck.",1)

    end

    if job == "news"  then
        TriggerEvent('DoLongHudText',"Get out there and report the news!",1)
    end

    if job == "driving instructor"  then
        TriggerEvent('DoLongHudText',"'/driving' to use access driving instructor systems",1)
    end
    
    if job == "pdm"  then
        TriggerEvent('DoLongHudText',"Go Sell Some Cars",1)
    end

    if job == "OffBurgerShot"  then
        TriggerEvent('DoLongHudText',"Success!",1)
    end

    if job == "BurgerShot"  then
    end
   -- TriggerServerEvent("ethical-items:updateID",job,exports["isPed"]:retreiveBusinesses())
end)

RegisterNetEvent("ethical-base:characterLoaded")
AddEventHandler("ethical-base:characterLoaded", function(character)
    local LocalPlayer = exports["ethical-base"]:getModule("LocalPlayer")
    LocalPlayer:setVar("job", "unemployed")
end)

RegisterNetEvent("ethical-base:exportsReady")
AddEventHandler("ethical-base:exportsReady", function()
    exports["ethical-base"]:addModule("JobManager", ETHICAL.Jobs)
end)


RegisterNetEvent("ethical-jobs:getjobcardid")
AddEventHandler("ethical-jobs:getjobcardid", function()
    TriggerServerEvent("ethical-jobs:payForId")
end)


RegisterNetEvent("ethical-jobs:getjobcard")
AddEventHandler("ethical-jobs:getjobcard", function()
    TriggerEvent("player:receiveItem",'jobcard', 1,true)
    Citizen.Wait(1000)
end)

function GetJobRankName()
    local jobs = exports["ethical-base"]:getModule("JobManager").ValidJobs
    local rank = exports["isPed"]:isPed("jobrank")
    local job = exports["isPed"]:isPed("myJob")
    local rankname = "Unknown"

    if rank == nil or rank == 0 or rank < 1 then
        rank = 1
    end
    for k,v in pairs(jobs) do
    if k == job then
        for z,x in pairs(v) do
            if z == "ranks" then
                for y,w in pairs(x) do
                    if y == rank then
                        rankname = w
                    end
                end
            end
        end
    end
    
    end
    return rankname
end
exports("GetJobRankName",GetJobRankName)

RegisterCommand("testjobnames", function(source)
  TriggerEvent("ethical-jobs:getrankname")
end)