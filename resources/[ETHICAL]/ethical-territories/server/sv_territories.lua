RegisterServerEvent('setupgangcash')
AddEventHandler('setupgangcash', function(amount)
local src = source
local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
if (tonumber(user:getBalance()) >= tonumber(amount)) then
    user:removeMoney(tonumber(amount))
else
    TriggerClientEvent('DoLongHudText', src, "Quit wasting my time and come back when you have the shit.", 1)
end
end)

RegisterServerEvent('setupgangdb')
AddEventHandler("setupgangdb", function(groupname)
    local src = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local char = user:getCurrentCharacter()
    local gang = groupname
	TriggerClientEvent('DoLongHudText', src, "The Group ".. groupname .." has been started!", 1)
        exports.ghmattimysql:execute('INSERT INTO gangs (cid, gang_name, reputation, leader, ingang, grove, covenant, brouge, forum, jamestown, mirrorpark, fudge, vespucci, cougar, harmony,is_mafia) VALUES (@cid, @gang_name, @reputation, @leader, @ingang, @grove, @covenant, @brouge, @forum, @jamestown, @mirrorpark, @fudge, @vespucci, @cougar, @harmony, @is_mafia)', {
        ['@cid'] = char.id,
        ['@gang_name'] = gang,
        ['@reputation'] = "0",
        ['@leader'] = "1",
        ['@ingang'] = "1",
        ['@grove'] = "0",
        ['@covenant'] = "0",
        ['@brouge'] = "0",
        ['@forum'] = "0",
        ['@jamestown'] = "0",
        ['@mirrorpark'] = "0",
        ['@fudge'] = "0",
        ['@vespucci'] = "0",
        ['@cougar'] = "0",
        ['@harmony'] = "0",
        ['@is_mafia'] = "0",
        },function(data)
            exports.ghmattimysql:execute('INSERT INTO gang_members (cid, gang_id, rank, ingang) VALUES (@cid, (select gid from gangs where gang_name = @gang_name limit 1), 1, 1)', {
                ['@cid'] = char.id,
                ['@gang_name'] = gang,
            },
            function(data)
                TriggerClientEvent("gang:approved", src, true, data.insertId, 0)
            end)
    end)
end)

RegisterServerEvent("ethical-territories:acceptinvite")
AddEventHandler("ethical-territories:acceptinvite", function(gmid, gang_id)
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('update gang_members set ingang = 1 where gang_member_id = ? and 1 = (select case when ingang =0 then 1 else 0 end as new_is_in_gang from gang_members where gang_member_id = ? and cid = ?)', {gmid, gmid, character.id}, function()
        TriggerClientEvent("gang:approved", src, true, gang_id)
        TriggerClientEvent('DoLongHudText', src, "You have successfully joined a new gang!", 1)
    end)
end)

RegisterServerEvent("ethical-territories:getgang")
AddEventHandler("ethical-territories:getgang", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('select gang_id, rank, (select is_mafia from gangs where gangs.gid = gang_members.gang_id) as is_mafia from gang_members where gang_members.cid = ? and ingang = 1 limit 1', {character.id}, function(data)
        if data == nil or data[1] == 0 or data[1] == nil then
          print('User is not in a gang')
        else
            TriggerClientEvent("gang:approved", src, true, data[1].gang_id, data[1].rank, data[1].is_mafia)
        end
    end)
end)

RegisterServerEvent("ethical-territories:setRank")
AddEventHandler("ethical-territories:setRank", function(gmid, rank)
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('update gang_members set rank = @rank where gang_member_id = @gmid and @value = (select gangs.cid as leader from gang_members inner join gangs on gang_members.gang_id = gangs.gid where gang_members.gang_member_id = @gmid limit 1)', 
    {
        ['@rank'] = rank,
        ['@gmid'] = gmid,
        ['@value'] = character.id
    }, function(data)
        if data.affectedRows > 0 then
            TriggerClientEvent('DoLongHudText', src, "Character Rank Set", 1)
            print("USER " .. character.id .. " DEMOTED GANG ENTRY " .. gmid .. " TO RANK " .. rank)
        else 
            TriggerClientEvent('DoLongHudText', src, "You need to be the owner to do this.", 1)
        end
    end)
end)

RegisterServerEvent("ethical-territories:transferownership")
AddEventHandler("ethical-territories:transferownership", function(data)
    local gang_id = data.gang_id
    local gmid = data.gang_member_id
    local cid = data.cid
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('UPDATE gangs set cid = @cid where gid = @gang_id and 1 = (SELECT 1 FROM gang_members where cid = @cid and gang_member_id = @gmid and gang_id = @gang_id and rank = 1 LIMIT 1) and 1 = (SELECT 1 FROM gangs where cid = @senderId and gid = @gang_id LIMIT 1)', {
        ['@senderId'] = character.id,
        ['@cid']= cid,
        ['@gmid'] = gmid,
        ['@gang_id'] = gang_id
    }, function(data)
        if data.affectedRows > 0 then
            TriggerClientEvent('DoLongHudText', src, "You are no longer the gang leader", 1)
            print("USER " .. character.id .. " GAVE GANG TO " .. cid)
        else 
            TriggerClientEvent('DoLongHudText', src, "This person needs to be a leader and you need to be the owner of the gang.", 1)
        end
    end)
end)


RegisterServerEvent("ethical-territories:viewganginvites")
AddEventHandler("ethical-territories:viewganginvites", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT gang_member_id, gang_name, gang_id FROM gang_members INNER JOIN gangs ON gangs.gid = gang_members.gang_id WHERE gang_members.cid = ? AND gang_members.ingang = 0', {character.id}, function(data)
        if data ~= nil and #data > 0 then
            TriggerClientEvent("nsrp_gangs:UpdateAndShowGangInvitations", src, data)
        else
            TriggerClientEvent("nsrp_gangs:UpdateAndShowGangInvitations", src, {})            
        end
    end)
end)

RegisterServerEvent("ethical-territories:getmembers")
AddEventHandler("ethical-territories:getmembers", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    fetchGangMembers(src, function(data)
        --print("get members")
        --print(json.encode(data))
        TriggerClientEvent("nsrp_gangs:receiveMemberList", src, data)
    end)
end)


exports('fetchGangMembers', function(cb)
    fetchGangMembers(src, cb)
end)

function fetchGangMembers(src, cb)
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    --print(json.encode(character))
    exports.ghmattimysql:execute('SELECT gang_members.cid as cid, gang_members.gang_member_id, gang_members.gang_id, gang_members.rank, gang_members.ingang, characters.first_name, characters.last_name FROM gang_members inner join characters on characters.id = gang_members.cid where gang_members.gang_id = (SELECT gang_id FROM gang_members WHERE cid = ? and ingang = 1 and RANK = 1 LIMIT 1)', 
    {character.id},
    function(data)
        --print(json.encode(data))
        cb(data)
    end)
end

RegisterServerEvent("ethical-territories:leader")
AddEventHandler("ethical-territories:leader", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT * FROM gangs WHERE `cid`= ? AND `leader` = ?', {character.id, "1"}, function(data)
		if data[1] then
            TriggerClientEvent("gang:leader", src, true)
        end
    end)
end)

RegisterServerEvent("ethical-territories:grove")
AddEventHandler("ethical-territories:grove", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT 1 FROM gang_members inner join gangs on gangs.gid = gang_members.gang_id WHERE gang_members.cid = ? AND grove = ?', {character.id, "1"}, function(data)
		if data[1] then
            TriggerClientEvent("gang:targettinggrove", src, true)
        end
    end)
end)

RegisterServerEvent("ethical-territories:covenant")
AddEventHandler("ethical-territories:covenant", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT 1 FROM gang_members inner join gangs on gangs.gid = gang_members.gang_id WHERE gang_members.cid = ? AND `covenant` = ?', {character.id, "1"}, function(data)
		if data[1] then
            TriggerClientEvent("gang:targettingcov", src, true)
        end
    end)
end)

RegisterServerEvent("ethical-territories:brouge")
AddEventHandler("ethical-territories:brouge", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT 1 FROM gang_members inner join gangs on gangs.gid = gang_members.gang_id WHERE gang_members.cid = ? AND `brouge` = ?', {character.id, "1"}, function(data)
		if data[1] then
            TriggerClientEvent("gang:targettingbrouge", src, true)
        end
    end)
end)

RegisterServerEvent("ethical-territories:forum")
AddEventHandler("ethical-territories:forum", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT 1 FROM gang_members inner join gangs on gangs.gid = gang_members.gang_id WHERE gang_members.cid = ? AND `forum` = ?', {character.id, "1"}, function(data)
		if data[1] then
            TriggerClientEvent("gang:targettingforum", src, true)
        end
    end)
end)

RegisterServerEvent("ethical-territories:jamestown")
AddEventHandler("ethical-territories:jamestown", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT 1 FROM gang_members inner join gangs on gangs.gid = gang_members.gang_id WHERE gang_members.cid = ? AND `jamestown` = ?', {character.id, "1"}, function(data)
		if data[1] then
            TriggerClientEvent("gang:targettingjamestown", src, true)
        end
    end)
end)

RegisterServerEvent("ethical-territories:mirrorpark")
AddEventHandler("ethical-territories:mirrorpark", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT 1 FROM gang_members inner join gangs on gangs.gid = gang_members.gang_id WHERE gang_members.cid = ? AND `mirrorpark` = ?', {character.id, "1"}, function(data)
		if data[1] then
            TriggerClientEvent("gang:targettingmirror", src, true)
        end
    end)
end)

RegisterServerEvent("ethical-territories:fudge")
AddEventHandler("ethical-territories:fudge", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT 1 FROM gang_members inner join gangs on gangs.gid = gang_members.gang_id WHERE gang_members.cid = ? AND `fudge` = ?', {character.id, "1"}, function(data)
		if data[1] then
            TriggerClientEvent("gang:targettingfudge", src, true)
        end
    end)
end)

RegisterServerEvent("ethical-territories:vespucci")
AddEventHandler("ethical-territories:vespucci", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT 1 FROM gang_members inner join gangs on gangs.gid = gang_members.gang_id WHERE gang_members.cid = ? AND `vespucci` = ?', {character.id, "1"}, function(data)
		if data[1] then
            TriggerClientEvent("gang:targettingvespucci", src, true)
        end
    end)
end)

RegisterServerEvent("ethical-territories:cougar")
AddEventHandler("ethical-territories:cougar", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT 1 FROM gang_members inner join gangs on gangs.gid = gang_members.gang_id WHERE gang_members.cid = ? AND `cougar` = ?', {character.id, "1"}, function(data)
		if data[1] then
            TriggerClientEvent("gang:targettingcougar", src, true)
        end
    end)
end)

RegisterServerEvent("ethical-territories:harmony")
AddEventHandler("ethical-territories:harmony", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local character = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT 1 FROM gang_members inner join gangs on gangs.gid = gang_members.gang_id WHERE gang_members.cid = ? AND `harmony` = ?', {character.id, "1"}, function(data)
		if data[1] then
            TriggerClientEvent("gang:targettingharmony", src, true)
        end
    end)
end)

RegisterServerEvent('gangs:invmembers')
AddEventHandler("gangs:invmembers", function(TargetID)
    local src = source
	local target = tonumber(TargetID)
	local user = exports["ethical-base"]:getModule("Player"):GetUser(target)
	local user1 = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user1:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT gid,gang_name,reputation FROM gangs WHERE gid = (select gang_id from gang_members where cid = @cid and rank = 1 and ingang = 1 limit 1)', { ['@cid'] = char.id }, function(result)
        if result ~= nil and #result > 0 then
            exports.ghmattimysql:execute('INSERT INTO gang_members (cid, gang_id, rank, ingang) VALUES (@cid, @gid, 0, 0)', {
                ['@cid'] = target,
                ['@gang_name'] = result[1].gang_name,
                ['@gid'] = result[1].gid,
            },function(data)
                -- TriggerClientEvent("gang:approved", target, true)
                TriggerClientEvent('DoLongHudText', src, "Invited CID ".. target .." to "..result[1].gang_name.."!", 1)
                TriggerClientEvent('DoLongHudText', target, "You have received an invite to join "..result[1].gang_name.."!", 1)
            end)
        end
    end)
end)

RegisterServerEvent('gangs:removemembers')
AddEventHandler("gangs:removemembers", function(TargetID, gang_id)
    local src = source
	local sourceUser = exports["ethical-base"]:getModule("Player"):GetUser(_source)
	local char = user:getCurrentCharacter(sourceUser)
	local target = tonumber(TargetID)
	local user = exports["ethical-base"]:getModule("Player"):GetUser(target)
    exports.ghmattimysql:execute('SELECT 1 as valid FROM gang_members WHERE cid = ? and gang_id = ? and rank = 1 and ingang = 1 and 0 = (SELECT CASE WHEN (select 1 from gangs where gid = ? and cid = ?) = 1 THEN 1 ELSE 0 END)', {char.id, gang_id, gang_id, target}, function(results)
        if #results > 0 then
            exports.ghmattimysql:execute('DELETE FROM gang_members WHERE `cid`= ? and gang_id = ? ', {target, gang_id})
            TriggerClientEvent("gang:approved", target, false)
            TriggerClientEvent('DoLongHudText', src, "Removed Player ".. target .." From The Gang!", 1)
            TriggerClientEvent('DoLongHudText', src, "You were removed from your gang", 1)
        else
            TriggerClientEvent('DoLongHudText', src, "You cannot remove the owner of the gang", 1)
        end
    end)
end)

RegisterServerEvent('gangs:leave')
AddEventHandler("gangs:leave", function(gang_id)
    --print(json.encode(gang_id))
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter(src)
    exports.ghmattimysql:execute('SELECT 1 as valid FROM gang_members WHERE cid = ? and gang_id = ?', {char.id, gang_id}, function(results)
        if #results > 0 then
            exports.ghmattimysql:execute('DELETE FROM gang_members WHERE `cid`= ? and gang_id = ? ', {char.id, gang_id})
            TriggerClientEvent("gang:approved", src, false)
            TriggerClientEvent('DoLongHudText', src, "You left the gang", 1)
        end
    end)
end)

function updatePlayerInfo(source)
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter()
	exports.ghmattimysql:execute('SELECT * FROM `gangs` WHERE `cid` = @cid', {['@cid'] = char.id}, function(rep)
        if ( rep and rep[1] ) then
              TriggerClientEvent('territories:getRep', src, rep[1].reputation)
          end
      end)
  end

RegisterServerEvent("ethical-territories:addrep")
AddEventHandler("ethical-territories:addrep", function(source, amount)
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT reputation, gang_name FROM gangs WHERE gid = (select gang_id from gang_members where cid = @cid)', { ['@cid'] = char.id }, function(rep)
		exports.ghmattimysql:execute('UPDATE gangs SET reputation = @reputation WHERE gang_name = @gang_name',
			{
			['@reputation'] = (rep[1].reputation + amount),
			['@gang_name'] = result[1].gang_name
        }, function ()
            updatePlayerInfo(source)
            TriggerClientEvent("phone:addnotification", "Unknown", "" .. result[1].gang_name .. "<br>Reputation Gained +" .. amount .. "")
        end)
    end)
end)

 RegisterServerEvent("territories:getRep")
 AddEventHandler("territories:getRep", function()
     local src = source
     local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
     local char = user:getCurrentCharacter()
     exports.ghmattimysql:execute('SELECT reputation FROM gangs WHERE gid = (select gang_id from gang_members where cid = @cid)', { ['@cid'] = char.id }, function(result)
        if result[1].reputation then
            local rep = json.decode(result[1].reputation)
            TriggerClientEvent("mt:missiontext", src, "Your Groups Reputation Is ".. rep, 2500)
        end
    end)
end)

RegisterServerEvent("targetgrove")
AddEventHandler("targetgrove", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter()

    exports.ghmattimysql:execute('SELECT gid FROM gangs WHERE gid = (select gang_id from gang_members where cid = @cid)', { ['@cid'] = char.id }, function(result)
        exports.ghmattimysql:execute('UPDATE `gangs` SET `grove` = @grove WHERE `gid` = @gid',
            {
            ['@grove'] = "1",
            ['@gid'] = result[1].gid
            }, function ()
            TriggerClientEvent("mt:missiontext", src, "Alright, your group is now targeting this area".. rep, 2500)
        end)
    end)
end)

RegisterServerEvent("targetcovenant")
AddEventHandler("targetcovenant", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT * FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result2)
        exports.ghmattimysql:execute('SELECT gang_name FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result)
            exports.ghmattimysql:execute('UPDATE `gangs` SET `covenant` = @covenant WHERE `gang_name` = @gang_name',
                {
                ['@covenant'] = "1",
                ['@gang_name'] = result[1].gang_name
                }, function ()
            end)
        end)
    end)
end)

RegisterServerEvent("targetbrouge")
AddEventHandler("targetbrouge", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT * FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result2)
        exports.ghmattimysql:execute('SELECT gang_name FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result)
            exports.ghmattimysql:execute('UPDATE `gangs` SET `brouge` = @brouge WHERE `gang_name` = @gang_name',
                {
                ['@brouge'] = "1",
                ['@gang_name'] = result[1].gang_name
                }, function ()
            end)
        end)
    end)
end)

RegisterServerEvent("targetforum")
AddEventHandler("targetforum", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT * FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result2)
        exports.ghmattimysql:execute('SELECT gang_name FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result)
            exports.ghmattimysql:execute('UPDATE `gangs` SET `forum` = @forum WHERE `gang_name` = @gang_name',
                {
                ['@forum'] = "1",
                ['@gang_name'] = result[1].gang_name
                }, function ()
            end)
        end)
    end)
end)

RegisterServerEvent("targetjamestown")
AddEventHandler("targetjamestown", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT * FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result2)
        exports.ghmattimysql:execute('SELECT gang_name FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result)
            exports.ghmattimysql:execute('UPDATE `gangs` SET `jamestown` = @jamestown WHERE `gang_name` = @gang_name',
                {
                ['@jamestown'] = "1",
                ['@gang_name'] = result[1].gang_name
                }, function ()
            end)
        end)
    end)
end)

RegisterServerEvent("targetmirror")
AddEventHandler("targetmirror", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT * FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result2)
        exports.ghmattimysql:execute('SELECT gang_name FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result)
            exports.ghmattimysql:execute('UPDATE `gangs` SET `mirrorpark` = @mirrorpark WHERE `gang_name` = @gang_name',
                {
                ['@mirrorpark'] = "1",
                ['@gang_name'] = result[1].gang_name
                }, function ()
            end)
        end)
    end)
end)

RegisterServerEvent("targetfudge")
AddEventHandler("targetfudge", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT * FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result2)
        exports.ghmattimysql:execute('SELECT gang_name FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result)
            exports.ghmattimysql:execute('UPDATE `gangs` SET `fudge` = @fudge WHERE `gang_name` = @gang_name',
                {
                ['@fudge'] = "1",
                ['@gang_name'] = result[1].gang_name
                }, function ()
            end)
        end)
    end)
end)

RegisterServerEvent("targetvespucci")
AddEventHandler("targetvespucci", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT * FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result2)
        exports.ghmattimysql:execute('SELECT gang_name FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result)
            exports.ghmattimysql:execute('UPDATE `gangs` SET `vespucci` = @vespucci WHERE `gang_name` = @gang_name',
                {
                ['@vespucci'] = "1",
                ['@gang_name'] = result[1].gang_name
                }, function ()
            end)
        end)
    end)
end)

RegisterServerEvent("targetcougar")
AddEventHandler("targetcougar", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT * FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result2)
        exports.ghmattimysql:execute('SELECT gang_name FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result)
            exports.ghmattimysql:execute('UPDATE `gangs` SET `cougar` = @cougar WHERE `gang_name` = @gang_name',
                {
                ['@cougar'] = "1",
                ['@gang_name'] = result[1].gang_name
                }, function ()
            end)
        end)
    end)
end)

RegisterServerEvent("targetharmony")
AddEventHandler("targetharmony", function()
    local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local char = user:getCurrentCharacter()
    exports.ghmattimysql:execute('SELECT * FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result2)
        exports.ghmattimysql:execute('SELECT gang_name FROM gangs WHERE `cid` = @cid', { ['@cid'] = char.id }, function(result)
            exports.ghmattimysql:execute('UPDATE `gangs` SET `harmony` = @harmony WHERE `gang_name` = @gang_name',
                {
                ['@harmony'] = "1",
                ['@gang_name'] = result[1].gang_name
                }, function ()
            end)
        end)
    end)
end)
