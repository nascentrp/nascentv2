--------------------------------------------------------------------------------------------------
--																				          	    --
--       ______ _______ _    _ _____ _____          _        _____  ________       _______      --
--      |  ____|__   __| |  | |_   _/ ____|   /\   | |      |  __ \|  ____\ \    / / ____|      --
--      | |__     | |  | |__| | | || |       /  \  | |      | |  | | |__   \ \  / / (___        --
--      |  __|    | |  |  __  | | || |      / /\ \ | |      | |  | |  __|   \ \/ / \___ \       --
--      | |____   | |  | |  | |_| || |____ / ____ \| |____  | |__| | |____   \  /  ____) |      --
--      |______|  |_|  |_|  |_|_____\_____/_/    \_\______| |_____/|______|   \/  |_____/       --
-- 																					            --
--                           joshua#5319 glacielgaming#6969 Woodz#1668                          --
--------------------------------------------------------------------------------------------------

Config = {}
Config.EnableSound = true; 
Config.SoundName = "Continue_Accepted" 
Config.Sound = 'DLC_HEIST_PLANNING_BOARD_SOUNDS' 

--------------------------------------------------------------------------------------------------
--																				          	    --
--       ______ _______ _    _ _____ _____          _        _____  ________       _______      --
--      |  ____|__   __| |  | |_   _/ ____|   /\   | |      |  __ \|  ____\ \    / / ____|      --
--      | |__     | |  | |__| | | || |       /  \  | |      | |  | | |__   \ \  / / (___        --
--      |  __|    | |  |  __  | | || |      / /\ \ | |      | |  | |  __|   \ \/ / \___ \       --
--      | |____   | |  | |  | |_| || |____ / ____ \| |____  | |__| | |____   \  /  ____) |      --
--      |______|  |_|  |_|  |_|_____\_____/_/    \_\______| |_____/|______|   \/  |_____/       --
-- 																					            --
--                           joshua#5319 glacielgaming#6969 Woodz#1668                          --
--------------------------------------------------------------------------------------------------