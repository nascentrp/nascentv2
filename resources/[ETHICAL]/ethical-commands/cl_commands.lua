 

currPause = false

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(500)
    local IsPauseMenuActive = IsPauseMenuActive()
    if IsPauseMenuActive and not currPause then
      currPause = true
      SendNUIMessage({hideCompletely = true})
    elseif not IsPauseMenuActive and currPause then
      currPause = false
      SendNUIMessage({showCompletely = true})
    end
  end
end)

 AddEventHandler('onResourceStart', function(resourceName)
  if (GetCurrentResourceName() ~= resourceName) then
    return
  end
  TriggerServerEvent("ethical-commands:buildCommands","")
end)

-- RegisterCommand("cpr", function(source, args, rawCommand)
--   TriggerEvent('trycpr')
-- end, false)

RegisterCommand("givekey", function(source, args, rawCommand)
  TriggerEvent("keys:give")
end, false)

RegisterCommand("givekeys", function(source, args, rawCommand)
  TriggerEvent("keys:give")
end, false)

-- RegisterCommand("window", function(source, args, rawCommand)
--   TriggerEvent("car:windows",args[2], args[3])
-- end, false)

-- RegisterCommand("rollup", function(source, args, rawCommand)
--   TriggerEvent("car:windowsup")
-- end, false)

-- RegisterCommand("finance", function(source, args, rawCommand)
--   TriggerEvent('finance1')
-- end, false)


-- RegisterCommand("news", function(source, args, rawCommand)
--   TriggerEvent('NewsStandCheck')
-- end, false)

RegisterCommand("confirm", function(source, args, rawCommand)
  TriggerEvent('housing:confirmed')
end, false)

-- RegisterCommand("notes", function(source, args, rawCommand)
--   TriggerEvent('Notepad:open')
-- end, false)

RegisterCommand("trunkkidnap", function(source, args, rawCommand)
  TriggerEvent('ped:forceTrunk')
end, false)

-- RegisterCommand("trunkeject", function(source, args, rawCommand)
--   TriggerEvent('ped:forceEjectTrunk')
-- end, false)

RegisterCommand("trunkgetin", function(source, args, rawCommand)
  TriggerEvent('ped:forceTrunkSelf')
end, false)

RegisterCommand("trunkejectself", function(source, args, rawCommand)
  TriggerEvent("ped:intrunk",false)
end, false)

RegisterCommand("anchor", function(source, args, rawCommand)
  TriggerEvent('client:anchor')
end, false)

-- RegisterCommand("pnum", function(source, args, rawCommand)
--   t, distance = GetClosestPlayer()
--   if (distance ~= -1 and distance < 7) then
--     playerped = PlayerPedId()
--     TriggerServerEvent("", GetPlayerServerId(t))
--   else
--     TriggerEvent("DoLongHudText", "No player near you (maybe get closer)!",2)
--   end
-- end, false)



-- RegisterCommand("atm", function(src, args, raw)
--   TriggerEvent('bank:checkATM')
-- end)

Citizen.CreateThread(function()
  TriggerEvent("chat:addSuggestion",  "/h1", "Put your hat on.")
  TriggerEvent("chat:addSuggestion",  "/h0", "Take your hat off.")
  TriggerEvent("chat:addSuggestion",  "/m1", "Put your mask on.")
  TriggerEvent("chat:addSuggestion",  "/m0", "Take your mask off.")
  TriggerEvent("chat:addSuggestion",  "/g1", "Put your glasses on.")
  TriggerEvent("chat:addSuggestion",  "/g0", "Take your glasses off.")
  TriggerEvent("chat:addSuggestion",  "/steal", "Attempt to rob someone!")
  TriggerEvent("chat:addSuggestion",  "/impound", "Impound nearest vehicle!")
  TriggerEvent("chat:addSuggestion",  "/bank", "Check your bank balance!")
  TriggerEvent("chat:addSuggestion",  "/cash", "Check your cash balance!")
  TriggerEvent("chat:addSuggestion",  "/outfits", "List all current outfits!")
  TriggerEvent("chat:addSuggestion",  "/pnum", "Shows your phone number to the nearest person!")
  TriggerEvent("chat:addSuggestion",  "/fix", "Fix your current car!")
  TriggerEvent("chat:addSuggestion",  "/finance", "Enables the finance on the nearest car!")
  TriggerEvent("chat:addSuggestion",  "/givekey", "Hand over the keys for your car!")
  TriggerEvent("chat:addSuggestion",  "/menu", "Opens the admin menu.")
  TriggerEvent("chat:addSuggestion",  "/news", "Check the news paper.")
  TriggerEvent("chat:addSuggestion",  "/a", "Used to answer the phone!")
  TriggerEvent("chat:addSuggestion",  "/togglealerts", "Disable job alerts.")
  TriggerEvent("chat:addSuggestion",  "/h", "Used to hangup the phone!")
  TriggerEvent("chat:addSuggestion",  "/trunkgetin", "Crawl in vehicle trunk!")
  TriggerEvent("chat:addSuggestion",  "/trunkejectself", "Attempt to remove self from trunk!")
  TriggerEvent("chat:addSuggestion",  "/trunkkidnap", "Throw someone in the trunk - /trunkeject to remove them!")
  --TriggerEvent("chat:addSuggestion",  "/trunkeject", "Eject anybody from the trunk of the vehicle you are in")
  TriggerEvent("chat:addSuggestion",  "/anchor", "Anchors Boat")
  TriggerEvent("chat:addSuggestion",  "/ooc_toggle", "Use this to toggle OOC")
  TriggerEvent("chat:addSuggestion",  "/cid", "Returns your citizen id")
  -- TriggerEvent(
  --     "chat:addSuggestion", 
  --     "/mechadd",
  --     "Add materials to your storage. Example: /mechadd [itemname] [amount]",
  --     {
  --         {name = "itemname", help = "Name of material"},
  --         {name = "amount", help = "Amount"}
  --     }
  -- )
  -- TriggerEvent(
  --     "chat:addSuggestion", 
  --     "/repair",
  --     "Used to repair a vehicle's internals",
  --     {
  --         {name = "part", help = "Part name"},
  --         {name = "amount", help = "Max amount is 10"}
  --     }
  -- )


  --Clothing Stuff
  -- TriggerEvent(
  --     "chat:addSuggestion", 
  --     "/outfitdel",
  --     "Delete an outfit",
  --     {
  --         {name = "slot", help = "Number of the outfit"}
  --     }
  -- )
  -- TriggerEvent(
  --     "chat:addSuggestion", 
  --     "/outfitadd",
  --     "Add a outfit.",
  --     {
  --         {name = "slot", help = "Slot to save it in"},
  --         {name = "name", help = "Name of the outfit"}
  --     }
  -- )



  -- PD Commands
  TriggerEvent(
      "chat:addSuggestion", 
      "/cctv",
      "View cameras in stores",
      {
          {name = "store id", help = "Number"}
      }
  )
  TriggerEvent(
      "chat:addSuggestion", 
      "/serial",
      "Retrieve owner of a weapon",
      {
          {name = "serial", help = "Weapon Serial"}
      }
  )
  -- TriggerEvent(
  --     "chat:addSuggestion", 
  --     "/callsign",
  --     "Set your callsign",
  --     {
  --         {name = "callsign", help = "Callsign"}
  --     }
  -- )
  TriggerEvent(
      "chat:addSuggestion", 
      "/311",
      "Non emergancy",
      {
          {name = "report", help = "Report"}
      }
  )
  TriggerEvent(
      "chat:addSuggestion", 
      "/311r",
      "Respond to a 311 call",
      {
          {name = "response", help = "Response"}
      }
  )
  TriggerEvent(
      "chat:addSuggestion", 
      "/911",
      "Emergancy line.",
      {
          {name = "report", help = "Your Emergacy"}
      }
  )
  TriggerEvent(
      "chat:addSuggestion", 
      "/911r",
      "Respond to a 911.",
      {
          {name = "callerid", help = "Callers UID"},
          {name = "Reply", help = "Response"}
      }
  )
  TriggerEvent(
      "chat:addSuggestion", 
      "/jail",
      "Send a person to jail",
      {
          {name = "id", help = "Their UID"},
          {name = "amount", help = "Time"}
      }
  )


  --DOJ
  -- TriggerEvent(
  --     "chat:addSuggestion", 
  --     "/announce",
  --     "Send out an announcement to the community!",
  --     {
  --         {name = "msg", help = "What is your msg?"}
  --     }
  -- )



  -- MISC
  TriggerEvent(
      "chat:addSuggestion", 
      "/e",
      "Express yourself with an emote!",
      {
          {name = "name", help = "Emote name"}
      }
  )
  TriggerEvent(
      "chat:addSuggestion", 
      "/givecash",
      "Give cash to someone",
      {
          {name = "id", help = "Server U id"},
          {name = "amount", help = "amount"}
      }
  )
  TriggerEvent(
      "chat:addSuggestion", 
      "/me",
      "Express your feeling.",
      {
          {name = "msg", help = "What do you want to say?"}
      }
  )

  -- TriggerEvent(
  --     "chat:addSuggestion", 
  --     "/disable",
  --     "Control Commands KEKW.",
  --     {
  --         {name = "cmd name", help = "What command?"}
  --     }
  -- )
  -- TriggerEvent(
  --     "chat:addSuggestion", 
  --     "/fire",
  --     "Make someone jobless.",
  --     {
  --         {name = "cid", help = "Whats their cid?"}
  --     }
  -- )
end)