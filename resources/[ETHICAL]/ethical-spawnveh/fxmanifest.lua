fx_version 'adamant'

game 'gta5'

client_scripts {
   'config.lua',
   'client/client.lua',
   "@ethical-errorlog/client/cl_errorlog.lua"
}

server_scripts {
   'server/server.lua'
}
