Config = {}

Config.Distance = 50

Config.PoliceGarage = {
    {423.0098, -998.5323, 30.70965},
    {394.8801, -1615.537, 29.29195},
    {1857.284, 3676.704, 33.68986},
}

Config.EMSGarage = {
    {289.153, -589.7777, 42.72568},
    {-238.2745, 6332.053, 32.42596},
}

Config.Vehlist = {
    [1] = 'emsnspeedo',
    [2] = 'emsv', 
    [3] = 'emsc',
    [4] = 'emsf',
    [5] = 'emst',
    [6] = 'emsair',  
}