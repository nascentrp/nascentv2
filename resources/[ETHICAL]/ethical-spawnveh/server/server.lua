local vehicleStock = {
	["policeb"] = 2,
	["bcpd10"] = 8,
	["bcpd6"] = 3,
	["bcpd7"] = 2,
    ["ambulance"] = 3,
    ["firetruk"] = 1,
    ["lsfd5"] = 2,
    ["20stinger"] = 2,
	["beanmachine"] = 1
}

RegisterServerEvent("RequestVehicleSpawnMenu")
AddEventHandler('RequestVehicleSpawnMenu', function(event)
    TriggerClientEvent("setStockCountAndRunClEvent", source, vehicleStock, event)
end)

RegisterServerEvent("VehicleSpawner:ReturnVehicle")
AddEventHandler('VehicleSpawner:ReturnVehicle', function(id)
	vehicleStock[id] = vehicleStock[id] + 1
    TriggerClientEvent("setStockCountAndRunClEvent", source, vehicleStock)
end)

RegisterServerEvent("SpawnVeh:RequestSpawnVehicle")
AddEventHandler('SpawnVeh:RequestSpawnVehicle', function(vehicle, event)
    if (vehicleStock[vehicle] - 1) >= 0 then
		vehicleStock[vehicle] = vehicleStock[vehicle] - 1
		TriggerClientEvent(event, source, vehicle)
	else
		TriggerClientEvent("DoLongHudText", source, "There are no  ".. vehicle .." left at the moment", 2)
	end
end)

