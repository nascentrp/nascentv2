--------------------
------ LOCALS ------
--------------------

local policeStations = {
	{423.0098, -998.5323, 30.70965},
	{394.8801, -1615.537, 29.29195},
	{1857.284, 3676.704, 33.68986},
}

local showPD = true
local hospitals = {
    {289.153, -589.7777, 42.72568},
    {-238.2745, 6332.053, 32.42596},
}

local showHospitals = true

----------------------
------ COMMANDS ------
----------------------

RegisterCommand("extra", function(source, args, rawCommand)
	if exports["isPed"]:isPed("myjob") == "police" then
		local ped = PlayerPedId()
		local veh = GetVehiclePedIsIn(ped, false)
		local extraID = tonumber(args[1])
		local extra = args[1]
		local toggle = tostring(args[2])
		for k,v in pairs(Config.PoliceGarage) do
			if IsPedInAnyVehicle(ped, true) then
				local veh = GetVehiclePedIsIn(ped, false)
				if GetDistanceBetweenCoords(GetEntityCoords(ped), v[1], v[2], v[3], true) <= Config.Distance then
					if toggle == "true" then
						toggle = 0
					end
					if veh ~= nil and veh ~= 0 and veh ~= 1 then
						TriggerEvent('DoLongHudText', 'Extra Toggled', 1)
				
						if extra == "all" then
							SetVehicleExtra(veh, 1, toggle)
							SetVehicleExtra(veh, 2, toggle)
							SetVehicleExtra(veh, 3, toggle)
							SetVehicleExtra(veh, 4, toggle)
							SetVehicleExtra(veh, 5, toggle)       
							SetVehicleExtra(veh, 6, toggle)
							SetVehicleExtra(veh, 7, toggle)
							SetVehicleExtra(veh, 8, toggle)
							SetVehicleExtra(veh, 9, toggle)               
							SetVehicleExtra(veh, 10, toggle)
							SetVehicleExtra(veh, 11, toggle)
							SetVehicleExtra(veh, 12, toggle)
							SetVehicleExtra(veh, 13, toggle)
							SetVehicleExtra(veh, 14, toggle)
							SetVehicleExtra(veh, 15, toggle)
							SetVehicleExtra(veh, 16, toggle)
							SetVehicleExtra(veh, 17, toggle)
							SetVehicleExtra(veh, 18, toggle)
							SetVehicleExtra(veh, 19, toggle)
							SetVehicleExtra(veh, 20, toggle)
						TriggerEvent('DoLongHudText', 'Extra All Toggled', 1)
						elseif extraID == extraID then
							SetVehicleExtra(veh, extraID, toggle)
						end
					end
				end
			end
		end
	elseif exports["isPed"]:isPed("myjob") == "ems" then
		local ped = PlayerPedId()
		local veh = GetVehiclePedIsIn(ped, false)
		local extraID = tonumber(args[1])
		local extra = args[1]
		local toggle = tostring(args[2])
		for k,v in pairs(Config.EMSGarage) do 
			if IsPedInAnyVehicle(ped, true) then
				local veh = GetVehiclePedIsIn(ped, false)
				if GetDistanceBetweenCoords(GetEntityCoords(ped), v[1], v[2], v[3], true) <= Config.Distance then
					if toggle == "true" then
						toggle = 0
					end
				end
				if veh ~= nil and veh ~= 0 and veh ~= 1 then
					TriggerEvent('DoLongHudText', 'Extra Toggled', 1)
					
					if extra == "all" then
						SetVehicleExtra(veh, 1, toggle)
						SetVehicleExtra(veh, 2, toggle)
						SetVehicleExtra(veh, 3, toggle)
						SetVehicleExtra(veh, 4, toggle)
						SetVehicleExtra(veh, 5, toggle)       
						SetVehicleExtra(veh, 6, toggle)
						SetVehicleExtra(veh, 7, toggle)
						SetVehicleExtra(veh, 8, toggle)
						SetVehicleExtra(veh, 9, toggle)               
						SetVehicleExtra(veh, 10, toggle)
						SetVehicleExtra(veh, 11, toggle)
						SetVehicleExtra(veh, 12, toggle)
						SetVehicleExtra(veh, 13, toggle)
						SetVehicleExtra(veh, 14, toggle)
						SetVehicleExtra(veh, 15, toggle)
						SetVehicleExtra(veh, 16, toggle)
						SetVehicleExtra(veh, 17, toggle)
						SetVehicleExtra(veh, 18, toggle)
						SetVehicleExtra(veh, 19, toggle)
						SetVehicleExtra(veh, 20, toggle)
						TriggerEvent('DoLongHudText', 'Extra All Toggled', 1)
					elseif extraID == extraID then
						SetVehicleExtra(veh, extraID, toggle)
					end
					
				end
			end
		end
	end
end, false)
  
-- RegisterCommand('fix', function(source)
-- 	if exports["isPed"]:isPed("myjob") == "police" then
-- 		policeFix()
-- 	elseif exports["isPed"]:isPed("myjob") == "ems" then
-- 		EMSFix()
-- 	end
-- end,false)


-- RegisterCommand('boat', function(source, args)
-- 	if exports["isPed"]:isPed("myjob") == "police" then
--       TriggerEvent('ethical:spawnVehicle', 'predator')
-- 	else
-- 		TriggerEvent('DoLongHudText', 'You are not Police!', 1)
--   end
-- end)

RegisterCommand('livery', function(source, args, raw)
	local coords = GetEntityCoords(GetPlayerPed(-1))
	local vehicle = GetVehiclePedIsIn(GetPlayerPed(-1))
	local job = exports["isPed"]:isPed("myjob")
	if job == 'police' or job == 'ems' and GetVehicleLiveryCount(vehicle) - 1 >= tonumber(args[1]) then
		SetVehicleLivery(vehicle, tonumber(args[1]))
		TriggerEvent('DoLongHudText', 'Livery Set', 1)
	else
		TriggerEvent('DoLongHudText', 'You are not a police officer!', 2)
	end
  end)

RegisterCommand("svlistuc", function(source, args, rawCommand)
	if exports["isPed"]:isPed("myjob") == "police" then
		TriggerEvent('chatMessagess', 'Undercover Vehicles:', 2, " \n [14] Galivanter Baller (UC) \n [15] Bravado Banshee (UC) \n [16] Bravado Buffalo (UC) \n [17] Pfister Comet (UC) \n [18] Invetero Coquette (UC) \n [19] Albany Primo (UC) \n [20] Declasse Rancher (UC) \n [21] Albany Washington (UC) ")
	end
end)


RegisterCommand("door", function(source, args, raw)
	local ped = PlayerPedId()
	local veh = GetVehiclePedIsUsing(ped)
	local Driver = GetPedInVehicleSeat(veh, -1)
	
	if args[1] ~= nil then
		door = tonumber(args[1]) - 1
	else
		door = nil
	end

	if door ~= nil then
		if DoesEntityExist(Driver) and IsPedInAnyVehicle(ped, false) then
			if GetVehicleDoorAngleRatio(veh, door) > 0 then
				SetVehicleDoorShut(veh, door, false)
			else	
				SetVehicleDoorOpen(veh, door, false, false)
			end
		end
	end
end)

RegisterCommand('seat', function(source, args)
	if args[1] ~= nil and tonumber(args[1]) >= 1 and tonumber(args[1]) <= 4 then
	  TriggerEvent('car:swapseat', tonumber(args[1]) - 2)
	end
end)


-----------------------
------ NETEVENTS ------
-----------------------

RegisterNetEvent('animation:impound')
AddEventHandler('animation:impound', function()
		inanimation = true
		local lPed = GetPlayerPed(-1)
		RequestAnimDict("amb@code_human_police_investigate@idle_a")
		while not HasAnimDictLoaded("amb@code_human_police_investigate@idle_a") do
			Citizen.Wait(0)
		end
		
		if IsEntityPlayingAnim(lPed, "amb@code_human_police_investigate@idle_a", "idle_b", 3) then
			ClearPedSecondaryTask(lPed)
		else
			TaskPlayAnim(lPed, "amb@code_human_police_investigate@idle_a", "idle_b", 8.0, -8, -1, 49, 0, 0, 0, 0)
			seccount = 4
			while seccount > 0 do
				Citizen.Wait(1000)
				seccount = seccount - 1
			end
			ClearPedSecondaryTask(lPed)
		end		
		inanimation = false
end)


local vehicleStock = {}

function getVehicleStock(vehicleName)
	return vehicleStock[vehicleName]
end

function setVehicleStock(vehicleName, stock)
	vehicleStock[vehicleName] = vehicleStock[vehicleName] + stock
end

RegisterNetEvent('PDSpawnVeh')
AddEventHandler('PDSpawnVeh', function()
	TriggerServerEvent("RequestVehicleSpawnMenu", 'PDSpawnVeh2')
end)

RegisterNetEvent('setStockCountAndRunClEvent')
AddEventHandler('setStockCountAndRunClEvent', function(inVehicleStock, event)
	vehicleStock = inVehicleStock
	if event ~= nil then
		TriggerEvent(event)
	end
end)
RegisterNetEvent('PDSpawnVeh2')
AddEventHandler('PDSpawnVeh2', function()
	local policeBAvailable = getVehicleStock('policeb')
	local police2Available = getVehicleStock('bcpd10')
	local trooper1Available = getVehicleStock('bcpd6')
	local interceptorAvailable = getVehicleStock('bcpd7')
	local rank = exports["isPed"]:isPed("jobrank")
	rank = tonumber(rank)
	
	local vehicleMenu = {
        {
            id = 1,
            header = "Police Garage",
            txt = ""
        }
	}
	local count = 1

	count = count + 1
	table.insert(vehicleMenu, {
		id = count,
		header = "Police Bike",
		txt = policeBAvailable .. " available.",
		params = {
			event = "spawn:veh:pd",
			args = {
				vehicle = "policeb"
			},
		}
	})

	count = count + 1
	table.insert(vehicleMenu, {
		id = count,
		header = "Cruiser",
		txt = police2Available .. " available.",
		params = {
			event = "spawn:veh:pd",
			args = {
				vehicle = "bcpd10"
			},
		}
	})

	if(rank >= 3) then
		count = count + 1
		table.insert(vehicleMenu, {
			id = count,
            header = "Trooper Cruiser",
			txt = trooper1Available .. " available.",
			params = {
                event = "spawn:veh:pd",
				args = {
					vehicle = "bcpd6"
				}
            },
        })
	end
	if(rank >= 8) then
		count = count + 1
		table.insert(vehicleMenu, {
			id = count,
			header = "Interceptor",
			txt = interceptorAvailable .. " available.",
			params = {
				event = "spawn:veh:pd",
				args = {
					vehicle = "bcpd7"
				}
			},
        })
	end
	count = count + 1
	table.insert(vehicleMenu, {
		id = count,
		header = "Return Vehicle",
		txt = "Park it on the red marker",
		params = {
			event = "spawn:veh:pd:return",
		}
	})

	TriggerEvent('ethical-context:sendMenu', vehicleMenu)
end)
RegisterNetEvent('requestBeanMachineVehicles')
AddEventHandler('requestBeanMachineVehicles', function()
	TriggerServerEvent("RequestVehicleSpawnMenu", 'beanMachineVehicles')
end)
RegisterNetEvent('beanMachineVehicles')
AddEventHandler('beanMachineVehicles', function()
	local beanMachineAvailable = getVehicleStock('beanmachine')
	TriggerEvent('ethical-context:sendMenu', {
        {
            id = 1,
            header = "Bean Machine Garage",
            txt = ""
        },
		{
			id = 2,
			header = "Dilettante",
			txt = beanMachineAvailable .. " available.",
			params = {
				event = "spawn:veh:beanmachine",
				args = {
					vehicle = "beanmachine"
				}
			},
        },
		{
			id = 3,
			header = "Return Vehicle",
			txt = "Park it outside the shop",
			params = {
				event = "spawn:veh:beanmachine:return",
			}
		}
	})
end)

RegisterNetEvent('spawn:veh:beanmachine')
AddEventHandler('spawn:veh:beanmachine', function(v)
	local vehicle = v.vehicle
	local event = 'spawn:veh:beanmachine2'
	TriggerServerEvent("SpawnVeh:RequestSpawnVehicle", vehicle, event)
end)

RegisterNetEvent('spawn:veh:beanmachine2')
AddEventHandler('spawn:veh:beanmachine2', function(v)
	print("spawning vehicle", v)
	exports['nsrp_util']:spawnVehicle(v, -624.7821, 206.679, 73.5955, function(veh)
		TaskWarpPedIntoVehicle(PlayerPedId(), veh, -1)
	end, -267.0)
end)

RegisterNetEvent('spawn:veh:ems:return')
AddEventHandler('spawn:veh:ems:return', function()
	local vehicleNames = {
		'ambulance',
		'firetruk',
		'20stinger',
		'lsfd5'
	}
	local vehName = deleteVehicleIfInZoneAndHasKeys(333.4349, -579.9741, 28.7968, vehicleNames)
	
	if vehName ~= 0 then
		TriggerServerEvent('VehicleSpawner:ReturnVehicle', vehName)
		setVehicleStock(vehName, 1) -- add 1 to stock
	else
		TriggerEvent("DoLongHudText", "No valid vehicle found, try to park closer.", 2)
	end
end)

RegisterNetEvent('spawn:veh:pd:return')
AddEventHandler('spawn:veh:pd:return', function()
	local vehicleNames = {
		'bcpd14',
		'bcpd10',
		'bcpd6',
		'bcpd7',
		'policeb'
	}
	local vehName = deleteVehicleIfInZoneAndHasKeys(435.70031738281, -975.87255859375, 25.699796676636, vehicleNames)
	
	if vehName ~= 0 then
		TriggerServerEvent('VehicleSpawner:ReturnVehicle', vehName)
		setVehicleStock(vehName, 1) -- add 1 to stock
	else
		TriggerEvent("DoLongHudText", "No valid vehicle found, try to park closer.", 2)
	end
end)

RegisterNetEvent('spawn:veh:beanmachine:return')
AddEventHandler('spawn:veh:beanmachine:return', function()
	local vehicleNames = {
		'beanmachine'
	}
	local vehName = deleteVehicleIfInZoneAndHasKeys(-622.69390869141, 251.88861083984, 81.182342529297, vehicleNames)
	
	if vehName ~= 0 then
		TriggerServerEvent('VehicleSpawner:ReturnVehicle', vehName)
		setVehicleStock(vehName, 1) -- add 1 to stock
	else
		TriggerEvent("DoLongHudText", "No valid vehicle found, try to park closer.", 2)
	end
end)

function deleteVehicleIfInZoneAndHasKeys(x,y,z,vehicle_hash_table)
	local found = 0
	local foundEntity = 0
	local v = GetVehiclePedIsIn(PlayerPedId(), true)
	if v == 0 then
		return 0
	end
	print("vv")
	print(v)
	local entity = GetEntityModel(v)
	print(entity)

	for i = 1, #vehicle_hash_table do
		local vv = GetHashKey(vehicle_hash_table[i])
		print(entity)
		print(vv)
		print(vehicle_hash_table[i])
		if entity == vv then
			print("success")
			foundEntity = vehicle_hash_table[i]
			found = 1
		end
	end

	if found then
		DeleteEntity(v)
		return foundEntity
	end
	return 0
--  Heading: 95.39427947998
end

RegisterNetEvent('EMSSpawnVeh')
AddEventHandler('EMSSpawnVeh', function()
	TriggerServerEvent("RequestVehicleSpawnMenu", 'EMSSpawnVeh2')
end)
RegisterNetEvent('EMSSpawnVeh2')
AddEventHandler('EMSSpawnVeh2', function()
	local ambulanceAvailable = getVehicleStock('ambulance')
	local firetrukAvailable = getVehicleStock('firetruk')
	local lsfd5Available = getVehicleStock('lsfd5')
	local stingerAvailable = getVehicleStock('20stinger')
	local rank = exports["isPed"]:isPed("jobrank")
	rank = tonumber(rank)
	
	local myRank = 'BLS'
	if rank == 1 then
		myRank = "BLS"
	elseif rank == 2 then
		myRank = "ILS"
	elseif rank == 3 then
		myRank = "ALS"
	elseif rank >= 4 then
		myRank = "CHIEF"
	end

	-- jobrank
	-- [1] = "EMT",
	-- [2] = "Paramedic",
	-- [3] = "Lieutenant of EMS",
	-- [4] = "Assistant Chief",
	-- [5] = "Chief of EMS"

	--local myRank = ''

	local count = 1

	local menuData = {
        {
            id = count,
            header = "EMS Garage",
            txt = ""
        }
	}
	count = count + 1

	table.insert(menuData, {
		id = count,
		header = "Ambulance",
		txt = ambulanceAvailable .. " available.",
		params = {
			event = "spawn:veh:ems",
			args = {
				vehicle = "ambulance"
			}
		}
	})
	count = count + 1
	
	if myRank == 'ALS' or myRank == 'ILS' or myRank == "CHIEF" then
		table.insert(menuData, {
			id = count,
			header = "Fire Truck",
			txt = firetrukAvailable .. " available.",
			params = {
				event = "spawn:veh:ems",
				args = {
					vehicle = "firetruk"
				}
			}
		})
		count = count + 1
		
		table.insert(menuData, {
			id = count,
			header = "Response Unit",
			txt = stingerAvailable .. " available.",
			params = {
				event = "spawn:veh:ems",
				args = {
					vehicle = "20stinger"
				}
			}
		})
		count = count + 1
	end
	if myRank == 'ALS' or myRank == "CHIEF" then
		table.insert(menuData, {
			id = count,
			header = "Kwagga Unit",
			txt = lsfd5Available .. " available.",
			params = {
				event = "spawn:veh:ems",
				args = {
					vehicle = "lsfd5"
				}
			}
		})
		count = count + 1
	end
	table.insert(menuData, {
		id = count,
		header = "Return Vehicle",
		txt = "Bring her home",
		params = {
			event = "spawn:veh:ems:return",
		},
	})
	TriggerEvent('ethical-context:sendMenu', menuData)
end)

RegisterNetEvent('request:spawn:veh:pd')
AddEventHandler('request:spawn:veh:pd', function(vehicle)
	TriggerServerEvent('spawn:veh:pd', vehicle)
end)

RegisterNetEvent('spawn:veh:pd')
AddEventHandler('spawn:veh:pd', function(type)
	local vehicle = type.vehicle
	TriggerServerEvent("SpawnVeh:RequestSpawnVehicle", vehicle, 'spawn:veh:pd2')
end)
RegisterNetEvent('spawn:veh:pd2')
AddEventHandler('spawn:veh:pd2', function(vehicle)
	SpawnVehPD(vehicle)
end)


RegisterNetEvent('spawn:veh:ems')
AddEventHandler('spawn:veh:ems', function(type)
	local vehicle = type.vehicle
	TriggerServerEvent("SpawnVeh:RequestSpawnVehicle", vehicle, 'spawn:veh:ems2')
end)
RegisterNetEvent('spawn:veh:ems2')
AddEventHandler('spawn:veh:ems2', function(vehicle)
	SpawnVehEMS(vehicle)	
end)

RegisterNetEvent("spawn:hei:pd")
AddEventHandler("spawn:hei:pd", function()
	Citizen.CreateThread(function()

		local spawnLocation = findClosestSpawnPointPD(GetEntityCoords(PlayerPedId()))
		local getVehicleInArea = GetClosestVehicle(spawnLocation, 3.000, 0, 70)
		if DoesEntityExist(getVehicleInArea) then
		  TriggerEvent("DoLongHudText", "The area is crowded", 2)
		  return
		end

		local v = exports['nsrp_util']:spawnVehicle('polmav', 449.45401000977, -981.42340087891, 43.691394805908,
			function(vehicle)
				SetVehicleModKit(vehicle, 0)
				SetVehicleMod(vehicle, 16, 4, false)
				SetVehicleDirtLevel(vehicle, 0)
				SetVehicleWindowTint(vehicle, 0)
				SetVehicleLivery(vehicle, 0)
				TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
			end,
		-20.0)        
        LastVehicle = vehicle
    end)
end)

RegisterNetEvent("spawn:hei:ems")
AddEventHandler("spawn:hei:ems", function()
	Citizen.CreateThread(function()

        local hash = GetHashKey("polmav")

        if not IsModelAVehicle(hash) then return end
        if not IsModelInCdimage(hash) or not IsModelValid(hash) then return end
        
        RequestModel(hash)

        while not HasModelLoaded(hash) do
            Citizen.Wait(0)
        end

		local spawnLocation = findClosestSpawnPointPD(GetEntityCoords(PlayerPedId()))
		local getVehicleInArea = GetClosestVehicle(spawnLocation, 3.000, 0, 70)
		if DoesEntityExist(getVehicleInArea) then
		  TriggerEvent("DoLongHudText", "The area is crowded", 2)
		  return
		end

        local vehicle = exports['nsrp_util']:spawnVehicle('polmav', 351.67828369141, -588.35443115234, 74.161727905273,
		function(vehicle)
			SetVehicleModKit(vehicle, 0)
			SetVehicleMod(vehicle, 16, 4, false)
			SetVehicleDirtLevel(vehicle, 0)
			SetVehicleWindowTint(vehicle, 0)
			SetVehicleLivery(vehicle, 1)
			TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
		end,
	-20.0)  
	    
	    SetEntityHeading(vehicle, 251.18246459961)

		--if name ~= "polmav" then
		--	SetVehicleModKit(vehicle, 0)
		--	SetVehicleMod(vehicle, 16, 4, false)
		--end

        local plate = GetVehicleNumberPlateText(vehicle)
        TriggerEvent("keys:addNew",vehicle,plate)
        SetModelAsNoLongerNeeded(hash)
        
        --SetVehicleDirtLevel(vehicle, 0)
		
        --SetVehicleWindowTint(vehicle, 0)
		--TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
        --if livery ~= nil then
        --    SetVehicleLivery(vehicle, tonumber(livery))
        --end
        LastVehicle = vehicle
    end)
end)

RegisterNetEvent("spawn:large:riot")
AddEventHandler("spawn:large:riot", function()
	Citizen.CreateThread(function()

        local hash = GetHashKey("riot")

        if not IsModelAVehicle(hash) then return end
        if not IsModelInCdimage(hash) or not IsModelValid(hash) then return end
        
        RequestModel(hash)

        while not HasModelLoaded(hash) do
            Citizen.Wait(0)
        end

		local spawnLocation = findClosestSpawnPointPD(GetEntityCoords(PlayerPedId()))
		local getVehicleInArea = GetClosestVehicle(spawnLocation, 3.000, 0, 70)
		if DoesEntityExist(getVehicleInArea) then
		  TriggerEvent("DoLongHudText", "The area is crowded", 2)
		  return
		end

        local vehicle = CreateVehicle(hash, 450.12786865234, -1020.5704345703, 28.423704147339, true, false)
		SetEntityHeading(vehicle, 91.702270507812)

        local plate = GetVehicleNumberPlateText(vehicle)
        TriggerEvent("keys:addNew",vehicle,plate)
        SetModelAsNoLongerNeeded(hash)
        
        SetVehicleDirtLevel(vehicle, 0)
        SetVehicleWindowTint(vehicle, 0)
		TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
        if livery ~= nil then
            SetVehicleLivery(vehicle, tonumber(livery))
        end
        LastVehicle = vehicle
    end)
end)

RegisterNetEvent("spawn:large:pbus")
AddEventHandler("spawn:large:pbus", function()
	Citizen.CreateThread(function()

        local hash = GetHashKey("pbus")

        if not IsModelAVehicle(hash) then return end
        if not IsModelInCdimage(hash) or not IsModelValid(hash) then return end
        
        RequestModel(hash)

        while not HasModelLoaded(hash) do
            Citizen.Wait(0)
        end

		local spawnLocation = findClosestSpawnPointPD(GetEntityCoords(PlayerPedId()))
		local getVehicleInArea = GetClosestVehicle(spawnLocation, 3.000, 0, 70)
		if DoesEntityExist(getVehicleInArea) then
		  TriggerEvent("DoLongHudText", "The area is crowded", 2)
		  return
		end

        local vehicle = CreateVehicle(hash, 450.12786865234, -1020.5704345703, 28.423704147339, true, false)
		SetEntityHeading(vehicle, 91.702270507812)

        local plate = GetVehicleNumberPlateText(vehicle)
        TriggerEvent("keys:addNew",vehicle,plate)
        SetModelAsNoLongerNeeded(hash)
        
        SetVehicleDirtLevel(vehicle, 0)
        SetVehicleWindowTint(vehicle, 0)
		TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
        if livery ~= nil then
            SetVehicleLivery(vehicle, tonumber(livery))
        end
        LastVehicle = vehicle
    end)
end)

RegisterNetEvent('ethical-spawnveh:showPD')
AddEventHandler('ethical-spawnveh:showPD', function()
    showPD = not showPD
   for _, item in pairs(policeStations) do
        if not showPD then
            if item.blip ~= nil then
                RemoveBlip(item.blip)
            end
        else
            item.blip = AddBlipForCoord(item[1], item[2], item[3])
            SetBlipSprite(item.blip, 60)
            SetBlipScale(item.blip, 0.7)
			SetBlipColour(item.blip, 38)
            SetBlipAsShortRange(item.blip, true)
            BeginTextCommandSetBlipName("STRING")
            AddTextComponentString("Police Department")
            EndTextCommandSetBlipName(item.blip)
        end
    end
end)
RegisterNetEvent('ethical-spawnveh:EMS')
AddEventHandler('ethical-spawnveh:EMS', function()
    showHospitals = not showHospitals
   for _, item in pairs(hospitals) do
        if not showHospitals then
            if item.blip ~= nil then
                RemoveBlip(item.blip)
            end
        else
            item.blip = AddBlipForCoord(item[1], item[2], item[3])
            SetBlipSprite(item.blip, 61)
            SetBlipScale(item.blip, 0.7)
			SetBlipColour(item.blip, 2)
            SetBlipAsShortRange(item.blip, true)
            BeginTextCommandSetBlipName("STRING")
            AddTextComponentString("Hospital")
            EndTextCommandSetBlipName(item.blip)
        end
    end
end)


-----------------------
------ FUNCTIONS ------
-----------------------


function SpawnVehPD(name)
	Citizen.CreateThread(function()

        local hash = GetHashKey(name)

        --if not IsModelAVehicle(hash) then return end
        --if not IsModelInCdimage(hash) or not IsModelValid(hash) then return end
        
       -- RequestModel(hash)

       -- while not HasModelLoaded(hash) do
       --     Citizen.Wait(0)
       -- end
		local nicePosition = exports["ethical-jobmanager"]:GetJobRankName()
        local isBCSO = false
		if string.match(nicePosition, "BCSO") then
			isBCSO = true
		else
			isBCSO = false
		end
		local spawnLocation = findClosestSpawnPointPD(GetEntityCoords(PlayerPedId()))
		local getVehicleInArea = GetClosestVehicle(spawnLocation, 3.000, 0, 70)
		if DoesEntityExist(getVehicleInArea) then
		  TriggerEvent("DoLongHudText", "The area is crowded", 2)
		  return
		end

        --local vehicle = CreateVehicle(hash, 451.25408935547, -975.93743896484, 25.69979095459, 85.700180053711, true, false)
		local vehicle = exports['nsrp_util']:spawnVehicle(name, 451.25408935547, -975.93743896484, 25.69979095459,
		function(vehicle)
			SetVehicleModKit(vehicle, 0)
			SetVehicleMod(vehicle, 16, 4, false)
			SetVehicleDirtLevel(vehicle, 0)
			SetVehicleWindowTint(vehicle, 0)
			--SetVehicleLivery(vehicle, 0)
			applyMaxUpgrades(vehicle)
			TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
			local job = exports["isPed"]:isPed("myjob")
			if job == "police" then
				if name == 'bcpd10' and isBCSO then
					SetVehicleLivery(vehicle, 1)
				else 
					SetVehicleLivery(vehicle, 0)
				end
			end
		end,
		-20.0)

        local plate = GetVehicleNumberPlateText(vehicle)
        TriggerEvent("keys:addNew",vehicle,plate)
       -- SetModelAsNoLongerNeeded(hash)
        
        --SetVehicleDirtLevel(vehicle, 0)
       -- SetVehicleWindowTint(vehicle, 0)
		--TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
		

        LastVehicle = vehicle
    end)
end

function SpawnVehEMS(name)
	Citizen.CreateThread(function()

        local hash = GetHashKey(name)
		print(name)
		print(IsModelAVehicle(hash))
		print(IsModelInCdimage(hash))
		print(IsModelValid(hash))
        if not IsModelAVehicle(hash) then return end
        if not IsModelInCdimage(hash) or not IsModelValid(hash) then return end
        
        RequestModel(hash)

        while not HasModelLoaded(hash) do
            Citizen.Wait(0)
        end
		print("passed basic test")
		local spawnLocation = findClosestSpawnPointEMS(GetEntityCoords(PlayerPedId()))
		local getVehicleInArea = GetClosestVehicle(spawnLocation, 3.000, 0, 70)
		if DoesEntityExist(getVehicleInArea) then
		  TriggerEvent("DoLongHudText", "The area is crowded", 2)
		  return
		end

        local vehicle = CreateVehicle(hash, 333.15982055664, -576.3837890625, 28.796867370605, 338.55960083008, true, false)

        local plate = GetVehicleNumberPlateText(vehicle)
        TriggerEvent("keys:addNew",vehicle,plate)
        SetModelAsNoLongerNeeded(hash)
        
        SetVehicleDirtLevel(vehicle, 0)
        SetVehicleWindowTint(vehicle, 0)
		TaskWarpPedIntoVehicle(PlayerPedId(), vehicle, -1)
        if livery ~= nil then
            SetVehicleLivery(vehicle, tonumber(livery))
        end
        LastVehicle = vehicle
    end)
end

function applyMaxUpgrades(veh)
	SetVehicleModKit(veh, 0)
    SetVehicleMod(veh, 16, 4, false)
end

function findClosestSpawnPointPD(pCurrentPosition)
	local coords = vector3(451.25408935547, -975.93743896484, 25.69979095459)
	local closestDistance = -1
	local closestCoord = pCurrentPosition
	local distance = #(coords - pCurrentPosition)
	if closestDistance == -1 or closestDistance > distance then
	  closestDistance = distance
	  closestCoord = coords
	end
	return closestCoord
end

function findClosestSpawnPointEMS(pCurrentPosition)
	local coords = vector3(333.15982055664, -576.3837890625, 28.796867370605)
	local closestDistance = -1
	local closestCoord = pCurrentPosition
	local distance = #(coords - pCurrentPosition)
	if closestDistance == -1 or closestDistance > distance then
	  closestDistance = distance
	  closestCoord = coords
	end
	return closestCoord
end

function policeFix()
	local ped = GetPlayerPed(-1)
	for k,v in pairs(Config.PoliceGarage) do 
		if IsPedInAnyVehicle(ped, true) then
			local veh = GetVehiclePedIsIn(ped, false)
			if GetDistanceBetweenCoords(GetEntityCoords(ped), v[1], v[2], v[3], true) <= Config.Distance then
				FreezeEntityPosition(veh, true)
				TriggerEvent('DoLongHudText', 'Your vehicle is being repaired please wait', 1)
				local finished = exports["ethical-taskbar"]:taskBar(5000, "Completing Task")
				if finished == 100 then
					TriggerEvent('DoLongHudText', 'Your vehicle has been repaired', 1)
					SetVehicleFixed(veh)
					SetVehicleDirtLevel(veh, 0.0)
					exports["ethical-oGasStations"]:SetFuel(veh, 100)
					FreezeEntityPosition(veh, false)
				else
					FreezeEntityPosition(veh, false)
				end
			end
		end
	end
end

function EMSFix()
	local ped = GetPlayerPed(-1)
	for k,v in pairs(Config.EMSGarage) do 
		if IsPedInAnyVehicle(ped, true) then
			local veh = GetVehiclePedIsIn(ped, false)
			if GetDistanceBetweenCoords(GetEntityCoords(ped), v[1], v[2], v[3], true) <= Config.Distance then
				FreezeEntityPosition(veh, true)
				TriggerEvent('DoLongHudText', 'Your vehicle is being repaired please wait', 1)
				local finished = exports["ethical-taskbar"]:taskBar(5000, "Completing Task")
				if finished == 100 then
					TriggerEvent('DoLongHudText', 'Your vehicle has been repaired', 1)
					SetVehicleFixed(veh)
					SetVehicleDirtLevel(veh, 0.0)
					exports["ethical-oGasStations"]:SetFuel(veh, 100)
					FreezeEntityPosition(veh, false)
				else
					FreezeEntityPosition(veh, false)
				end
			end
		end
	end
end

Citizen.CreateThread(function()
    showPD = false
    TriggerEvent('ethical-spawnveh:showPD')
end)

Citizen.CreateThread(function()
    showHospitals = false
    TriggerEvent('ethical-spawnveh:EMS')
end)