local hmm = vehicleBaseRepairCost

RegisterServerEvent('ethical-bennys:attemptPurchase')
AddEventHandler('ethical-bennys:attemptPurchase', function(cheap, type, upgradeLevel)
	local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    if type == "repair" then
        if user:getBalance() >= hmm then
            user:removeBank(hmm)
            TriggerClientEvent('ethical-bennys:purchaseSuccessful', source)
        else
            TriggerClientEvent('ethical-bennys:purchaseFailed', source)
        end
    elseif type == "performance" then
        if user:getBalance() >= vehicleCustomisationPrices[type].prices[upgradeLevel] then
            TriggerClientEvent('ethical-bennys:purchaseSuccessful', source)
            user:removeBank(vehicleCustomisationPrices[type].prices[upgradeLevel])
        else
            TriggerClientEvent('ethical-bennys:purchaseFailed', source)
        end
    else
        if user:getBalance() >= vehicleCustomisationPrices[type].price then
            TriggerClientEvent('ethical-bennys:purchaseSuccessful', source)
            user:removeBank(vehicleCustomisationPrices[type].price)
        else
            TriggerClientEvent('ethical-bennys:purchaseFailed', source)
        end
    end
end)

RegisterServerEvent('ethical-bennys:updateRepairCost')
AddEventHandler('ethical-bennys:updateRepairCost', function(cost)
    hmm = cost
end)

RegisterServerEvent('ethical-bennys:repairciv')
AddEventHandler('ethical-bennys:repairciv', function(repaircost)
    local src = source
    local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    user:removeMoney(repaircost)
end)