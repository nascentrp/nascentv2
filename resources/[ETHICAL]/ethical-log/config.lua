Config = {}

Config.AllLogs = true
Config.postal = false
Config.username = "Nascent RP 2.0 Logs" 
Config.avatar = "https://nascentrp.co.za/wp-content/uploads/2021/06/Nascent2-1024x560.png"
Config.communtiyName = "Nascent RP"
Config.communtiyLogo = "https://nascentrp.co.za/wp-content/uploads/2021/06/Nascent2-1024x560.png"


Config.weaponLog = true  	
Config.weaponLogDelay = 100000

Config.playerID = true
Config.steamID = true		
Config.steamURL = true	
Config.discordID = true			



Config.joinColor = "#3AF241" 	 	-- Player Connecting
Config.leaveColor = "#F23A3A"		-- Player Disconnected
Config.chatColor = "#A1A1A1"		-- Chat Message
Config.shootingColor = "#2E66F2"	-- Shooting a weapon
Config.deathColor = "#000000"		-- Player Died
Config.resourceColor = "#EBEE3F"	-- Resource Stopped/Started



Config.webhooks = {
	all = "",
	chat = "",
	joins = "https://discord.com/api/webhooks/804496617692069888/9aotmBZqxU4C9x2fKRk7tmuqFxad8ypSUASgWPVHTXzLFHPT5pnGDpkEvtVEOou5KayC",
	leaving = "https://discord.com/api/webhooks/804496617692069888/9aotmBZqxU4C9x2fKRk7tmuqFxad8ypSUASgWPVHTXzLFHPT5pnGDpkEvtVEOou5KayC",
	deaths = "https://discord.com/api/webhooks/804496553687121931/1RAULBcWAql4olFsKrGSJ_M7G6e9m6UUARrCSdbADEsZ-Us-KO9mr7qDubR_fKQPy09H",
	shooting = "https://discord.com/api/webhooks/814678436928028702/SEzF1dtx0qfippW-FS4RO5oFN-pM3bLWPIl9M7MTRm5x363zkSmV8ZA4x5hY-RwegHXg",
	resources = "https://discord.com/api/webhooks/814678740738113558/Oy95SrmE1pBm50bZO_Olcw_h6BBRV91GDisNpmbuY5543Ap_F4t_rk0at9an5Li5rrzY",
}


 --Debug poopies :D
Config.debug = false
Config.versionCheck = "1.1.1"
