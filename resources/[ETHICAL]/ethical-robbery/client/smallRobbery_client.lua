local shouldBeOpen = false
local shouldSquareOpen = false
killTimer = false
local Drawing = false
local DrawingV = false
local isAtBank = false
local lootCount = 0
local banks = {
  ["Bank1"] = { ["x"]=146.869, ["y"]=-1046.060, ["z"]= 29.374, ["robbing"] = false, ["robbingvault"] = false, ["lastRobbed"] = 1, ["rob"] = {}, ["started"] = false },
  ["Bank2"] = { ["x"]=-1210.80822, ["y"]=-336.562, ["z"]= 37.781, ["robbing"] = false, ["robbingvault"] = false, ["lastRobbed"] = 1, ["rob"] = {}, ["started"] = false },
  ["Bank3"] = { ["x"]=-2956.595, ["y"]=481.708, ["z"]=15.697, ["robbing"] = false, ["robbingvault"] = false, ["lastRobbed"] = 1, ["rob"] = {}, ["started"] = false },
  ["Bank4"] = { ["x"]=311.199, ["y"]=-284.428, ["z"]= 54.164, ["robbing"] = false, ["robbingvault"] = false, ["lastRobbed"] = 1, ["rob"] = {}, ["started"] = false },
  ["Bank5"] = { ["x"]=-353.815, ["y"]=-55.363, ["z"]=49.036 , ["robbing"] = false, ["robbingvault"] = false, ["lastRobbed"] = 1, ["rob"] = {}, ["started"] = false },
  ["Bank6"] = { ["x"]=1176.052, ["y"]=2712.854, ["z"]= 38.088, ["robbing"] = false, ["robbingvault"] = false, ["lastRobbed"] = 1, ["rob"] = {}, ["started"] = false },
}

Citizen.CreateThread(function()
  --print('object1',json.encode(banks))
  for k,v in pairs(banks) do 
    local coords = vector3(v.x,v.y,v.z) + vector3(2.5,-3.0,0.0)
    --print('coords',json.encode(coords))
    --print('object',json.encode(banks))
    exports["ethical-polyzone"]:AddBoxZone(k, coords, 3.0, 3.5, {
      name=k,
      heading=91,
      debugPoly=false,
      minZ=coords[3] - 1.0,
      maxZ=coords[3] + 1.0
    })
  end
end)

local nearbank = 0

RegisterNetEvent("robbery:openDoor")
AddEventHandler("robbery:openDoor", function(Doortype)
  print("opening", Doortype)
  if Doortype == "vault" then
    shouldBeOpen = true
    local VaultDoor = GetClosestObjectOfType(255.2283, 223.976, 102.3932, 25.0, getHashKey('v_ilev_bk_vaultdoor'), 0, 0, 0)
    local CurrentHeading = GetEntityHeading(VaultDoor)
    SetEntityHeading(VaultDoor, 10.0)
    FreezeEntityPosition(VaultDoor,true)
    CurrentHeading = GetEntityHeading(VaultDoor)
  elseif Doortype == "square" then
    shouldSquareOpen = true
    local VaultDoor = GetClosestObjectOfType(147.26,-1045.25,29.37, 25.0, 2121050683, 0, 0, 0)
    local CurrentHeading = GetEntityHeading(VaultDoor)
    SetEntityHeading(VaultDoor, 180.0) 
    FreezeEntityPosition(VaultDoor,true)
    CurrentHeading = GetEntityHeading(VaultDoor)
  end

end)

RegisterNetEvent("robbery:closeDoor")
AddEventHandler("robbery:closeDoor", function(Doortype)
  if Doortype == "vault" then
    shouldBeOpen = false
    local VaultDoor = GetClosestObjectOfType(255.2283, 223.976, 102.3932, 25.0, getHashKey('v_ilev_bk_vaultdoor'), 0, 0, 0)
    local CurrentHeading = GetEntityHeading(VaultDoor)
    SetEntityHeading(VaultDoor, 160.0)
    FreezeEntityPosition(VaultDoor,true)
    CurrentHeading = GetEntityHeading(VaultDoor)
    lootCount = 0
    nearbank = 0
  elseif Doortype == "square" then
    shouldSquareOpen = false
    local VaultDoor = GetClosestObjectOfType(147.26,-1045.25,29.37, 25.0,2121050683, 0, 0, 0)
    local CurrentHeading = GetEntityHeading(VaultDoor)
    SetEntityHeading(VaultDoor, 160.0)
    FreezeEntityPosition(VaultDoor,true)
    CurrentHeading = GetEntityHeading(VaultDoor)
    lootCount = 0
    nearbank = 0
  end
end)

local doorsRotated = {}

RegisterNetEvent("robbery:SmallBankDoorClosed")
AddEventHandler("robbery:SmallBankDoorClosed", function(bank)
    killTimer = true
    Drawing = false
    DrawingV = false
    local PosPly = GetEntityCoords(PlayerPedId())
    local VaultDoor = GetClosestObjectOfType(bank["x"],bank["y"],bank["z"], 15.0,bank["id"], 0, 0, 0)
    if VaultDoor == 0 then
      return
    end
    local factor = 50

    FreezeEntityPosition(VaultDoor,false)
    local CurrentHeading = GetEntityHeading(VaultDoor)

    FreezeEntityPosition(VaultDoor,true)
    for i = 1, factor do
      SetEntityHeading(VaultDoor, CurrentHeading + i)
      Wait(5)
    end
    Wait(1000)
    killTimer = false
end)

RegisterNetEvent("robbery:SmallBankDoor")
AddEventHandler("robbery:SmallBankDoor", function(coords,hex)
    local PosPly = GetEntityCoords(PlayerPedId())
    local VaultDoor = GetClosestObjectOfType(coords[1], coords[2], coords[3], 15.0, hex, 0, 0, 0)
    if VaultDoor == 0 then
      return
    end
    local factor = 50

    FreezeEntityPosition(VaultDoor,false)
    local CurrentHeading = GetEntityHeading(VaultDoor)

    if doorsRotated[VaultDoor] == nil then

      if hex == -131754413 then
        factor = 90
        print("draw bank makers with a factor of 90")
        TriggerEvent("DrawBankMarkers",VaultDoor,true)
      else
        print("draw bank makers with a falsey")
        TriggerEvent("DrawBankMarkers",VaultDoor,false)
      end

      doorsRotated[VaultDoor] = true
      for i = 1, factor do
        SetEntityHeading(VaultDoor, CurrentHeading - i)
        Wait(5)
      end

    elseif doorsRotated[VaultDoor] ~= nil then

      if hex == -131754413 and not Drawing then
        TriggerEvent("DrawBankMarkers",VaultDoor,true)
      else
        if hex ~= -131754413 and not DrawingV then
          TriggerEvent("DrawBankMarkers",VaultDoor,false)
        end
      end

    end
    
    FreezeEntityPosition(VaultDoor,true)

end)

RegisterNetEvent("robbery:receiveAndStartBanks")
AddEventHandler("robbery:receiveAndStartBanks", function(banks) --playerConnecting
  banks = banks -- THIS SHOULD REPLACE THE HARDCODED BANKS!! MAKE SURE IT RUNS FIRST
  print("old")
  print(json.encode(oldBanks))
  print("new")
  print(json.encode(banks))
end)

function createBlips()
    Citizen.CreateThread(function()
    for i = 1, #banks do
      local blip = AddBlipForCoord(banks[i]["location"]["x"], banks[i]["location"]["y"], banks[i]["location"]["z"])
      SetBlipSprite(blip, 52)
      SetBlipScale(blip, 1.0)
      SetBlipAsShortRange(blip, true)
      BeginTextCommandSetBlipName("STRING")
      AddTextComponentString("HEIST")
      EndTextCommandSetBlipName(blip)
      rentedOfficesBlips[i] = blip
    end
  end)
end


local RobberyTimers = {}
curhrs = "0"

RegisterNetEvent("timeheader")
AddEventHandler("timeheader", function(hrs,mins)

  curhrs = tonumber(hrs)
  ----print("current hours : " .. curhrs)
end)

RegisterNetEvent('send:email')
AddEventHandler('send:email', function()
TriggerServerEvent('request:BankUpdate')
end)

-- cards are 124 - 128
function PassCard(cardType)
  local answer = false
  --if RobberyTimers[1] ~= nil then
  print(cardType)
    if cardType == "securityblue" then
      answer = true
    end

    if cardType == "securityblack" then
      answer = true
    end

    if cardType == "securitygreen" then
      answer = true
    end

    if cardType == "securitygold" then
      answer = true
    end

    if cardType == "securityred" then
      answer = true
    end

  --end
  return answer
end

local lockpicking = false
RegisterNetEvent('animation:fuckyou')
AddEventHandler('animation:fuckyou', function()
    local lPed = PlayerPedId()
    RequestAnimDict("mini@repair")
    while not HasAnimDictLoaded("mini@repair") do
        Citizen.Wait(0)
    end
    while lockpicking do

        if not IsEntityPlayingAnim(lPed, "mini@repair", "fixing_a_player", 3) then
            ClearPedSecondaryTask(lPed)
            TaskPlayAnim(lPed, "mini@repair", "fixing_a_player", 8.0, -8, -1, 16, 0, 0, 0, 0)
        end
        Citizen.Wait(1)
    end
    ClearPedTasks(lPed)
end)


local onGoingRobberyHackSuccessful = false
local onGoingRobbery = false
function startHack(maxAttempts, attempt)
  if attempt == nil then 
    attempt = 1
  end
  exports["nsrp_bankhack"]:OpenHackingGame(function(success)
    print("Hacking game state", success)
    if not success then
      if attempt >= maxAttempts then
        onGoingRobbery = false
        onGoingRobberyHackSuccessful = false
      else
        TriggerEvent("DoLongHudText","Attempt Failed. " .. maxAttempts-attempt .. "/" .. maxAttempts .. " remaining" , 2)
        attempt = attempt + 1
        startHack(maxAttempts, attempt)
      end
    else
      onGoingRobbery = false
      onGoingRobberyHackSuccessful = true
    end
  end)
end


local bankenabled = true
RegisterNetEvent("restart:soon")
AddEventHandler("restart:soon", function()
  bankenabled = false
end)  

RegisterNetEvent("robbery:scanLock")
AddEventHandler("robbery:scanLock", function(lockpick,cardType)
  print("nearbank", nearbank)
  if nearbank == 0 then
    return
  end
  print("nearbank22 ", nearbank)
  -- if not bankenabled then
  --   TriggerEvent("DoLongHudText","Too late to rob this bank, Pepega.")
  --   return
  -- end

  --print(nearbank,"LWEOWEKWEOEWFK")
  if banks["Bank"..nearbank] == nil then return end
  print("bank is not nil")
  if not banks["Bank"..nearbank]["started"] then
    TriggerEvent("DoLongHudText","This is not ready to rob, Pepega.")
    return
  end
  local PosPly = GetEntityCoords(PlayerPedId())
  if lockpick then
    lockpicking = false
    return 
  else
    local passCard = PassCard(cardType)
    TriggerEvent("animation:PlayAnimation","id")
    local VaultDoor = GetClosestObjectOfType(PosPly["x"],PosPly["y"],PosPly["z"], 3.0,2121050683, 0, 0, 0)
    local VaultDoor2 = GetClosestObjectOfType(PosPly["x"],PosPly["y"],PosPly["z"], 3.0,-63539571, 0, 0, 0)
    if VaultDoor ~= 0 or VaultDoor2 ~= 0 then
      if not passCard then 
        TriggerEvent("DoLongHudText","Incorrect card or you cancelled..",2)
        return
      end
      TriggerEvent("ethical-dispatch:bankrobbery")
      -- TriggerServerEvent("dispatch:svNotify", {
      --   dispatchCode = "10-90E",
      --   origin = {
      --     x = banks["Bank"..nearbank]["x"],
      --     y = banks["Bank"..nearbank]["y"],
      --     z = banks["Bank"..nearbank]["z"]
      --   }
      -- })
      --print("vault trigger")
      --local finished = exports["ethical-taskbar"]:taskBar(35000,"Requesting Access")
      onGoingRobbery = true
      startHack(3)
      while onGoingRobbery do
        Wait(100)
      end
      if not onGoingRobberyHackSuccessful then
        TriggerEvent("DoLongHudText","Your security card has bent out of shape.",2)
        TriggerEvent('inventory:removeItem',cardType, 1)
        return

      end
      
      TriggerEvent('inventory:removeItem',cardType, 1)
      TriggerServerEvent("rob:doorOpen",nearbank,"robbingvault")
      TriggerEvent("client:newStress",true,550)
    end
    print("WOOOT!!!!")
  end
end)

RegisterNetEvent("updateBanksNow")
AddEventHandler("updateBanksNow", function(newBanks, setNearest)
    if newBanks then
      banks = newBanks
      print("We updated banks")
    end
    if setNearest ~= nil then
      nearbank = setNearest
      local currenBank = banks["Bank"..nearbank]
      print(json.encode(currenBank))
      SetWaypointOff()
      SetNewWaypoint(currenBank.x, currenBank.y)
    end
end)

RegisterNetEvent("robbery:scanbank")
AddEventHandler("robbery:scanbank", function(bank)

    -- if newBanks then
    --   banks = newBanks
    --   ----print("We updated banks because of a lockpick / card")
    -- end
    print("SCANNED", json.encode(bank))
    local bankCoords = vector3(bank["x"],bank["y"],bank["z"])
    if #(GetEntityCoords(PlayerPedId()) - bankCoords) < 10.0 then
      if bank["robbing"] then
        OpenNearestBank(bankCoords, false)
      end
      if bank["robbingvault"] then
        OpenNearestBank(bankCoords, true)
      end
    end

end)

function OpenNearestBank(coords, vault)
    -- small vaults
    if vault then
      TriggerEvent("robbery:SmallBankDoor",coords,2121050683)
      TriggerEvent("robbery:SmallBankDoor",coords,-63539571)
    else
      TriggerEvent("robbery:SmallBankDoor",coords,-131754413)
    end
end


RegisterNetEvent("robbery:disablescans")
AddEventHandler("robbery:disablescans", function()
  --print("cancelled drawing")
  nearbank = 0
  Drawing = false
  DrawingV = false
end)

RegisterNetEvent("robbery:disablescansServer")
AddEventHandler("robbery:disablescansServer", function(resetID)
  

  if resetID == nearbank then
    --print("cancelled drawing due to server timer")
    Drawing = false
    DrawingV = false
  end

end)



RegisterNetEvent("robbery:secure")
AddEventHandler("robbery:secure", function()
  if nearbank == 0 then
    return
  end

  local finished = exports["ethical-taskbar"]:taskBar(25000,"Securing Bank")
  if finished == 100 then
    TriggerServerEvent("robbery:shutdown",nearbank)
  end
  
end)

RegisterNetEvent("robbery:giveleitem")
AddEventHandler("robbery:giveleitem", function(confirmed,slotid)
    if confirmed then
      if slotid < 5 then
        -- low quality
        TriggerServerEvent( 'mission:finished', math.random(1200) )
        if math.random(100) == 100 then
          local myluck = math.random(5)
          if myluck == 1 then
            TriggerEvent("player:receiveItem","securityblue",1)
          elseif myluck == 2 then
            TriggerEvent("player:receiveItem","securityblack",1)
          elseif myluck == 3 then
            TriggerEvent("player:receiveItem","securitygreen",1)
          elseif myluck == 4 then
            TriggerEvent("player:receiveItem","securitygold",1)
          else
            TriggerEvent("player:receiveItem","securityred",1)
          end
        end
      else
        -- goods
        GiveRareItem()
      end
      --print("gif itemsz")
    else
      --print("no item :) alrdy dun")
    end
end)


gunListRob = {
  [1] = 453432689,
  [2] = 453432689,
  [3] = 584646201,
  [4] = 453432689,  
  [5] = 453432689,
  [6] = -2009644972,
  [7] = 453432689,
  [8] = 584646201,
  [9] = 584646201,
  [10] = -2009644972,
}

function GiveRareItem()
  
  if math.random(100) > 99 then
    TriggerEvent("player:receiveItem","goldbar",10)
  end

  if math.random(1,100) > 5 then

    local roll = math.random(7)
    if roll == 1 then
      TriggerEvent("player:receiveItem","cashstack",math.random(50,60))
    elseif roll == 2 then
      TriggerEvent("player:receiveItem","cashroll",math.random(70,80))
    elseif roll == 3 then
      TriggerEvent("player:receiveItem","markedbills",1)
    elseif roll == 4 then
      TriggerEvent("player:receiveItem","goldbar",math.random(3,4))
    else
      TriggerServerEvent( 'mission:finished', math.random(1500,4500) )
    end
  else
    TriggerServerEvent( 'mission:finished', math.random(500,2500) )
  end

end
RegisterNetEvent('ethical-polyzone:exit')
AddEventHandler('ethical-polyzone:exit', function(name)
  if isAtBank == true then
    TriggerEvent('ethical-ui:HideUI')
    isAtBank = false
  end    
end)

RegisterNetEvent('ethical-polyzone:enter')
AddEventHandler('ethical-polyzone:enter', function(name)
  for k,v in pairs(banks) do 
    if name == k then
      isAtBank = true
      -- do action
      TriggerEvent('ethical-ui:ShowUI', 'show', ("[E] %s"):format("To start looting"))
      Citizen.CreateThread(function()
        local isTouched = false
        while isAtBank do
          if IsControlJustPressed(0,38) and not isTouched and nearbank ~= 0 then
            isTouched = true
            TriggerEvent("animation:PlayAnimation", "dance")
            startLoot()
            Citizen.Wait(1000)
          end
          Citizen.Wait(0)
        end
      end)
    end
  end
end)

function startLoot()
  TriggerEvent("mkbuss:open5mscriptscom", "bankcase")
  lootCount = lootCount + 1
  Citizen.Wait(17000)
  if lootCount < 8 then
    startLoot()
  else
    TriggerEvent("DoLongHudText","You have completed the bank robbery." , 1)
    TriggerEvent("animation:PlayAnimation", "")
  end
end

-- RegisterCommand("test", function()
--   startLoot()
-- end)

RegisterNetEvent("ethical-robbery:resetLootCount")
AddEventHandler("ethical-robbery:resetLootCount", function()
  isLooting = false
end)

RegisterNetEvent("DrawBankMarkers")
AddEventHandler("DrawBankMarkers", function(Door,Wood)
  -- local crd = GetEntityCoords(Door)

  -- local coords = GetOffsetFromEntityInWorldCoords(Door, -1.0, 1 + 0.0, 0.0)

  -- exports["ethical-polyzone"]:AddBoxZone("bankrob", coords, 2.9, 5, {
  --   name="bankrob",hehe
  --   heading=91,
  --   debugPoly=true,
  --   minZ=coords[3] - 1.0,
  --   maxZ=coords[3] + 1.0
  -- })
  -- local x2,y2,z2 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, -1.0, 2.5 + 0.0, 0.0))
  -- local x3,y3,z3 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, -1.0, 4.0 + 0.0, 0.0))
  -- local x4,y4,z4 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, -1.0, 5.5 + 0.0, 0.0))     


  -- local x5,y5,z5 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, 0.19, 1.2 + 0.0, 0.0))
  -- local x6,y6,z6 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, 0.19, 2.55 + 0.0, 0.0))
  -- local x7,y7,z7 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, 1.1, 3.5 + 0.0, 0.0))
  -- local x8,y8,z8 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, 3.6, 4.1 + 0.0, 0.0))
  -- local x9,y9,z9 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, 4.9, 4.1 + 0.0, 0.0))
  -- local x10,y10,z10 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, 3.6, 0.5 + 0.0, 0.0))
  -- local x11,y11,z11 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, 4.9, 0.5 + 0.0, 0.0))
  -- local x12,y12,z12 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, 5.8, 1.2 + 0.0, 0.0))
  -- local x13,y13,z13 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, 5.8, 2.4 + 0.0, 0.0))
  -- local x14,y14,z14 = table.unpack(GetOffsetFromEntityInWorldCoords(Door, 5.8, 3.6 + 0.0, 0.0))
  -- if Wood and not Drawing then
  --   Drawing = true
  --   --print("starting wood --print")
  --   while Drawing and not killTimer do 
  --       Wait(1)
  --       DrawTextAndScan(x1,y1,z1, 1)
  --       DrawTextAndScan(x2,y2,z2, 2)
  --       DrawTextAndScan(x3,y3,z3, 3)
  --       DrawTextAndScan(x4,y4,z4, 4)    
  --   end

  -- elseif not DrawingV and not Wood then
  --   --print("starting vaulktoweopierghioj")
  --   DrawingV = true
  --   while DrawingV and not killTimer do 
  --       Wait(1)
  --       DrawTextAndScan(x5,y5,z5, 5)
  --       DrawTextAndScan(x6,y6,z6, 6)
  --       DrawTextAndScan(x7,y7,z7, 7)
  --       DrawTextAndScan(x8,y8,z8, 8)
  --       DrawTextAndScan(x9,y9,z9, 9)
  --       DrawTextAndScan(x10,y10,z10, 10)
  --       DrawTextAndScan(x11,y11,z11, 11)
  --       DrawTextAndScan(x12,y12,z12, 12)
  --       DrawTextAndScan(x13,y13,z13, 13)
  --       DrawTextAndScan(x14,y14,z14, 14)    
  --   end
  -- end
end)



function DrawTextAndScan(x,y,z, inputType)

    local dst = #(vector3(x,y,z) - GetEntityCoords(PlayerPedId()))
    if dst > 3.0 then
      return
    end
    local text = "Search"
    local bank = "Bank"..nearbank

    local robbed = false

    if banks[bank]["rob"] then
      if banks[bank]["rob"][inputType] then
        text = "Empty"
        robbed = true
      end
    end

    if robbed then
      return
    end

    if dst < 1.0 then
      if IsControlJustPressed(0,38) then
        lockpicking = true
        TriggerEvent("animation:fuckyou")
        local timer = 15000
        if inputType < 5 then
          timer = 20000
        end
        local finished = exports["ethical-taskbar"]:taskBar(timer,"Opening")
        if finished == 100 then
          TriggerServerEvent("robbery:checkSearch", nearbank, inputType)
        end
        lockpicking = false
        Wait(1000)
      end
    end
    
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    SetTextScale(0.35, 0.35)
    SetTextFont(4)
    SetTextProportional(1)
    SetTextColour(255, 255, 255, 215)
    SetTextEntry("STRING")
    SetTextCentre(1)
    AddTextComponentString(text)
    DrawText(_x,_y)
    local factor = (string.len(text)) / 370
    DrawRect(_x,_y+0.0125, 0.015+ factor, 0.03, 41, 11, 41, 68)
end
