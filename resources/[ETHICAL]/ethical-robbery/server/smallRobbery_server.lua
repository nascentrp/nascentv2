banks = {
    ["Bank1"] = { ["id"] = 2121050683, ["x"]=146.869, ["y"]=-1046.060, ["z"]= 29.374, ["robbing"] = false, ["robbingvault"] = false, ["lastRobbed"] = 1, ["rob"] = {}, ["started"] = false },
    ["Bank2"] = { ["id"] = 2121050683, ["x"]=-1210.80822, ["y"]=-336.562, ["z"]= 37.781, ["robbing"] = false, ["robbingvault"] = false, ["lastRobbed"] = 1, ["rob"] = {}, ["started"] = false },
    ["Bank3"] = { ["id"] = -63539571, ["x"]=-2956.595, ["y"]=481.708, ["z"]=15.697, ["robbing"] = false, ["robbingvault"] = false, ["lastRobbed"] = 1, ["rob"] = {}, ["started"] = false },
    ["Bank4"] = { ["id"] = 2121050683, ["x"]=311.199, ["y"]=-284.428, ["z"]= 54.164, ["robbing"] = false, ["robbingvault"] = false, ["lastRobbed"] = 1, ["rob"] = {}, ["started"] = false },
    ["Bank5"] = { ["id"] = 2121050683, ["x"]=-353.815, ["y"]=-55.363, ["z"]=49.036 , ["robbing"] = false, ["robbingvault"] = false, ["lastRobbed"] = 1, ["rob"] = {}, ["started"] = false },
    ["Bank6"] = { ["id"] = 2121050683, ["x"]=1176.052, ["y"]=2712.854, ["z"]= 38.088, ["robbing"] = false, ["robbingvault"] = false, ["lastRobbed"] = 1, ["rob"] = {}, ["started"] = false },
}

AddEventHandler("playerConnecting", function()

end)

function refreshBanksForUsers(src)
    TriggerClientEvent("robbery:receiveAndStartBanks", src)
end

RegisterServerEvent("rob:doorOpen")
AddEventHandler("rob:doorOpen", function(bankId,robbing)
    local src = source
    local bank = "Bank"..bankId
    if robbing == "robbingvault" then
        banks[bank]["started"] = false
        banks[bank]["robbing"] = true
        banks[bank]["robbingvault"] = true
        --TriggerClientEvent('robbery:openDoor',-1,"square")
        TriggerClientEvent('robbery:scanbank', -1, banks[bank])
        TriggerClientEvent("DoLongHudText", src, "The vault will close in 3 minutes.", 2)
        Citizen.CreateThread(function() 
            local isBankStillRunning = true
            local count = 0
            while isBankStillRunning do
                Wait(150000)
                TriggerClientEvent("DoLongHudText", src, "You have 30 seconds remaining.", 2)
                Wait(40000) -- This is tick, not MS, so we give 10 sec leniency
                TriggerClientEvent('robbery:SmallBankDoorClosed', -1, banks[bank])
                TriggerClientEvent("DoLongHudText", src, "The vault is now closing.", 2)
                isBankStillRunning = false
            end
        end)
    end
    if robbing == "robbing" then

    end
end)

RegisterServerEvent("robbery:checkSearch")
AddEventHandler("robbery:checkSearch", function(nearbank, inputType)
    local src = source
    TriggerClientEvent('robbery:giveleitem', source, nearbank, inputType)
    local bank = "Bank"..nearbank

    local bankRob = banks[bank]
    table.insert(bankRob.rob, inputType)

    TriggerClientEvent("robbery:scanbank",src, bankRob)
end)

RegisterServerEvent("request:BankUpdate")
AddEventHandler("request:BankUpdate", function()
    local src = source
    local timers = {1,2,3,4,5,6}
    print("Starting banks")
    for i=1,6 do 
        banks['Bank'..i]["started"] = true
    end
    print("banks started")
    local nearbank = math.random(1,6)
    Citizen.Wait(1000)
    TriggerClientEvent("chatMessage", src, "EMAIL", 8, "I've enabled your access card to open Fleeca bank vault " .. nearbank .. " check your map for a waypoint")
    TriggerClientEvent('updateBanksNow',src, banks, nearbank)
    TriggerClientEvent('robbery:setNearestBank', src, nearbank)
end)

RegisterServerEvent("robbery:decrypt")
AddEventHandler("robbery:decrypt", function()
    local src = source
    TriggerClientEvent('send:email', src)
end)

RegisterServerEvent('robbery:shutdown')
AddEventHandler('robbery:shutdown', function(bankID)
    TriggerClientEvent('robbery:shutdownBank',-1,bankID,true)
    local bankSecured = banks[bank]
    bankSecured["started"] = false
    TriggerClientEvent("robbery:closeDoor", -1, "square")
 end)
