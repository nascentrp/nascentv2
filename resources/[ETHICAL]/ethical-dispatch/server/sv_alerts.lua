RegisterServerEvent("ethical-alerts:teenA")
AddEventHandler("ethical-alerts:teenA",function(targetCoords)
    TriggerClientEvent('ethical-alerts:policealertA', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:teenB")
AddEventHandler("ethical-alerts:teenB",function(targetCoords)
    TriggerClientEvent('ethical-alerts:policealertB', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:teenpanic")
AddEventHandler("ethical-alerts:teenpanic",function(targetCoords)
    TriggerClientEvent('ethical-alerts:panic', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:fourA")
AddEventHandler("ethical-alerts:fourA",function(targetCoords)
    TriggerClientEvent('ethical-alerts:tenForteenA', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:fourB")
AddEventHandler("ethical-alerts:fourB",function(targetCoords)
    TriggerClientEvent('ethical-alerts:tenForteenB', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:downperson")
AddEventHandler("ethical-alerts:downperson",function(targetCoords)
    TriggerClientEvent('ethical-alerts:downalert', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:shoot")
AddEventHandler("ethical-alerts:shoot",function(targetCoords)
    TriggerClientEvent('ethical-outlawalert:gunshotInProgress', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:storerob")
AddEventHandler("ethical-alerts:storerob",function(targetCoords)
    TriggerClientEvent('ethical-alerts:storerobbery', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:crypto")
AddEventHandler("ethical-alerts:crypto",function(targetCoords)
    TriggerClientEvent('ethical-alerts:cryptohack', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:houserob")
AddEventHandler("ethical-alerts:houserob",function(targetCoords)
    TriggerClientEvent('ethical-alerts:houserobbery', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:tbank")
AddEventHandler("ethical-alerts:tbank",function(targetCoords)
    TriggerClientEvent('ethical-alerts:banktruck', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:robjew")
AddEventHandler("ethical-alerts:robjew",function()
    TriggerClientEvent('ethical-alerts:jewelrobbey', -1)
	return
end)

RegisterServerEvent("ethical-alerts:AlertUnion:sv")
AddEventHandler("ethical-alerts:AlertUnion:sv",function()
    TriggerClientEvent('ethical-alerts:AlertUnion:cl', -1)
	return
end)

RegisterServerEvent("ethical-alerts:AlertUnion:svC4")
AddEventHandler("ethical-alerts:AlertUnion:svC4",function()
    TriggerClientEvent('ethical-alerts:AlertUnion:clC4', -1)
	return
end)

RegisterServerEvent("ethical-alerts:AlertPacific:sv")
AddEventHandler("ethical-alerts:AlertPacific:sv",function()
    TriggerClientEvent('ethical-alerts:AlertPacific:cl', -1)
	return
end)

RegisterServerEvent("ethical-alerts:methexplosion:sv")
AddEventHandler("ethical-alerts:methexplosion:sv",function()
    TriggerClientEvent('ethical-alerts:methexplosion:cl', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:taxicallsv")
AddEventHandler("ethical-alerts:taxicallsv",function(targetCoords)
    TriggerClientEvent('ethical-alerts:taxicall', -1, targetCoords)
	return
end)

RegisterServerEvent("ethical-alerts:dojjobcallsv")
AddEventHandler("ethical-alerts:dojjobcallsv",function(targetCoords)
    TriggerClientEvent('ethical-alerts:dojjobcall', -1, targetCoords)
	return
end)
