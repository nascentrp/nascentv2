RegisterServerEvent('ethicalburgershot:bill:player')
AddEventHandler("ethicalburgershot:bill:player", function(TargetID, amount)
	local src = source
	local sender = exports["ethical-base"]:getModule("Player"):GetUser(src)
	local senderCharacterId = sender:getCurrentCharacter().id
	local target = tonumber(TargetID)
	local fine = tonumber(amount)
	local user = exports["ethical-base"]:getModule("Player"):GetUser(target)
	local characterId = user:getCurrentCharacter().id
	if user ~= false then
		if user:getBalance() >= fine then
			user:removeBank(fine)
			
			TriggerClientEvent("player:receiveItem", src, "receipt", 1, {
				Price = fine,
				Creator = senderCharacterId,
				Billed = characterId,
				Comment = "BurgerShot"
			})
            
			TriggerClientEvent("burgershot:createcustomerreceipt",src,fine,characterId)
			TriggerClientEvent("DoLongHudText", target, "You have been billed for R"..fine, 1)
			TriggerClientEvent("DoLongHudText", src, "You have charged customer R"..fine, 1)
			TriggerEvent("bank:addlog", characterId, fine, "Burgershot", false)
		else
			TriggerClientEvent("DoLongHudText", target, "Card Declined - Insufficient funds", 2)
			TriggerClientEvent("DoLongHudText", src, "Card Declined", 2)
		end
	else
		TriggerClientEvent("DoLongHudText", src, "Card Invalid", 2)
	
	end
end)

-- RegisterServerEvent("PD:SETCOUNT")
-- AddEventHandler("PD:SETCOUNT", function(ct)
-- 	TriggerClientEvent("job:counts", -1, ct)
-- end)
-- RegisterServerEvent('ethicalburgershot:craft:item')
-- AddEventHandler("ethicalburgershot:craft:item", function(qty)

-- end)

RegisterServerEvent('ethicalburgershot:payout:employee')
AddEventHandler("ethicalburgershot:payout:employee", function(qty)
	local src = source
	local user = exports["ethical-base"]:getModule("Player"):GetUser(src)
    local job = user:getVar("Job") -- do a job check TO DO
	--local cash = math.random(20, 30)*qty
	TriggerClientEvent("inventory:removeItem", src, "receipt", qty)	
	--user:addBank(cash)
end)