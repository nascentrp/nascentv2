local ongoing = false
local cooking = false

RegisterNetEvent("lsd:start")
AddEventHandler("lsd:start", function()
	local playerped = PlayerPedId()
	local plyCoords = GetEntityCoords(PlayerPedId())
	local distance = (GetDistanceBetweenCoords(plyCoords.x, plyCoords.y, plyCoords.z, 1390.5190, 3604.7275, 38.9419, false))		
	if exports["ethical-inventory"]:hasEnoughOfItem("acetone",5,false) and exports["ethical-inventory"]:hasEnoughOfItem("foodingredient",4,false) and ongoing == false then
		TriggerEvent("animation:lsd")
		FreezeEntityPosition(PlayerPedId(),true)
		SetEntityHeading(PlayerPedId(), 178.82402038574)
		local finished = exports["ethical-taskbar"]:taskBar(10000,"Putting Materials Into The Cooker")
		if (finished == 100) then
			FreezeEntityPosition(PlayerPedId(),false)
			TriggerEvent("inventory:removeItem","acetone", 5)
			TriggerEvent("inventory:removeItem","foodingredient", 4)
			if math.random(1,30) == 18  then
				TriggerEvent("DoLongHudText", "You Placed The Ingredients In Wrong!")
				TriggerEvent("player:receiveItem","badlsdtab", 1)
			else
				TriggerEvent("DoLongHudText", "You Successfully Turned On The Cooker, Wait 2 Minutes For Results.")
				ongoing = true
				cooking = true
				
				Citizen.Wait(120000)
				-- finished = 0
				-- finished = exports["ethical-taskbar"]:taskBar(175000,"Cooking")
				-- if (finished == 100) then
				-- 	finished = 0
				-- 	local finished = exports["ethical-taskbar"]:taskBar(10000,"Checking results...")
				-- 	if (finished == 100) then
				-- 		Citizen.Wait(10000)
						if cooking == true then
							TriggerEvent("DoLongHudText", "Finished!")
							TriggerEvent("player:receiveItem","lsdtab", 1)
							ongoing = false
							cooking = false
						elseif cooking == false then
							TriggerEvent("DoLongHudText", "Failed!")
						end
					-- end
				-- end
			end
		end
	else
		TriggerEvent("DoLongHudText", "Are you sure you know what you're doing?")
	end
end)


RegisterNetEvent('animation:load')
AddEventHandler('animation:load', function(dict)
    while ( not HasAnimDictLoaded( dict ) ) do
        RequestAnimDict( dict )
        Citizen.Wait( 5 )
    end
end)

RegisterNetEvent('animation:lsd')
AddEventHandler('animation:lsd', function()
	inanimation = true
	local lPed = GetPlayerPed(-1)
	RequestAnimDict("mini@repair")
	while not HasAnimDictLoaded("mini@repair") do
		Citizen.Wait(0)
	end

	if IsEntityPlayingAnim(lPed, "mini@repair", "fixing_a_player", 3) then
		ClearPedSecondaryTask(lPed)
	else
		TaskPlayAnim(lPed, "mini@repair", "fixing_a_player", 8.0, -8, -1, 49, 0, 0, 0, 0)
		seccount = 4
		while seccount > 0 do
			Citizen.Wait(10000)
			FreezeEntityPosition(PlayerPedId(),false)
			seccount = seccount - 1
		end
		ClearPedSecondaryTask(lPed)
	end		
	inanimation = false
end)

RegisterNetEvent("lsd:sell")
AddEventHandler("lsd:sell", function()
	if exports["ethical-inventory"]:hasEnoughOfItem("lsdtab",1,false) then
		TriggerEvent("inventory:removeItem","lsdtab", 1)
		local finished = exports["ethical-taskbar"]:taskBar(3000,"Handing Over Something")
		if (finished == 100) then
			TriggerEvent("DoLongHudText", "Thanks Man, Heres a lil something for that.")
			TriggerServerEvent("lsd:givemoney")
		end
	elseif exports["ethical-inventory"]:hasEnoughOfItem("badlsdtab",1,false) then
		TriggerEvent("DoLongHudText", "That product looks scuffed. I don't want it.")
	end
end)

RegisterNetEvent("stopcooking")
AddEventHandler("stopcooking", function()
	if cooking == true then
		TriggerEvent("DoLongHudText", "Failed. You are too far from the cooking zone.")
		cooking = false
		ongoing = false
	end
end)

Citizen.CreateThread(function()
	createLsdPed()
	print("DAD BABY")
end)

function createLsdPed()
	modelHash = GetHashKey("s_m_y_dealer_01")
	RequestModel(modelHash)
	while not HasModelLoaded(modelHash) do
		Wait(1)
	end
	local createdPedLSD = CreatePed(0, modelHash, -255.40719604492, -1542.0688476562, 30.936147689819,true)
	FreezeEntityPosition(createdPedLSD, true)
	SetEntityHeading(createdPedLSD,  178.12237548828)
	SetEntityInvincible(createdPedLSD, true)
	SetBlockingOfNonTemporaryEvents(createdPedLSD, true)
	TaskStartScenarioInPlace(createdPedLSD, "WORLD_HUMAN_SMOKING_POT", 0, true)
	-- elseif roll == 3 then
	-- 	modelHash = GetHashKey("s_m_y  _dealer_01")
	-- 	RequestModel(modelHash)
	-- 	while not HasModelLoaded(modelHash) do
	-- 		Wait(1)
	-- 	end
	-- 	created_ped5 = CreatePed(0, modelHash , -255.40719604492, -1542.0688476562, 30.936147689819,true)
	-- 	FreezeEntityPosition(created_ped5, true)
	-- 	SetEntityHeading(created_ped5,  180.49224853516)
	-- 	SetEntityInvincible(created_ped5, true)
	-- 	SetBlockingOfNonTemporaryEvents(created_ped5, true)
	-- 	TaskStartScenarioInPlace(created_ped5, "WORLD_HUMAN_SMOKING_POT", 0, true)
	-- elseif roll == 4 then
	-- 	modelHash = GetHashKey("s_m_y_dealer_01")
	-- 	RequestModel(modelHash)
	-- 	while not HasModelLoaded(modelHash) do
	-- 		Wait(1)
	-- 	end
	-- 	created_ped5 = CreatePed(0, modelHash , 1235.3387451172, -413.82913208008, 67.928161621094,true)
	-- 	FreezeEntityPosition(created_ped5, true)
	-- 	SetEntityHeading(created_ped5,  303.48379516602)
	-- 	SetEntityInvincible(created_ped5, true)
	-- 	SetBlockingOfNonTemporaryEvents(created_ped5, true)
	-- 	TaskStartScenarioInPlace(created_ped5, "WORLD_HUMAN_SMOKING_POT", 0, true)
	-- end
end