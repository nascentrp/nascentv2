Citizen.CreateThread(function()
	CreateOxyPed()
end)


function CreateOxyPed()

	modelHash = GetHashKey("s_m_y_dealer_01")
	RequestModel(modelHash)
	while not HasModelLoaded(modelHash) do
		 Wait(1)
	end
	created_ped5 = CreatePed(0, modelHash , 56.782905578613, 3690.2495117188, 39.021279907227,true)
	FreezeEntityPosition(created_ped5, true)
	SetEntityHeading(created_ped5,  325.24865722656)
	SetEntityInvincible(created_ped5, true)
	SetBlockingOfNonTemporaryEvents(created_ped5, true)
	TaskStartScenarioInPlace(created_ped5, "WORLD_HUMAN_SMOKING_POT", 0, true)

end

RegisterNetEvent("oxy:sell")
AddEventHandler("oxy:sell", function()
	if exports["ethical-inventory"]:hasEnoughOfItem("oxy",5,false) then
        TriggerEvent("inventory:removeItem","oxy", 5)
		local finished = exports["ethical-taskbar"]:taskBar(3000,"Selling Oxy...")
		if (finished == 100) then
            local roll = math.random(4,10)
            TriggerEvent("player:receiveItem", "rollcash", roll)
            TriggerEvent("DoLongHudText", "Thanks Homie, Heres a lil something for that.")


            if math.random(1,200) == 100  then
     
                Citizen.Wait(500)
                TriggerEvent("DoLongHudText", "I got you something extra, here you go")
                TriggerEvent("player:receiveItem", "decryptersess", 1)
            end
		end
	else
        TriggerEvent("DoLongHudText", "I need at least 5 of those Homie")
    end
end)

RegisterNetEvent("oxy:selljewelery") --oxy:selljewelery
AddEventHandler("oxy:selljewelery", function()
	if exports["ethical-inventory"]:hasEnoughOfItem("erpring",5,false) then
        TriggerEvent("inventory:removeItem","erpring", 1)
		local finished = exports["ethical-taskbar"]:taskBar(3000,"Inspecting the Jewells...")
		if (finished == 100) then
            local roll = math.random(15,25)
            TriggerEvent("player:receiveItem", "rollcash", roll)
            TriggerEvent("DoLongHudText", "Thanks Homie, I will have to get the gold smelted but heres some dosh for the diamonds")
		end
	elseif exports["ethical-inventory"]:hasEnoughOfItem("bdiamond",1,false) then
			TriggerEvent("inventory:removeItem","bdiamond", 1)
			local finished = exports["ethical-taskbar"]:taskBar(3000,"Selling Oxy...")
			if (finished == 100) then
				local roll = math.random(25,30)
				TriggerEvent("player:receiveItem", "rollcash", roll)
				TriggerEvent("DoLongHudText", "Thanks Homie, a diamond like this will fetch a nice price on the black market")
			end
	else
        TriggerEvent("DoLongHudText", "I need at least 5 rings or 1 diamond Homie")
    end
end)
